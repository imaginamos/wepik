<?php

class HybridAuthIdentity extends CUserIdentity
{
    const VERSION = '2.4.1';

    /**
     *
     * @var Hybrid_Auth
     */
    public $hybridAuth;

    /**
     *
     * @var Hybrid_Provider_Adapter
     */
    public $adapter;

    /**
     *
     * @var Hybrid_User_Profile
     */
    public $userProfile;

    public $allowedProviders = array('google', 'facebook', 'linkedin', 'yahoo', 'live',);

    protected $config;

    function __construct()
    {
        $path = Yii::getPathOfAlias('ext.HybridAuth');
        require_once $path . '/hybridauth-' . self::VERSION . '/hybridauth/Hybrid/Auth.php';  //path to the Auth php file within HybridAuth folder

        $this->config = array(
            //"base_url" => "http://localhost/wepiku_web/dist/site/sociallogin", //url de producción
            "base_url" => "http://localhost/wepiku_web/dist/site/sociallogin", //url local

            "providers" => array(
                "Google" => array(
                    "enabled" => true,
                    "keys" => array(
                        "id" => "google client id",
                        "secret" => "google secret",
                    ),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile " . "https://www.googleapis.com/auth/userinfo.email",
                    "access_type" => "online",
                ),
                "Facebook" => array (
                   "enabled" => true,
                   "keys" => array (
                       //"id" => "912078938822941", //app_id: produccion
                       //"id" => "1643117832584938", //app_id: pruebas ricardo
                       "id" => "635013036633861", //app_id: pruebas jeiver
                       //"secret" => "639a077ba78c49355544e7f3f76a675a", //app_secret: produccion
                       //"secret" => "f0e8a7aea32128336c662a9c8c2d43c5", //app_secret: pruebas ricardo
                       "secret" => "b993a0d2bcfb5bb83fb92030981967b1", //app_secret: pruebas jeiver
                   ),
                   "scope" => "user_about_me, user_likes, user_friends, email, user_birthday",
                   "display" => "popup"
                ),
                "Live" => array (
                   "enabled" => true,
                   "keys" => array (
                       "id" => "windows client id",
                       "secret" => "Windows Live secret",
                   ),
                   "scope" => "email"
                ),
                "Yahoo" => array(
                   "enabled" => true,
                   "keys" => array (
                       "key" => "yahoo client id",
                       "secret" => "yahoo secret",
                   ),
                ),
                "LinkedIn" => array(
                   "enabled" => true,
                   "keys" => array (
                       "key" => "linkedin client id",
                       "secret" => "linkedin secret",
                   ),
                ),
            ),

            "debug_mode" => false,

            // to enable logging, set 'debug_mode' to true, then provide here a path of a writable file
            "debug_file" => "",
        );

        $this->hybridAuth = new Hybrid_Auth($this->config);
    }

    /**
     *
     * @param string $provider
     * @return bool
     */
    public function validateProviderName($provider)
    {
        if (!is_string($provider))
            return false;
        if (!in_array($provider, $this->allowedProviders))
            return false;

        return true;
    }

    public function login()
    {
        $this->username = $this->userProfile->email;  //CUserIdentity
        Yii::app()->user->login($this, 0);
    }

    public function authenticate() 
    {
        return true;
    }
}
