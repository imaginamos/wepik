<?php

class MapasController extends Controller{
    
    public $layout='main';
    
    public function filters()
    {
      return array(
        'accessControl', // perform access control for CRUD operations
      );
    }    
    
    public function accessRules()
    {
        return array(
//            array('allow',  // allow all users to perform 'list' and 'show' actions
//                'actions'=>array('index', 'view'),
//                'users'=>array('*'),
//            ),
            array('allow', // allow authenticated users to perform any action
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }      
    
    public function missingAction($actionID)
    {
            // Yii::app()->user->setFlash('warning','This is a test');
            // Yii::app()->user->setFlash('danger','This is a test');
            // Yii::app()->user->setFlash('success','This is a test');

            if($actionID=='' or $actionID=='main.php'):
                    $actionID='index';
                    $this->layout = 'landing';
            endif;

            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            $this->render(strtr($actionID,array('.php'=>'')));
    }
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            $this->render('error', $error);
            }
    }
    
    public function actionMapaSucurCampaniaSelecBackend(){        
        if(isset($_GET['c'])):             
            $url = $this->url_services() . '/marca/api_marcasucursal/get_sucursales_campania2?campania_id='.$_GET['c'];
            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);
            $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     
            $jsondecode = CJSON::decode($response);
            $responseData =(array)$jsondecode;
            $datos = $responseData['data'];
            $json_string = json_encode($datos);
            $this->render('/site/mapa', array('sucursales'=>$json_string)); 
        elseif(isset($_GET['m'])):
            $url = $this->url_services() . '/marca/api_marcasucursal/get_sucursales_marca2?marca_id='.$_GET['m'];
            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            
            $auth = array('Authorization: '.$token);
            $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     
            $jsondecode = CJSON::decode($response);
            $responseData =(array)$jsondecode;
            $datos = $responseData['data'];
            $json_string = json_encode($datos);
            $this->render('/site/mapa', array('sucursales'=>$json_string));
        elseif($_GET['t']==1):
            $url = $this->url_services() . '/marca/api_marcasucursal/get_map_deals2';
            if(isset($_GET['f']))
            {
                $url = $url.'?momento_id='.$_GET['f'];
            }
            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            
            $auth = array('Authorization: '.$token);
            $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     
            $jsondecode = CJSON::decode($response);
            $responseData =(array)$jsondecode;
            $datos = $responseData['data'];
            $json_string = json_encode($datos);
            
            $momentos = array();
            foreach($datos as $dato)
            {
                foreach($dato['momentos'] as $momento)
                {
                    if(!(in_array($momento, $momentos)))
                    {
                        $momentos[] = $momento;
                    }
                }
            }

            $this->render('/site/mapa', array('sucursales'=>$json_string,'momentos'=>$momentos,'filtro'=>$datos[0]['filtro']));
                           
            //echo CJSON::encode($this->createUrl('/site/mapa'));
        else:
            throw new CHttpException('403', 'Forbidden access.');    
        endif;               
    }
}