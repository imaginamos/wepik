<?php

require_once( dirname(__FILE__) . '/../controllers/MarcasController.php');
require_once( dirname(__FILE__) . '/../controllers/MapasController.php');
require_once( dirname(__FILE__) . '/../controllers/SiteController.php');

class HotdealsController extends Controller {

    public $layout = 'main';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
//            array('allow',  // allow all users to perform 'list' and 'show' actions
//                'actions'=>array('index', 'view'),
//                'users'=>array('*'),
//            ),
            array('allow', // allow authenticated users to perform any action
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function missingAction($actionID) {
        // Yii::app()->user->setFlash('warning','This is a test');
        // Yii::app()->user->setFlash('danger','This is a test');
        // Yii::app()->user->setFlash('success','This is a test');

        if ($actionID == '' or $actionID == 'main.php'):
            $actionID = 'index';
            $this->layout = 'landing';
        endif;

        if (isset($_GET['theme']))
            Yii::app()->theme = $_GET['theme'];
        $this->render(strtr($actionID, array('.php' => '')));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if (isset($_GET['theme']))
            Yii::app()->theme = $_GET['theme'];
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionIndex() {

        $this->hasNewsFeed = true;
        $response_marca = array();
        $lat = 0;
        $lng = 0;

        $this->layout = 'main';
        if (isset($_GET['theme']))
            Yii::app()->theme = $_GET['theme'];

        $latitude = Yii::app()->getRequest()->getParam('lat', false);
        $longitude = Yii::app()->getRequest()->getParam('lng', false);
        if ($latitude and $longitude):
            $lat = $latitude;
            $lng = $longitude;
        endif;

//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=> true,
//            'jquery.yii.js' => true
//        );          
//        Yii::app()->clientScript->scriptMap['jquery.yii.js'] = true;        
        //$url = $this->url_services() . '/campania/api_campanias?city_id=11001';
        $url = $this->url_services() . '/campania/api_campanias?lat=' . $lat . '&lng=' . $lng . '&marca_id=0';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $response_hotdeals = (array) $jsondecode;

        //fecha_creacion ---ordenar por---
        //costo_multimedia_id ---ordenar por---

        HotdealsController::actionImpresionesXUsuarioBackend($response_hotdeals); //habilitar cuando se suba al servidor

        foreach ($response_hotdeals['data'] as $hotdeals):
            $existe_marca_hotdeals = false;

            foreach ($response_marca as $marca):
                if ($marca == $hotdeals['marca_id']):
                    $existe_marca_hotdeals = true;
                endif;
            endforeach;
            if (!$existe_marca_hotdeals):
                $response_marca[$hotdeals['marca_id']] = MarcasController::actionDetalleMarcaBackend($hotdeals['marca_id']);
            endif;
        endforeach;

        //print_r($response_marca);
//        foreach ($response_marca as $key => $marca):
//            echo 'marca'.$key.' '.$marca['nombre'].'<br/>';
//        endforeach;

        $this->render('index', array('data_hotdeals_json' => $response_hotdeals, 'data_marca_json' => $response_marca));
    }

    public static function actionNewsFeedBackend() {

        $post_data = array();

        $url = self::url_services() . '/usuario/api_usuarioaccion/newfeeds';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        //$wepiku_token = 'a8dYbYn8w4mRc6x6pAy65Tg258fTnSoQ3SgMiSgT';
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

        $jsondecode = CJSON::decode($response);
        $responseData = (array) $jsondecode;
        return $responseData;
    }

    public function actionImpresionesXUsuarioBackend($data_hotdeals_json) {

        foreach ($data_hotdeals_json['data'] as $campania):
            $post_data = array(
                'campanias_ids' => $campania['id'],
            );

            $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_dibujar';

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

            //$jsondecode = CJSON::decode($response);
            //$responseData =(array)$jsondecode;
            //print_r($responseData['data']);
            //echo 'campania_id: '.$campania['id'];
        endforeach;
    }

    public function actionVerDetalleCampania($id_camp) {
        $post_data = array(
            'campania_id' => $id_camp,
        );

        $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_verdetalle';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
    }

    public function actionGetInfoCampaniaAjax() {

        $infocampania = array();
        $campania_id = Yii::app()->request->getPost('camp', false);
        $modal_name = Yii::app()->request->getPost('modal', false);
        $bar_admin = Yii::app()->request->getPost('baradmin', false);
        $estado_campania = Yii::app()->request->getPost('estado', false);
        if (!$bar_admin):
            $bar_admin = 0;
        endif;
        if (!$estado_campania): //A: Activas, P: Pausadas, T: Terminadas
            $estado_campania = 0;
        endif;
        //$campania_id = 1;        
        $modal = str_replace("#", "", $modal_name);
        $answered = 0;

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

            HotdealsController::actionVerDetalleCampania($campania_id);

            $infocampania = HotdealsController::actionGetInfoCampaniaBackend($campania_id);

            if (isset($infocampania['encuestas']['answered'])):
                $answered = $infocampania['encuestas']['answered'];
            endif;

            if ($modal == 'modal-poll-list' and $answered == 1 and $bar_admin == 0):

                echo CJSON::encode(array(
                    'answered_poll' => $answered,
                    'mensaje' => '<div class="modredes">Esta encuesta ya fué respondida</div>'
                ));
                Yii::app()->end();
            else:
                $marca = MarcasController::actionDetalleMarcaBackend($infocampania['campanias']['marca_id']);

                $content = $this->renderPartial($modal . '/_Body', array(
                    'campania' => $infocampania['campanias'],
                    'imagen_multimedia' => $infocampania['imagen_multimedia'],
                    'bar_admin' => $bar_admin,
                    'estado_campania' => $estado_campania,
                    'marca' => $marca,
                    'comentarios' => $infocampania['comentariosData'],
                    'encuestas' => $infocampania['encuestas'],
                    'campania_id' => $campania_id,
                    'marca' => $marca,
                    'modal' => $modal), true, true);

                echo CJSON::encode(array(
                    'content' => $content,
                    'answered_poll' => 0
                ));
                Yii::app()->end();
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionObtenerPrimerMultimedia($multimedios) {
        foreach ($multimedios as $multimedia):
            if ($multimedia['is_video'] == 0):
                return $multimedia['enlace'];
            endif;
            Yii::app()->end();
        endforeach;

        return '';
    }

    public function actionGetInfoCampaniaBackend($campania_id) {

        $comentariosData = array();
        $encuestas = array();
        $url = $this->url_services() . '/campania/api_campanias/obtener_detalle?campania_id=' . $campania_id;

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);
        $jsondecode = CJSON::decode($response);
        $responseData = (array) $jsondecode;
        $campanias = $responseData['data'];

        if (isset($campanias['campaniaComentarios'])):
            foreach ($campanias['campaniaComentarios'] as $key => $comentario):
                $fecha_comentario = HotdealsController::actionCalcularFechaComentario($comentario['fecha_creacion']);

                if ($comentario['comentario_padre_id'] != 0):
                    $comentariosData[$comentario['comentario_padre_id']]['comentario_hijo_id'][$key] = $comentario;
                    $comentariosData[$comentario['comentario_padre_id']]['comentario_hijo_id'][$key]['fecha_comentario'] = $fecha_comentario;
                else:
                    $comentariosData[$comentario['id']] = $comentario;
                    $comentariosData[$comentario['id']]['fecha_comentario'] = $fecha_comentario;
                endif;
            endforeach;
        endif;

        if (isset($campanias['campaniaEncuestas']['encuesta']['datos']['nombre'])):
            $nombre = $campanias['campaniaEncuestas']['encuesta']['datos']['nombre'];
            $descripcion = $campanias['campaniaEncuestas']['encuesta']['datos']['descripcion'];
            $id = $campanias['campaniaEncuestas']['encuesta']['datos']['id'];
            $answered = $campanias['campaniaEncuestas']['encuesta']['datos']['answered'];

            $preguntasData = $campanias['campaniaEncuestas']['encuesta']['preguntas'];

            $encuestas = array('id' => $id, 'nombre' => $nombre, 'descripcion' => $descripcion, 'answered' => $answered, 'preguntas' => $preguntasData);
        endif;

        if (isset($campanias['multimedia'])) {
            $imagen_multimedia = HotdealsController::actionObtenerPrimerMultimedia($campanias['multimedia']);
        } else {
            $imagen_multimedia = $campanias['enlace'];
        }
        return array('campanias' => $campanias, 'comentariosData' => $comentariosData, 'encuestas' => $encuestas, 'imagen_multimedia' => $imagen_multimedia);
    }

    public function actionDarPikAPromoBackendAjax() {

        $campania_id = Yii::app()->request->getPost('camp', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):
            $post_data = array();

            $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_darpik?campania_id=' . $campania_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

//            $jsondecode = CJSON::decode($response);
//            $responseData =(array)$jsondecode;
//            print_r($responseData);               
        endif;
    }

    public function actionCalcularFechaComentario($fecha_new_comentario) {

        $fecha_calculada = 0;

        $fechainicial = new DateTime('now');
        $fecha_comentario = new DateTime($fecha_new_comentario);
        $diferencia = $fechainicial->diff($fecha_comentario);
//        $meses = ( $diferencia->y * 12 ) + $diferencia->m;
        $anos = $diferencia->format('%y');
        $meses = $diferencia->format('%m');
        $dias = $diferencia->format('%d');
        $horas = $diferencia->format('%h');
        $minutos = $diferencia->format('%i');
        $segundos = $diferencia->format('%s'); //
        if ($anos == 1):
            $fecha_calculada = 'Hace ' . $anos . ' año';
        elseif ($anos > 1):
            $fecha_calculada = 'Hace ' . $anos . ' años';
        elseif ($anos == 0 && $meses == 1):
            $fecha_calculada = 'Hace ' . $meses . ' mes';
        elseif ($anos == 0 && $meses > 1):
            $fecha_calculada = 'Hace ' . $meses . ' meses';
        elseif ($meses == 0 && $dias == 1):
            $fecha_calculada = 'Hace ' . $dias . ' día';
        elseif ($meses == 0 && $dias > 1):
            $fecha_calculada = 'Hace ' . $dias . ' días';
        elseif ($dias == 0 && $horas == 1):
            $fecha_calculada = 'Hace ' . $horas . ' hora';
        elseif ($dias == 0 && $horas > 1):
            $fecha_calculada = 'Hace ' . $horas . ' horas';
        elseif ($horas == 0 && $minutos == 1):
            $fecha_calculada = 'Hace ' . $minutos . ' minuto';
        elseif ($horas == 0 && $minutos > 1):
            $fecha_calculada = 'Hace ' . $minutos . ' minutos';
        elseif ($minutos == 0):
            $fecha_calculada = 'Ahora';
        endif;

        return $fecha_calculada;
    }

    public function actionCompartirCampaniaFacebook() {

        $provider = Yii::app()->request->getPost('provider', false);
        $mensaje = Yii::app()->request->getPost('mensaje', false);
        $descripcion = Yii::app()->request->getPost('descrip', false);
        $imagen = Yii::app()->request->getPost('imagen', false);
        $marca = Yii::app()->request->getPost('marca', false);
        $campania_id = Yii::app()->request->getPost('camp', false);
        $modal = Yii::app()->request->getPost('modal', false);

        $haComp = new HybridAuthIdentity();
        if (!$haComp->validateProviderName($provider))
            throw new CHttpException('500', 'Acción Inválida. Por favor intente nuevamente.');

        $haComp->adapter = $haComp->hybridAuth->authenticate($provider);

        if (isset($provider) && $provider == 'facebook'):
            $app_id = $haComp->adapter->api()->getAppId();
            $url = 'http://www.facebook.com/dialog/feed?app_id=' . $app_id
                    . '&link=' . 'http://www.wepiku.com/hotdeals/index?m=' . $marca
                    . "%26i=" . $campania_id
                    . '&message=' . $mensaje
                    . '&picture=' . $imagen
                    . '&name=' . $mensaje
//              '&caption='.encodeURIComponent(FBVars.fbShareCaption) 
                    . '&description=' . $descripcion
                    . '&redirect_uri=http://www.wepiku.com/hotdeals/close?camp=' . $campania_id . '_' . $modal //produccion
//                .'&redirect_uri=http://localhost/wepiku_web/dist/hotdeals/close?camp='.$campania_id.'_'.$modal //pruebas
                    . '&display=popup';

            echo CJSON::encode($url);
        endif;
    }

    public function actionComentarCampaniaBackend() {

        $texto_comentario = Yii::app()->request->getPost('com', false);
        $campania_id = Yii::app()->request->getPost('camp', false);
        $comentario_padre_id = 0;
        $usuario_id_camp = Yii::app()->request->getPost('campuid', false);
        $logo = Yii::app()->request->getPost('logo', false);
        $nombre_marca = Yii::app()->request->getPost('marc', false);
        //$comentarios = array(); 

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );
//            Yii::app()->clientScript->scriptMap['*.js'] = false;        

            $post_data = array();

            $url = $this->url_services() . '/campania/api_comentarios/create?campania_id=' . $campania_id . '&comentario=' . $texto_comentario . '&comentario_padre_id=' . $comentario_padre_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

            $infocampania = HotdealsController::actionGetInfoCampaniaBackend($campania_id);
//                        print_r($infocampania['comentariosData']);
//            Yii::app()->user->setFlash('success','Successfully Updated');
            $partial = $this->renderPartial('_comentarios', array('comentarios' => $infocampania['comentariosData'], 'campania_id' => $campania_id, 'usuario_id_camp' => $usuario_id_camp, 'logo' => $logo, 'nombre_marca' => $nombre_marca), true, true);
            echo CJSON::encode($partial);
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionNotifReproduccionVideoBackend() {

        $camp_id = Yii::app()->request->getPost('camp', false);
        $multi_id = Yii::app()->request->getPost('multi', false);

        if ($camp_id != 0 and Yii::app()->request->isAjaxRequest):
            $post_data = array();

            $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_vervideo?campania_id=' . $camp_id . '&multimedia_id=' . $multi_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

//            $jsondecode = CJSON::decode($response);
//            $responseData =(array)$jsondecode;
//            print_r($responseData);
//            echo CJSON::encode('reproducido');
        endif;
    }

    public function actionEditarComentarioAjax() {

        $comentario_id = Yii::app()->getRequest()->getParam('comid', false);
        $campania_id = Yii::app()->getRequest()->getParam('camp', false);
        $comentario = Yii::app()->getRequest()->getParam('com', false);

        if (isset($comentario_id) and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
//            Yii::app()->clientScript->corePackages = array();
            $this->renderPartial('_editarComentario', array('comentario_id' => $comentario_id, 'campania_id' => $campania_id, 'comentario' => $comentario), false, true);
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionActualizarComentarioBackendAjax() {

        $text_comment_edit = Yii::app()->request->getPost('edcom', false);
        $campania_id = Yii::app()->request->getPost('camp', false);
        $comentario_id = Yii::app()->request->getPost('comid', false);

        if (isset($text_comment_edit) and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

            $post_data = array();
            $url = $this->url_services() . '/campania/api_comentarios/edit?new_text=' . $text_comment_edit . '&campania_id=' . $campania_id . '&comentario_id=' . $comentario_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
//            $jsondecode = CJSON::decode($response);
//            $responseData =(array)$jsondecode;
//            print_r($responseData); 
//            echo 'algo';
            echo CJSON::encode('<p class="text-justify">' . $text_comment_edit . '</p>');
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionBorrarComentarioAjax() {

        $campania_id = Yii::app()->getRequest()->getParam('camp', false);
        $comentario_id = Yii::app()->getRequest()->getParam('comid', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):
            $post_data = array();
            $url = $this->url_services() . '/campania/api_comentarios/delete?campania_id=' . $campania_id . '&comentario_id=' . $comentario_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
//            $jsondecode = CJSON::decode($response);
//            $responseData =(array)$jsondecode;
//            print_r($responseData);    
            echo '';
        endif;
    }

    public function actionEditRespuComentarioAjax() {

        $comentario_id = Yii::app()->getRequest()->getParam('comid', false);
        $campania_id = Yii::app()->getRequest()->getParam('camp', false);
        $usuario_id_camp = Yii::app()->getRequest()->getParam('campuid', false);
        $logo = Yii::app()->getRequest()->getParam('logo', false);
        $nombre_marca = Yii::app()->getRequest()->getParam('marc', false);

        if ($comentario_id != 0 and Yii::app()->request->isAjaxRequest):
            $this->renderPartial('_respuestaComentario', array('comentario_id' => $comentario_id, 'campania_id' => $campania_id, 'usuario_id_camp' => $usuario_id_camp, 'logo' => $logo, 'nombre_marca' => $nombre_marca), false, true);
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionResponderComentarioBackendAjax() {

        $texto_comentario = Yii::app()->request->getPost('com', false);
        $campania_id = Yii::app()->request->getPost('camp', false);
        $comentario_padre_id = Yii::app()->request->getPost('comid', false);
        $usuario_id_camp = Yii::app()->request->getPost('campuid', false);
        $logo = Yii::app()->request->getPost('logo', false);
        $nombre_marca = Yii::app()->request->getPost('marc', false);
        //$comentarios = array(); 

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );
            Yii::app()->clientScript->scriptMap['*.js'] = false;

            $post_data = array();

            $url = $this->url_services() . '/campania/api_comentarios/create?campania_id=' . $campania_id . '&comentario=' . $texto_comentario . '&comentario_padre_id=' . $comentario_padre_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

            $infocampania = HotdealsController::actionGetInfoCampaniaBackend($campania_id);
//            Yii::app()->user->setFlash('success','Successfully Updated');
            $partial = $this->renderPartial('_comentarios', array('comentarios' => $infocampania['comentariosData'], 'campania_id' => $campania_id, 'usuario_id_camp' => $usuario_id_camp, 'logo' => $logo, 'nombre_marca' => $nombre_marca), true, true);
            echo CJSON::encode($partial);
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionEnviarRespuestasBackend() {

        $attr_encuesta = array();

        if (Yii::app()->request->isAjaxRequest):

            $attr_encuesta['campania_id'] = htmlspecialchars(Yii::app()->request->getPost('campania_id', false));
            $attr_encuesta['encuesta_id'] = htmlspecialchars(Yii::app()->request->getPost('encuesta_id', false));

            $i = 0;
            $j = 0;
            $k = 0;

            foreach (Yii::app()->request->getPost('preg_tipo_id') as $key => $preg_y_tipo_id):

                $preg_tipo_id = explode("+", $preg_y_tipo_id);
                $pregunta_id = $preg_tipo_id[0];
                $tipo_pregunta_id = $preg_tipo_id[1];

                if ($tipo_pregunta_id == 1):
                    if (Yii::app()->request->getPost('text_preg_abierta' . $i) !== ''):
                        $attr_encuesta['respuestas'][$key]['pregunta_id'] = $pregunta_id;
                        $attr_encuesta['respuestas'][$key]['respuesta_id'] = htmlspecialchars(Yii::app()->request->getPost('respu_abierta_id' . $i));
                        $attr_encuesta['respuestas'][$key]['texto'] = htmlspecialchars(Yii::app()->request->getPost('text_preg_abierta' . $i));
                        $i++;
                    endif;
                elseif ($tipo_pregunta_id == 2):
                    if (Yii::app()->request->getPost('preg_unic_resp' . $j) !== null):
                        $attr_encuesta['respuestas'][$key]['pregunta_id'] = $pregunta_id;
                        $valor_unica_resp = explode("+", Yii::app()->request->getPost('preg_unic_resp' . $j));
                        $attr_encuesta['respuestas'][$key]['respuesta_id'] = $valor_unica_resp[0];
                        $attr_encuesta['respuestas'][$key]['texto'] = $valor_unica_resp[1];
                        $j++;
                    endif;
                elseif ($tipo_pregunta_id == 3):
                    if (is_array(Yii::app()->request->getPost('preg_multi_resp' . $k))):
                        foreach (Yii::app()->request->getPost('preg_multi_resp' . $k) as $key_multi => $multiple_respuesta):
                            $attr_encuesta['respuestas'][$key + $key_multi]['pregunta_id'] = $pregunta_id;
                            $valor_multi_resp = explode("+", $multiple_respuesta);
                            $attr_encuesta['respuestas'][$key + $key_multi]['respuesta_id'] = $valor_multi_resp[0];
                            $attr_encuesta['respuestas'][$key + $key_multi]['texto'] = $valor_multi_resp[1];
                        endforeach;
                        $k++;
                    endif;
                endif;
            endforeach;
            if (isset($attr_encuesta['respuestas'])):
                $json = str_replace("'", "", CJSON::encode($attr_encuesta, true));

                //print_r($json);
                $url = $this->url_services() . '/campania/api_campaniarespuestausuario/responder';

                $wepiku_token = Yii::app()->session['wepiku_access_token'];
                $token = 'Bearer ' . $wepiku_token;
                $response = Yii::app()->curl->setHeaders(array('Authorization' => $token, 'Content-Type' => 'application/json'))->post($url, $json);

                $jsondecode = CJSON::decode($response, true);
                $responseData = (array) $jsondecode;

                if ($responseData['success'] == 1):
                    echo CJSON::encode(array(
                        'mensaje' => '<div class="modredes">' . $responseData['data'] . '</div>',
                        'resp' => 1
                    ));
                else:
                    echo CJSON::encode(array(
                        'mensaje' => '<div class="modredes">¡Algo sucedió!, no fué posible enviar las respuestas. Intente más tarde</div>',
                        'resp' => 1
                    ));
                endif;
            else:
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">Responde al menos una pregunta</div>',
                    'resp' => 0
                ));
            endif;
        endif;
    }

    public function actionNuevoReportePublicacionAjax() {

        $campania_id = Yii::app()->request->getPost('camp', false);
        $marca = Yii::app()->request->getPost('marca', false);
        $imagen_multimedia = Yii::app()->request->getPost('imagmulti', false);
        $modal = Yii::app()->request->getPost('modal', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

            $accion = 'reporte';
            $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);

            $content = $this->renderPartial('modal-report/_Body', array(
                'imagen_multimedia' => $imagen_multimedia,
                'motivos_reporte' => $wepiku_data['motivos_reporte'],
                'campania_id' => $campania_id,
                'marca' => $marca,
                'modal' => $modal
                    ), true, true);

            echo CJSON::encode(array(
                'content' => $content
            ));
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionEnviarReporteBackend() {

        $campania_id = Yii::app()->request->getPost('camp', false);
        $motivo_reporte = Yii::app()->request->getPost('motivrepo', false);
        $modal = Yii::app()->request->getPost('modal', false);
//        $modal = '#modal-product';
        //$comentarios = array(); 

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );
            if ($motivo_reporte != 0):
                $post_data = array();
                $valor_motivo_reporte = explode("+", $motivo_reporte);
                $motivo_reporte_id = $valor_motivo_reporte[0];
                $motivo_reporte_text = $valor_motivo_reporte[1];

                $url = $this->url_services() . '/campania/api_campaniahasreporte/reportar_campania?campania_id=' . $campania_id . '&motivo_id=' . $motivo_reporte_id . '&texto_usuario=' . $motivo_reporte_text;

                $wepiku_token = Yii::app()->session['wepiku_access_token'];
                $token = 'Bearer ' . $wepiku_token;
                $auth = array('Authorization: ' . $token);
                $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

                $jsondecode = CJSON::decode($response, true);
                $responseData = (array) $jsondecode;

                if ($responseData['success'] == 1):
                    echo CJSON::encode(array(
                        'modal' => '#' . $modal,
                        'mensaje' => '<div class="modredes">' . $responseData['data'] . '</div>',
                        'rep' => 1
                    ));
                else:
                    echo CJSON::encode(array(
                        'modal' => '#' . $modal,
                        'mensaje' => '<div class="modredes">¡Algo sucedió!, no fué posible reportar contenido. Intente más tarde</div>',
                        'rep' => 1
                    ));
                endif;
            else:
                echo CJSON::encode(array(
                    'modal' => '#' . $modal,
                    'mensaje' => '<div class="modredes">Elije alguna opción para reportar</div>',
                    'rep' => 0
                ));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionClose() {
        $this->render('close');
    }

}
