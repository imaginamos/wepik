<?php
require_once( dirname(__FILE__) . '/../controllers/HotdealsController.php');
require_once( dirname(__FILE__) . '/../controllers/CampaniasController.php');

class SiteController extends Controller
{

    public function filters()
    {
      return array(
        'accessControl', // perform access control for CRUD operations
      );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'list' and 'show' actions
                'actions'=>array('login', 'loginMarca', 'politicas', 'contacto', 'socialLogin','CompartirApp'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated users to perform any action
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public $layout='main';
    public function missingAction($actionID)
    {
            // Yii::app()->user->setFlash('warning','This is a test');
            // Yii::app()->user->setFlash('danger','This is a test');
            // Yii::app()->user->setFlash('success','This is a test');

            if($actionID=='' or $actionID=='main.php'):
                    $actionID='login';
                    $this->layout = 'login';
            endif;

            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            $this->render(strtr($actionID,array('.php'=>'')));
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            $this->render('error', $error);
            }
    }

    public function actionLogin($marca = 0){

        if($marca == 0):
            $this->layout = 'login';
        elseif($marca == 1):
            $this->layout = 'marcaLogin';
        elseif($marca == 2):
            $this->layout = 'politicasLogin';
        endif;
        if(isset($_GET['theme']))
                Yii::app()->theme=$_GET['theme'];
        if (!isset($_GET['provider']))
           {
                $this->render('login');
           } else {
                $provider = $_GET['provider'];
                $datos_usuario_json = array();

                try
                {

                    //Yii::import('components.HybridAuthIdentity'); //ext.components.HybridAuthIdentity
                    $haComp = new HybridAuthIdentity();
                    if (!$haComp->validateProviderName($provider))
                        throw new CHttpException ('500', 'Acción Inválida. Por favor intente nuevamente.');

                    $haComp->adapter = $haComp->hybridAuth->authenticate($provider);
                    $haComp->userProfile = $haComp->adapter->getUserProfile();
                    $haComp->Login();

                    if(isset($haComp->userProfile)){
                        ///
                        $token = $haComp->adapter->getAccessToken();
                        Yii::app()->session['facebook_nombre'] = $haComp->userProfile->displayName;
                        Yii::app()->session['email'] = $haComp->userProfile->email;
                        
                        //$haComp->userProfile;
                        $login = 1;
                        $datos_usuario_json = SiteController::actionLoginBackend($haComp->userProfile->identifier, $token["access_token"], $login);
                        if(isset($datos_usuario_json)):
                            SiteController::actionIngresoDiarioBackend();
                            SiteController::actionNotifProximosAVencer($datos_usuario_json['data']['wepiku_config_general']['0']);
                        endif;

                        if(Yii::app()->session['is_new'] == true):
                            $this->redirect($this->createUrl('/marcas/index'));
                        else:
                            $this->redirect($this->createUrl('/hotdeals/index'));  //redirect to the user logged in section..
                        endif;
                        //print_r($datos_usuario_json);
                        //echo 'id: '.$haComp->userProfile->identifier;
                        //echo 'token: '.$token["access_token"];
                        //$haComp->login();
                        //$this->render('index', array('datos_usuario_json' => $datos_usuario_json));
                    }
                }
                catch (Exception $e)
                {
                    //process error message as required or as mentioned in the HybridAuth 'Simple Sign-in script' documentation
                    $this->render('login', array('error' => $e->getMessage()));

                }
           }
    }

    public function actionLoginMarca(){
        
        $this->layout = 'marcaLogin'; 
        $marca = 1;
        SiteController::actionLogin($marca);
    }

    public function actionPoliticas(){
        $this->layout = 'politicasLogin';
        $marca = 2;
        SiteController::actionLogin($marca);
    }

    public function actionSocialLogin()
    {
        //Yii::import('components.HybridAuthIdentity'); //ext.components.HybridAuthIdentity
        $path = Yii::getPathOfAlias('ext.HybridAuth');
        require_once $path . '/hybridauth-' . HybridAuthIdentity::VERSION . '/hybridauth/index.php';

    }

    public function actionLogout(){

        //logout
        //unset(Yii::app()->session['facebook_token']);
        //Yii::app()->session->remove('facebook_token');
        Yii::app()->session->clear();
        Yii::app()->session->destroy();

        $this->redirect(array('login'));
    }

    public function actionPuntos(){

//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.min.js' => false
//        );
        $this->hasNewsFeed = true;
        $cat_puntos = array();
        $response_marca = array();

        if(isset($_GET['theme']))
                Yii::app()->theme=$_GET['theme'];

        $url = $this->url_services() . '/usuario/api_usuarioaccion/obtener_puntos';

        //$wepiku_token = 'a8dYbYn8w4mRc6x6pAy65Tg258fTnSoQ3SgMiSgT';
        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $responseData =$jsondecode;

        foreach ($responseData['data']['categorias'] as $cat_punto):
            $cat_puntos[$cat_punto['id']] = $cat_punto['puntos'];
        endforeach;

        $this->render('puntos', array('cat_puntos' => $cat_puntos, 'data_puntos_json' => $responseData['data']));
    }


    public static function actionLoginBackend($facebook_id, $facebook_token, $login){

        //$header = array('Content-Type: multipart/form-data');

        $post_data = array(
            'client_id' => 'ios',
            'client_secret' => 'TjBtAmVtNqIj83SvHi8hK0I32k4e27S7',
            'facebook_token' => $facebook_token,
            'facebook_id' => $facebook_id
              );

        $url = self::url_services() . '/usuario/api_usuarios/login';

        $response = Yii::app()->curl->post($url, $post_data, array(), true);

        $jsondecode = CJSON::decode($response);

        if($login == 1):
            Yii::app()->session['wepiku_access_token'] =$jsondecode['data']['wepiku_token']['access_token'];
            Yii::app()->session['wepiku_user_img'] =$jsondecode['data']['wepiku_user']['img'];
            Yii::app()->session['wepiku_user_id'] =$jsondecode['data']['wepiku_user']['id'];
            Yii::app()->session['url_img'] =$jsondecode['data']['urlimg'];
            Yii::app()->session['facebook_token'] = $facebook_token;
            Yii::app()->session['facebook_id'] = $facebook_id;
            Yii::app()->session['piks_a_vencer'] = 0;
            Yii::app()->session['is_new'] = $jsondecode['data']['is_new'];
            if($jsondecode['data']['is_new']['wepiku_user']['mail_valido'])
            {
                Yii::app()->session['email'] = $jsondecode['data']['is_new']['wepiku_user']['email'];
            }else{
                Yii::app()->session['email'] = null;
            }
//            Yii::app()->session['is_new'] = 1;
        endif;

        $responseData =(array)$jsondecode;
        return $responseData;
    }

    public function actionIngresoDiarioBackend(){

        $post_data = array();

        $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_ingreso_diario';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
    }

    public function actionInfoUsuarioBackend(){

        $post_data = array();

        $url = $this->url_services() . '/usuario/api_usuarios/datos_usuario';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

        $jsondecode = CJSON::decode($response, true);
        $responseData =(array)$jsondecode;
        return $responseData;
    }

    public function actionCompartirApp(){
               $provider = Yii::app()->getRequest()->getParam('provider');
//
//        if(isset($provider)):
//            $haComp = new HybridAuthIdentity();
//            if (!$haComp->validateProviderName($provider))
//                throw new CHttpException ('500', 'Acción Inválida. Por favor intente nuevamente.');
//
//            $haComp->adapter = $haComp->hybridAuth->authenticate($provider);
//
//            $haComp->adapter->setUserStatus(
//                array(
//                    "message" => "mensaje de wepiku", // status or message content
//                    "link" => "http://45.56.120.97/wepiku_web/dist/site/login", // webpage link
//                    "picture" => Yii::app()->session['url_img']."/img/brand-logo.png", // a picture link
//                )
//            );
//            SiteController::actionNotifCompartirAppBackend($provider);
//        endif;

        //$provider = Yii::app()->request->getPost('provider', false);
        $mensaje = 'Ingresa a Wepiku';
        $descripcion = 'Wepiku la mejor forma de dar a conocer tus marcas';
        $imagen = "http://www.wepiku.com/themes/front/img/brand-logo.png";
        //$marca = Yii::app()->request->getPost('marca', false);
        //$campania_id = Yii::app()->request->getPost('camp', false);
        //$modal = Yii::app()->request->getPost('modal', false);

        $haComp = new HybridAuthIdentity();
        if (!$haComp->validateProviderName($provider))
            throw new CHttpException ('500', 'Acción Inválida. Por favor intente nuevamente.');

        $haComp->adapter = $haComp->hybridAuth->authenticate($provider);

        if(isset($provider) && $provider == 'facebook'):
            $app_id = $haComp->adapter->api()->getAppId();
            $url = 'http://www.facebook.com/dialog/feed?app_id='.$app_id
                .'&link=http://www.wepiku.com/hotdeals/index'
                .'&message='.$mensaje
                .'&picture='.$imagen
                .'&name='.$mensaje
//              '&caption='.encodeURIComponent(FBVars.fbShareCaption)
                .'&description='.$descripcion
                .'&redirect_uri=http://www.wepiku.com/hotdeals/index' //produccion
//                .'&redirect_uri=http://localhost/wepiku_web/dist/hotdeals/index' //pruebas
                .'&display=popup';

            echo CJSON::encode($url);
        endif;
    }

    public function actionNotifCompartirAppBackend($provider){

        $post_data = array();

        $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_compartir_app';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);

        $jsondecode = CJSON::decode($response, true);
        $responseData =(array)$jsondecode;
        print_r($responseData);
        echo $provider;
    }

    public function actionNotifProximosAVencer($data_config_general){

        $dias_aviso_piks = $data_config_general['dias_aviso_piks'];

        $responseData = CampaniasController::actionObtenerPiksBackend();
        if($responseData['data'] != null):
            function ordenar($a, $b ) {
                return strtotime($a['fecha_finalizacion']) - strtotime($b['fecha_finalizacion']);
            }

            usort($responseData['data'], 'ordenar');
//            foreach($responseData['data'] as $ffin):
//                echo '<br/>ffin: '.$ffin['fecha_finalizacion'];
//            endforeach;
            $fecha_final = $responseData['data'][0]['fecha_finalizacion'];
            $diaNow = date('d-m-Y');
            $fecha = date_create($fecha_final);
            $diaf = date_format($fecha, 'd-m-Y');
            $diferencia_dias = floor((strtotime($diaf)-strtotime($diaNow))/86400);

            if($diferencia_dias <= $dias_aviso_piks):
                Yii::app()->session['piks_a_vencer'] = $diferencia_dias;
            endif;
        endif;
    }

    public function actionContacto(){
        if(Yii::app()->request->isPostRequest) {
            try {
                Yii::import('ext.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage('Un usuario ha solicitado información de Wepiku');
                $message->view = 'contact';
                $message->setBody(array(
                    'request' => Yii::app()->request
                ), 'text/html');
                $message->addTo('federico.navarro@imaginamos.com');
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
                echo 'Su solicitud ha sido enviada';
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
        header('Content-type: text/plain');
//        $this->renderJSON($response);
        Yii::app()->end();
    }

    public function actionGetInfoPerfilBackend(){

//        $campania_id = Yii::app()->request->getPost('camp', false);
        $modal_name = Yii::app()->request->getPost('modal', false);
        $modal = str_replace("#", "", $modal_name);
        if(Yii::app()->request->isAjaxRequest):
            $facebook_token = Yii::app()->session['facebook_token'];
            $post_data = array('facebook_token'=>$facebook_token);            
            
            $url = self::url_services() . '/usuario/api_usuarios/datos_usuario';

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);            
            $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);   
            
            $jsondecode = CJSON::decode($response);
            $responseData =(array)$jsondecode;   
            $i=0;
            foreach($responseData['data']['acciones'] as $accion){
                $accion['fecha_creacion'] = SiteController::actionCalcularFechaComentario($accion['fecha_creacion']);
                $responseData['data']['acciones'][$i] =$accion;
                $i++;
            }
            
            $header = $this->renderPartial($modal.'/_Header', array('usuario' => $responseData['data']), true, true);
            $body = $this->renderPartial($modal.'/_Body', array('usuario' => $responseData['data']), true, true);
            $footer = $this->renderPartial($modal.'/_Footer', array('usuario' => $responseData['data']), true, true);

            echo CJSON::encode(array(
                'content' => $header.$body.$footer
            ));
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }
    
    public function actionCalcularFechaComentario($fecha_new_comentario){
        
        $fecha_calculada = 0;
        
        $fechainicial = new DateTime('now');
        $fecha_comentario = new DateTime($fecha_new_comentario);
        $diferencia = $fechainicial->diff($fecha_comentario);
//        $meses = ( $diferencia->y * 12 ) + $diferencia->m;
        $anos = $diferencia->format('%y');
        $meses = $diferencia->format('%m');
        $dias = $diferencia->format('%d');
        $horas = $diferencia->format('%h');
        $minutos = $diferencia->format('%i');
        $segundos = $diferencia->format('%s'); //
        if($anos == 1):
            $fecha_calculada = 'Hace '.$anos.' año';
        elseif($anos > 1):
            $fecha_calculada = 'Hace '.$anos.' años';                
        elseif($anos==0 && $meses == 1):
            $fecha_calculada = 'Hace '.$meses.' mes';
        elseif($anos==0 && $meses > 1):
            $fecha_calculada = 'Hace '.$meses.' meses';    
        elseif($meses == 0 && $dias == 1):
            $fecha_calculada = 'Hace '.$dias.' día';     
        elseif($meses == 0 && $dias > 1):
            $fecha_calculada = 'Hace '.$dias.' días';  
        elseif($dias == 0 && $horas == 1):
            $fecha_calculada = 'Hace '.$horas.' hora';
        elseif($dias == 0 && $horas > 1):
            $fecha_calculada = 'Hace '.$horas.' horas';
        elseif($horas == 0 && $minutos == 1):
            $fecha_calculada = 'Hace '.$minutos.' minuto';
        elseif($horas == 0 && $minutos > 1):
            $fecha_calculada = 'Hace '.$minutos.' minutos';  
        elseif($minutos == 0):
            $fecha_calculada = 'Ahora';       
        endif;
        
        return $fecha_calculada;
    }
}
//id: 10153265537439146
//token: CAAXWaC68PuoBAF5lxI1J5nWbMPpiBo1q6tVY5unJX5AOf8HpAdVL2dtsMeeKsZBZBR3On3LhhzU2ZAGON9OTi11ZAhQvCick39keir23pYRfRlgu6bHHK1JrrjV3WboYu2q6GMpNDY4cd94Bct0aQtZB6R3IP7QYHWMq22szeZA71ZC37mgPj1LddUefGkpZBCcZD
//wepiku_token: bZSjwSpgxrUxMGjjmqRuymbvYIIt5gi7BWeukT07


//**Llamar action desde otro controlador**//
//$cat=Yii::app()->createController('marcas');//returns array containing controller instance and action index.
//$cat=$cat[0]; //get the controller instance.
//$infoAdminActiva = $cat->actionMarcaAdminActivas($marca_id); //use a public method.