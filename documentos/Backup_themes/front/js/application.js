/*!
  Author: Stive Zambrano
  Creation date: March of 2015
  Project name: wepiku
  File name: application.js
  Description: Global functions
*/


/*jslint browser: true*/
/*jshint undef: true, unused: false*/
/*global $, jQuery, console, alert, confirm, promp, google, Handlebars, TimelineLite:false, TimelineMax:false, TweenLite:false, TweenMax:false, Back:false, Bounce:false, Circ:false, Cubic:false, Ease:false, EaseLookup:false, Elastic:false, Expo:false, Linear:false, Power0:false, Power1:false, Power2:false, Power3:false, Power3:false, Power4:false, Quad:false, Quart:false, Quint:false, RoughEase:false, Sine:false, SlowMo:false, SteppedEase:false, Strong:false, Draggable:false, SplitText:false, VelocityTracker:false, ThrowPropsPlugin:false, CSSPlugin:false, BezierPlugin:false*/


var
    tMs = TweenMax.set,
    tMt = TweenMax.to,
    tLs = TweenLite.set,
    tLt = TweenLite.to,
    container,
    Masonry,
    msnry,
    imagesLoaded;


function loadFn() {
    "use strict";
    tLt(".wepiku-item", 0.6, {
        className: "+=active",
        ease: Cubic.easeOut
    });
    if ($(window).innerHeight() < 650) {
        $(".nav-main").addClass("nav-static");
    }

    tLt(".poll-banner-icon1, .poll-banner-icon2", 0.1, {
        className: "+=active"
    });


    tLt(".poll-banner-step1", 0.1, {
        className: "+=active",
        delay: 0.8
    });

    tLt(".poll-banner-step2", 0.1, {
        className: "+=active",
        delay: 1
    });

    tLt(".poll-banner-step3", 0.1, {
        className: "+=active",
        delay: 1.2
    });

    $("#my-points").circliful();

}





$(window).on("load resize", function () {
    "use strict";
    var
        // center = map.getCenter(),
        windowH = $(window).innerHeight();


    $("#map-container").height(windowH - 54);
    $("#bh-sl-map").height(windowH - 54);
    // google.maps.event.trigger(map, "resize");
    // map.setCenter(center);


});


// hasClass
function hasClass(elem, className) {
    "use strict";
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}


// toggleClass
function toggleClass(elem, className) {
    "use strict";
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, " ") + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0) {
            newClass = newClass.replace(" " + className + " ", " ");
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}






// if ($(".col-main").hasClass("grid-section")) {
$(document).on("click", ".btn-sub-nav", function () {
    "use strict";
    var wrapper = document.querySelector(".sub-nav");
    toggleClass(wrapper, "active");
});
// }




$(document).on("click", ".call-stores", function () {
    "use strict";
    var wrapper = document.querySelector("body");
    toggleClass(wrapper, "app-active");
    $(".close-pro").trigger("click");
});


$(document).on("click", ".close-app", function () {
    "use strict";
    var wrapper = document.querySelector("body");
    toggleClass(wrapper, "app-active");
});




if ($(".col-main").hasClass("brand-section")) {
    $(document).on("click", ".btn-favorite", function () {
        "use strict";
        var
            wrapper = document.querySelector(".brand-like");
        toggleClass(wrapper, "active");
    });
}


$(document).on("click", "btn-test", function () {
    "use strict";
    var
        sedes = [
            /*Marker*/
            ['<div class="row tool-map">' +
                '<div class="col-md-6"><img src="img/map-img.jpg"></div>' +
                '<div class="col-md-6 col-info-contact">' +
                '<h4>Lorem ipsum</h4><div class="clearfix"></div>' +
                '<p>Cll. 999 No. 999-999</p><div class="clearfix"></div>' +
                '<p>Tel: 123 4567</p><div class="clearfix"></div>' +
                '<p>Bogotá - Colombia</p><div class="clearfix"></div>' +
                '</div>' +
                '</div>', 4.598056, -74.075833
                ]
            /*Fin marker*/
        ],
        map = new google.maps.Map(document.getElementById("map"), {
            //scrollwheel: false,
            zoom: 14,
            center: new google.maps.LatLng(4.598056, -74.075833)
        }),
        image = {
            url: "img/marker-default.png",
            size: new google.maps.Size(48, 42),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(16, 42)
        },
        infowindow = new google.maps.InfoWindow(), marker, i;

    for (i = 0; i < sedes.length; i = i + 1) {
        marker = new google.maps.Marker({
            icon: image,
            position: new google.maps.LatLng(sedes[i][1], sedes[i][2]),
            map: map,
            zIndex: sedes[3]
        });
        google.maps.event.addListener(marker, "click", (function (marker, i) {
            return function () {
                infowindow.setContent(sedes[i][0]);
                infowindow.open(map, marker);
            };
        }(marker, i)));
    }
    document.addEventListener("DOMContentLoaded", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
});


if ($(".col-main").hasClass("grid-section")) {
    container = document.querySelector(".wepiku-grid");
    imagesLoaded(container, function () {
        "use strict";
        msnry = new Masonry(container, {
            columnWidth: 375,
            itemSelector: ".wepiku-item"
        });
    });
}


if ($(".section-id").hasClass("section-plan") || $(".section-id").hasClass("section-encuesta")) {
    $(".sub-nav").hide();
}


if ($(".section-id").hasClass("section-home")) {
    $(".nav0").addClass("active");
}

if ($(".section-id").hasClass("section-new-mark")) {
    $(".sub-nav, .top-search").hide();
    $(".nav5").addClass("active");
}

if ($(".section-id").hasClass("section-how") || $(".section-id").hasClass("section-plan")) {
    $(".nav7").addClass("active");
}

if ($(".section-id").hasClass("section-piks")) {
    $(".nav2").addClass("active");
}




$(window).on("load", function () {
    "use strict";
    tLt("#preload", 0.8, {
        css: {
            autoAlpha: 0,
            scale: 2
        },
        delay: 2,
        ease: Cubic.easeOut,
        onComplete: loadFn
    });

    if ($(".main-section").data("section") === "map") {
        $("#map-container").storeLocator();
    }
});




$(document).scroll(function () {
    "use strict";
    $(".brand-page-head").css({
        marginTop: (-$(this).scrollTop() / 10)
    });
    $(".brand-head-fx").css({
        marginTop: (-$(this).scrollTop())
    });
});





function multiSelect1(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "MarcaForm[categoria][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "MarcaForm[categoria][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}


function multiSelect2(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "MarcaForm[mom_consumo][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "MarcaForm[mom_consumo][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}


function multiSelect3(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value || parseInt(option.value) === 0) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "CampaniaForm[cobertura][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "CampaniaForm[cobertura][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}


function multiSelect4(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value || parseInt(option.value) === 0) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "CampaniaForm[sucursales][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "CampaniaForm[sucursales][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}


function multiSelect5(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value || parseInt(option.value) === 0) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "CampaniaForm[cobertura2][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "CampaniaForm[cobertura2][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}


function multiSelect6(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value || parseInt(option.value) === 0) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "CampaniaForm[sucursales2][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "CampaniaForm[sucursales2][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}


function multiSelect7(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        input2 = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;
    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value || parseInt(option.value) === 0) {
            return;
        }
    }
    input.type = "hidden";
    input.name = "EncuestaForm[coberturaenc][" + choices.length + "][id]";
    input.value = option.value;
    input2.type = "hidden";
    input2.name = "EncuestaForm[coberturaenc][" + choices.length + "][nombre]";
    input2.value = option.text;
    li.appendChild(input);
    li.appendChild(input2);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");
    ul.appendChild(li);
}

// var attr = $("iframe").attr("id");
//
// console.log("attr: " + attr);

var video;
function tgVideo(state) {
    if( $("#player").length >0) // Verificar que existe
    {
        video = $("#player")[0].contentWindow;
        //func = state == "pause" ? "pauseVideo" : "playVideo";
        func = state == "pause" ? "pause" : "play";
        try {
            video.postMessage('{"event": "command", "func": "' + func + '", "args": ""}', '*');
        } catch (e) {
            $("#player")[0].postMessage('{"event": "command", "func": "' + func + '", "args": ""}', '*');
        }

    }
    // if ($("iframe").attr("id") === "player") {
    //     video.postMessage('{"event": "command", "func": "' + func + '", "args": ""}', '*');
    // } else {
    //     console.log("video empty");
    // }
}




$(document).ready(function () {
    "use strict";

    var
        // wH = $(window).innerHeight(),
        // wW = $(window).innerWidth(),
        Fn = Function,
        // tMs = TweenMax.set,
        tMt = TweenMax.to,
        videoOne = $(".video-content"),
        videoTwo = $(".video-promotion"),
        btnVideoOne = $(".btn-add-video-content"),
        btnVideoTwo = $(".btn-add-video-promotion");

    // Misc
    if (new Fn("/*@cc_on return document.documentMode===10@*/")()) {
        document.documentElement.className += " ie10";
    }

    $(".over-bw").hover(function () {
        tMt($(this).find(".icon-bw"), 0.6, {marginTop: -12, ease: Cubic.easeOut});
    }, function () {
        tMt($(this).find(".icon-bw"), 0.6, {marginTop: 0, ease: Cubic.easeOut});
    });

    $(".bt-bw").on("click", function () {
        tMt(".con-bw", 0.6, {autoAlpha: 0, ease: Cubic.easeOut});
    });


    $("html, body").keyup(function (e) {
        if (e.keyCode === 27) {
            $(".modal-item").modal("hide");
            $(".modal-stores").modal("hide");
            $("body").removeClass("app-active");
        }
    });


    // Custom check
    $(".check-active").iCheck({
        checkboxClass: "icheckbox_minimal",
        radioClass: "iradio_minimal",
        increaseArea: "20%" // optional
    });

    $(".check-active-content").on("ifChecked", function () {
        
        //$("#CampaniaForm_imagen_tamano").children().removeAttr("disabled");
        EsFreemium(true);
        
        $(".item-poll-content").addClass("active");
        $(".item-poll-content").find(".numeros_costos").each(function () {
        var
            counter = $(this).val(),
            attrid = $(this).attr('id'),
            tipo,
            id_cost2,
            cost_img2,
            calc_costo_unformat,
            id_cost,
            cost_img,
            total;

            tipo = $(".tipoCamp").val();
            if (parseInt(tipo, 0) === 1 || parseInt(tipo, 0) === 4) {
                id_cost2 = $("#CampaniaForm_imagen_tamano2").find(":selected").val();
                if (id_cost2) {
                    cost_img2 = id_cost2.split("+");
                    calc_costo_unformat = counter * $("." + attrid).val() * cost_img2[1];
                } else {
                    calc_costo_unformat = counter * $("." + attrid).val();
                }
            } else if (parseInt(tipo, 0) === 2 || parseInt(tipo, 0) === 5) {
                id_cost = $("#CampaniaForm_imagen_tamano").find(":selected").val();
                if (id_cost) {
                    cost_img = id_cost.split("+");
                    calc_costo_unformat = counter * $("." + attrid).val() * cost_img[1];
                } else {
                    calc_costo_unformat = counter * $("." + attrid).val();
                }
            } else if (parseInt(tipo, 0) === 3) {
                calc_costo_unformat = counter * $("." + attrid).val();
            }
            $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({
                region: "es-CO",
                decimalSymbol: ',',
                digitGroupSymbol: '.'
            });
            total = 0;
            $(".valor-costo").each(function () {
                var textnumber = $(this).children("p").text();
                total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
            });
            $("#total").children("p").html(total).formatCurrency({ region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
        });
    }).on("ifUnchecked", function () {
        
//        $("#CampaniaForm_imagen_tamano").children().attr("disabled", "disabled");
//        $("#CampaniaForm_imagen_tamano option:nth-child(1)").removeAttr('disabled');
//        $("#CampaniaForm_imagen_tamano option:nth-child(2)").removeAttr('disabled');
        EsFreemium(false);
        
        $(".item-poll-content").removeClass("active");
        $(".valor-costo").each(function () {
            $(this).children("p").html(0).formatCurrency({
                region: "es-CO",
                decimalSymbol: ',',
                digitGroupSymbol: '.'
            });
        });
        $("#total").children("p").html(0).formatCurrency({region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
//        $(".check-active-content").val(2);
        // alert(event.type + " callback");
    });
    
    $('#CampaniaForm_marcas').change( function (){
        
    })
    

    $(".check-active-promotion").on("ifChecked", function () {
        $(".item-poll-promotion").addClass("active");
        $(".item-poll-promotion").find(".numeros_costos").each(function () {
        var
            counter = $(this).val(),
            attrid = $(this).attr('id'),
            tipo,
            id_cost2,
            cost_img2,
            calc_costo_unformat,
            id_cost,
            cost_img,
            total;
            tipo = $(".tipoCamp").val();
            if (parseInt(tipo, 0) === 1 || parseInt(tipo, 0) === 4) {
                id_cost2 = $("#CampaniaForm_imagen_tamano2").find(":selected").val();
                if (id_cost2) {
                    cost_img2 = id_cost2.split("+");
                    calc_costo_unformat = counter * $("." + attrid).val() * cost_img2[1];
                } else {
                    calc_costo_unformat = counter * $("." + attrid).val();
                }
            } else if (parseInt(tipo, 0) === 2 || parseInt(tipo, 0) === 5) {
                id_cost = $("#CampaniaForm_imagen_tamano").find(":selected").val();
                if (id_cost) {
                    cost_img = id_cost.split("+");
                    calc_costo_unformat = counter * $("." + attrid).val() * cost_img[1];
                } else {
                    calc_costo_unformat = counter * $("." + attrid).val();
                }
            } else if (parseInt(tipo, 0) === 3) {
                calc_costo_unformat = counter * $("." + attrid).val();
            }
            $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({
                region: "es-CO",
                decimalSymbol: ',',
                digitGroupSymbol: '.'
            });
            total = 0;
            $(".valor-costo").each(function () {
                var textnumber = $(this).children("p").text();
                total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
            });
            $("#total").children("p").html(total).formatCurrency({ region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
        });
    }).on("ifUnchecked", function () {
        $(".item-poll-promotion").removeClass("active");
        $(".valor-costo").each(function () {
            $(this).children("p").html(0).formatCurrency({
                region: "es-CO",
                decimalSymbol: ',',
                digitGroupSymbol: '.'
            });
        });
        $("#total").children("p").html(0).formatCurrency({region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
//        $(".check-active-promotion").val(2);
        // alert(event.type + " callback");
    });

    //    $(".item-poll-box-text").on("click", function () {
    //        $(this).next(".item-poll-box-edit").addClass("active");
    //    });


    // Dates


    if ($(".section-id").hasClass("section-plan") || $(".section-id").hasClass("section-encuesta")) {
        $("#date-poll-content").dateRangePicker({
            alwaysOpen: true,
            container: ".item-poll-date-content",
            format: "DD MMMM",
            language: "es",
            inline: true,
            separator: "",
            // shortcuts: null,
            showShortcuts: false,
            startOfWeek: "monday",
            stickyMonths: true
        });
    }


    if ($(".section-id").hasClass("section-plan")) {
        $("#date-poll-promotion").dateRangePicker({
            alwaysOpen: true,
            container: ".item-poll-date-promotion",
            format: "DD MMMM",
            language: "es",
            inline: true,
            separator: "",
            // shortcuts: null,
            showShortcuts: false,
            startOfWeek: "monday",
            stickyMonths: true
        });
    }


    // Add videos content
    btnVideoOne.click(function () {
        $('.input-video').removeClass("error");
        if($('.input-video').val()!="")
        {
            if(EsYoutube($('.input-video').val()))
            {
                $(this).siblings(".row-videos").find(".list-videos").append("<li onclick='this.parentNode.removeChild(this);' class='see-video'>Youtube - " + videoOne.val() + "<input name='CampaniaForm[videos][]' type='hidden' class='input-hidden' value='" + videoOne.val() + "' /></li>");
                videoOne.val("");
            }
            else
            {
                $('.input-video').addClass("error");
            }
        }
        else
        {
            $('.input-video').addClass("error");
        }
    });

    

    // Add videos promotion
    btnVideoTwo.click(function () {
        $(this).siblings(".row-videos").find(".list-videos").append("<li onclick='this.parentNode.removeChild(this);' class='see-video'>Youtube - " + videoTwo.val() + "<input name='CampaniaForm[videos2][]' type='hidden' class='input-hidden' value='" + videoTwo.val() + "' /></li>");
        videoTwo.val("");
    });


    $('#CampaniaForm_imagen_tamano').change( function () {
        if($(".check-active-content").is(":checked")){            
            $(".item-poll-content").find(".numeros_costos").each(function () {
            var
                counter = $(this).val(),
                attrid = $(this).attr('id'),
                calc_costo_unformat,
                id_cost,
                cost_img,
                total;

                    id_cost = $("#CampaniaForm_imagen_tamano").find(":selected").val();
                    if (id_cost) {
                        cost_img = id_cost.split("+");
                        calc_costo_unformat = counter * $("." + attrid).val() * cost_img[1];
                    } else {
                        calc_costo_unformat = counter * $("." + attrid).val();
                    }
                $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({
                    region: "es-CO",
                    decimalSymbol: ',',
                    digitGroupSymbol: '.'
                });
                total = 0;
                $(".valor-costo").each(function () {
                    var textnumber = $(this).children("p").text();
                    total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
                });
                $("#total").children("p").html(total).formatCurrency({ region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});             
            });
        }
    });
    
    $('#CampaniaForm_imagen_tamano2').change( function () {
        if($(".check-active-promotion").is(":checked")){            
            $(".item-poll-promotion").find(".numeros_costos").each(function () {
            var
                counter = $(this).val(),
                attrid = $(this).attr('id'),
                calc_costo_unformat,
                id_cost,
                cost_img,
                total;

                    id_cost = $("#CampaniaForm_imagen_tamano2").find(":selected").val();
                    if (id_cost) {
                        cost_img = id_cost.split("+");
                        calc_costo_unformat = counter * $("." + attrid).val() * cost_img[1];
                    } else {
                        calc_costo_unformat = counter * $("." + attrid).val();
                    }
                $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({
                    region: "es-CO",
                    decimalSymbol: ',',
                    digitGroupSymbol: '.'
                });
                total = 0;
                $(".valor-costo").each(function () {
                    var textnumber = $(this).children("p").text();
                    total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
                });
                $("#total").children("p").html(total).formatCurrency({ region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});             
            });
        }
    });    

    $(document).on("click", ".btn-count-up", function () {
        var
            counter = +$(this).siblings(".counter-number").find(".number").text() + 1,
            attrid,
            tipo,
            id_cost2,
            cost_img2,
            calc_costo_unformat,
            id_cost,
            cost_img,
            total;


        $(this).siblings(".counter-number").find(".number").html(counter);
        //START: Ricardo Carmona
        $(this).siblings(".numeros_costos").val(counter);
        attrid = $(this).siblings(".numeros_costos").attr('id');

        tipo = $(".tipoCamp").val();
        if (parseInt(tipo, 0) === 1 || parseInt(tipo, 0) === 4) {
            id_cost2 = $("#CampaniaForm_imagen_tamano2").find(":selected").val();
            if (id_cost2) {
                cost_img2 = id_cost2.split("+");
                calc_costo_unformat = counter * $("." + attrid).val() * cost_img2[1];
            } else {
                calc_costo_unformat = counter * $("." + attrid).val();
            }
        } else if (parseInt(tipo, 0) === 2 || parseInt(tipo, 0) === 5) {
            id_cost = $("#CampaniaForm_imagen_tamano").find(":selected").val();
            if (id_cost) {
                cost_img = id_cost.split("+");
                calc_costo_unformat = counter * $("." + attrid).val() * cost_img[1];
            } else {
                calc_costo_unformat = counter * $("." + attrid).val();
            }
        } else if (parseInt(tipo, 0) === 3) {
            calc_costo_unformat = counter * $("." + attrid).val();
        }
//        var calc_costo = calc_costo_unformat.toString().replace(/(\d)(?=(\d\d\d)+$)/g, "$1,");
        $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({
            region: "es-CO",
            decimalSymbol: ',',
            digitGroupSymbol: '.'
        });
        total = 0;
        $(".valor-costo").each(function () {
            var textnumber = $(this).children("p").text();
            total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
        });
        $("#total").children("p").html(total).formatCurrency({ region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
        //FIN: Ricardo Carmona
        // $(this).siblings(".counter-number").find(".number").stop().animate({top: "-100%"}, 200, function () {
        //  $(this).css({opacity: 0, top: "100%"}).stop().animate({opacity: 1, top: 0}, 50, function () {
        //    $(this).html(counter);
        //    });
        // });
    });


    $(document).on("click", ".btn-count-down", function () {
        var
            counter = +$(this).siblings(".counter-number").find(".number").text() - 1,
            attrid,
            tipo,
            id_cost2,
            cost_img2,
            calc_costo_unformat,
            id_cost,
            cost_img,
            total;

        if (counter >= 0) {
            $(this).siblings(".counter-number").find(".number").html(counter);
            //START: Ricardo Carmona
            $(this).siblings(".numeros_costos").val(counter);
            attrid = $(this).siblings(".numeros_costos").attr('id');

            tipo = $(".tipoCamp").val();
            if (parseInt(tipo, 0) === 1 || parseInt(tipo, 0) === 4) {
                id_cost2 = $("#CampaniaForm_imagen_tamano2").find(":selected").val();
                if (id_cost2) {
                    cost_img2 = id_cost2.split("+");
                    calc_costo_unformat = counter * $("." + attrid).val() * cost_img2[1];
                } else {
                    calc_costo_unformat = counter * $("." + attrid).val();
                }
            } else if (parseInt(tipo, 0) === 2 || parseInt(tipo, 0) === 5) {
                id_cost = $("#CampaniaForm_imagen_tamano").find(":selected").val();
                if (id_cost) {
                    cost_img = id_cost.split("+");
                    calc_costo_unformat = counter * $("." + attrid).val() * cost_img[1];
                } else {
                    calc_costo_unformat = counter * $("." + attrid).val();
                }
            } else if (parseInt(tipo, 0) === 3) {
                calc_costo_unformat = counter * $("." + attrid).val();
            }
//            var calc_costo = calc_costo_unformat.toString().replace(/(\d)(?=(\d\d\d)+$)/g, "$1,");
            $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({
                region: "es-CO",
                decimalSymbol: ',',
                digitGroupSymbol: '.'
            });
            total = 0;
            $(".valor-costo").each(function () {
                var textnumber = $(this).children("p").text();
                total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
            });
            $("#total").children("p").html(total).formatCurrency({region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
            //FIN: Ricardo Carmona
        }
        // if (counter >= 1) {
        //  $(this).siblings(".counter-number").find(".number").stop().animate({top: "100%"}, 200, function () {
        //    $(this).css({opacity: 0, top: "-100%"}).stop().animate({opacity: 1, top: 0}, 50, function () {
        //      $(this).html(counter);
        //    });
        //  });
        // }
    });


    $('.numeros_costos').on('keyup', function () {
        var
            number = $(this).val(),
            attrid,
            tipo,
            id_cost2,
            cost_img2,
            calc_costo_unformat,
            id_cost,
            cost_img,
            total;

        $(this).siblings(".counter-number").find(".number").html(number);
        attrid = $(this).attr('id');

        tipo = $(".tipoCamp").val();
        if (parseInt(tipo, 0) === 1 || parseInt(tipo, 0) === 4) {
            id_cost2 = $("#CampaniaForm_imagen_tamano2").find(":selected").val();
            if (id_cost2) {
                cost_img2 = id_cost2.split("+");
                calc_costo_unformat = number * $("." + attrid).val() * cost_img2[1];
            } else {
                calc_costo_unformat = number * $("." + attrid).val();
            }
        } else if (parseInt(tipo, 0) === 2 || parseInt(tipo, 0) === 5) {
            id_cost = $("#CampaniaForm_imagen_tamano").find(":selected").val();
            if (id_cost) {
                cost_img = id_cost.split("+");
                calc_costo_unformat = number * $("." + attrid).val() * cost_img[1];
            } else {
                calc_costo_unformat = number * $("." + attrid).val();
            }
        } else if (parseInt(tipo, 0) === 3) {
            calc_costo_unformat = number * $("." + attrid).val();
        }
//        var calc_costo = calc_costo_unformat.toString().replace(/(\d)(?=(\d\d\d)+$)/g, "$1,");
        $("#elemento_costo").find("." + attrid + " p").html(calc_costo_unformat).formatCurrency({region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
        total = 0;
        $(".valor-costo").each(function () {
            var textnumber = $(this).children("p").text();
            total += parseFloat(textnumber.replace(/[^0-9\,]+/g, ""));
        });
        $("#total").children("p").html(total).formatCurrency({region: "es-CO", decimalSymbol: ',', digitGroupSymbol: '.'});
    });


    // $(".go-to").on("click", function () {
    //     tMt($("html, body"), 1.2, {scrollTop: $($(this).attr("data-rel")).offset().top, ease: Cubic.easeOut});
    // });

    if ($(".main-section").data("section") === "points") {
        $(".nav1").addClass("active");
    }


    if ($(".main-section").data("section") === "map") {
        $(".nav3").addClass("active");
        $(".top-search").addClass("visible");
        $(document).on("click", ".map-filter", function () {
            var wrapper = document.querySelector(".categories-filter");
            toggleClass(wrapper, "active");
            // $(this).text(function (i, tx) {
            $(this).text(function (tx) {
                return tx === "Filtro" ? "Todos" : "Filtro";
            });
        });
    }


    if ($(".main-section").data("section") === "marks") {
        $(".nav4").addClass("active");
        $(".top-search").addClass("visible");
        //$(".top-search").addClass("top-search-begin visible");
    }


    if ($(".main-section").data("section") === "form-mark") {
        $(".nav5").addClass("active");
    }


    if ($(".main-section").data("section") === "mark-admin") {
        $(".nav6").addClass("active");
    }


    if ($(".main-section").data("section") === "create") {
        $(".nav7").addClass("active");
    }

    $(".perfil-info-close").on("click", function () {
        $(this).parent().remove();
    });

    $(".btn-add-fburl").on("click", function () {
        var fburl = $(".input-fburl").val();
        if (fburl !== "") {
          $(".row-fburl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11'><p>" + fburl + "</p><input class='input' name='MarcaForm[pagina_facebook][]' type='hidden' value='" + fburl + "' /></div><div class='col-xs-1'><button class='btn btn-remove-fburl text-center' type='button'><span></span></button></div></div><span class='br-fburl'></span></div>");
          $(".input-fburl").val("");
          $(".input-fburl").removeClass("error");
          $(".input-fburl").siblings(".errorMessage").remove();
        } else {
          $(".input-fburl").addClass("error");
        }
    });


    $(".btn-add-encuestaurl").on("click", function () {
        // AGREGAR RESPUESTA CAROL
        var encuestaurl = $(".input-encuestaurl").val();
        $(".row-encuestaurl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11'><p>" + encuestaurl + "</p><input class='input' name='encuesta[]' type='hidden' value='" + encuestaurl + "' /></div><div class='col-xs-1'><button class='btn btn-remove-encuestaurl text-center' type='button'><span></span></button></div></div><span class='br-encuestaurl'></span></div>");
        $(".input-encuestaurl").val("");
        enumerarEncuestas();
        
    });


    if ($(".input-fburl").siblings("div").hasClass("errorMessage")) {
        $(".input-fburl").addClass("error");
    }


    // $("#my-points").circliful();


    $(".custom-check").iCheck({
        checkboxClass: "icheckbox_square-blue",
        radioClass: "iradio_square-blue",
        increaseArea: "20%"
    });


    $(".see-poll-info").on("click", function () {
        $(this).hide();
        $(".row-steps").addClass("active");
    });



    $('#MarcaForm_category option:nth-child(1)').attr('disabled', 'disabled');
    $('#MarcaForm_moments option:nth-child(1)').attr('disabled', 'disabled');



    $('.fondo-modal , .caja_mod_marca img').click(function () {
        $('.caja_mod_marca').slideToggle(0);
        $('.fondo-modal').slideToggle(0);
    });


    /* Modal directorio de marcas */

    $('.fondo-modal').click(function () {
        $('.modal-email').slideToggle(0);
    });

    $("form").keypress(function (e) {
        if (e.which === 13) {
            return false;
        }
    });



    $(".wepiku-item .item a[id*='open-modal']").addClass("mark-modal");


  /*Add*/
    if ($(".footer-ahorranito").size() > 0) {
        $(".footer-ahorranito").ahorranito();
    }
});




$(function () {
    "use strict";
    $("#list-abc").listnav();
    // $(".filter-list a").click(function(e) {
    //    e.preventDefault();
    // });

    $(".filter-list .mark-item .logo").siblings("a").removeAttr("href");

    $(".filter-list .mark-item .logo").siblings("a").click(function (e) {
        e.preventDefault();
    });
});


$(document).on("click", ".see-video", function () {
    "use strict";
    $(this).hide();
});


//$(document).on("click", ".mark-item .state", function () {
//    "use strict";
//    $(this).toggleClass("favorite");
//});


$(document).on("click", ".btn-year", function () {
    "use strict";
    $(".btn-year").removeClass("active");
    $(this).addClass("active");
});


$(document).on("click", ".btn-remove", function () {
    "use strict";
    var grandparentid = $(this).parents().eq(1).attr('id');
    $(this).parents().eq(1).siblings("#" + grandparentid).children(".input-eliminado").val(1);
    $(this).parents().eq(1).remove();
});


$(document).on("click", ".btn-remove-fburl", function () {
    "use strict";
    $(this).parents().eq(2).remove();
});

$(document).on("click", ".btn-remove-encuestaurl", function () {
    "use strict";
    $(this).parents().eq(2).remove();
    enumerarEncuestas();
});


$("[data-call='main-slider']").each(function () {
    "use strict";
    $(this).bxSlider();
});


// $(".modal-item").each(function () {
//     var bullets = $(this).find(".item");
//
//     console.log(modals);
//
//     if (bullets < 2) {
//         console.log("menor");
//         $(this).find(".carousel-indicators").hide();
//     }
//
// });

//$(document).ready(function(){
//    $("#tipo-pre").change(function(){
//        $('.descrip_encu , .itmes_encu').removeAttr('style');
//        var valor=$("#tipo-pre").val();
//        var texto=$("#tipo-pre option:selected").text();
//        //alert(valor+" - "+texto);
//        if(valor == 1){
//            $('.descrip_encu').css({'display':'block'});
//        } else {
//            $('.itmes_encu').css({'display':'block'});
//        }
//    });
//});
//Inicio: Ricardo Carmona

// this will open the Modal with the given id
function openModal(id, content) {
    "use strict";
    $(id + " .modal-content").html(content);
    $(id).modal("show");
}



function setTipoCampania(tipo) {
    "use strict";
    $(".form-tabs .tipoCamp").val(tipo);
}

window.twttr = (function (d, s, id) {
    "use strict";
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function (f) {t._e.push(f)}});

}(document, "script", "twitter-wjs"));

$(document).on("click", ".close-question", function () {
    "use strict";
    $(this).parents().eq(1).remove();
});

function addAnswer(button) {
    
    "use strict";
        
        $(button).parent().siblings(".input-respuesta").children(".input-encuestaurl").removeClass('error');
        var
        num_preguntas = $("#num_preguntas_multiple").val(),
        encuestaurl = $(button).parent().siblings(".input-respuesta").children(".input-encuestaurl").val(),
        respuestacorrecta = $(button).parent().siblings(".check-correcta").children(".input-encuestaurl-correcta"),
        correcta = 0;

    if(encuestaurl)
    {
        if (respuestacorrecta.is(":checked")) {
            correcta = 1;
            $(button).parent().siblings(".row-encuestaurl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11 col-sm-9'><p>" + encuestaurl + "</p><input id='EncuestaForm_respuesta_enc_multiple' class='input' name='EncuestaForm[respuesta_enc_multiple][" + num_preguntas + "][]' type='hidden' value='" + encuestaurl + "' /></div><div class='col-xs-11 col-sm-2'><img src='/wepiku_web/dist/themes/front/img/check.png' alt='' width='24px' height='24px'></img><input id='EncuestaForm_respuesta_enc_multiple_correcta' name='EncuestaForm[respuesta_enc_multiple_correcta][" + num_preguntas + "][]' type='hidden' value='" + correcta + "'></div><div class='col-xs-1'><button class='btn btn-remove-encuestaurl text-center' type='button'><span></span></button></div></div><span class='br-encuestaurl'></span></div>");
        } else {
            $(button).parent().siblings(".row-encuestaurl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11 col-sm-9'><p>" + encuestaurl + "</p><input id='EncuestaForm_respuesta_enc_multiple' class='input' name='EncuestaForm[respuesta_enc_multiple][" + num_preguntas + "][]' type='hidden' value='" + encuestaurl + "' /></div><div class='col-xs-11 col-sm-2'><input id='EncuestaForm_respuesta_enc_multiple_correcta' name='EncuestaForm[respuesta_enc_multiple_correcta][" + num_preguntas + "][]' type='hidden' value='" + correcta + "'></div><div class='col-xs-1'><button class='btn btn-remove-encuestaurl text-center' type='button'><span></span></button></div></div><span class='br-encuestaurl'></span></div>");
        }
        $(".input-encuestaurl").val("");
        $(".input-encuestaurl-correcta").attr("checked", false);
    }
    else
    {
        $(button).parent().siblings(".input-respuesta").children(".input-encuestaurl").addClass('error');
    }
    enumerarEncuestas();
}

function addAnswerUnic(button) {
    "use strict";
        var
        num_preguntas = $("#num_preguntas_multiple_unic").val(),
        encuestaurl = $(button).parent().siblings(".input-respuesta").children(".input-encuestaurl").val(),
        respuestacorrecta = $(button).parent().siblings(".check-correcta").children(".input-encuestaurl-correcta"),
        correcta = 0;

    $(button).parent().siblings(".input-respuesta").children(".input-encuestaurl").removeClass("error");
    
    if(encuestaurl != "")
    {    
        if (respuestacorrecta.is(":checked")) {
            correcta = 1;
            $(button).parent().siblings(".row-encuestaurl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11 col-sm-9'><p>" + encuestaurl + "</p><input id='EncuestaForm_respuesta_enc_multiple_unic' class='input' name='EncuestaForm[respuesta_enc_multiple_unic][" + num_preguntas + "][]' type='hidden' value='" + encuestaurl + "' /></div><div class='col-xs-11 col-sm-2'><img src='/wepiku_web/dist/themes/front/img/check.png' alt='' width='24px' height='24px'></img><input id='EncuestaForm_respuesta_enc_multiple_unic_correcta' name='EncuestaForm[respuesta_enc_multiple_unic_correcta][" + num_preguntas + "][]' type='hidden' value='" + correcta + "'></div><div class='col-xs-1'><button class='btn btn-remove-encuestaurl text-center' type='button'><span></span></button></div></div><span class='br-encuestaurl'></span></div>");
        } else {
            $(button).parent().siblings(".row-encuestaurl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11 col-sm-9'><p>" + encuestaurl + "</p><input id='EncuestaForm_respuesta_enc_multiple_unic' class='input' name='EncuestaForm[respuesta_enc_multiple_unic][" + num_preguntas + "][]' type='hidden' value='" + encuestaurl + "' /></div><div class='col-xs-11 col-sm-2'><input id='EncuestaForm_respuesta_enc_multiple_unic_correcta' name='EncuestaForm[respuesta_enc_multiple_unic_correcta][" + num_preguntas + "][]' type='hidden' value='" + correcta + "'></div><div class='col-xs-1'><button class='btn btn-remove-encuestaurl text-center' type='button'><span></span></button></div></div><span class='br-encuestaurl'></span></div>");
        }
        $(".input-encuestaurl").val("");
        $(".input-encuestaurl-correcta").attr("checked", false);
    }
    else
    {
        $(button).parent().siblings(".input-respuesta").children(".input-encuestaurl").addClass('error');
    }
    
    enumerarEncuestas();
}

function getCantidadPreguntas(tipoPreg) {
    "use strict";
    var
        cantidad_preguntas = 0,
        cant_abiertas = 0,
        cant_multi_unic = 0,
        cant_multi = 0;

    if (parseInt(tipoPreg) === 1) {
        cant_abiertas = parseInt($("#num_preguntas_abierta").val(), 0) + 1;
        $("#num_preguntas_abierta").val(cant_abiertas);
        cantidad_preguntas = $("#num_preguntas_abierta").val();
    } else if (parseInt(tipoPreg) === 2) {
        cant_multi_unic = parseInt($("#num_preguntas_multiple_unic").val(), 0) + 1;
        $("#num_preguntas_multiple_unic").val(cant_multi_unic);
        cantidad_preguntas = $("#num_preguntas_multiple_unic").val();
    } else if (parseInt(tipoPreg) === 3) {
        cant_multi = parseInt($("#num_preguntas_multiple").val(), 0) + 1;
        $("#num_preguntas_multiple").val(cant_multi);
        cantidad_preguntas = $("#num_preguntas_multiple").val();
    }
    return cantidad_preguntas;
}
//Fin: Ricardo Carmona


$(".modal-item").on("shown.bs.modal", function () {
    "use strict";
    
    //$(this).children(".item").children(".mivideo").attr('src','');
    
    $(this).find(".carousel-inner").each(function () {
        
        var slideHeight = $(this).first("item").innerHeight();
        $(this).css({height: slideHeight, maxHeight: slideHeight, minHeight: slideHeight});
        $(this).children(".item").children("#player").css({height: slideHeight, maxHeight: slideHeight, minHeight: slideHeight});
        // $(this).children(".item").children("iframe").css({height: slideHeight, maxHeight: slideHeight, minHeight: slideHeight});
        if ($(this).children("div").length === 1) {
            $(this).siblings(".carousel-control, .carousel-indicators").hide();
        }
    });
});


$(document).on("click", ".close-pro", function () {
    //tgVideo("pause");
    $('.miyoutube').empty();
});

$(".carousel-indicators li").live("click", function () {
    $('.miyoutube').empty();
    indice = $(this).attr('data-slide-to');
    var elemento = $(this);
    $('.miyoutube').empty();
    $.each(key, function(num, valor){
        if(num == indice)
        {
            if(valor == 1);
            {
                $('.miyoutube').html("<iframe class='mivideo' width='524' height='245' src='https://www.youtube.com/embed/"+ urlvideo[num] + "?autoplay=1&controls=0&autohide=1&showinfo=0&iv_load_policy=3&modestbranding=1' frameborder='0'></iframe>");
            }
        }
    })
    
    
});

$(document).live("click", ".carousel-indicators li", function () {
    //tgVideo("pause");
});

function EsYoutube(url)
{ 
    var res = RegExp(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/).test(url); 
    if(res)
        return true;
    else
        return false;
}

function EsFreemium(valor)
{
    if(valor )
    {
        $("#CampaniaForm_imagen_tamano").children().removeAttr("disabled");
    }
    else
    {
        $("#CampaniaForm_imagen_tamano").children().attr("disabled", "disabled");
        $("#CampaniaForm_imagen_tamano option:nth-child(1)").removeAttr('disabled');
        $("#CampaniaForm_imagen_tamano option:nth-child(2)").removeAttr('disabled');
    }
}

function enumerarEncuestas()
{
    $('.item_pregunta').each(function(w){
        $('.col-sm-9', this).each(function(i){
            $(this).children('p').children('span').remove();
            $(this).children('p').prepend('<span>'+ i + '. </span>');
        });
    });
}