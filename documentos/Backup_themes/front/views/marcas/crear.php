<?php

echo $this->renderPartial('_form', array(
    'categorias' => $categorias,
    'momentos_consumo' => $momentos_consumo,
    'amigos' => $amigos,
    'form' => $form,
    'mensaje' => $mensaje,
    'hasDataform' => $hasDataform,
    'marca_id' => $marca_id, 
    'imagencover' => $imagencover,
    'imagenperfil' => $imagenperfil
));
