
	 <?php foreach ($marcas_seguidas as $marca): ?>
        <!-- Item -->
            <?php if($marca['is_checked'] == 1): ?>
                <li class="col-xs-2">
                        <div class="mark-item">
                                <span class="mark-name"><?php echo $marca['nombre']; ?></span>           
                                <?php $image_marca = CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class'=>'img-circle', "width"=>"110px" ,"height"=>"110px")); ?>                                
                                
                                <?php if (Yii::app()->session['is_new'] == false): ?>
                                    <?php echo CHtml::link(
                                            $image_marca, 
                                            $this->createUrl('marcas/marca', array('m' => $marca['id'])),
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>
                                <?php else: ?>
                                    <?php echo CHtml::link(
                                            $image_marca, 
                                            "#",
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>   
                                <?php endif; ?>
                                <?php
                                    echo CHtml::link(
                                        '<div class="state favorite"><span></span></div>', 
                                        '#',
                                        array(
                                            'id'=>'send-link2-'.uniqid(),
                                            'class' => 'marca-'.$marca['id'] ,
//                                            'return' => false, // this is already false by default - read documentation
                                            'ajax' => array( 
                                                'url' => $this->createUrl('marcas/setMarcaLikeBackend'),
                                                'dataType' => 'json', 
                                                'type' => 'POST',
                                                'data' => array(
                                                    'mid' => $marca['id'],
                                                    'l'   => 'js:$(".filter-list .mark-item .marca-'.$marca['id'].'").find("div").hasClass("favorite")'
                                                ),                                                
                                                'success'=>'function(){
                                                    var divheart = $(".filter-list .mark-item .marca-'.$marca['id'].'").find("div");
                                                    if (!divheart.hasClass("favorite")) {
                                                        divheart.addClass("favorite");
                                                    } else {
                                                        divheart.removeClass("favorite");
                                                    }                                                    
                                                }'
                                            )
                                        ));                                
                                ?>                                
                        </div>
                        <!--</a>-->
                </li>
            <?php endif; ?>
        <?php endforeach; ?>   
        <?php foreach ($marcas_seguidas as $marca): ?>                    
            <?php if ($marca['is_suggested'] == 1 and $marca['is_checked'] == 0): ?>
                <li class="col-xs-2">
                        <div class="mark-item">
                                <span class="mark-name"><?php echo $marca['nombre']; ?></span>
                                <?php $image_marca_sug = CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class'=>'img-circle', "width"=>"110px" ,"height"=>"110px")); ?>
                                
                                <?php if (Yii::app()->session['is_new'] == false): ?>
                                    <?php echo CHtml::link(
                                            $image_marca_sug, 
                                            $this->createUrl('marcas/marca', array('m' => $marca['id'])),
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>
                                <?php else: ?>
                                    <?php echo CHtml::link(
                                            $image_marca_sug, 
                                            "#",
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>   
                                <?php endif; ?>                                
                                <?php
                                    echo CHtml::link(
                                        '<div class="state"><span></span></div>', 
                                        '#',
                                        array(
                                            'id'=>'send-link2-'.uniqid(),
                                            'class' => 'marca-'.$marca['id'] ,
//                                            'return' => false, // this is already false by default - read documentation
                                            'ajax' => array( 
                                                'url' => $this->createUrl('marcas/setMarcaLikeBackend'),
                                                'dataType' => 'json', 
                                                'type' => 'POST',
                                                'data' => array(
                                                    'mid' => $marca['id'],
                                                    'l'   => 'js:$(".filter-list .mark-item .marca-'.$marca['id'].'").find("div").hasClass("favorite")'
                                                ),                                                
                                                'success'=>'function(){
                                                    var divheart = $(".filter-list .mark-item .marca-'.$marca['id'].'").find("div");
                                                    if (!divheart.hasClass("favorite")) {
                                                        divheart.addClass("favorite");
                                                    } else {
                                                        divheart.removeClass("favorite");
                                                    }                                                    
                                                }'
                                            )
                                        ));                                
                                ?>  
                        </div>
                        <!--</a>-->
                </li>
            <?php endif; ?>
        <?php endforeach; ?> 
        <?php foreach ($marcas_seguidas as $marca): ?>                                 
            <?php if($marca['is_suggested'] == 0 and $marca['is_checked'] == 0): ?>
                <li class="col-xs-2">
                        <div class="mark-item">
                                <span class="mark-name"><?php echo $marca['nombre']; ?></span>
                                <?php $image_marca_no_check = CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class'=>'img-circle', "width"=>"110px" ,"height"=>"110px")); ?>
                                
                                <?php if (Yii::app()->session['is_new'] == false): ?>
                                    <?php echo CHtml::link(
                                            $image_marca_no_check, 
                                            $this->createUrl('marcas/marca', array('m' => $marca['id'])),
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>
                                <?php else: ?>
                                    <?php echo CHtml::link(
                                            $image_marca_no_check, 
                                            "#",
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>   
                                <?php endif; ?>                                   
                                <?php
                                    echo CHtml::link(
                                        '<div class="state"><span></span></div>', 
                                        '#',
                                        array(
                                            'id'=>'send-link2-'.uniqid(),
                                            'class' => 'marca-'.$marca['id'] ,
//                                            'return' => false, // this is already false by default - read documentation
                                            'ajax' => array( 
                                                'url' => $this->createUrl('marcas/setMarcaLikeBackend'),
                                                'dataType' => 'json', 
                                                'type' => 'POST',
                                                'data' => array(
                                                    'mid' => $marca['id'],
                                                    'l'   => 'js:$(".filter-list .mark-item .marca-'.$marca['id'].'").find("div").hasClass("favorite")'
                                                ),                                                
                                                'success'=>'function(){
                                                    var divheart = $(".filter-list .mark-item .marca-'.$marca['id'].'").find("div");
                                                    if (!divheart.hasClass("favorite")) {
                                                        divheart.addClass("favorite");
                                                    } else {
                                                        divheart.removeClass("favorite");
                                                    }                                                    
                                                }'
                                            )
                                        ));                                
                                ?>  
                        </div>
                        <!--</a>-->
                </li>                 
            <?php endif; ?>
        <?php endforeach; ?>                 
        <!-- Item -->

