<!--					<section>-->


<div class="container-fluid brand-page-head main-section section-id" data-section="mark-admin">
   <img src="<?php echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover']; ?>" alt="">
</div>
<div class="brand-head-fx"></div>


<div class="col-xs-12 col-sm-7 col-grid col-main grid-section brand-section">

    <div class="mg-brand-logo text-center">
        <div class="table">
            <div class="table-cell">
                <div class="brand-page-logo">
                    <img class="img-circle" src="<?php echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil']; ?>">
                    <span class="brand-like active">
                        <div class="like-icon"></div>
                    </span>
                </div>
                <h1><?php echo $data_marca_json['nombre'];?></h1>
            </div>
        </div>
    </div>


    <div class="brand-info">
        <h6 class="text-center">
            <?php echo $data_marca_json['descripcion'];?>
        </h6>
        <div class="row row-table row-brand-links text-center">
            <div class="col-sm-4 col-cell">
                <?php echo CHtml::link(
                        '<p class="tab-text text-left active">Campañas activas</p>', 
                        "#",
                        array(
                            'submit'=>  $this->createUrl('marcas/marcaAdminActivas'),
                            'params'=>array('m' => $data_marca_json['id']),
                            'id' => 'tab-camp-activa-'.uniqid()
                        )
                    ); ?>                     
            </div>
            <div class="col-sm-4 col-cell">
                <?php echo CHtml::link(
                        '<p class="tab-text text-left">Campañas pausadas</p>', 
                        "#",
                        array(
                            'submit'=>  $this->createUrl('marcas/marcaAdminPausadas'),
                            'params'=>array('m' => $data_marca_json['id']),
                            'id' => 'tab-camp-pausada-'.uniqid()
                        )
                    ); ?>                   
            </div>
            <div class="col-sm-4 col-cell">
                <?php echo CHtml::link(
                        '<p class="tab-text text-left">Campañas terminadas</p>', 
                        "#",
                        array(
                            'submit'=>  $this->createUrl('marcas/marcaAdminTerminadas'),
                            'params'=>array('m' => $data_marca_json['id']),
                            'id' => 'tab-camp-terminada-'.uniqid()
                        )
                    ); ?>                  
            </div>
        </div>
    </div>


    <div class="wepiku-grid">
        <?php 
        $con = 1;
        foreach ($data_hotdeals_json as $campania): ?>
            <?php if ($campania['tipo_id'] == 3): ?>
                <!-- Item -->
                <div class="wepiku-item">
                    <div class="item item-type3" data-toggle="modal" data-target="#modal-poll-list">
                        <div class="item-type3-bg" style="background-image: url(<?php echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover']; ?>);">
                            <?php $img_encuesta = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover'], "",array("style"=>"margin-bottom: -135px;height: 134px;position: relative;z-index: 9;opacity: 0;")); ?>
                        </div>
                        
                        <?php                                
                            // ajax button to open the modal
                            echo TbHtml::ajaxLink(
                                $img_encuesta,
                                $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                array( 
                                    'dataType' => 'json', 
                                    'type' => 'POST',
                                    'data' => array(
                                        'camp' => $campania['id'],
                                        'modal' => '#modal-poll-list',
                                        'baradmin' => 1,
                                        'estado' => 'A'                                        
                                    ),
                                    'success' => 'function(data){
                                        openModal("#modal-poll-list", data.content);
                                    }'
                                ),
                                array(
                                    'id' => 'open-modal-'.uniqid()
                                )
                            );                                
                        ?>                        
                        <div class="caption">
                            <p>
                            <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_pencil.png" alt=""></span>
                            </p>
                            <h2 class="clearfix">
                                <?php echo $campania['nombre']; ?>
                            </h2>
                            <div class="logo">
                                <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Item -->

            <?php elseif ($campania['tipo_id'] == 1 || $campania['tipo_id'] == 4): ?>
                <?php
                if ($campania['tipo_id'] == 1):
                    $modal = "#modal-product";
                elseif ($campania['tipo_id'] == 4):
                    $modal = "#modal-poll-product";
                endif;
                ?>
                <?php if ($campania['costo_multimedia_id'] == 1): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo1,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal,
                                            'baradmin' => 1,
                                            'estado' => 'A'
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>  
                        </div> 
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                                <p>
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                                </p>
                            </div>
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <?php $img_promo_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo2,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal,
                                            'baradmin' => 1,
                                            'estado' => 'A'
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                              
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">                                                                 
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                    <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>                                
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>   
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo3,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal,
                                            'baradmin' => 1,
                                            'estado' => 'A'
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>   
                        </div>
                    </div>
                    <!--/ Item -->
                <?php endif; ?>

            <?php elseif ($campania['tipo_id'] == 2 || $campania['tipo_id'] == 5): ?>
                <?php
                if ($campania['tipo_id'] == 2):
                    $modal2 = "#modal-product";
                elseif ($campania['tipo_id'] == 5):
                    $modal2 = "#modal-poll-product";
                endif;
                ?>
                <?php if ($campania['costo_multimedia_id'] == 1): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php 
                            echo "<script>
                                $(document).ready(function(){
                                    $('#cli".$con."').click(function(){
                                    $('#img_cli".$con."').trigger('click');
                                         });
                                 });                            
                                  </script>";
                          
                            $img_cont_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main ' , 'id' => 'img_cli'.$con)); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_cont_costo1,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2,
                                            'baradmin' => 1,
                                            'estado' => 'A'
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid()
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption" id="cli<?php echo $con; ?>">
                                <div class="table">
                                    <div class="table-cell">
                                        <div class="row row-table">
                                            <div class="col-xs-4 col-cell">
                                                <div class="logo">
                                                    <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                    <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-cell text-right">
                                                <h2 class="clearfix">
                                                    <?php echo $campania['nombre']; //'contenidomulti1'. ?>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php 
                            echo "<script>
                                $(document).ready(function(){
                                    $('#cli".$con."').click(function(){
                                    $('#img_cli".$con."').trigger('click');
                                         });
                                 });                            
                                  </script>";
                            $img_cont_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main', 'id' => 'img_cli'.$con)); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_cont_costo2,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2,
                                            'baradmin' => 1,
                                            'estado' => 'A'
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid()
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                              
                            <div class="caption" id="cli<?php echo $con; ?>">
                                <div class="table">
                                    <div class="table-cell">
                                        <div class="row row-table">
                                            <div class="col-xs-4 col-cell">
                                                <div class="logo">
                                                    <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                    <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-cell text-right">
                                                <h2 class="clearfix"><?php echo $campania['nombre']; //'contenidomulti2'. ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php 
                            echo "<script>
                                $(document).ready(function(){
                                    $('#cli".$con."').click(function(){
                                    $('#img_cli".$con."').trigger('click');
                                         });
                                 });                            
                                  </script>";
                            $img_cont_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main', 'id' => 'img_cli'.$con)); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_cont_costo3,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2,
                                            'baradmin' => 1,
                                            'estado' => 'A'
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid()
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                              
                            <div class="caption" id="cli<?php echo $con; ?>">
                                <div class="table">
                                    <div class="table-cell">
                                        <div class="row row-table">
                                            <div class="col-xs-4 col-cell">
                                                <div class="logo">
                                                    <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                    <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-cell text-right">
                                                <h2 class="clearfix">
                                                    <?php echo $campania['nombre']; //'contenidomulti3'. ?>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                            
                    </div>
                    <!--/ Item -->
                <?php endif; ?>
            <?php endif; ?>
                    
        <?php 
        $con++;
        endforeach; ?>
    </div>
    </div>
<!--</div>-->
<div class="col-xs-12 col-sm-2 col-sub-nav">
    <div class="sub-nav active subnav-hide">
        <button class="btn-sub-nav" type="button">
            <span class="btn-arrow"></span>
            <span class="btn-icon"></span>
        </button>
        <button class="btn-favorite"></button>
    </div>


    <div class="plan-cost card-nav" data-offset-top="9" data-offset-bottom="50">
        <div class="plan-cost-head">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-nav.png" alt="" />
            <h4 class="text-center">Saldo a favor para crear campañas.</h4>
        </div>
        <div class="plan-cost-total">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h6>SALDO TOTAL</h6>
                </div>
                <div class="col-xs-12 text-center">
                    <h4><?php echo '$'.$saldo;?></h4>
                </div>
            </div>
        </div>		
	<div class="clearfix">
            <?php echo CHtml::beginForm('nueva', 'post', array ('class' => 'grl-form', 'enctype' => 'multipart/form-data')); ?>   
                <?php echo CHtml::hiddenfield('mid', $marca_id); ?>         
                <?php
                 echo CHtml::submitButton('Editar', array(
                        'class'=>'edit-marc',
                        'type'=>'button',
                        'name'=>'button'
                     )
                 ); ?>
            <?php echo CHtml::endForm(); ?>
        </div>
    </div>


    <!--div class="year-nav">
        <ul>
            <li><button class="btn-year active">Reciente</button></li>
            <li><button class="btn-year">2015</button></li>
            <li><button class="btn-year">2014</button></li>
            <li><button class="btn-year">2013</button></li>
            <li><button class="btn-year">2012</button></li>
        </ul>
    </div-->


</div>


<!--					</section>-->
