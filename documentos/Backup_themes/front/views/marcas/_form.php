
<!--					<section>-->

<?php echo CHtml::beginForm('nueva', 'post', array ('class' => 'grl-form', 'enctype' => 'multipart/form-data')); ?>
    <div class="col-xs-12 col-sm-7 col-grid col-main section-id section-new-mark">
<!--            <div class="top-search clearfix">
                    <form action="#" class="top-form pull-left clearfix">
                            <button class="btn-search" type="button"></button>
                            <div id="auto-search">
                            <input class="typeahead" type="text">
                            </div>
                    </form>
                    <div class="dropdown pull-right">
                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                        <span class="sub-list-icon"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                      </ul>
                    </div>
            </div>-->


              <div class="info-grl info-form clearfix">
                <div class="row row-info-form">
                  <div class="col-xs-12">
                    <?php if(isset($marca_id) or isset($form->marcaid)): ?>
                        <h1>EDITAR MARCA</h1>
                    <?php else: ?>
                        <h1>CREAR MARCA</h1>
                    <?php endif; ?>
                  </div>
                  <br/>
                  <br/>
                  <br/>
                  <div class="col-xs-12">
                  <?php if($mensaje): ?>
                    <div class="modredes" style="display: block;"><?php echo $mensaje?></div>
                    <br/>
                    <br/>
                    <br/>
                  <?php endif; ?>
                    <?php // echo CHtml::errorSummary($form); ?>
                    <?php // $mid = null ?>
                        <?php $valmarca_id = isset($marca_id)?$marca_id:$form->marcaid; ?>
                        <?php echo CHtml::hiddenfield('MarcaForm[marcaid]', $valmarca_id); ?>
                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                            <?php echo CHtml::Label('* Nombre de la Marca', 'nombre_marca'); ?>
                            <?php echo CHtml::activeTextField($form, 'nombre_marca', array('class' => 'input')) ?>
                            <?php echo CHtml::error($form,'nombre_marca'); ?>
                        </fieldset>
                      </div>
                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                            <?php echo CHtml::Label('* Descripción de la marca', 'desc_marca') ?>
                            <?php echo CHtml::activeTextArea($form, 'desc_marca', array('class' => 'textarea')) ?>
                            <?php echo CHtml::error($form,'desc_marca'); ?>
                        </fieldset>
                      </div>

                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <?php echo CHtml::Label('* Categoría', 'category') ?>
                          <div class="row row-multi category-sele">
                          <?php echo CHtml::activeDropDownList($form, 'category', $categorias, array('class' => 'multi-select col-xs-4', 'empty'=>'Elija opción',  'onchange' => 'multiSelect1(this);')) ?>
                              <ul class="multi-list col-xs-8">
                                <?php if($hasDataform and isset($form->categoria)): ?>
                                    <?php foreach($form->categoria as $key => $dataCategory): ?>
                                        <li onclick="this.parentNode.removeChild(this);">
                                            <input type="hidden" name="MarcaForm[categoria][<?php echo $key ?>][id]" value="<?php echo $dataCategory['id'] ?>">
                                            <input type="hidden" name="MarcaForm[categoria][<?php echo $key ?>][nombre]" value="<?php echo $dataCategory['nombre'] ?>">
                                            <?php echo $dataCategory['nombre']; ?>
                                       </li>
                                    <?php endforeach; ?>
                               <?php endif; ?>
                            </ul>
                          </div>
                          <?php echo CHtml::error($form,'categoria'); ?>
                        </fieldset>
                      </div>
                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <?php echo CHtml::Label('* Momentos de consumo asociados', 'moments') ?>
                          <div class="row row-multi">
                            <?php echo CHtml::activeDropDownList($form, 'moments', $momentos_consumo, array('class' => 'multi-select col-xs-4', 'empty'=>'Elija opción', 'onchange' => 'multiSelect2(this);')) ?>
                            <ul class="multi-list col-xs-8">
                                <?php if($hasDataform and isset($form->mom_consumo)): ?>
                                    <?php foreach($form->mom_consumo as $key => $dataMoments): ?>
                                        <li onclick="this.parentNode.removeChild(this);">
                                            <input type="hidden" name="MarcaForm[mom_consumo][<?php echo $key ?>][id]" value="<?php echo $dataMoments['id'] ?>">
                                            <input type="hidden" name="MarcaForm[mom_consumo][<?php echo $key ?>][nombre]" value="<?php echo $dataMoments['nombre'] ?>">
                                            <?php echo $dataMoments['nombre']; ?>
                                       </li>
                                    <?php endforeach; ?>
                               <?php endif; ?>
                            </ul>
                          </div>
                          <?php echo CHtml::error($form,'mom_consumo'); ?>
                        </fieldset>
                      </div>
                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                            <?php echo CHtml::Label('Página web', 'pagina_web'); ?>
                            <?php echo CHtml::activeTextField($form, 'pagina_web', array('class' => 'input', 'placeholder' => 'http://www.website.com')) ?>
                            <?php echo CHtml::error($form,'pagina_web'); ?>
                        </fieldset>
                      </div>
                      <div class="row">
                        <fieldset class="col-xs-11 con-field">
                            <?php echo CHtml::Label('Página de Facebook', 'page_facebook'); ?>
                            <?php echo CHtml::textField('page_facebook',null, array('class' => 'input input-fburl', 'placeholder' => 'https://www.facebook.com/name')) ?>
                            <?php echo CHtml::error($form,'page_facebook'); ?>                            
                            <?php echo CHtml::error($form,'pagina_facebook'); ?>
                        </fieldset>
                        <fieldset class="col-xs-1 con-field con-field-add">
                          <label>&nbsp;</label>
                          <button class="btn btn-add-fburl text-center" type="button"><span></span></button>
                        </fieldset>
                      </div>
                    <div class="row row-fburl">
                        <?php if($hasDataform and isset($form->pagina_facebook)): ?>
                            <?php foreach($form->pagina_facebook as $dataFacebookPage): ?>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <p><?php echo $dataFacebookPage; ?></p>
                                            <input class="input" name="MarcaForm[pagina_facebook][]" type="hidden" value="<?php echo $dataFacebookPage; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                            <button class="btn btn-remove-fburl text-center" type="button"><span></span></button>
                                        </div>
                                    </div>
                                    <span class="br-fburl"></span>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
					<div class="styl-file">
						<div class="row">
                                                    <fieldset class="col-xs-8 con-field">
                                                        <?php echo CHtml::Label('* Foto de cover', 'foto_cover'); ?>
                                                        <?php echo CHtml::activeFileField($form, 'foto_cover', array('id' => 'file-cover')) ?> <!--'class' => 'file-loading'-->
                                                        <?php echo CHtml::error($form,'foto_cover'); ?>
                                                        <?php isset($imagencover)? $cover = Yii::app()->session['url_img'].$imagencover:$cover = null; ?>
                                                            <div style="float: left; margin-top: 10px; <?php echo (!$imagencover)?'display: none;':'' ?>" class="brand-page-logo-cover">
                                                                <?php echo CHtml::image($cover, "", array('id' => 'file-cover-img', 'width' => '100%', 'height' => '110')); ?>
                                                            </div>
                                                    </fieldset>
						</div>

						<div class="row">
                                                    <fieldset class="col-xs-8 con-field">
                                                        <?php echo CHtml::Label('* Foto de perfil', 'foto_perfil'); ?>
                                                        <?php echo CHtml::activeFileField($form, 'foto_perfil', array('id' => 'file-perfil')) ?>
                                                        <?php echo CHtml::error($form,'foto_perfil'); ?>
                                                        <?php isset($imagenperfil)? $perfil = Yii::app()->session['url_img'].$imagenperfil:$perfil = null; ?>
                                                            <div style="float: left; margin-top: 10px; <?php echo (!$imagenperfil)?'display: none;':'' ?>" class="brand-page-logo-perfil">
                                                                <?php echo CHtml::image($perfil, "", array('class' => 'img-circle', 'id' => 'file-perfil-img', 'width' => '110', 'height' => '110')); ?>
                                                            </div>
                                                    </fieldset>
						</div>
                    </div>

                    <div class="con-points">
                            <h4 class="points-title">AGREGAR SUCURSAL</h4>
                            <div class="row row-point">
                                    <fieldset class="col-xs-12 col-sm-6 con-field">
                                        <input class="input location" placeholder="* Ingresar nombre sucursal">
                                    </fieldset>
                                    <fieldset class="col-xs-12 col-sm-5 con-field">
                                            <input class="input position" value="Ubicación" readonly>
                                            <button type="button" class="btn btn-marker" data-toggle="modal" data-target="#modal-map-new"><span></span></button>
                                    </fieldset>
                                    <fieldset class="col-xs-12 col-sm-1 con-field con-field-add">
                                            <button type="button" class="btn btn-add-point"><span></span></button>
                                    </fieldset>
                                    <div class="clearfix"></div>
                                    <div class="info-location">
                                    <?php if(isset($valmarca_id)): ?>
                                        <?php if($hasDataform and isset($form->location) and isset($form->position)): ?>
                                            <?php foreach($form->location as $key => $dataLocation): ?>
                                                <?php $uniqueid = uniqid(); ?>
                                                <div id="<?php echo 'iden'.$uniqueid ?>">
                                                    <input name="MarcaForm[location][]" class="point-location" value="<?php echo $dataLocation ?>" type="hidden">
                                                    <input name="MarcaForm[position][]" class="point-position" value="<?php echo $form->position[$key] ?>" type="hidden">
                                                    <input name="MarcaForm[address][]" value="<?php echo $form->address[$key] ?>" type="hidden">
                                                    <input name="MarcaForm[delete][]" class="input-eliminado" value="0" type="hidden">
                                                </div>
                                                <div id="<?php echo 'iden'.$uniqueid ?>" class="row row-new-position">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <h5><?php echo $dataLocation ?></h5>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <h5 class="position-reg"><?php echo $form->position[$key] ?></h5>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-1">
                                                        <button class="btn btn-edit-position" type="button" data-toggle="modal" data-target="#modal-map-edit"></button>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-1">
                                                        <button class="btn btn-remove" type="button"></button>
                                                    </div>
                                                    <span class="row-br"></span>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if($hasDataform and isset($form->location) and isset($form->position)): ?>
                                            <?php foreach($form->location as $key => $dataLocation): ?>
                                                <div class="row row-new-position">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <h5><?php echo $dataLocation ?></h5>
                                                        <input name="MarcaForm[location][]" type="hidden" class="point-location" value="<?php echo $dataLocation ?>">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <h5 class="position-reg"><?php echo $form->position[$key] ?></h5>
                                                        <input name="MarcaForm[position][]" type="hidden" class="point-position" value="<?php echo $form->position[$key] ?>">
                                                        <input name="MarcaForm[address][]" type="hidden" value="<?php echo $form->address[$key] ?>">
                                                    </div>
                                                    <div class="col-xs-6 col-sm-1">
                                                        <button class="btn btn-edit-position" type="button" data-toggle="modal" data-target="#modal-map-edit"></button>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-1">
                                                        <button class="btn btn-remove" type="button"></button>
                                                    </div>
                                                    <span class="row-br"></span>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </div>
                                    <?php echo CHtml::error($form,'location'); ?>
                                    <?php echo CHtml::error($form,'position'); ?>
                            </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


                <div class="col-xs-12 col-sm-2 col-sub-nav section-plan col-plan">
                        <div class="sub-nav active">
                                <button class="btn-sub-nav" type="button">
                                        <span class="btn-arrow"></span>
                                        <span class="btn-icon"></span>
                                </button>
                        </div>
                        <div class="plan-cost">

                                        <div class="plan-cost-edit">
                                                <h4>Usuario Administrador</h4>
                                                <fieldset class="fieldset_amigo_admin">
                                                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                                    'name'=>'search_admin',
                                                    'value'=>'',
                                                    'source' => $amigos,
                                                    'options'=>array(
                                                    'showAnim'=>'fold',
                                                    'minLength'=>'1',
                                                    'select'=>'js:function( event, ui ) {
                                                        $.ajax({
                                                            url:"' . $this->createUrl('marcas/getFriendsAdminAjax') . '",
                                                            dataType:"json",
                                                            data:{selected:ui.item.value, img:ui.item.img, id:ui.item.id},
                                                            success:function(data){
                                                                var size = 0,
                                                                    repetido = 0;
                                                                $(".plan-cost-edit #data_admin_id").each(function () {
                                                                    if(data.id === $(this).val()){
                                                                        repetido = 1;
                                                                    }
                                                                });
                                                            if(parseInt(repetido) === 0){
                                                                size = $(".plan-cost-edit #data_admin").length;
                                                                $(".plan-cost-edit").append(
                                                                  $("<div/>")
                                                                    .addClass("perfil-info")
                                                                    .append(
                                                                        $("<div/>")
                                                                            .addClass("perfil-info-img")
                                                                                .append(
                                                                                    $("<img/>")
                                                                                      .attr({
                                                                                      src: data.img,
                                                                                      class: "img-circle",
                                                                                      alt: ""
                                                                                    })
                                                                                )
                                                                            )
                                                                    .append(
                                                                        $("<div/>")
                                                                            .addClass("perfil-info-text")
                                                                            .append("<span/>")
                                                                                .text(data.name)
                                                                            )
                                                                    .append(
                                                                        $("<button/>")
                                                                            .attr({
                                                                                type: "button",
                                                                                class: "perfil-info-close"
                                                                                })
                                                                            )
                                                                    .append(
                                                                        $("<input/>")
                                                                            .attr({
                                                                                id: "data_admin",
                                                                                type: "hidden",
                                                                                name: "MarcaForm[admin]["+size+"][name]",
                                                                                value: data.name
                                                                                })
                                                                            )
                                                                    .append(
                                                                        $("<input/>")
                                                                            .attr({
                                                                                id: "data_admin_img",
                                                                                type: "hidden",
                                                                                name: "MarcaForm[admin]["+size+"][img]",
                                                                                value: data.img
                                                                                })
                                                                            )
                                                                    .append(
                                                                        $("<input/>")
                                                                            .attr({
                                                                                id: "data_admin_id",
                                                                                type: "hidden",
                                                                                name: "MarcaForm[admin]["+size+"][id]",
                                                                                value: data.id
                                                                                })
                                                                            )
                                                                );
                                                            }
                                                                $("#search_admin").val("");
                                                                return true;
                                                        }
                                                        })
                                                    }',
                                                    ),
                                                    'htmlOptions'=>array(
                                                    //'onfocus' => 'js: this.value = null;',
                                                    'class' => 'input',
                                                    //'placeholder' => "Digite un Admin. de Marca",
                                                    ),
                                                    ));
                                                ?>
                                                <?php // echo CHtml::hiddenfield('MarcaForm[admin][0][name]', '', array('id'=>'data_admin')); ?>
                                                <?php // echo CHtml::hiddenfield('MarcaForm[admin][0][img]', '', array('id'=>'data_admin_img')); ?>
                                                </fieldset>
                                                <?php if($hasDataform and isset($form->admin)): ?>
                                                <?php // print_r($form->admin) ?>
                                                    <?php foreach($form->admin as $key => $dataAdmin): ?>
                                                        <div class="perfil-info">
                                                            <div class="perfil-info-img">
                                                                <img src="<?php echo $dataAdmin['img']; ?>" class="img-circle" alt="">
                                                            </div>
                                                            <div class="perfil-info-text"><?php echo $dataAdmin['name']; ?></div>
                                                            <button type="button" class="perfil-info-close"></button>
                                                            <?php echo CHtml::hiddenfield('MarcaForm[admin]['.$key.'][name]', $dataAdmin['name'], array('id'=>'data_admin')); ?>
                                                            <?php echo CHtml::hiddenfield('MarcaForm[admin]['.$key.'][img]', $dataAdmin['img'], array('id'=>'data_admin_img')); ?>
                                                            <?php echo CHtml::hiddenfield('MarcaForm[admin]['.$key.'][id]', $dataAdmin['id'], array('id'=>'data_admin_id')); ?>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                        </div>

                                        <div class="plan-cost-head">
                                                <h3>Admin</h3>
                                                <p> Invita a otros usuarios para que te ayuden con la administración de tu marca.</p>
                                        </div>
                                        <div class="plan-cost-create save-admin">

                                                <?php
                                                 echo CHtml::submitButton('GUARDAR', array(
                                                    'class'=>'btn',
                                                    'type'=>'button'
                                                     )
                                                 ); ?>

                                        </div>
                        </div>
                </div>
<?php echo CHtml::endForm(); ?>

<!--					</section>-->


						<!-- Modal -->
						<div class="modal fade modal-map" id="modal-map-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close close-map" data-dismiss="modal" aria-label="Close">
										</button>
						      </div>
						      <div class="modal-body">
										<div class="row">
											<div class="col-xs-12">
												<div class="map">
													<div id="map"></div>
												</div>
											</div>
										</div>
						      </div>
						    </div>
						  </div>
						</div>
						<!--/ Modal -->

                                                <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

    </style>
						<!-- Modal -->
						<div class="modal fade modal-map" id="modal-map-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close close-map" data-dismiss="modal" aria-label="Close">
										</button>
						      </div>
						      <div class="modal-body">
										<div class="row">
											<div class="col-xs-12">
												<div class="map">
                                                                                                        <input id="pac-input" class="controls" type="text"
        placeholder="Enter a location">
    <div id="type-selector" class="controls">
      <input type="radio" name="type" id="changetype-all" checked="checked">
      <label for="changetype-all">All</label>

      <input type="radio" name="type" id="changetype-establishment">
      <label for="changetype-establishment">Establishments</label>

      <input type="radio" name="type" id="changetype-address">
      <label for="changetype-address">Addresses</label>

      <input type="radio" name="type" id="changetype-geocode">
      <label for="changetype-geocode">Geocodes</label>
    </div>
													<div id="map"></div>
												</div>
											</div>
										</div>
						      </div>
						    </div>
						  </div>
						</div>
						<!--/ Modal -->
                                                <script>  
                                                function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13
  });
  var input = /** @type {!HTMLInputElement} */(
      document.getElementById('pac-input'));

  var types = document.getElementById('type-selector');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });

  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    radioButton.addEventListener('click', function() {
      autocomplete.setTypes(types);
    });
  }

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}
</script>
                                                
						<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&&key=AIzaSyCNXjfVg60zUlKgNz4kz_R1HbNMTDbomtM&libraries=places" type="text/javascript"></script>
						<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/mark-map.js" type="text/javascript"></script>
