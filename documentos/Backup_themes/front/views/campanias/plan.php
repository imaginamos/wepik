
<?php echo $this->renderPartial('_form', array(
    'coverage' => $coverage, 
    'subsidiary' => $subsidiary, 
    'marcas_usuario' => $marcas_usuario, 
    'form' => $form, 
    'boolmensaje' => $boolmensaje, 
    'hasDataform' => $hasDataform, 
    'free' => $free,
    'value_free' =>  $value_free,
    'value_free2' => $value_free2,
    'imagen_tamano' => $imagen_tamano,
    'costos' => $costos,
    'saldo' => $saldo,
    'costos_elementos' => $costos_elementos,
    'tipo_id' => $tipo_id,
    'imagenesperfil' => $imagenesperfil,
    'data_encuesta' => $data_encuesta,
    'tipo_id_camp' => $tipo_id_camp
)); 
