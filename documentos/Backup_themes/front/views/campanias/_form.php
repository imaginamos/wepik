<!--					<section>-->
<script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
<div class="col-xs-12 col-sm-7 col-grid col-main section-id section-plan">
    <div class="top-search clearfix">
        <form action="#" class="top-form pull-left clearfix">
            <button class="btn-search" type="button"></button>
            <div id="auto-search">
                <input class="typeahead" type="text">
            </div>
        </form>
        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="sub-list-icon"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
            </ul>
        </div>
    </div>

    <div class="info-grl info-form clearfix">
        <div class="row row-info-form">
            <div class="col-xs-12">
                <h1>CREAR UNA CAMPAÑA</h1>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="col-xs-12">
                <?php if($boolmensaje == 1): ?>
                    <div class="modredes" style="display: block;">¡Algo sucedió!, no fué posible crear la Campaña. Intente más tarde.</div>
                    <br/>
                    <br/>
                    <br/>
                <?php endif; ?>
                <div id="flash-message-campania">
                    <?php if($boolmensaje == 2): ?>             
                        <div class="modredes" style="display: none;">¡La Campaña ha sido creada exitosamente!</div>
                        <br/>
                        <br/>
                        <br/>             
                    <?php endif; ?>             
                </div>     
                <div id="flash-message-campania">
                <?php if($boolmensaje == 3): ?>             
                    <div class="modredes" style="display: none;">¡El pago ha fallado!. Intente nuevamente.</div>
                    <br/>
                    <br/>
                    <br/>             
                <?php endif; ?>             
                </div>    
                <?php echo CHtml::beginForm('plan', 'post', array ('id' => 'encuesta-form', 'class' => 'grl-form', 'enctype' => 'multipart/form-data')); ?>
                <div>
                        <?php $num_dias_vigencia = $value_free['num_dias_vigencia']!=0?$value_free['num_dias_vigencia']:$form->dias_vigencia ?>
                        <?php $num_recomendaciones = $value_free['num_recomendaciones']!=0?$value_free['num_recomendaciones']:$form->recomendaciones ?>
                        <?php $num_ingresos = $value_free['num_ingresos']!=0?$value_free['num_ingresos']:$form->ingresos ?>
                        <?php $num_encuestas = (isset($value_free['num_encuestas'])&&$value_free['num_encuestas']!=0)?$value_free['num_encuestas']:0 ?>
                        <?php $num_dias_vigencia2 = $value_free2['num_dias_vigencia']!=0?$value_free2['num_dias_vigencia']:$form->dias_vigencia2 ?>
                        <?php $num_piks2 = $value_free2['num_piks']!=0?$value_free2['num_piks']:$form->piks2 ?>
                        <?php $num_recomendaciones2 = $value_free2['num_recomendaciones']!=0?$value_free2['num_recomendaciones']:$form->recomendaciones2 ?>
                        <?php $num_ingresos2 = $value_free2['num_ingresos']!=0?$value_free2['num_ingresos']:$form->ingresos2 ?>
                        <?php $num_encuestas2 = (isset($value_free2['num_encuestas'])&&$value_free2['num_encuestas']!=0)?$value_free2['num_encuestas']:0 ?>
                    <?php $val_free = array('num_dias_vigencia' => $num_dias_vigencia, 'num_recomendaciones' => $num_recomendaciones, 'num_ingresos' => $num_ingresos, 'num_encuestas' => $num_encuestas) ?>
                    <?php $val_free2 = array('num_dias_vigencia' => $num_dias_vigencia2, 'num_piks' => $num_piks2, 'num_recomendaciones' => $num_recomendaciones2, 'num_ingresos' => $num_ingresos2, 'num_encuestas' => $num_encuestas2) ?>
                    <?php echo CHtml::HiddenField('CampaniaForm[costo_cpc]', $costos['costo_cpc'], array('class' => 'costo_cpc CampaniaForm_ingresos CampaniaForm_ingresos2')); ?>
                    <?php echo CHtml::HiddenField('CampaniaForm[costo_cpl]', $costos['costo_cpl'], array('class' => 'costo_cpl CampaniaForm_piks2')); ?>
                    <?php echo CHtml::HiddenField('CampaniaForm[costo_cpa]', $costos['costo_cpa'], array('class' => 'costo_cpa')); ?>
                    <?php echo CHtml::HiddenField('CampaniaForm[costo_recomendacion]', $costos['costo_recomendacion'], array('class' => 'costo_recomendacion CampaniaForm_recomendaciones CampaniaForm_recomendaciones2')); ?>
                    <?php echo CHtml::HiddenField('CampaniaForm[costo_dia]', $costos['costo_dia'], array('class' => 'costo_dia')); ?>
                    <?php echo CHtml::HiddenField('tipo_id_camp', isset($tipo_id_camp)?$tipo_id_camp:null); ?>
                    <?php echo CHtml::HiddenField('dataEncuesta', $data_encuesta?CJSON::encode($data_encuesta):null); ?>                    

                </div>
                <fieldset class="con-field">
                    <?php echo CHtml::Label('* Marcas', 'marcas') ?>
                        <?php echo CHtml::activeDropDownList($form, 'marcas', $marcas_usuario, array(
                        'class' => 'multi-select col-xs-4',
                            'empty'=>'Seleccione Marca ',
                            'ajax'=>array(
                            'dataType' => 'json',
                                'type'=>'POST',
                                'url'=>$this->createUrl('campanias/getInfoCrearCampaniaBackend'),
                                'data'=>array(
                                'selected' => 'js:this.value',
                                'tipo' => 'js:$("#tipo_id").val()'
                            ),
                            'success' => 'function(data){
                                    $.each(data.freemium, function(key, value){
                                        $("."+key).html(value);
                                    });
                                    $.each(data.value_freemium, function(key, value){
                                        $("."+key).val(value);
                                        $("."+key).siblings(".counter-number").find(".number").html(value);
                                    });
                                    $("#CampaniaForm_imagen_tamano").html(data.imagen_tamano);
                                    $("#CampaniaForm_imagen_tamano2").html(data.imagen_tamano);
                                    $.each(data.costos, function(key, value){
                                        $("."+key).val(value);
                                    });
                                    var values = "";
                                    $.each(data.cost_element, function(key, value){
                                        values = values.concat(value);
                                    });
                                    $("#elemento_costo").html(values);
                                    $(".saldo").html(data.saldo);
                                    $("#total").children("p").html(data.total);
                                    
                                    if($(".check-active-content").is(":checked"))
                                    {
                                        EsFreemium(true);
                                    }
                                    else
                                    {
                                        EsFreemium(false);
                                    }
                                    
                                    
                                }'
                        ),
                        )); ?>
<?php echo CHtml::error($form, 'marcas'); ?>
                    <!--alert(data.multimedia, data.costos, data.freemium, data.saldo, data.ciudades, data.paises);-->
                </fieldset>

                <fieldset class="con-field">
                    <?php echo CHtml::Label('* Nombre de Campaña', 'nombre_campania'); ?>
                <?php echo CHtml::activeTextField($form, 'nombre_campania', array('class' => 'input')) ?>
                        <?php echo CHtml::error($form,'nombre_campania'); ?>
                </fieldset>
                    <?php if($tipo_id == 1 or $tipo_id == 4):
                    $vfree = $val_free;
                else:
                    $vfree = $val_free2;
                    endif; 
                    ?>
                    
                <div class="form-tabs" role="tabpanel">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active" id="tab-content">
                                <?php echo CHtml::ajaxLink(
                                    'CONTENIDO',
                                    CController::createUrl('campanias/getCostFromTabsAjax'),
                                        array(
                                'dataType' => 'json',
                                'type' => 'POST',
                                'data' => array(
                                    'val_free' => $vfree,
                                    'cost' => $costos,
                                    'is_free' => 'js:$(".check-active-content").val()',
                                    'cost_multi' => 'js:$("#CampaniaForm_imagen_tamano").find(":selected").val()',
                                    'tipoid' => $tipo_id    
                                ),
                                'success' => 'function(data){
                                                $(".form-tabs").find("#promotion").removeClass("active");
                                                $(".form-tabs").find("#content").addClass("active");

                                                $(".form-tabs .nav-tabs").find("#tab-promotion").removeClass("active");
                                                $(".form-tabs .nav-tabs").find("#tab-content").addClass("active");
                                                if($(".check-active-content").is(":checked")) {
                                                        $(".item-poll-content").removeClass("active");
                                                        $(".check-active-content").attr("checked", false);
                                                        $(".check-active-content").val(2);
                                                        $(".check-active-content").parent().removeClass("checked");
                                                    }                                                
                                                setTipoCampania(2);
                                                var values = "";
                                                $.each(data.cost_elem, function(key, value){
                                                    values = values.concat(value);
                                                });
                                                $("#elemento_costo").html(values);
                                                $("#total").children("p").html(data.total);
                                            }'
                                        ),
                                    array(
                                'aria-controls' => 'content',
                                'role' => 'tab',
                                'data-toggle' => 'tab'
                                    )
                                ); ?>
                        </li>
                        <li role="presentation" id="tab-promotion">
                                <?php echo CHtml::ajaxLink(
                                    'PROMOCIÓN',
                                    CController::createUrl('campanias/getCostFromTabsAjax'),
                                        array(
                                'dataType' => 'json',
                                'type' => 'POST',
                                'data' => array(
                                    'val_free' => $vfree,
                                    'cost' => $costos,
                                    'is_free' => 'js:$(".check-active-promotion").is(":checked")',
                                    'cost_multi' => 'js:$("#CampaniaForm_imagen_tamano2").find(":selected").val()',
                                    'tipoid' => $tipo_id
                                ),
                                'success' => 'function(data){
                                                $(".form-tabs .tab-content").find("#promotion").addClass("active");
                                                $(".form-tabs .tab-content").find("#content").removeClass("active");

                                                $(".form-tabs .nav-tabs").find("#tab-promotion").addClass("active");
                                                $(".form-tabs .nav-tabs").find("#tab-content").removeClass("active");
                                                if($(".check-active-promotion").is(":checked")) {
                                                        $(".item-poll-promotion").removeClass("active");
                                                        $(".check-active-promotion").attr("checked", false);
                                                        $(".check-active-promotion").val(2);
                                                        $(".check-active-promotion").parent().removeClass("checked");
                                                    }                                                
                                                setTipoCampania(1);
                                                var values = "";
                                                $.each(data.cost_elem, function(key, value){
                                                    values = values.concat(value);
                                                });
                                                $("#elemento_costo").html(values);
                                                $("#total").children("p").html(data.total);
                                            }'
                                        ),
                                    array(
                                'aria-controls' => 'promotion',
                                'role' => 'tab',
                                'data-toggle' => 'tab',
                                    )
                                ); ?>
                        </li>
                    </ul>
<?php echo CHtml::HiddenField('tipo_id', $tipo_id, array('class' => 'tipoCamp')); ?>
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="content">

                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('* Descripción', 'desc_campania') ?>
<?php echo CHtml::activeTextArea($form, 'desc_campania', array('class' => 'textarea')) ?>
<?php echo CHtml::error($form, 'desc_campania') ?>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('Términos y condiciones', 'term_cond'); ?>
<?php echo CHtml::activeTextArea($form, 'term_cond', array('class' => 'textarea')) ?>
                                    <?php echo CHtml::error($form,'term_cond') ?>
                                </fieldset>
                            </div>

                            
                            <div class="row">
                                
                                <fieldset class="col-xs-12 con-field">
                                    <div class="label-card">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-small.png" alt=""></span>
                                                <p><strong>Paga con tarjeta de crédito,</strong> Puedes pagar tus campañas con tarjeta de crédito personal o corporativa, solo tienes que seguir los pasos, proporcionar la información solicitada y listo!                                                                    </div>
                                            <div class="col-xs-2 text-right">
                                                                <?php echo CHtml::activeCheckBox($form, 'is_freemium', array(
                                                                    'value'=>1,
//                                                                    'id'=>'is_freemium',
                                                                    'class'=>'check-active check-active-content',
                                                                    'uncheckValue'=>2,
//                                                                    'ajax'=>array(
//                                                                        'type'=>'POST',
//                                                                        'url'=>$this->createUrl('campanias/oberturasBackendAjax'),
//                                                                        'data'=>array(
//                                                                            'sel_pais_ciudad' => 'js:this.value',
//                                                                            'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
//                                                                        ),
//                                                                        'success' => 'function(data){
//                                                                            $("#pais_ciudad2").attr("checked", true);
//                                                                            $("#CampaniaForm_coverage").html(data);
//
//                                                                        }'
//                                                                    )
                                                ));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            
                            <fieldset class="col-xs-3 con-field">
                                <?php echo CHtml::Label('* Género', 'sexo') ?>
                                <?php echo CHtml::activeDropDownList(
                                        $form,
                                        'sexo',
                                        array('M' => 'Masculino', 'F' => 'Femenino', 'T' => 'Todos'),
                                        array(
                                    'class' => 'select',
                                            'empty'=>'Elija opción'
                                        )) ?>
                                <?php echo CHtml::error($form,'sexo') ?>
                            </fieldset>
                            <fieldset class="col-xs-1 con-field">
                                <label><br><p></p>de:</label>
                            </fieldset>
                            <?php
                            $edades = array();
                                for($i=15; $i<101; $i++):
                                $edades[$i] = $i.' años';
                            endfor;  ?>
                            <fieldset class="col-xs-3 con-field">
                                <?php echo CHtml::Label('* Rango de edades', 'edad_inicial') ?>
                                <?php echo CHtml::activeDropDownList(
                                        $form,
                                        'edad_inicial',
                                        $edades,
                                        array(
                                    'class' => 'select',
                                            'empty'=>'Elija opción'
                                        )) ?>
                                <?php echo CHtml::error($form,'edad_inicial') ?>
                            </fieldset>
                            <fieldset class="col-xs-1 con-field">
                                <label><br><p></p>hasta:</label>
                            </fieldset>
                            <fieldset class="col-xs-3 con-field">
                                <label>&nbsp;</label>
                                    <?php echo CHtml::activeDropDownList(
                                            $form,
                                            'edad_final',
                                            $edades,
                                            array(
                                    'class' => 'select',
                                                'empty'=>'Elija opción'
                                            )) ?>
                                    <?php echo CHtml::error($form,'edad_final') ?>
                            </fieldset>

                            <div class="row">
                                <fieldset class="col-xs-12 con-field con-field-video">
                                    <?php echo CHtml::Label('Videos o imágenes', 'film'); ?>
                                    <div class="row row-videos">
                                        <div class="col-xs-12">
                                            <ul class="list-videos clearfix">
                                                        <?php if($hasDataform and isset($form->videos)): ?>
                                                            <?php foreach($form->videos as $dataVideo): ?>
                                                        <li onclick="this.parentNode.removeChild(this);" class="see-video">Youtube - <?php echo $dataVideo ?>
                                                            <input name="CampaniaForm[videos][]" class="input-hidden" value="<?php echo $dataVideo ?>" type="hidden">
                                                        </li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--<input class="input input-video video-content" placeholder="Ingresa URL">-->
                                        <?php echo CHtml::TextField('film',null, array('class' => 'input input-video video-content', 'placeholder' => 'https://www.youtube.com/id')) ?>
                                    <button class="btn btn-primary btn-add-video-content" type="button">Subir video</button>
                                        <?php echo CHtml::error($form,'videos'); ?>
                                </fieldset>
                            </div>
                            <div class="styl-file">
                                <div class="row">
                                    <fieldset class="col-xs-4 con-field">
                                        <?php echo CHtml::activeDropDownList(
                                            $form,
                                            'imagen_tamano',
                                            $imagen_tamano,
                                            array(
                                            'class' => 'select',
                                                'empty'=>'Elija tamaño',
                                            )) ?>
                                        <?php echo CHtml::error($form, 'imagen_tamano') ?>
                                    </fieldset>
                                    
                                    <fieldset class="col-xs-8 con-field">
                                        <input id="file-content" type="file" name="CampaniaForm[imagen_camp][]"/>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#file-content').change(function () {
                                                    inputChange(this, '');
                                                });
                                            });
                                        </script>
                                        <?php // echo CHtml::error($form,'imagen_camp'); ?>
                                        <?php if ($form->hasErrors('imagen_camp')) : ?>
                                            <?php $errorList = $form->getErrors('imagen_camp'); ?>
                                            <?php foreach ($errorList as $error) : ?>
                                                <div class="errorMessage">
                                                    <?php echo $error ?>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                        <?php if (isset($imagenesperfil)): ?>
                                            <ul id="imagesList" class="multi-list col-xs-8">
                                                <?php foreach ($imagenesperfil as $imagenperfil): ?>
                                                    <li style="margin-top: 10px;" class="brand-page-logo-content">
                                                        <?php echo CHtml::image($imagenperfil, "", array('height' => '110')); ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php else: ?>
                                            <div style="float: left; margin-top: 10px; display: none;" class="brand-page-logo-content">
                                                <?php echo CHtml::image('imagen', "", array('id' => 'file-content-img', 'width' => '100%', 'height' => '110')); ?>
                                            </div>
                                        <?php endif; ?>
                                    </fieldset>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('* Coberturas de paises y/o ciudades', 'pais_ciudad') ?>
                                    <fieldset class="con-field" id="check_pais_ciudad">

                                    <?php echo CHtml::activeRadioButton($form, 'pais_ciudad', array(
                                        'value'=>1,
                                        'id'=>'pais_ciudad1',
                                        'class'=>'radioCob',
                                        'uncheckValue'=>null,
                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('campanias/getCoberturasBackendAjax'),
                                            'data'=>array(
                                                    'sel_pais_ciudad' => 'js:this.value',
                                                    'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
                                                ),
                                                'success' => 'function(data){
                                                $("#pais_ciudad1").attr("checked", true);
                                                $("#CampaniaForm_coverage").html(data);
                                                $("#CampaniaForm_coverage").siblings("ul").empty();
                                                $("#CampaniaForm_subsidiary").siblings("ul").empty();
                                                
                                            }'
                                            )
                                    )); ?>
                                        <?php echo CHtml::Label('Paises', 'pais_ciudad1', array('style' => "display: inline; margin-right: 20px;")); ?>

                                    <?php echo CHtml::activeRadioButton($form, 'pais_ciudad', array(
                                        'value'=>2,
                                        'id'=>'pais_ciudad2',
                                        'class'=>'radioCob',
                                        'uncheckValue'=>null,
                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('campanias/getCoberturasBackendAjax'),
                                            'data'=>array(
                                                    'sel_pais_ciudad' => 'js:this.value',
                                                    'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
                                                ),
                                                'success' => 'function(data){
                                                $("#pais_ciudad2").attr("checked", true);
                                                $("#CampaniaForm_coverage").html(data);
                                                $("#CampaniaForm_coverage").siblings("ul").empty();
                                                $("#CampaniaForm_subsidiary").siblings("ul").empty();
                                                
                                            }'
                                            )
                                        ));
                                        ?>
<?php echo CHtml::Label('Ciudades', 'pais_ciudad2', array('style' => "display: inline; margin-right: 20px;")); ?>
                                        <?php echo CHtml::error($form, 'pais_ciudad') ?>
                                    </fieldset>
                                    <div class="row row-multi">
                                        <?php echo CHtml::activeDropDownList(
                                            $form,
                                            'coverage',
                                            $coverage,
                                            array(
                                            'class' => 'multi-select col-xs-4',
                                            'onchange' => 'multiSelect3(this);',
                                            'ajax' => array(
                                                    'type'=>'POST',
                                                    'url'=>$this->createUrl('campanias/getSucursalesBackendAjax'),
                                                    'data'=>array(
                                                    'sel_pais_ciudad' => 'js:$("#check_pais_ciudad").find(":checked").val()',
                                                    'cob' => 'js:this.value',
                                                    'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
                                                ),
                                                'success' => 'function(data){
                                                        $("#CampaniaForm_subsidiary").html(data);
                                                        $("#CampaniaForm_coverage option:nth-child(1)").attr("disabled", "disabled");
                                                    }'
                                            )
                                            )) ?>
                                        <ul class="multi-list col-xs-8">
                                            <?php if($hasDataform and isset($form->cobertura)): ?>
                                                <?php foreach($form->cobertura as $key => $cobertura): ?>
                                                    <li onclick="this.parentNode.removeChild(this);">
                                                        <input type="hidden" name="CampaniaForm[cobertura][<?php echo $key ?>][id]" value="<?php echo $cobertura['id'] ?>">
                                                        <input type="hidden" name="CampaniaForm[cobertura][<?php echo $key ?>][nombre]" value="<?php echo $cobertura['nombre'] ?>">
                                                    <?php echo $cobertura['nombre']; ?>
                                                    </li>
                                                <?php endforeach; ?>
<?php endif; ?>
                                        </ul>
                                    </div>
<?php echo CHtml::error($form, 'cobertura') ?>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                        <?php echo CHtml::Label('* Sucursales', 'sucursales') ?>
                                    <div class="row row-multi">
                                    <?php echo CHtml::activeDropDownList(
                                        $form,
                                        'subsidiary',
                                        $subsidiary,
                                        array(
                                            'class' => 'multi-select col-xs-4',
                                            'onchange' => 'multiSelect4(this); $("#CampaniaForm_subsidiary option:nth-child(1)").attr("disabled", "disabled");'
                                    )) ?>
                                        <ul class="multi-list col-xs-8">
                                        <?php if($hasDataform and isset($form->sucursales)): ?>
                                            <?php foreach($form->sucursales as $key => $sucursal): ?>
                                                    <li onclick="this.parentNode.removeChild(this);">
                                                        <input type="hidden" name="CampaniaForm[sucursales][<?php echo $key ?>][id]" value="<?php echo $sucursal['id'] ?>">
                                                        <input type="hidden" name="CampaniaForm[sucursales][<?php echo $key ?>][nombre]" value="<?php echo $sucursal['nombre'] ?>">
                                                        <?php echo $sucursal['nombre']; ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <?php echo CHtml::error($form, 'sucursales') ?>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-4 con-field text-left">
                                    
                                        <?php echo CHtml::link(
                                                'INCLUIR ENCUESTA<span></span>',
                                                "#",
                                                array(
                                                    'submit' => $this->createUrl('campanias/encuesta'),
                                                    'params' => array(
                                                        'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()',
                                                        'pago' => 'js:$(".check-active-content").is(":checked")'
                                                    ),
                                                    'class' => 'btn btn-primary btn-add-poll',
                                                    'id' => 'incluir_encuesta'.uniqid()
                                                )); ?>    
                                </fieldset>
                                <fieldset class="col-xs-8 con-field">
                                    <p class="p-small text-left">
                                        <strong>Crea tu propia encuesta,</strong> Con esta herramienta puedes explorar tu mercado potencial, recibir feedback de tus clientes actuales y descubrir nuevas oportunidades para tu negocio.
                                    </p>
                                </fieldset>
                            </div>

                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <div class="label-card label-diamont">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamont-small.png" alt=""></span>
                                                <p><strong>Campañas exclusivas,</strong> Activando esta opción puedes dirigir tu campaña únicamente a clientes Diamante Wepiku, aquellos que tienen mayor actividad en la comunidad, son influenciadores naturales y están prestos a aportar mayor valor a tu marca. Esto con el fin de enfocar tus recursos de mercadeo a tus mejores seguidores.                                                                    </div>
                                            <div class="col-xs-2 text-right">
                                                              <?php echo CHtml::activeCheckBox($form, 'is_diamond', array(
                                                                    'value'=>1,
                                                                    'class'=>'check-active',
                                                                    'uncheckValue'=>2,
                                                ));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            

                            <div class="row row-item-polls heg-calen">

                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Días Vigencia
                                            <span class="item-icon1"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2  class="cantidad_dias">
<?php echo $free['cantidad_dias'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-content white-bg">
                                                <div class="item-poll-date item-poll-date-content"></div>
                                                <div class="item-poll-date-info">
                                                    <input id="date-poll-content" value="">
                                                                    <?php echo CHtml::HiddenField('CampaniaForm[dias_vigencia]', $num_dias_vigencia, array('class'=>'num_dias_vigencia')); ?>
                                                                    <?php echo CHtml::HiddenField('CampaniaForm[fini_dias_vigencia]', false, array('class'=>'fini_dias_vigencia')); ?>
                                                                    <?php echo CHtml::HiddenField('CampaniaForm[ffin_dias_vigencia]', false, array('class'=>'ffin_dias_vigencia')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--                                    <div class="col-xs-4">
                                                                            <div class="item-poll">
                                                                                    <h3>
                                                                                            Número de piks
                                                                                            <span class="item-icon2"></span>
                                                                                    </h3>
                                                                                    <div class="item-poll-box">
                                                                                            <div class="item-poll-box-text clearfix">
                                                                                                    <div class="table">
                                                                                                            <div class="table-cell">
                                                                                                                    <h2 class="cantidad_pik">
<?php echo $free['cantidad_pik'] ?>
                                                                                                                    </h2>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <p>&nbsp;</p>
                                                                                            </div>
                                                                                            <div class="item-poll-box-edit item-poll-content">
                                                                                                    <div class="table">
                                                                                                            <div class="table-cell">
                                                                                                                    <div class="cart-counter">
                                                                                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                                                                                            <span class="counter-number">
                                                                                                    <span class="number"><?php echo $value_free['num_piks']!=0?$value_free['num_piks']:$form->piks ?></span>
                                                                                                                            </span>
                                                                                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                            <?php echo CHtml::TextField('CampaniaForm[piks]', $value_free['num_piks']!=0?$value_free['num_piks']:$form->piks, array('class'=>'input numeros_costos num_piks')); ?>
<?php echo CHtml::error($form, 'piks') ?>
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>-->

                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Recomendaciones
                                            <span class="item-icon3"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2 class="cantidad_recomendaciones">
<?php echo $free['cantidad_recomendaciones'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-content">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <div class="cart-counter">
                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                            <span class="counter-number">
                                                                <span class="number"><?php echo $num_recomendaciones ?></span>
                                                            </span>
                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                            <?php echo CHtml::TextField('CampaniaForm[recomendaciones]', $num_recomendaciones, array('class'=>'numeros_costos num_recomendaciones')); ?>
                                                            <?php echo CHtml::error($form, 'recomendaciones') ?>
                                                            <?php echo CHtml::error($form, 'dias_vigencia') ?>
                                                                                            <?php // echo CHtml::error($form, 'fini_dias_vigencia') ?>
                                                                                            <?php // echo CHtml::error($form, 'ffin_dias_vigencia') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Ingresos al detalle
                                            <span class="item-icon3"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2 class="cantidad_ingresos_detalle">
<?php echo $free['cantidad_ingresos_detalle'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-content">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <div class="cart-counter">
                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                            <span class="counter-number">
                                                                <span class="number"><?php echo $num_ingresos ?></span>
                                                            </span>
                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                            <?php echo CHtml::TextField('CampaniaForm[ingresos]', $num_ingresos, array('class'=>'numeros_costos num_ingresos input')); ?>
<?php echo CHtml::error($form, 'ingresos') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-item-polls">
            <?php if($data_encuesta): ?>  
            <div class="col-xs-4">
                <div class="item-poll">
                  <h3>
                    Cantidad de encuestas
                    <span class="item-icon2"></span>
                  </h3>
                  <div class="item-poll-box">
                    <div class="item-poll-box-text clearfix">
                      <div class="table">
                        <div class="table-cell">
                            <h2  class="cantidad_encuestas">
                                <?php echo $free['cantidad_encuestas'] ?>
                            </h2>
                        </div>
                      </div>
                      <p>&nbsp;</p>
                    </div>
                    <div class="item-poll-box-edit item-poll-content">
                      <div class="table">
                        <div class="table-cell">
                          <div class="cart-counter">
                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                            <span class="counter-number">
                                    <span class="number"><?php echo $num_encuestas ?></span>
                            </span>                            
                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                            <?php echo CHtml::TextField('CampaniaForm[encuestas_enc]', $num_encuestas, array('class'=>'numeros_costos num_encuestas input')); ?> 
                            <?php echo CHtml::error($form, 'encuestas_enc') ?>                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

                                <!--                                    <div class="col-xs-4">
                                                                            <div class="item-poll">
                                                                                    <h3>
                                                                                            Visualizaciones
                                                                                            <span class="item-icon3"></span>
                                                                                    </h3>
                                                                                    <div class="item-poll-box">
                                                                                            <div class="item-poll-box-text clearfix">
                                                                                                    <div class="table">
                                                                                                            <div class="table-cell">
                                                                                                                    <h2 class="cantidad_reproducciones">
<?php echo $free['cantidad_reproducciones'] ?>
                                                                                                                    </h2>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <p>&nbsp;</p>
                                                                                            </div>
                                                                                            <div class="item-poll-box-edit item-poll-content">
                                                                                                    <div class="table">
                                                                                                            <div class="table-cell">
                                                                                                                    <div class="cart-counter">
                                                                                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                                                                                            <span class="counter-number">
                                                                                                    <span class="number"><?php echo $value_free['num_visualizaciones']!=0?$value_free['num_visualizaciones']:$form->visualizaciones ?></span>
                                                                                                                            </span>
                                                                                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                            <?php echo CHtml::TextField('CampaniaForm[visualizaciones]', $value_free['num_visualizaciones']!=0?$value_free['num_visualizaciones']:$form->visualizaciones, array('class'=>'numeros_costos num_visualizaciones')); ?>
<?php echo CHtml::error($form, 'visualizaciones') ?>
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>

                                                                    <div class="col-xs-4">
                                                                            <div class="item-poll">
                                                                                    <h3>
                                                                                            Impresiones
                                                                                            <span class="item-icon3"></span>
                                                                                    </h3>
                                                                                    <div class="item-poll-box">
                                                                                            <div class="item-poll-box-text clearfix">
                                                                                                    <div class="table">
                                                                                                            <div class="table-cell">
                                                                                                                    <h2 class="cantidad_impresiones">
<?php echo $free['cantidad_impresiones'] ?>
                                                                                                                    </h2>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                    <p>&nbsp;</p>
                                                                                            </div>
                                                                                            <div class="item-poll-box-edit item-poll-content">
                                                                                                    <div class="table">
                                                                                                            <div class="table-cell">
                                                                                                                    <div class="cart-counter">
                                                                                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                                                                                            <span class="counter-number">
                                                                                                    <span class="number"><?php echo $value_free['num_impresiones']!=0?$value_free['num_impresiones']:$form->impresiones ?></span>
                                                                                                                            </span>
                                                                                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                            <?php echo CHtml::TextField('CampaniaForm[impresiones]', $value_free['num_impresiones']!=0?$value_free['num_impresiones']:$form->impresiones, array('class'=>'numeros_costos num_impresiones')); ?>
<?php echo CHtml::error($form, 'impresiones') ?>
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>-->

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="promotion">
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('* Descripción', 'desc_campania2') ?>
<?php echo CHtml::activeTextArea($form, 'desc_campania2', array('class' => 'textarea')) ?>
<?php echo CHtml::error($form, 'desc_campania2') ?>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('Términos y condiciones', 'term_cond2'); ?>
<?php echo CHtml::activeTextArea($form, 'term_cond2', array('class' => 'textarea')) ?>
                                    <?php echo CHtml::error($form,'term_cond2') ?>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('* Ingresar oferta del producto <br>(máximo 5 caracteres)', 'oferta2'); ?>
<?php echo CHtml::activeTextField($form, 'oferta2', array('class' => 'inputsmall', 'placeholder' => '2X1 - 50%')) ?>
                                    <?php echo CHtml::error($form,'oferta2'); ?>
                                </fieldset>
                            </div>
                            
                            
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <div class="label-card">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-small.png" alt=""></span>
                                                <p><strong>Paga con tarjeta de crédito,</strong> Puedes pagar tus campañas con tarjeta de crédito personal o corporativa, solo tienes que seguir los pasos y proporcionar la información solicitada y listo!
                                            </div>
                                            <div class="col-xs-2 text-right">
                                                <!--<input class="check-active check-active-promotion" type="checkbox">-->
                                                                    <?php echo CHtml::activeCheckBox($form, 'is_freemium2', array(
                                                                        'value'=>1,
                                                                        'class'=>'check-active check-active-promotion',
                                                                        'uncheckValue'=>2,
                                                ));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            
                            
                            <fieldset class="col-xs-3 con-field">
                                <?php echo CHtml::Label('* Género', 'sexo2') ?>
                                    <?php echo CHtml::activeDropDownList(
                                            $form,
                                            'sexo2',
                                            array('M' => 'Masculino', 'F' => 'Femenino', 'T' => 'Todos'),
                                            array(
                                    'class' => 'select',
                                                'empty'=>'Elija opción'
                                            )) ?>
                                    <?php echo CHtml::error($form,'sexo2') ?>
                            </fieldset>
                            <fieldset class="col-xs-1 con-field">
                                <label><br><p></p>de:</label>
                            </fieldset>
                            <fieldset class="col-xs-3 con-field">
                                <?php echo CHtml::Label('* Rango de edades', 'edad_inicial2') ?>
                                    <?php echo CHtml::activeDropDownList(
                                            $form,
                                            'edad_inicial2',
                                            $edades,
                                            array(
                                    'class' => 'select',
                                                'empty'=>'Elija opción'
                                            )) ?>
                                    <?php echo CHtml::error($form,'edad_inicial2') ?>
                            </fieldset>
                            <fieldset class="col-xs-1 con-field">
                                <label><br><p></p>hasta:</label>
                            </fieldset>
                            <fieldset class="col-xs-3 con-field">
                                <label>&nbsp;</label>
                                <?php echo CHtml::activeDropDownList(
                                        $form,
                                        'edad_final2',
                                        $edades,
                                        array(
                                    'class' => 'select',
                                            'empty'=>'Elija opción'
                                        )) ?>
                                <?php echo CHtml::error($form,'edad_final2') ?>
                            </fieldset>
                            <div class="row">
                                
                                <fieldset class="col-xs-12 con-field con-field-video">
                                    <?php echo CHtml::Label('* Videos o imágenes', 'film2'); ?>
                                    <div class="row row-videos">
                                        <div class="col-xs-12">
                                            <ul class="list-videos clearfix">
                                                        <?php if($hasDataform and isset($form->videos2)): ?>
                                                            <?php foreach($form->videos2 as $dataVideo): ?>
                                                        <li onclick="this.parentNode.removeChild(this);" class="see-video">Youtube - <?php echo $dataVideo ?>
                                                            <input name="CampaniaForm[videos2][]" class="input-hidden" value="<?php echo $dataVideo ?>" type="hidden">
                                                        </li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                        <?php echo CHtml::TextField('film2',null, array('class' => 'input input-video video-promotion', 'placeholder' => 'https://www.youtube.com/id')) ?>
                                    <button class="btn btn-primary btn-add-video-promotion" type="button">Subir video</button>
                                        <?php echo CHtml::error($form,'videos2'); ?>
                                </fieldset>
                            </div>

                            <div class="styl-file">
                                <div class="row">
                                    
                                    
                                    <fieldset class="col-xs-4 con-field">
                                            <?php echo CHtml::activeDropDownList(
                                                $form,
                                                'imagen_tamano2',
                                                $imagen_tamano,
                                                array(
                                            'class' => 'select',
                                                    'empty'=>'Elija tamaño',
                                                )) ?>
                                        <?php echo CHtml::error($form, 'imagen_tamano2') ?>
                                    </fieldset>
                                    
                                    <fieldset class="col-xs-8 con-field">
                                        <input id="file-promotion" type="file" name="CampaniaForm[imagen_camp2][]"/>
                                        <?php if ($form->hasErrors('imagen_camp2')): ?>
                                            <?php $errorList2 = $form->getErrors('imagen_camp2'); ?>
                                            <?php foreach ($errorList2 as $error): ?>
                                                <div class="errorMessage">
                                                    <?php echo $error ?>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                        <ul id="imagesList2" class="multi-list col-xs-8"></ul>
                                        <script type="text/javascript">
                                            $(function (){
                                                $('#file-promotion').change(function () {
                                                    inputChange(this, 2);
                                                });
                                            });
                                        </script>
                                    </fieldset>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field" id="check_pais_ciudad2">
<?php echo CHtml::Label('* Coberturas de paises y/o ciudades', 'pais_ciudadb') ?>

                                            <?php echo CHtml::activeRadioButton($form, 'pais_ciudadb', array(
                                                'value'=>1,
                                                'id'=>'pais_ciudad1b',
                                                'class'=>'radioCob',
                                                'uncheckValue'=>null,
                                                'ajax'=>array(
                                                    'type'=>'POST',
                                                    'url'=>$this->createUrl('campanias/getCoberturasBackendAjax'),
                                                    'data'=>array(
                                                'sel_pais_ciudad' => 'js:this.value',
                                                'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
                                            ),
                                            'success' => 'function(data){
                                                        $("#pais_ciudad1b").attr("checked", true);
                                                        $("#CampaniaForm_coverage2").html(data);
                                                        $("#CampaniaForm_coverage2").siblings("ul").empty();
                                                        $("#CampaniaForm_subsidiary2").siblings("ul").empty();                                                        
                                                    }'
                                        )
                                            )); ?>
                                    <?php echo CHtml::Label('Paises', 'pais_ciudad1b', array('style' => "display: inline; margin-right: 20px;")); ?>

                                            <?php echo CHtml::activeRadioButton($form, 'pais_ciudadb', array(
                                                'value'=>2,
                                                'id'=>'pais_ciudad2b',
                                                'class'=>'radioCob',
                                                'uncheckValue'=>null,
                                                'ajax'=>array(
                                                    'type'=>'POST',
                                                    'url'=>$this->createUrl('campanias/getCoberturasBackendAjax'),
                                                    'data'=>array(
                                                'sel_pais_ciudad' => 'js:this.value',
                                                'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
                                            ),
                                            'success' => 'function(data){
                                                        $("#pais_ciudad2b").attr("checked", true);
                                                        $("#CampaniaForm_coverage2").html(data);
                                                        $("#CampaniaForm_coverage2").siblings("ul").empty();
                                                        $("#CampaniaForm_subsidiary2").siblings("ul").empty();
                                                        
                                                    }'
                                        )
                                    ));
                                    ?>
                                    <?php echo CHtml::Label('Ciudades', 'pais_ciudad2b', array('style' => "display: inline; margin-right: 20px;")); ?>
<?php echo CHtml::error($form, 'pais_ciudadb') ?>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <div class="row row-multi">
                                                <?php echo CHtml::activeDropDownList(
                                                    $form,
                                                    'coverage2',
                                                    $coverage,
                                                    array(
                                            'class' => 'multi-select col-xs-4',
                                            'onchange' => 'multiSelect5(this);',
                                            'ajax' => array(
                                                            'type'=>'POST',
                                                            'url'=>$this->createUrl('campanias/getSucursalesBackendAjax'),
                                                            'data'=>array(
                                                    'sel_pais_ciudad' => 'js:$("#check_pais_ciudad2").find(":checked").val()',
                                                    'cob' => 'js:this.value',
                                                    'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()'
                                                ),
                                                'success' => 'function(data){
                                                                $("#CampaniaForm_subsidiary2").html(data); 
                                                                $("#CampaniaForm_coverage2 option:nth-child(1)").attr("disabled", "disabled");
                                                            }'
                                            )
                                                    )) ?>
                                        <ul class="multi-list col-xs-8">
                                                    <?php if($hasDataform and isset($form->cobertura2)): ?>
                                                        <?php foreach($form->cobertura2 as $key => $cobertura): ?>
                                                    <li onclick="this.parentNode.removeChild(this);">
                                                        <input type="hidden" name="CampaniaForm[cobertura2][<?php echo $key ?>][id]" value="<?php echo $cobertura['id'] ?>">
                                                        <input type="hidden" name="CampaniaForm[cobertura2][<?php echo $key ?>][nombre]" value="<?php echo $cobertura['nombre'] ?>">
                                                        <?php echo $cobertura['nombre']; ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <?php echo CHtml::error($form, 'cobertura2') ?>
                                </fieldset>
                            </div>

                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <?php echo CHtml::Label('* Sucursales', 'sucursales2') ?>
                                    <div class="row row-multi">
                                        <?php echo CHtml::activeDropDownList(
                                            $form,
                                            'subsidiary2',
                                            $subsidiary,
                                            array(
                                            'class' => 'multi-select col-xs-4',
                                            'onchange' => 'multiSelect6(this); 
                                                          $("#CampaniaForm_subsidiary2 option:nth-child(1)").attr("disabled", "disabled");                                                                 
'
                                        )) ?>
                                        <ul class="multi-list col-xs-8">
                                            <?php if($hasDataform and isset($form->sucursales2)): ?>
                                                <?php foreach($form->sucursales2 as $key => $sucursal): ?>
                                                    <li onclick="this.parentNode.removeChild(this);">
                                                        <input type="hidden" name="CampaniaForm[sucursales2][<?php echo $key ?>][id]" value="<?php echo $sucursal['id'] ?>">
                                                        <input type="hidden" name="CampaniaForm[sucursales2][<?php echo $key ?>][nombre]" value="<?php echo $sucursal['nombre'] ?>">
                                                        <?php echo $sucursal['nombre']; ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <?php echo CHtml::error($form, 'sucursales2') ?>
                                </fieldset>
                            </div>

                            <div class="row">
                                <fieldset class="col-xs-4 con-field text-left">
                                    encuesta promo
                                        <?php echo CHtml::link(
                                                'INCLUIR ENCUESTA<span></span>',
                                                "#",
                                                array(
                                                    'submit' => $this->createUrl('campanias/encuesta'),
                                                    'params' => array(
                                                        'mid' => 'js:$("#CampaniaForm_marcas").find(":selected").val()',
                                                        'pagopromo' => 'js:$(".check-active-promotion").is(":checked")'
                                                    ),
                                                    'class' => 'btn btn-primary btn-add-poll',
                                                    'id' => 'incluir_encuesta'.uniqid()
                                                )); ?>                                    
                                </fieldset>
                                <fieldset class="col-xs-8 con-field">
                                    <p class="p-small text-left">
                                        <strong>Crea tu propia encuesta,</strong> Con esta herramienta puedes explorar tu mercado potencial, recibir feedback de tus clientes actuales y descubrir nuevas oportunidades para tu negocio.
                                    </p>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="col-xs-12 con-field">
                                    <div class="label-card label-diamont">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamont-small.png" alt=""></span>
                                                <p><strong>Campañas exclusivas,</strong> Activando esta opción puedes dirigir tu campaña únicamente a clientes Diamante Wepiku, aquellos que tienen mayor actividad en la comunidad, son influenciadores naturales y están prestos a aportar mayor valor a tu marca. Esto con el fin de enfocar tus recursos de mercadeo a tus mejores seguidores.
                                            </div>
                                            <div class="col-xs-2 text-right">
                                                    <!--<input class="check-active" type="checkbox">-->
                                                                    <?php echo CHtml::activeCheckBox($form, 'is_diamond2', array(
                                                                          'value'=>1,
                                                                          'class'=>'check-active',
                                                                          'uncheckValue'=>2,
                                                ));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="row row-item-polls heg-calen">
                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Días Vigencia
                                            <span class="item-icon1"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2  class="cantidad_dias">
<?php echo $free['cantidad_dias'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-promotion white-bg">
                                                <div class="item-poll-date item-poll-date-promotion"></div>
                                                <div class="item-poll-date-info">
                                                    <input id="date-poll-promotion" value="">
                                                                        <?php echo CHtml::HiddenField('CampaniaForm[dias_vigencia2]', $num_dias_vigencia2, array('class'=>'num_dias_vigencia')); ?>
                                                                        <?php echo CHtml::HiddenField('CampaniaForm[fini_dias_vigencia2]', false, array('class'=>'fini_dias_vigencia')); ?>
                                                                        <?php echo CHtml::HiddenField('CampaniaForm[ffin_dias_vigencia2]', false, array('class'=>'ffin_dias_vigencia')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Número de piks
                                            <span class="item-icon2"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2 class="cantidad_pik">
<?php echo $free['cantidad_pik'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-promotion">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <div class="cart-counter">
                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                            <span class="counter-number">
                                                                <span class="number"><?php echo $num_piks2 ?></span>
                                                            </span>
                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                                <?php echo CHtml::TextField('CampaniaForm[piks2]', $num_piks2, array('class'=>'input numeros_costos num_piks')); ?>
<?php echo CHtml::error($form, 'piks2') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Recomendaciones
                                            <span class="item-icon3"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2 class="cantidad_recomendaciones">
<?php echo $free['cantidad_recomendaciones'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-promotion">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <div class="cart-counter">
                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                            <span class="counter-number">
                                                                <span class="number"><?php echo $num_recomendaciones2 ?></span>
                                                            </span>
                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                                <?php echo CHtml::TextField('CampaniaForm[recomendaciones2]', $num_recomendaciones2, array('class'=>'numeros_costos num_recomendaciones')); ?>
                                                            <?php echo CHtml::error($form, 'recomendaciones2') ?>
<?php echo CHtml::error($form, 'dias_vigencia2') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-item-polls">
                                <div class="col-xs-4">
                                    <div class="item-poll">
                                        <h3>
                                            Ingresos al detalle
                                            <span class="item-icon3"></span>
                                        </h3>
                                        <div class="item-poll-box">
                                            <div class="item-poll-box-text clearfix">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <h2 class="cantidad_ingresos_detalle">
<?php echo $free['cantidad_ingresos_detalle'] ?>
                                                        </h2>
                                                    </div>
                                                </div>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="item-poll-box-edit item-poll-promotion">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <div class="cart-counter">
                                                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                            <span class="counter-number">
                                                                <span class="number"><?php echo $num_ingresos2 ?></span>
                                                            </span>
                                                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                                                            <?php echo CHtml::TextField('CampaniaForm[ingresos2]', $num_ingresos2, array('class'=>'numeros_costos num_ingresos input')); ?>
<?php echo CHtml::error($form, 'ingresos2') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($data_encuesta): ?>  
                                <div class="col-xs-4">
                                    <div class="item-poll">
                                      <h3>
                                        Cantidad de encuestas
                                        <span class="item-icon2"></span>
                                      </h3>
                                      <div class="item-poll-box">
                                        <div class="item-poll-box-text clearfix">
                                          <div class="table">
                                            <div class="table-cell">
                                                <h2  class="cantidad_encuestas">
                                                    <?php echo $free['cantidad_encuestas'] ?>
                                                </h2>
                                            </div>
                                          </div>
                                          <p>&nbsp;</p>
                                        </div>
                                        <div class="item-poll-box-edit item-poll-promotion">
                                          <div class="table">
                                            <div class="table-cell">
                                              <div class="cart-counter">
                                                <button class="btn-count-down pull-left" type="button"><span></span></button>
                                                <span class="counter-number">
                                                        <span class="number"><?php echo $num_encuestas ?></span>
                                                </span>                            
                                                <button class="btn-count-up pull-right" type="button"><span></span></button>
                                                <?php echo CHtml::TextField('CampaniaForm[encuestas_enc2]', $num_encuestas, array('class'=>'numeros_costos num_encuestas input')); ?> 
                                                <?php echo CHtml::error($form, 'encuestas_enc2') ?>                            
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <?php endif; ?>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-2 col-sub-nav">
    <div class="sub-nav active">
        <button class="btn-sub-nav" type="button">
            <span class="btn-arrow"></span>
            <span class="btn-icon"></span>
        </button>
    </div>
    <div class="plan-cost">
        <div class="plan-cost-head">
            <h3>COTIZA TU CAMPAÑA</h3>
            <p>
                Con esta herramienta puedes ir calculando cuánto te va a costar tu campaña. Añade los ítems que deseas, según tus objetivos de mercadeo y ajusta la campaña según tu presupuesto.
            </p>
            <div class="row row-cost-info">
                <div class="col-xs-7 text-left">
                    <h5><span class="blue">SALDO ACTIVO</span></h5>
                </div>
                <div class="col-xs-5 text-right">
                    <p><span class="blue saldo"><?php echo $saldo ?></span></p>
                </div>
            </div>
            <div class="row row-cost-info">
                <div class="col-xs-12 text-left">
                    <h5>COSTO CAMPAÑA</h5>
                </div>
                <div class="col-xs-12" id="elemento_costo">
                                            <?php if($hasDataform and isset($costos_elementos)): ?>
                        <?php foreach ($costos_elementos['cost_elem'] as $accion => $costo_accion): ?>
                            <?php echo $costo_accion ?>
                        <?php endforeach; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
        <div class="plan-cost-total">
            <div class="row">
                <div class="col-xs-4 text-left">
                    <h5>TOTAL</h5>
                </div>
                <div class="col-xs-8 text-right" id="total">
                    <p><?php echo $costos_elementos['tot'] ?></p>
                </div>
                <br><b><h6 style='color: red; text-align: center;'><br>*Costos no incluyen IVA</h6></b>
            </div>
        </div>
        <div class="plan-cost-create">
            <?php
            echo CHtml::submitButton('CREAR', array(
                                    'class'=>'btn',
                                    'type'=>'button'
                    )
                                 ); ?>
        </div>
    </div>
<?php echo CHtml::endForm(); ?>
</div>
<script type="text/javascript">
$(window).load(function() {

var WIDTHima;
var HEIGTima;
        
    var mensaje = '<?php echo $boolmensaje ?>',
        marca_id = '<?php echo $form->marcas ?>';
    if(parseInt(mensaje) === 2){
        $("#flash-message-campania .modredes").css({ display: "block" });
        $("#flash-message-campania .modredes").animate({
            opacity: 0
            }, 6000, function() {
                    jQuery.yii.submitForm(this,"/marcas/marcaAdminActivas",{"m":marca_id});
                    return false;
            });        
    }
    if(parseInt(mensaje) === 3){
        $("#flash-message-campania .modredes").css({ display: "block" });
        $("#preload").css({display: "none"});
    }   
    
    if($(".check-active-content").is(":checked")) {
            $(".item-poll-content").addClass("active");
//        $(this).next().attr('class', 'blue');
        }
    if($(".check-active-promotion").is(":checked")){
            $(".item-poll-promotion").addClass("active");
        }

        var tipo = $(".form-tabs .tipoCamp").val();
    if(parseInt(tipo) === 1 || parseInt(tipo) === 4){
            $(".form-tabs .tab-content").find("#promotion").addClass("active");
            $(".form-tabs .tab-content").find("#content").removeClass("active");

            $(".form-tabs .nav-tabs").find("#tab-promotion").addClass("active");
            $(".form-tabs .nav-tabs").find("#tab-content").removeClass("active");
    } else if(parseInt(tipo) === 2 || parseInt(tipo) === 5) {
            $(".form-tabs").find("#promotion").removeClass("active");
            $(".form-tabs").find("#content").addClass("active");

            $(".form-tabs .nav-tabs").find("#tab-promotion").removeClass("active");
            $(".form-tabs .nav-tabs").find("#tab-content").addClass("active");
        }
    });

$('#file-content').on('change', function(evt){
        if (0 < evt.target.files.length) {
            // get file object which is seled by user
        var _image               = evt.target.files[0];
        var _reader              = new FileReader();
            _reader.onload       = function(evt) {
                $('.brand-page-logo-content').css('display', 'inline');
                $('#file-content-img').attr('src', evt.target.result);
                
                //_________CAROL____________
//                var miima = new Image();
//                miima.src = evt.target.result;
//                miima.onload = function (){
//                    WIDTHima = this.width;
//                    HEIGTima = this.height;
//                    alert( this.width);
//                    alert( this.height);
//                }
                //----------------------------
            };
            _reader.readAsDataURL(_image);
        } else {
            $('#file-content-img').attr('src', null);
        }
    });

var markup = '\
<li style="margin-top:10px" onclick="this.parentNode.removeChild(this);">\n\
    <img src="${src}" style="max-height:110px"/>\n\
    <input type="file" name="CampaniaForm[imagen_camp${num}][]"/>\n\
</li>';
$.template('imageTemplate', markup);
function inputChange(imgDom, num) {
    if (imgDom.files && imgDom.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            
            //_________CAROL____________
                var miima = new Image();
                
                miima.src = e.target.result;
                miima.onload = function (){
                    WIDTHima = this.width;
                    HEIGTima = this.height;
                    
                    var VALIDACION = validarTamanio();
                    if(VALIDACION=='OK')
                    {
                        $(imgDom).hide();
                        var imageList = '#imagesList' + num;
                        var imageListJ = $(imageList);
                        var count = imageListJ.find('li').length + 1;
                        var image = {src: e.target.result, num: num, count:count};
                        $.tmpl('imageTemplate', image).appendTo(imageListJ);
        
                        var li = imageList + ' li:last-child';
                        $(imgDom).appendTo(li);
                        var newInputFile = $(li + ' input');
                        $(newInputFile[0]).prependTo(imageListJ);
                        $(newInputFile[0]).change(function () {
                            inputChange(this, num);
                        });
                    }
                    else
                    {
                        alert(VALIDACION);
                        $('#file-content').val('');
                    }
                }
                
            //----------------------------
//            if(validarTamanio())
//            {
//                $(imgDom).hide();
//                var imageList = '#imagesList' + num;
//                var imageListJ = $(imageList);
//                var count = imageListJ.find('li').length + 1;
//                var image = {src: e.target.result, num: num, count:count};
//                $.tmpl('imageTemplate', image).appendTo(imageListJ);
//
//                var li = imageList + ' li:last-child';
//                $(imgDom).appendTo(li);
//                var newInputFile = $(li + ' input');
//                $(newInputFile[0]).prependTo(imageListJ);
//                $(newInputFile[0]).change(function () {
//                    inputChange(this, num);
//                });
//            }
//            else
//            {
//                $('#file-content').val('');
//            }
            
        };
        reader.readAsDataURL(imgDom.files[0]);
    }
}

function validarTamanio()
{
    var WIDTH;
    var HEIGHT;
    var MensajeError="";
    
    if($('#CampaniaForm_imagen_tamano').val()=='1+1')
    {
        WIDTH = 1920;
        HEIGHT = 600;
        MensajeError ="Imagen debe ser de 1920x600";
    }
    else if($('#CampaniaForm_imagen_tamano').val()=='2+1.2')
    {
        WIDTH = 1920;
        HEIGHT = 900;      
        MensajeError ="Imagen debe ser de 1920x900";
    }
    else if($('#CampaniaForm_imagen_tamano').val()=='3+1.5')
    {
        WIDTH = 1920;
        HEIGHT = 1500;
        MensajeError ="Imagen debe ser de 1920x1500";
    }
    else
    {
        MensajeError ="Seleccione una campaña y luego un tamaño";
    }
    
    if(WIDTHima == WIDTH && HEIGTima == HEIGHT)
    {
        MensajeError="OK";
    }
    else
    {
        MensajeError="El tamaño de la imagen no es el adecuado. Verifique que halla seleccionado un tamaño.";
    }
    return MensajeError;
}

</script>

<!--					</section>-->
