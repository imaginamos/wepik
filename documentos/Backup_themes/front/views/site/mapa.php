<section>
    <div class="col-xs-12 col-sm-7 col-map col-main main-section section-id" data-section="map">
        <!--div class="top-search top-search-map clearfix">
            <form action="#" class="top-form pull-left clearfix">
                <button class="btn-search" type="button"></button>
                <div id="auto-search">
                    <input class="typeahead" type="text">
                </div>
            </form>
            <div class="dropdown pull-right">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    <span class="sub-list-icon"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
            </div>
        </div-->


        <div class="map">
            <div class="bh-sl-container">
            </div>
            <div id="bh-sl-map" class="bh-sl-map"></div>
        </div>
    </div>


    <!-- <div class="con-section">
      <div class="mg-grl clearfix">
        <div class="row">
          <div class="col-md-6">
            <div class="map">
              <div id="map"></div>
            </div>
          </div>
        </div>
      </div>
    </div> -->




    <div class="col-xs-12 col-sm-2 col-sub-nav col-sub-nav-map">
        <div class="sub-nav active">
            <button class="btn-sub-nav" type="button" disabled="true">
                <!--span class="btn-arrow"></span>
                <span class="btn-icon-map"></span-->
            </button>
            <button class="map-filter" type="button"><?php if (isset($filtro)):echo $filtro;
else:echo "Todos";
endif; ?></button>
            <div class="categories-filter">
                <form class="filter-form" action="" method="post">
                    <input type="text" name="name" placeholder="Escribe aquí el momento de consumo" value=""  onkeyup="reloadMoment(this)" >
                </form>
                <div class="filter-labels" id="filterMoment">
                    <a href = "<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1'); ?>"><span>Todos</span></a>
                    <?php
                    if (isset($momentos)):
                        foreach ($momentos as $momento) {
                            ?>
                            <a href = "<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1&f=' . $momento['id']) ?>"><span><?php echo $momento['nombre']; ?></span></a>
    <?php
    }
endif;
?>
                </div>
            </div>


            <div class="sub-nav-list">
                <div id="map-container" class="bh-sl-map-container">
                    <div class="bh-sl-loc-list">
                        <ul class="list cont-list-item"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hide" id="sucursales"><?php echo $sucursales; ?></div>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.scrollTo.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/stores.js" type="text/javascript"></script>
    <script type="text/javascript">
                        function reloadMoment(element) {
                            var filtro = element.value;
                            if (filtro.length > 0) {
                                var str = "<?php if (isset($momentos)):foreach ($momentos as $momento) { ?><a href = \"<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1&f=' . $momento['id']) ?>\"><span><?php echo $momento['nombre']; ?></span></a>*<?php }endif; ?>";
                                            var res = str.split("*");
                                            var text = "";
                                            var i;
                                            for (i = 0; i < res.length; i++) {
                                                var posicion = res[i].lastIndexOf('<span>');
                                                var posicionfin = res[i].lastIndexOf('</span>');
                                                var cad = res[i].substring(posicion + 6, posicionfin);
                                                var cadU = cad.toUpperCase();
                                                var cadL = cad.toLowerCase();
                                                if (cadU.indexOf(filtro) > -1 || cadL.indexOf(filtro) > -1) {
                                                    text += res[i];
                                                }
                                            }
                                            document.getElementById("filterMoment").innerHTML = "<a href = \"<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1'); ?>\"><span>Todos</span></a>" + text;
                                        } else {
                                            document.getElementById("filterMoment").innerHTML = "<a href = \"<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1'); ?>\"><span>Todos</span></a><?php if (isset($momentos)):foreach ($momentos as $momento) { ?><a href = \"<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1&f=' . $momento['id']) ?>\"><span><?php echo $momento['nombre']; ?></span></a><?php }endif; ?>";
                                        }
                                    }

    </script>
</section>
