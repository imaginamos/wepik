<html>
<body class="landing-page">
<?php if(isset($error)):
        echo $error;
 endif; ?>

<?php if(!Yii::app()->session['facebook_nombre']): ?>
    <?php echo CHtml::link('Login with Facebook', $this->createAbsoluteUrl('/site/login',array('provider'=>'facebook')), array( 'class' => 'fb_login right')); ?>
<?php else: ?>
    <p>BIENVENIDO, <?php echo Yii::app()->session['facebook_nombre']; ?> - <a href="<?php echo $this->createUrl("/hotdeals/index"); ?>">Ingresar al Sitio</a> - <a href="<?php echo $this->createUrl("/site/logout"); ?>">Salir</a></p>
    <?php if(isset($datos_usuario_json)): ?>
        <?php //echo 'json: '.print_r($datos_usuario_json); ?>
    <?php endif; ?>
<?php endif; ?>
</body>
</html>
