<!--					<section>-->


            <div class="container-fluid brand-page-head main-section section-id" data-section="points">
              <img class="points-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-img.jpg" alt="">
            </div>
            <div class="brand-head-fx"></div>


						<div class="col-xs-12 col-sm-7 col-grid col-main grid-section">
							<div class="top-search clearfix">
								<form action="#" class="top-form pull-left clearfix">
									<button class="btn-search" type="button"></button>
									<div id="auto-search">
		  							<input class="typeahead" type="text">
									</div>
								</form>
								<div class="dropdown pull-right">
								  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
								    <span class="sub-list-icon"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
								  </ul>
								</div>
							</div>


              <div class="mg-brand-logo text-center">
                <div class="table">
                  <div class="table-cell">

                    <div class="con-chart">
                        <?php if($data_puntos_json['Es_diamante'] == 1): ?>
                              <span class="text-chart">
                                Ya eres<br>
                                <strong>DIAMANTE</strong>
                                <?php setlocale(LC_TIME, 'es_ES.UTF-8'); ?>
                                
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($data_puntos_json['Fecha_max']);
                                ?>
                                <br><?php echo 'Hasta el  día: '. date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?>
                                <br><br>Total puntos acumulados: <b><?php echo $data_puntos_json['Puntaje_tot'];?></b>
                              </span>
                        <?php else: ?>
                            <div id="my-points" data-startdegree="90" data-dimension="164" data-width="8" data-fontsize="60" data-percent="<?php echo $data_puntos_json['Puntaje_tot']*100/$data_puntos_json['Puntaje_max'];?>" data-fgcolor="#fff" data-bgcolor="rgba(77,77,77,.3)">
                              <span class="text-chart">
                                te faltan<br>
                                <strong><?php echo $total_puntos = $data_puntos_json['Puntaje_max'] - $data_puntos_json['Puntaje_tot']; ?></strong><br>
                                Puntos
                              </span>
                              <span class="diamont-chart"></span>
                            </div>                        
                        <?php endif; ?>
                    </div>

                  </div>
                </div>
              </div>


							<div class="wepiku-grid points-grid">

                <!-- Item -->
								<div class="wepiku-item">
                  <a class="points-item">
                    <div class="row-table">
                      <div class="col-xs-2 col-cell">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-icon1.png" alt="" />
                      </div>
                      <div class="col-xs-6 col-cell">
                        <h4>Compartir <br> la App</h4>
                      </div>
                      <div class="col-xs-4 col-cell">
                        <h4 class="text-right"><?php echo $cat_puntos[6].' pts';  ?></h4>
                      </div>
                    </div>
                  </a>
								</div>
								<!--/ Item -->

                <!-- Item -->
								<div class="wepiku-item">
                  <a class="points-item">
                    <div class="row-table">
                      <div class="col-xs-2 col-cell">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-icon2.png" alt="" />
                      </div>
                      <div class="col-xs-6 col-cell">
                        <h4>Ver video</h4>
                      </div>
                      <div class="col-xs-4 col-cell">
                        <h4 class="text-right"><?php echo $cat_puntos[4].' pts';  ?></h4>
                      </div>
                    </div>
                  </a>
								</div>
								<!--/ Item -->

                <!-- Item -->
								<div class="wepiku-item">
                  <a class="points-item">
                    <div class="row-table">
                      <div class="col-xs-2 col-cell">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-icon3.png" alt="" />
                      </div>
                      <div class="col-xs-6 col-cell">
                        <h4>Responder <br> FAQ</h4>
                      </div>
                      <div class="col-xs-4 col-cell">
                        <h4 class="text-right"><?php echo $cat_puntos[10].' pts';  ?></h4>
                      </div>
                    </div>
                  </a>
								</div>
								<!--/ Item -->

                <!-- Item -->
								<div class="wepiku-item">
                  <a class="points-item">
                    <div class="row-table">
                      <div class="col-xs-2 col-cell">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-icon4.png" alt="" />
                      </div>
                      <div class="col-xs-6 col-cell">
                        <h4>Ingreso <br> app diario</h4>
                      </div>
                      <div class="col-xs-4 col-cell">
                        <h4 class="text-right"><?php echo $cat_puntos[8].' pts';  ?></h4>
                      </div>
                    </div>
                  </a>
								</div>
								<!--/ Item -->

                <!-- Item -->
								<div class="wepiku-item">
                  <a class="points-item">
                    <div class="row-table">
                      <div class="col-xs-2 col-cell">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-icon5.png" alt="" />
                      </div>
                      <div class="col-xs-6 col-cell">
                        <h4>Compartir <br> contenido <br> en redes</h4>
                      </div>
                      <div class="col-xs-4 col-cell">
                        <h4 class="text-right"><?php echo $cat_puntos[7].' pts';  ?></h4>
                      </div>
                    </div>
                  </a>
								</div>
								<!--/ Item -->

                <!-- Item -->
								<div class="wepiku-item">
                  <a class="points-item">
                    <div class="row-table">
                      <div class="col-xs-2 col-cell">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/points-icon6.png" alt="" />
                      </div>
                      <div class="col-xs-6 col-cell">
                        <h4>Hacer <br> comentario</h4>
                      </div>
                      <div class="col-xs-4 col-cell">
                        <h4 class="text-right"><?php echo $cat_puntos[9].' pts';  ?></h4>
                      </div>
                    </div>
                  </a>
								</div>
								<!--/ Item -->

							</div>
						</div>  
<!--					</section>-->
