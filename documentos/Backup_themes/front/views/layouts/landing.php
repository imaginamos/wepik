<?php // Yii::app()->clientScript->registerCoreScript('jquery-front');?>
<?php echo Yii::app()->bootstrap->register(); ?>
<!DOCTYPE html>
	<!--[if lt IE 8]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="es"> <![endif]-->
	<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9 ie8" lang="es"> <![endif]-->
	<!--[if IE 9]> <html class="no-js lt-ie10 ie9" lang="es"> <![endif]-->
	<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
	<head>
		<title>Wepiku</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="date" content="<?php echo date("Y"); ?>" />
		<meta name="author" content="diseño web: imaginamos.com" />
		<meta name="robots" content="All" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
		<meta name="viewport" content="width=1320, maximum-scale=2" />
		<meta name="Keywords" content="palabras clave" lang="es" />
		<meta name="Description" content="texto empresarial" lang="es" />
		<link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />

		<!--Styles after front-end-->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/add/styles.css" />

		<!-- <script async src="http://modernizr.com/downloads/modernizr-latest.js" type="text/javascript"></script> -->
		<script async type="text/javascript">var baseUrl = "<?php echo Yii::app()->theme->baseUrl; ?>";</script>

		<?php echo $this->builtHeader()?>
	</head>
	<body>
		<div id="preload"><div class="preload-logo"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png"></div></div>
		<div class="con-bw"><div class="info-bw"><div class="head-bw clearfix"><div class="logo-bw left"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png"></div><div class="tx-bw left"><p>Oops!... Lo sentimos, este sitio se ha desarrollado para navegadores modernos con el fin de mejorar tu experiencia.</p><p>Para que lo puedas disfrutar es necesario actualizar tu navegador o simplemente descargar e instalar uno mejor.</p></div></div><div class="con-icon-bw left"><a href="https://www.google.com/intl/es/chrome/browser/?hl=es" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-1.png"></div><p>Chrome</p></a></div><div class="con-icon-bw left"><a href="http://www.mozilla.org/es-ES/firefox/new/" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-2.png"></div><p>Firefox</p></a></div><div class="con-icon-bw left"><a href="http://www.opera.com/es-419/computer/" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-3.png"></div><p>Opera</p></a></div><div class="con-icon-bw left"><a href="http://support.apple.com/kb/DL1531?viewlocale=es-ES" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-4.png"></div><p>Safari</p></a></div><div class="con-icon-bw left"><a href="http://windows.microsoft.com/es-es/internet-explorer/download-ie" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-5.png"></div><p>Explorer</p></a></div><div class="con-bt-bw left clearfix"><a class="bt-bw"></a></div></div></div>




    <div id="fb-root"></div>


    <!--<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1426391070998402',
          xfbml      : true,
          version    : 'v2.3'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/es_LA/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>-->


    <!-- // <script>(function(d, s, id) {
    //   var js, fjs = d.getElementsByTagName(s)[0];
    //   if (d.getElementById(id)) return;
    //   js = d.createElement(s); js.id = id;
    //   js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.3";
    //   fjs.parentNode.insertBefore(js, fjs);
    // }(document, 'script', 'facebook-jssdk'));</script> -->



		<div class="line-h"></div>
		<div class="line-v"></div>
		<div class="line-ml"></div>
		<div class="line-mr"></div>



<!--
		<div id="main-wrapper">
			<div class="animated fadeIn">
-->

				<!--div class="container session-message">
					<?php #if(($msgs=$this->builtMessages())!==null and $msgs!==array()):?>
					<div class="row">
						<div class="col-lg-12">
							<?php #foreach($msgs as $type => $message):?>
								<div class="alert alert-<?php #echo $type?>">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php #echo $message?>
								</div>
							<?php #endforeach;?>
						</div>
					</div>
					<?php #endif;?>
				</div-->


				<section>
					<div class="header-label"></div>
					<div class="container-fluid">
						<div class="container">
							<div class="row">

                <div class="col-xs-12" style="margin-top: 64px;">
                  <div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="true"></div>
                </div>

								<?php echo $content; ?>

							</div>
						</div>
					</div>
				</section>


<!--
			</div>
		</div>
-->


    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/landing.js" type="text/javascript"></script>


		<!--script src="<?php #echo Yii::app()->theme->baseUrl; ?>/js/scripts.min.js" type="text/javascript"></script-->

		<!--Functions after front-end-->
		<!--script src="<?php #echo Yii::app()->theme->baseUrl; ?>/add/scripts.js" type="text/javascript"></script-->

		<?php echo $this->builtEndBody()?>
	</body>
</html>
