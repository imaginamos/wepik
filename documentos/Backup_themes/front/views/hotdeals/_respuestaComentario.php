<div class="comment comment-right">
        <div class="comment-user">
            <?php echo CHtml::image(Yii::app()->session['url_img'].$logo, "", array('class' => 'img-circle', 'width' => '44')) ?>
        </div>
        <div class="comment-text">
                <h4><?php echo $nombre_marca ?></h4>
                <p class="text-justify">
                    <?php echo CHtml::beginForm(null, null, array (
                        'class' => 'modal-comment clearfix',
                        )); 
                    ?>                        
                        <?php echo CHtml::TextArea('com',null, array('class' => 'textarea')) ?>
                        <?php echo CHtml::hiddenfield('camp', $campania_id); ?>  
                        <?php echo CHtml::hiddenfield('campuid', $usuario_id_camp); ?> 
                        <?php echo CHtml::hiddenfield('comid', $comentario_id); ?> 
                        <?php echo CHtml::hiddenfield('logo', $logo); ?>
                        <?php echo CHtml::hiddenfield('marc', $nombre_marca); ?>

                    <?php
                     echo CHtml::ajaxSubmitButton(
                             '', 
                            $this->createurl('hotdeals/responderComentarioBackendAjax'), 
                            array( 
                                'dataType' => 'json', 
                                'type' => 'POST',
                                'cache' => true,
                                'success' => 'function(data){
                                    $("#list-comment").html(data);
                                }'
                            ),
                            array(
                                'id' => 'send-answer-comment'.uniqid(),
                                'live'=>false,
                                'class' => 'buttom-new-comment',
                                'type'=>'submit'
                            )                     
                     ); ?>                        
                    <?php echo CHtml::endForm(); ?>                
                </p>
                <!--<div class="comment-actions clearfix">-->
                        <!--<button class="comment-actions-left"></button>-->
<!--                        <button class="comment-actions-right">
                                <?php // echo $comentario_hijo['fecha_comentario'] ?>
                                <span></span>
                        </button>-->
                <!--</div>-->
        </div>
</div>  