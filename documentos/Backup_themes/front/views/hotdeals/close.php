<?php   $camp_modal = $_GET['camp']; 
        $campania_modal = explode("_", $camp_modal);
        $camp_id =  $campania_modal[0];
        $modal = $campania_modal[1];  
?>        
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
          dataType: 'json',
          type: 'POST',
          url: '<?php echo CController::createUrl('campanias/compartirCampaniaBackend') ?>',
          data: { 
            camp: '<?php echo $camp_id ?>',
            modal: '<?php echo $modal ?>',
            shwepi: 0
          },
          success:function(data){
            //alert('notificado '+data);
            window.close();
            //opener.document
            window.opener.$('#'+data.modal+' .modal-body #flash-share-wepiku').html(data.mensaje);
            window.opener.$('#flash-share-wepiku .modredes').slideToggle(0);  
            window.opener.$('#flash-share-wepiku .modredes').animate(
                    {opacity: 0}, 
                    6000).fadeOut('slow');
          },
          error:function(request){
            alert('Request: '+JSON.stringify(request));
          }
        });
    });      
</script>