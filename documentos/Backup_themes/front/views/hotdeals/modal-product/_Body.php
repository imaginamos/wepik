<div class="modal-header">
<button type="button" class="close close-pro" data-dismiss="modal"></button>
<?php if($bar_admin == 1): ?>
    <div class="admin-actions">
        <div class="caja-iz">
            <?php if($campania['acumulado_piks'] > $campania['cantidad_pik']){ $acumulado_piks = $campania['cantidad_pik'];} else {$acumulado_piks = $campania['acumulado_piks'];} ?>
            <?php if($campania['acumulado_vistas_detalle'] > $campania['cantidad_vistas_detalle']){ $acumulado_vistas = $campania['cantidad_vistas_detalle'];} else {$acumulado_vistas = $campania['acumulado_vistas_detalle'];} ?>
            <?php if($campania['acumulado_recomendaciones'] > $campania['cantidad_recomendaciones']){ $acumulado_recomend = $campania['cantidad_recomendaciones'];} else {$acumulado_recomend = $campania['acumulado_recomendaciones'];} ?>
            <button type="button"><span class="admin-icon1"></span><strong><?php echo $acumulado_piks."/".$campania['cantidad_pik'] ?></strong></button>
            <button type="button"><span class="admin-icon2"></span><strong><?php echo $acumulado_vistas."/".$campania['cantidad_vistas_detalle'] ?></strong></button>
            <button type="button"><span class="admin-icon3"></span><strong><?php echo $campania['acumulado_compartir'] ?></strong></button>
            <button type="button"><span class="admin-icon8"></span><strong><?php echo $acumulado_recomend."/".$campania['cantidad_recomendaciones'] ?></strong></button>
        </div>
        <div class="caja-dr">
            <?php if($estado_campania=='A'): ?>
                <?php echo TbHtml::ajaxButton(
                        '<span class="admin-icon5"></span>Pausar campaña',
                        $this->createUrl('campanias/pausarCampaniaBackendAjax'),
                        array(
                            'dataType' => 'json',
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'm' => $campania['marca_id']
                            ),
                            'success' => 'function(data){
                                $("#modal-product .modal-header #flash-pause-campania").html(data.mensaje);
                                $("#flash-pause-campania .modredes").slideToggle(0);
                                $(".dropdown-menu").css({"display": "none"});
                                $("#flash-pause-campania .modredes").animate({
                                    opacity: 0
                                    }, 4000, function() {
                                            $("#flash-pause-campania .modredes").removeAttr("style");
                                            $(".dropdown-menu").removeAttr("style");
                                            jQuery.yii.submitForm(this,"/marcas/marcaAdminActivas",{"m":data.mid});
                                            return false;
                                    });
                            }'
                        ),
                        array(
                            'id' => 'pause-camp-'.uniqid(),
                            'class' => 'pull-right',
                            'reporte' => 1
                        )
                        ); ?>
            <?php echo TbHtml::ajaxButton(
                        '<span class="admin-icon8"></span>Recomendar',
                        $this->createUrl('campanias/recomendarCampaniaBackendAjax'),
                        array(
                            'dataType' => 'json',
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'm' => $campania['marca_id']
                            ),
                            'success' => 'function(data){
                                $("#modal-product .modal-header #flash-pause-campania").html(data.mensaje);
                                $("#flash-pause-campania .modredes").slideToggle(0);
                                $(".dropdown-menu").css({"display": "none"});
                                $("#flash-pause-campania .modredes").animate({
                                    opacity: 0
                                    }, 4000, function() {
                                        $("#flash-pause-campania .modredes").removeAttr("style");
                                        $(".dropdown-menu").removeAttr("style");
                                        if(parseInt(data.success) === 1){
                                            jQuery.yii.submitForm(this,"/marcas/marcaAdminActivas",{"m":data.mid});
                                            return false;
                                        }else{
                                            jQuery.yii.submitForm(this,"/campanias/plan",{"idcamp":data.campid});
                                            return false;
                                        }
                                    });
                            }'
                        ),
                        array(
                            'id' => 'recommend-camp-'.uniqid(),
                            'class' => 'pull-right',
                            'reporte' => 1
                        )
                    ); ?>
            <?php elseif($estado_campania=='P'): ?>
                <?php echo TbHtml::ajaxButton(
                        '<span class="admin-icon6"></span>Continuar campaña',
                        $this->createUrl('campanias/continuarCampaniaBackendAjax'),
                        array(
                            'dataType' => 'json',
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'm' => $campania['marca_id']
                            ),
                            'success' => 'function(data){
                                $("#modal-product .modal-header #flash-pause-campania").html(data.mensaje);
                                $("#flash-pause-campania .modredes").slideToggle(0);
                                $(".dropdown-menu").css({"display": "none"});
                                $("#flash-pause-campania .modredes").animate({
                                    opacity: 0
                                    }, 4000, function() {
                                        $("#flash-pause-campania .modredes").removeAttr("style");
                                        $(".dropdown-menu").removeAttr("style");
                                        if(parseInt(data.success) === 1){
                                            jQuery.yii.submitForm(this,"/marcas/marcaAdminPausadas",{"m":data.mid});
                                            return false;
                                        }else{
                                            jQuery.yii.submitForm(this,"/campanias/plan",{"idcamp":data.campid});
                                            return false;
                                        }
                                    });
                            }'
                        ),
                        array(
                            'id' => 'continue-camp-'.uniqid(),
                            'class' => 'pull-right',
                            'reporte' => 1
                        )
                        ); ?>
            <?php echo TbHtml::ajaxButton(
                        '<span class="admin-icon9"></span>Terminar campaña',
                        $this->createUrl('campanias/terminarCampaniaBackendAjax'),
                        array(
                            'dataType' => 'json',
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'm' => $campania['marca_id']
                            ),
                            'success' => 'function(data){
                                $("#modal-product .modal-header #flash-pause-campania").html(data.mensaje);
                                $("#flash-pause-campania .modredes").slideToggle(0);
                                $(".dropdown-menu").css({"display": "none"});
                                $("#flash-pause-campania .modredes").animate({
                                    opacity: 0
                                    }, 4000, function() {
                                            $("#flash-pause-campania .modredes").removeAttr("style");
                                            $(".dropdown-menu").removeAttr("style");
                                            jQuery.yii.submitForm(this,"/marcas/MarcaAdminTerminadas",{"m":data.mid});
                                            return false;
                                    });
                            }'
                        ),
                        array(
                            'id' => 'finish-camp-'.uniqid(),
                            'class' => 'pull-left',
                            'reporte' => 1
                        )
                        ); ?>
            <?php elseif($estado_campania=='T'): ?>
                <?php echo CHtml::HtmlButton('<span class="admin-icon7"></span>Reactivar campaña', array('submit' => array('campanias/plan'), 'params'=>array('idcamp'=>$campania['id']), 'class' => 'pull-right', 'id' => 'reactive-camp-'.uniqid())); ?>
            <?php endif; ?>
            <?php echo CHtml::HtmlButton('<span class="admin-icon4"></span>Generar reportes', array('submit' => array('campanias/exportReportToExcel'), 'params'=>array('idcamp'=>$campania['id'], 'tipo'=>$campania['tipo_id']), 'class' => 'pull-right', 'id' => 'report-gen-camp-'.uniqid())); ?>
            <!--<button type="button" class="pull-right"><span class="admin-icon4"></span>Generar reportes</button>-->
        </div>
        <div class="clear"></div>
    </div>
<?php endif; ?>
<div id="flash-pause-campania"></div>
<div class="con-modal-slider">
    <?php if($campania['tipo_id'] == 1 or $campania['tipo_id'] == 4): ?>
        <div class="discount">
                <?php echo $campania['descuento'] ?>
        </div>
    <?php endif; ?>
        <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="8000">
          <ol class="carousel-indicators">       
            <!--
            <li data-target=".carousel-fade" data-slide-to="0" class="active"></li>
            <li data-target=".carousel-fade" data-slide-to="1"></li>
            -->
<!--            CAROL-->

                <script>
                    var key = new Array();
                    var urlvideo = new Array();
                </script>
            <?php foreach ($campania['multimedia'] as $key => $multimedia): ?>
                <?php echo ' <li data-target=".carousel-fade" data-slide-to="'.$key.'"></li>' ?>
                <script>
                    key.push('<?php echo $multimedia['is_video'] ?>');
                    urlvideo.push('<?php echo str_replace("https://www.youtube.com/embed/", "", stripslashes($multimedia['enlace'])); ?>');
                </script>
            <?php endforeach; ?>
          </ol>
          <div class="carousel-inner">
            <?php foreach ($campania['multimedia'] as $key => $multimedia): ?>
                <?php if($key == 0): ?>
                    <div class="item active">
                <?php else: ?>
                    <div class="item">
                <?php endif; ?>
                        <?php if($multimedia['is_video'] == 1): ?>
                            
                            <?php //$code_video = str_replace("https://www.youtube.com/embed/", "https://www.youtube.com/iframe_api", $multimedia['enlace']) ?>
                            <?php $code_video = str_replace("https://www.youtube.com/embed/", "", stripslashes($multimedia['enlace'])); ?>
                            
                            <div class="miyoutube">

                            </div>
                        
                        
                            <div id="player"></div>
                            <?php echo CHtml::hiddenfield('code', $code_video); ?>
                            <?php $campania_id = $campania['id']; ?>
                            <?php $multimedia_id = $multimedia['id']; ?>
                        <?php else: ?>
                            <?php echo CHtml::image($multimedia['enlace'], "", array('class' => 'img-responsive')); ?>
                        <?php endif; ?>
                    </div>
            <?php endforeach; ?>
          </div>
        </div>
</div>
</div>
</div>
<div class="modal-body">
    
<div class="brand-head">
    <?php //echo 'id: '.$campania['id'] ?>
    <?php //echo 'tipo_id: '.$campania['tipo_id'] ?>
        <div class="modal-share menu-redes">
                <?php $link_comp_wepiku = CHtml::ajaxLink(
                                        '<div class="btn_compartir com-wepiku"></div>',
                                        CController::createUrl('campanias/compartirCampaniaBackend'),
                                        array(
                                            'dataType' => 'json',
                                            'type' => 'POST',
                                            'data' => array(
                                                'camp' => $campania['id'],
                                                'shwepi' => 1,
                                                'modal' => 'modal-product'
                                            ),
                                            'success' => 'function(data){
                                                $("#modal-product .modal-body #flash-share-wepiku").html(data.mensaje);
                                                $("#flash-share-wepiku .modredes").slideToggle(0);
                                                $(".dropdown-menu").css({"display": "none"});
                                                $("#flash-share-wepiku .modredes").animate({
                                                    opacity: 0
                                                    }, 6000, function() {
                                                        $("#flash-share-wepiku .modredes").removeAttr("style");
                                                            $(".dropdown-menu").removeAttr("style");
                                                        });
                                            }'
                                        ),
                                        array(
                                            'id' => 'share-link-wepiku'.uniqid()
                                            )
                                    ); ?>
                <?php $link_comp_facebook = CHtml::ajaxLink(
                                        '<div class="btn_compartir com-face"></div>',
                                        CController::createUrl('hotdeals/CompartirCampaniaFacebook'),
                                        array(
                                            'dataType' => 'json',
                                            'type' => 'POST',
                                            'data' => array(
                                                'provider'=>'facebook',
                                                'imagen' => stripslashes($campania['enlace']),
                                                'mensaje' => $campania['nombre'],
                                                'descrip' => $campania['descripcion'],
                                                'marca' => $campania['marca_id'],
                                                'camp' => $campania['id'],
                                                'modal' => 'modal-product'
                                            ),
                                            'success' => 'function(data){
                                                $(".brand-head .modal-share .btn-group").removeClass("open");
                                                $(".brand-head .modal-share .btn-group").children("button").attr("aria-expanded", "false");
                                                var left = (screen.width/2)-(558/2);
                                                var top = (screen.height/2)-(336/2);
                                                window.open(data,
                                                 "feedDialog",
                                                    "toolbar=0,status=0,scrollbars=no,width=558,height=336, top="+top+", left="+left);

                                            }'
                                        ),
                                        array(
                                            'id' => 'share-link-facebook'.uniqid()
                                            )
                                    ); ?>
                <?php $url =
                        Yii::app()->getBaseUrl(true)
//                        'http://www.wepiku.com'
                        . "/hotdeals/index?m=".$campania['marca_id']."&i=".$campania['id'] ?>
                <?php $text = urlencode($campania['nombre'] . ' ' . $url); ?>
                <?php $twit_link = 'https://twitter.com/intent/tweet?'
                        . 'text='.$text.'&' . 'via=wepiku' ?>
                <?php $link_comp_twitter = CHtml::Link(
                                        '<div class="btn_compartir com-twitter"></div>', $twit_link,
                                        array(
                                            'id' => 'share-link-twitter'.uniqid(),
                                            'target' => '_blank'
                                        )
                                    ); ?>
                <?php echo TbHtml::buttonDropdown('<span></span>',
                        array(
                            chtml::tag(
                                'li',
                                array('role' => 'menuitem'),
                                $link_comp_facebook
                            ),
                            chtml::tag(
                                'li',
                                array('role' => 'menuitem'),
                                $link_comp_twitter
                            ),
                            chtml::tag(
                                'li',
                                array('role' => 'menuitem'),
                                $link_comp_wepiku
                            ),
                        ),
                        array('class' => 'btn-share')
                    ); ?>
                <?php if($campania['tipo_id'] == 1 or $campania['tipo_id'] == 4): ?>
                    <?php if($campania['is_piked'] == 0): ?>
                        <?php echo TbHtml::ajaxButton(
                                    '<span></span>',
                                    $this->createurl('hotdeals/darPikAPromoBackendAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id']
                                        ),
                                        'success' => 'function(data){
                                            $(".modal-share .btn-pick").css("background-color","#fbb224");
                                        }'
                                    ),
                                    array('update'=>'.modal-share .btn-pick', 'class' => 'btn-pick')
                                ); ?>
                    <?php elseif($campania['is_piked'] == 1): ?>
                        <?php echo TbHtml::button('<span></span>', array('class' => 'btn-pick', 'style' => 'background-color: #fbb224;')); ?>
                    <?php endif; ?>
                <?php endif; ?>
        </div>
        <?php $multi_enlace= CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class' => 'img-circle', 'width' => '110')); ?>
        <?php echo CHtml::link($multi_enlace, $this->createUrl('marcas/marca?m='.$campania['marca_id']), array('class' => 'brand-logo')); ?>
        <h2><?php echo $campania['nombre']; ?></h2>
</div>
<div id="flash-share-wepiku"></div>
<!--<div class="row modal-main-info">-->
<!--        <div class="col-sm-6">
                <h4>Puntos de venta</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>-->
<!--        <div class="col-sm-2 text-center">
                <a class="go-map" href="">
                        <span></span>
                        <h6>¡Llévame!</h6>
                </a>
        </div>-->
<!--        <div class="col-sm-4 text-center">
                <h4>Disponibilidad</h4>
                <h2>3</h2>
        </div>-->
<!--</div>-->

<!-- Modal -->


<div class="row modal-icons row-table">
    <?php if($campania['tipo_id'] == 1 or $campania['tipo_id'] == 4): ?>
        <div class="col-sm-3 col-cell">
                <span class="icon1"></span>
        </div>
    <?php endif; ?>
    <?php if($campania['tipo_id'] == 1 or $campania['tipo_id'] == 4): ?>
        <div class="col-sm-3 col-cell">
                <span class="icon2"></span>
        </div>
    <?php endif; ?>
        <div class="col-sm-3 col-cell">
                <span class="icon3"></span>
        </div>
        <div class="col-sm-3 col-cell">
                <span class="icon4"></span>
        </div>
</div>
<div class="row modal-icons-info text-center row-table">
    <?php if($campania['tipo_id'] == 1 or $campania['tipo_id'] == 4): ?>
        <div class="col-sm-3 col-cell">
                <h4><?php echo $campania['descuento'] ?></h4>
        </div>
    <?php endif; ?>
        <?php
            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $fecha_final = $campania['fecha_finalizacion'];
            $diaNow = date('d-m-Y');
            $fecha = date_create($fecha_final);
            $diaf = date_format($fecha, 'd-m-Y');
            $diferencia_dias = floor((strtotime($diaf)-strtotime($diaNow))/86400);
        ?>
    <?php if($campania['tipo_id'] == 1 or $campania['tipo_id'] == 4): ?>
        <div class="col-sm-3 col-cell">
                <h5>Quedan</h5>
                <h4><?php echo $diferencia_dias ?> días</h4>
                <h6><?php echo 'Hasta el ' . date_format($fecha, 'd') . " de " . $meses[date_format($fecha, 'n') - 1] . " del " . date_format($fecha, 'Y'); ?></h6>
        </div>
    <?php endif; ?>
        <div class="col-sm-3 col-cell">
                <h5>
                    <?php echo CHtml::link('Visitar web', $marca['pagina_web'], array('target' => '_blank')); ?>
                </h5>
        </div>
        <div class="col-sm-3 col-cell">
            <a href="<?php echo $this->createurl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=3&c='.$campania['id'])?>">
            <h5>¡Llévame!</h5>
            </a>
        </div>
</div>
<h3>Descripcion</h3>
<p>
    <?php echo $campania['descripcion']; ?>
</p>
<h3>Términos y condiciones</h3>
<p>
    <?php echo $campania['texto_oferta']; ?>
</p>
<div id="list-comment">
    <?php $this->renderPartial('_comentarios', array('comentarios' => $comentarios, 'campania_id' => $campania['id'], 'usuario_id_camp' => $campania['usuario_id'], 'logo' => $marca['img_imagenPerfil'], 'nombre_marca' => $marca['nombre'])); ?>
</div>

<div class="comment-area">
        <?php echo CHtml::beginForm(null, 'GET', array (
            'class' => 'modal-comment clearfix',
            ));
        ?>
            <?php echo CHtml::TextArea('com',null, array('class' => 'textarea')) ?>
            <?php echo CHtml::hiddenfield('camp', $campania['id'], array('id' => 'id-camp')); ?>
            <?php echo CHtml::hiddenfield('campuid', $campania['usuario_id']); ?>
            <?php echo CHtml::hiddenfield('logo', $marca['img_imagenPerfil']); ?>
            <?php echo CHtml::hiddenfield('marc', $marca['nombre']); ?>
<span>
            <?php
             echo CHtml::ajaxSubmitButton(
                     '',
                    $this->createurl('hotdeals/comentarCampaniaBackend'),
                    array(
                        'dataType' => 'json',
                        'type' => 'POST',
//                        'cache' => true,
                        'success' => 'function(data){
                            $("#list-comment").html(data);
                            $(".modal-comment #com").val("");
                        }'
                    ),
                    array(
                        'id' => 'submit-comment'.uniqid(),
                        'name' => 'submit-comment'.uniqid(),
//                        'live'=>false,
                        'class' => 'buttom-new-comment',
                        'type'=>'submit'
                    )
             ); ?>
</span>
        <div id="flash-report"></div>
        <?php echo CHtml::endForm(); ?>
</div>
</div>
<div class="modal-footer">
<p>
        Si consideras que este cupón tiene información que

        <?php
            // ajax button to open the modal
            echo TbHtml::ajaxLink(
                'reportar',
                $this->createUrl('hotdeals/nuevoReportePublicacionAjax'),
                array(
                    'dataType' => 'json',
                    'type' => 'POST',
                    'data' => array(
                        'camp' => $campania_id,
                        'marca' => $marca,
                        'imagmulti' => $imagen_multimedia,
                        'modal' => $modal
                    ),
                    'success' => 'function(data){
                        openModal("#modal-report", data.content);
                    }'
                ),
                array(
                    'id' => 'open-modal-'.uniqid()
                )
            );
        ?>
</p>
</div>
<script>
    twttr.ready(function(twttr) {
        $(".brand-head .modal-share .btn-group").removeClass("open");
        $(".brand-head .modal-share .btn-group").children("button").attr("aria-expanded", "false");
        twttr.events.bind("tweet", function() {
            var campid = $("#id-camp").val();
            var mod = 'modal-product';
            $.ajax({
              dataType: 'json',
              type: 'POST',
              url: '<?php echo CController::createUrl('campanias/compartirCampaniaBackend') ?>',
              data: {
                camp: campid,
                modal: mod,
                shwepi: 0
              },
              success:function(data){
                //alert('notificado '+data);
                //opener.document
                $('#'+data.modal+' .modal-body #flash-share-wepiku').html(data.mensaje);
                $('#flash-share-wepiku .modredes').slideToggle(0);
                $('#flash-share-wepiku .modredes').animate(
                    {opacity: 0},
                    6000).fadeOut('slow');
              },
              error:function(request){
                alert('Request: '+JSON.stringify(request));
              }
            });
        });
    });

      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
      tag.src = "//www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      var code_id = $("#code").val();
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '349',
          width: '524',
          videoId: code_id,
          playerVars: {
            controls: 0,
            disablekb: 1
          },
          events: {
            'onStateChange': onPlayerStateChange
          }
        });
      }
      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data === YT.PlayerState.PLAYING && !done) {
          ajaxrequest();
          done = true;
        }
      }
      function ajaxrequest() {
        $.ajax({
          dataType: 'json',
          type: 'POST',
          url: '<?php echo CController::createUrl("hotdeals/notifReproduccionVideoBackend") ?>',
          data: {
            camp: '<?php echo $campania['id'] ?>',
            multi: '<?php echo $multimedia['id'] ?>'
          },
          success:function(){
            // successful request; do something with the data
            //alert('notificado '+data);
          },
          error:function(request){
            alert("Request: "+JSON.stringify(request));
          }
        });
      }
</script>
