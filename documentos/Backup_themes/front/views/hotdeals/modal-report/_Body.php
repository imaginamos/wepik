<div class="modal-header">
<button type="button" class="close close-pro" data-dismiss="modal"></button>
<div class="con-modal-slider modal-header-bg">
        <div class="header-mask"></div>
        <!--<img src="https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xat1/v/t1.0-9/10308768_1006847629326981_4776827276152655950_n.jpg?oh=4c8fdae0ebad916d96adb7d36ee63dcc&oe=562C33A8&__gda__=1442631584_fa27191a7da2d546a2b08b26ea2f1fef" alt="">-->
        <?php echo CHtml::image($imagen_multimedia, ""); ?>
</div>
</div>
<div class="modal-body">
<div id="flash-report"></div>    
<div class="brand-head">
        <?php $enlace= CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class' => 'img-circle', 'width' => '110')); ?>
        <?php echo CHtml::link($enlace, $this->createUrl('marcas/marca?m='.$marca['id']), array('class' => 'brand-logo')); ?>    
        <h2>REPORTAR PUBLICACIÓN</h2>
</div>
<?php echo CHtml::beginForm(null, null, array ('class' => 'form-poll-report clearfix')); ?>     

        <h3>Por que desea reportar esta publicación? :</h3>

        <?php echo CHtml::hiddenfield('camp', $campania_id); ?>    
        <?php echo CHtml::hiddenfield('modal', $modal); ?>  
        <?php foreach($motivos_reporte as $motivo_reporte): ?>
            <fieldset>
                    <?php echo CHtml::radioButton('motivrepo', false, array(
                        'value'=>$motivo_reporte['id'].'+'.$motivo_reporte['descripcion_tipo'],
                        'name'=>'question1', 
                        'uncheckValue'=>null,
                    ),
                    array('class' => 'custom-check')
                    ); ?>                    
                    <?php echo CHtml::Label($motivo_reporte['descripcion_tipo'], 'descripcion'); ?> <!--'style' => "display: inline; margin-right: 20px;"-->                  
            </fieldset>            
        <?php endforeach; ?>
        <hr>
        
    <?php 
     echo CHtml::ajaxSubmitButton(
            'ENVIAR',
            $this->createurl('hotdeals/enviarReporteBackend'),
            array( 
                'dataType' => 'json', 
                'type' => 'POST',
                'success' => 'function(data){
                    if(data.rep == 1){
                        $("#modal-report").modal("hide");
                        $(data.modal+" .modal-body #flash-report").html(data.mensaje);
                        $("#flash-report .modredes").slideToggle(0);  
                        $(".dropdown-menu").css({"display": "none"});
                        $("#flash-report .modredes").animate({
                            opacity: 0
                            }, 5000, function() {
                                $("#flash-report .modredes").removeAttr("style");
                                    $(".dropdown-menu").removeAttr("style");
                                });                     
                    }else{
                        $("#modal-report .modal-body #flash-report").html(data.mensaje);
                        $("#flash-report .modredes").slideToggle(0);  
                        $(".dropdown-menu").css({"display": "none"});
                        $("#flash-report .modredes").animate({
                            opacity: 0
                            }, 5000, function() {
                                $("#flash-report .modredes").removeAttr("style");
                                    $(".dropdown-menu").removeAttr("style");
                                });                     
                    }                   
                }'                
            ),             
            array(
                'id' => 'send-report-form'.uniqid(),
                'class'=>'btn btn-primary',
                'type'=>'button',
                'name'=>'button'
            )
     ); ?>            
<?php echo CHtml::endForm(); ?>
</div>
<div class="modal-footer">
</div>