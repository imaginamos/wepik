<?php
require_once( dirname(__FILE__) . '/../controllers/SiteController.php');
require_once( dirname(__FILE__) . '/../controllers/HotdealsController.php');

class MarcasController extends Controller{

    public $layout='main';

    public function filters()
    {
      return array(
        'accessControl', // perform access control for CRUD operations
      );
    }

    public function accessRules()
    {
        return array(
//            array('allow',  // allow all users to perform 'list' and 'show' actions
//                'actions'=>array('index', 'view'),
//                'users'=>array('*'),
//            ),
            array('allow', // allow authenticated users to perform any action
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function missingAction($actionID)
    {
            // Yii::app()->user->setFlash('warning','This is a test');
            // Yii::app()->user->setFlash('danger','This is a test');
            // Yii::app()->user->setFlash('success','This is a test');

            if($actionID=='' or $actionID=='main.php'):
                    $actionID='marca';
                    $this->layout = 'landing';
            endif;

            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            $this->render(strtr($actionID,array('.php'=>'')));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            $this->render('error', $error);
            }
    }

    public function actionIndex(){

        $this->hasNewsFeed = true;
        $response_marca = array();
        $response_newsfeed = array();
        //$search_cat_id = 0;

        if(isset($_GET['theme']))
            Yii::app()->theme=$_GET['theme'];

        $accion = 'directorio';
        $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);

        if(Yii::app()->session['is_new'] == false):
            $response_newsfeed = HotdealsController::actionNewsFeedBackend();

            foreach ($response_newsfeed['data'] as $newfeed):
                $existe_marca_newsfeed = false;

                foreach ($response_marca as $marca):
                    if($marca == $newfeed['campania']['marca_id']):
                        $existe_marca_newsfeed = true;
                    endif;
                endforeach;
                if(!$existe_marca_newsfeed):
                    $response_marca[$newfeed['campania']['marca_id']] = MarcasController::actionDetalleMarcaBackend($newfeed['campania']['marca_id']);
                endif;
            endforeach;

        endif;
         //print_r($marcas_seguidas);
        $this->render('index', array('marcas_seguidas' => $wepiku_data['marcas_seguidas'], 'categorias' => $wepiku_data['categorias'], 'data_newsfeed_json' => $response_newsfeed, 'data_marca_json' => $response_marca));

    }

    public function actionBuscarMarcas(){

        $query = Yii::app()->getRequest()->getParam('q');
        //$query = 't';
        $result = array();

        $accion = 'directorio';
        $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);

        if(isset($query)):
            foreach ($wepiku_data['marcas_seguidas'] as $key => $marca):
                //echo '<br/>marca '.$key,': '.$marca['nombre'];
                    if(strstr($marca['nombre'], $query)):
                        $result[] = $marca['nombre'];
                    endif;
            endforeach;
            return CJSON::encode($result);
        endif;
    }

    public function actionMarca(){

        $this->hasNewsFeed = true;
        $response_marca = array();

        $this->layout = 'main';
        if(isset($_GET['m'])):
            $response_marca = MarcasController::actionDetalleMarcaBackend($_GET['m']);
        endif;
        $url = $this->url_services() . '/campania/api_campanias?lat=0&lng=0&marca_id='.$_GET['m'];

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $response_hotdeals =(array)$jsondecode;
        $response_newsfeed = HotdealsController::actionNewsFeedBackend();

        $this->render('marca', array('data_marca_json' => $response_marca,'data_hotdeals_json' => $response_hotdeals,'data_newsfeed_json' => $response_newsfeed));
    }

    public function actionNueva(){
        
        $attr_marca = array();
        $images = array();
        $accion = 'crear';
        $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);
        $latlng = array();
        $mensaje = Yii::app()->request->getPost('mensaje');
        $hasDataform = false;
        $img_imagenCover = false;
        $img_imagenPerfil = false;
        $form = new MarcaForm;

        $marca_id = Yii::app()->request->getPost('mid');
        if(isset($marca_id)):
            $marca = MarcasController::actionDetalleMarcaBackend($marca_id);
            $form->nombre_marca = $marca['nombre'];
            $form->desc_marca = $marca['descripcion'];
            $img_imagenCover = $marca['img_imagenCover'];
            $img_imagenPerfil = $marca['img_imagenPerfil'];
            $form->pagina_web = $marca['pagina_web'];

            foreach($marca['urls_facebook'] as $key => $webfacebook):
                $form->pagina_facebook[$key] = $webfacebook;
            endforeach;
            foreach($marca['categorias'] as $key => $categoria):
                $form->categoria[$key]['id'] = $categoria['id'];
                $form->categoria[$key]['nombre'] = $categoria['nombre'];
            endforeach;
            foreach($marca['momentos'] as $key => $momento):
                $form->mom_consumo[$key]['id'] = $momento['id'];
                $form->mom_consumo[$key]['nombre'] = $momento['nombre'];
            endforeach;
            foreach($marca['sucursales'] as $key => $sucursal):
                $form->location[$key] = $sucursal['nombre'];
                $position = implode(",", array($sucursal['map_direccion_lat'], $sucursal['map_direccion_lng']));
                $form->position[$key] = $position;
                $form->address[$key] = $sucursal['map_direccion'];
            endforeach;
            foreach($marca['administradores'] as $key => $admini):
                $form->admin[$key]['name'] = $admini['name'];
                $form->admin[$key]['img'] = $admini['img'];
                $form->admin[$key]['id'] = $admini['id'];
            endforeach;
            $hasDataform = true;
        endif;
//        print_r($marca);
//        print_r($form);

        if(Yii::app()->request->isPostRequest and Yii::app()->request->getPost('MarcaForm')):

            $form->attributes=Yii::app()->request->getPost('MarcaForm');
            if(!is_numeric($form->marcaid)):
                $form->validatorList->add(
                    CValidator::createValidator('file', $form, 'foto_cover, foto_perfil', array('allowEmpty'=>false))
                );
            endif;
            $hasDataform = true;
            if($form->validate()):
                $marcaidedit = $form->marcaid;
                $attr_marca['nombre'] = htmlspecialchars($form->nombre_marca);
                
                  $cad_mar = $form->desc_marca;             
            //filtro los enlaces normales
            $res_cad_mar = preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blanck">$0</a>', $cad_mar);
            //miro si hay enlaces con solamente www, si es así le añado el http://
            $res_cad_mar = preg_replace("/href=\"www/", 'href="http://www', $res_cad_mar);
            
            
                $attr_marca['descripcion'] = nl2br($res_cad_mar);
                
                
                $attr_marca['pagina_web'] = htmlspecialchars($form->pagina_web);
                $attr_marca['facebook_token'] = Yii::app()->session['facebook_token'];

                $pags_facebook = $form->pagina_facebook;
                if(isset($pags_facebook)):
                    foreach($pags_facebook as $key => $pag_facebook):
                         $attr_marca['facebook_links']["'$key'"] = htmlspecialchars($pag_facebook);
                    endforeach;
                else:
                    $attr_marca['facebook_links'] = array();
                endif;
                $sucursales = $form->location;
                if(isset($sucursales)):
                    foreach($sucursales as $key => $sucursal):
                         $attr_marca['sucursales'][$key]['nombre'] = htmlspecialchars($sucursal);
                    endforeach;
                endif;
                $ubicaciones = $form->position;
                if(isset($ubicaciones)):
                    foreach($ubicaciones as $key => $ubicacion):
                        $latlng = explode(",", $ubicacion);
                        $attr_marca['sucursales'][$key]['lat'] = htmlspecialchars($latlng[0]);
                        $attr_marca['sucursales'][$key]['lng'] = htmlspecialchars(str_replace(' ', '', $latlng[1]));
                    endforeach;
                endif;
                $direcciones = $form->address;
                if(isset($direcciones)):
                    foreach($direcciones as $key => $direccion):
                        $attr_marca['sucursales'][$key]['direccion'] = htmlspecialchars($direccion);
                        $attr_marca['sucursales'][$key]['inactiva'] = 0;
                    endforeach;
                endif;
                $borradas = $form->delete;
                if(isset($borradas) and $marcaidedit):
                    foreach($borradas as $key => $borrada):
                        $attr_marca['sucursales'][$key]['eliminada'] = $borrada;
                    endforeach;
                endif;
                $attr_marca['sucursales'][$key]['inactiva'] = 0;

                $categorias = $form->categoria;
                if(isset($categorias)):
                    foreach($categorias as $key => $categoria):
                        $attr_marca['categorias']["'$key'"] = htmlspecialchars($categoria['id']);
                    endforeach;
                endif;
                $momentos_consumo = $form->mom_consumo;
                if(isset($momentos_consumo)):
                    foreach($momentos_consumo as $key => $momento_consumo):
                         $attr_marca['momentos']["'$key'"] = htmlspecialchars($momento_consumo['id']);
                    endforeach;
                endif;
                $admins = $form->admin;
                if(isset($admins)):
                    foreach($admins as $key => $admin):
                        if($key == 0 and $admin['name'] == ''):
                            $attr_marca['administradores'][""] = "";
                        else:
                            $attr_marca['administradores']["'$key'"] = htmlspecialchars($admin['id']);
                        endif;
                    endforeach;
                else:
                    $attr_marca['administradores'][""] = "";
                endif;

                $images['foto_cover'] = CUploadedFile::getInstance($form, 'foto_cover');
                $images['foto_perfil'] = CUploadedFile::getInstance($form, 'foto_perfil');

                if(is_numeric($marcaidedit)):
                    $marca_creada = MarcasController::actionEditar($attr_marca, $marcaidedit);
					
                else:
                    $marca_creada = MarcasController::actionCrear($attr_marca);
					//echo "no";
				
                endif;
           /* print_r($marca_creada);
			exit;*/
//                print_r($attr_marca);
                    
                if(isset($marca_creada['data']['id'])):
                    if(!is_numeric($marcaidedit) or (is_numeric($marcaidedit) and ($images['foto_cover'] or $images['foto_perfil']))):
                        MarcasController::actionEnviarImagenesBackend($marca_creada['data']['id'], $images);
                    endif;
//                    $this->redirect(array('marcaAdminActivas?m='.$marca_creada['data']['id']));
                    $responseData = MarcasController::actionMarcaAdminActivas($marca_creada['data']['id']);

                    $this->render('marca-admin-activas', array(
                        'marca_id' => $marca_creada['data']['id'],'data_marca_json' => $responseData['data_marca_json'],'data_hotdeals_json' => $responseData['data_hotdeals_json'],'saldo'=>$responseData['saldo'],
                    ));
                    Yii::app()->end();
                else:
                    $mensaje = '¡Algo sucedió!, no fué posible crear la Marca. Intente más tarde.';
                endif;
            endif;
        endif;

        $this->render('crear', array(
            'categorias' => $wepiku_data['categorias'],
            'momentos_consumo' => $wepiku_data['momentos_consumo'],
            'amigos' => $wepiku_data['amigos'],
            'form' => $form,
            'mensaje' => $mensaje,
            'hasDataform' => $hasDataform,
            'marca_id' => $marca_id,
            'imagencover' => $img_imagenCover,
            'imagenperfil' => $img_imagenPerfil
        ));
    }

    public function actionCrear($post_marca){

            $json = str_replace("'","",CJSON::encode($post_marca, true));
//            print_r($json);
            $url = $this->url_services() . '/marca/api_marcamarca/create';
            
            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token, 'Content-Type' => 'application/json'))->post($url, $json);
			/*echo "token: ".$wepiku_token."<br>";
			echo "url: ".$url."<br>";
			echo "json: ".$json."<br>";
			exit;*/
            $jsondecode = CJSON::decode($response, true);
		
            $responseData =(array)$jsondecode;
            return $responseData;
    }

    public function actionEditar($post_marca, $marca_id){

            $json = str_replace("'","",CJSON::encode($post_marca, true));
//            print_r($json);
            $url = $this->url_services() . '/marca/api_marcamarca/update?marca_id='.$marca_id;
            
            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token, 'Content-Type' => 'application/json'))->post($url, $json);

            $jsondecode = CJSON::decode($response, true);
            $responseData =(array)$jsondecode;
            return $responseData;
    }

    public function actionEmail() {
        
        $email = Yii::app()->request->getPost('email', false);

        $url = $this->url_services().'/usuario/api_usuarios/actuaizar_mail?email';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        echo Yii::app()->curl->setHeaders(array(
            'Authorization' => $token
        ))->post($url, array('email' => $email));
        header('Content-type: application/json');
//        $this->renderJSON($response);
        Yii::app()->end();
    }

	public static function actionTreaeImgmarca(){
		
		$marca_id = Yii::app()->request->getPost('marca_id', false);
		
		$response_marca = MarcasController::actionDetalleMarcaBackend($marca_id);
		echo $response_marca['img_imagenPerfil'];		
	}
    public static function actionDetalleMarcaBackend($marca_id){

        $url = self::url_services() . '/marca/api_marcamarca/obtener_detalle?marca_id='.$marca_id;

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $responseData =(array)$jsondecode;

        return $responseData['data'];
    }

    public function actionEnviarImagenesBackend($marca_id, $images){

        $post_data = array();
        if ($images === null):
            return;
        endif;
//        print_r($images);
        if(isset($images['foto_cover'])):
            $name_cover = $images['foto_cover']->name;
            $type_cover = $images['foto_cover']->type;
            $path_cover = $images['foto_cover']->tempName;
            $post_data['img_cover'] = new CURLFile($path_cover, $type_cover, $name_cover);
        endif;
        if(isset($images['foto_perfil'])):   
            $name_perfil = $images['foto_perfil']->name;
            $type_perfil = $images['foto_perfil']->type;
            $path_perfil = $images['foto_perfil']->tempName;
            $post_data['img_perfil'] = new CURLFile($path_perfil, $type_perfil, $name_perfil);
        endif;
        
        $url = $this->url_services() . '/marca/api_marcamarca/upload_files?is_marca=1&id='.$marca_id;

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

//        $jsondecode = CJSON::decode($response, true);
//        $responseData =(array)$jsondecode;
//        print_r($responseData);
    }

    public function actionMarcaAdminActivas($id = 0){
		
		$marca_id_new = 0;
        //$marca_id = Yii::app()->request->getPost('m', false);
      
		
		if($id != 0){
			$marca_id = $id;
		}else{
			$marca_id = Yii::app()->request->getPost('m', false);
		}
        if($marca_id_new != 0):
            $marca_id = $marca_id_new;
        endif;

        $response_marca = Array();
//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.min.js' => false
//        );

        if($marca_id):
            $response_marca = MarcasController::actionDetalleMarcaBackend($marca_id);
            $url = $this->url_services() . '/marca/api_marcamarca/obtener_campanias_marca?marca_id='.$marca_id;


            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);
            $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

            $jsondecode = CJSON::decode($response);
            $response_hotdeals =(array)$jsondecode;

            if($marca_id_new != 0):
                return array('marca_id' => $marca_id,'data_marca_json' => $response_marca,'data_hotdeals_json' => $response_hotdeals['data']['campanias_activas'],'saldo'=>$response_hotdeals['data']['saldo']);
            else:
                $this->render('marca-admin-activas', array(
                    'marca_id' => $marca_id,'data_marca_json' => $response_marca,'data_hotdeals_json' => $response_hotdeals['data']['campanias_activas'],'saldo'=>$response_hotdeals['data']['saldo'],
                ));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

     public function actionMarcaAdminPausadas(){

//        $marca_id = Yii::app()->getRequest()->getParam('m', false);
        $marca_id = Yii::app()->request->getPost('m', false);
        $response_marca = Array();
//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.min.js' => false
//        );

        if(isset($marca_id)):
            $response_marca = MarcasController::actionDetalleMarcaBackend($marca_id);
            $url = $this->url_services() . '/marca/api_marcamarca/obtener_campanias_marca?marca_id='.$marca_id;


            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);
            $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

            $jsondecode = CJSON::decode($response);
            $response_hotdeals =(array)$jsondecode;

            $this->render('marca-admin-pausadas', array(
                'marca_id' => $marca_id,'data_marca_json' => $response_marca,'data_hotdeals_json' => $response_hotdeals['data']['campanias_pausadas'],'saldo'=>$response_hotdeals['data']['saldo'],
            ));
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }
     public function actionMarcaAdminTerminadas(){

//        $marca_id = Yii::app()->getRequest()->getParam('m', false);

        $marca_id = Yii::app()->request->getPost('m', false);
     ;
        $response_marca = Array();
//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.min.js' => false
//        );

        if(isset($marca_id)):
            $response_marca = MarcasController::actionDetalleMarcaBackend($marca_id);
    
            $url = $this->url_services() . '/marca/api_marcamarca/obtener_campanias_marca?marca_id='.$marca_id;


            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);
            $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

            $jsondecode = CJSON::decode($response);
            $response_hotdeals =(array)$jsondecode;
           /// print_r($response_hotdeals);
            /*echo "Ok";
            exit;*/
            $this->render('marca-admin-terminadas', array(
                'marca_id' => $marca_id,'data_marca_json' => $response_marca,'data_hotdeals_json' => $response_hotdeals['data']['campanias_finalizadas'],'saldo'=>$response_hotdeals['data']['saldo'],
            ));
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public static function actionGetWepikuDataBackend($accion){

        $facebook_id = Yii::app()->session['facebook_id'];
        $facebook_token = Yii::app()->session['facebook_token'];
        $login = 0;
        $data_usuario = SiteController::actionLoginBackend($facebook_id, $facebook_token, $login);
        $wepiku_data = array(
            'amigos' => array()
        );
                
        if($accion == 'crear'):
            foreach ($data_usuario['data']['wepiku_momentos'] as $momentos):
                $wepiku_data['momentos_consumo'][$momentos['id']] = $momentos['nombre'];
            endforeach;
            foreach ($data_usuario['data']['wepiku_categorias'] as $categorias):
                $wepiku_data['categorias'][$categorias['id']] = $categorias['nombre'];
            endforeach;
            foreach ($data_usuario['data']['facebook_friends'] as $key => $amigos):
                $wepiku_data['amigos'][$key]['id'] = $amigos['usuario_amigo_id'];
                $wepiku_data['amigos'][$key]['value'] = $amigos['nombre'];
                $wepiku_data['amigos'][$key]['img'] = htmlspecialchars($amigos['img']);
            endforeach;
        elseif($accion == 'directorio'):
            $wepiku_data['marcas_seguidas'] = $data_usuario['data']['wepiku_marcas'];

           /*function ordenar_marcas($a, $b ) {
                if ($a['followers'] == $b['followers']) {
                    return 0;
                }
                return ($a['followers'] > $b['followers']) ? -1 : 1;
            }
            usort($wepiku_data['marcas_seguidas'], 'ordenar_marcas');*/

            foreach ($data_usuario['data']['wepiku_categorias'] as $categorias):
                $wepiku_data['categorias'][$categorias['id']] = $categorias;
            endforeach;
            
        elseif($accion == 'directorio_marcas'):
            $wepiku_data['marcas_seguidas'] = $data_usuario['data']['wepiku_marcas'];

            /*function ordenar_marcas($a, $b ) {
                if ($a['followers'] == $b['followers']):
                    return 0;
                endif;

                return ($a['followers'] > $b['followers']) ? -1 : 1;
            }
            usort($wepiku_data['marcas_seguidas'], 'ordenar_marcas');*/
        elseif($accion == 'reporte'):
            foreach ($data_usuario['data']['wepiku_motivos_reporte'] as $key => $motivo_reporte):
                $wepiku_data['motivos_reporte'][$key]['id'] = $motivo_reporte['id'];
                $wepiku_data['motivos_reporte'][$key]['descripcion_tipo'] = $motivo_reporte['descripcion_tipo'];
            endforeach;
        endif;

        return $wepiku_data;
    }

    public function actionGetFriendsAdminAjax(){

        $nombre = Yii::app()->getRequest()->getParam('selected', false);
        $img = Yii::app()->getRequest()->getParam('img', false);
        $id = Yii::app()->getRequest()->getParam('id', false);

        echo CJSON::encode(array(
            'name' => $nombre,
            'img' =>  $img,
            'id' =>  $id
        ));
//        Yii::app()->end();
    }

    public function actionGetMarcasAjax(){
      

        Yii::app()->clientScript->scriptMap=array(
            'jquery.js'=>false,
            'jquery.min.js' => false
        );
        $marcas_seguidas = array();

        $selected_marca = Yii::app()->getRequest()->getParam('seach', false);

        if ($selected_marca and Yii::app()->request->isAjaxRequest):
            $accion = 'directorio_marcas';
            $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);
            foreach($wepiku_data['marcas_seguidas'] as $marca):
                if(stripos($marca['nombre'], $selected_marca)!== false):
                    $marcas_seguidas[$marca['id']] = $marca;
                endif;
            endforeach;
            if(count($marcas_seguidas) == 0):
                echo 'No se han encontrado marcas...';
            else:
                $this->renderPartial('_listamarcas', array('marcas_seguidas' => $marcas_seguidas, 'search' => 1), false, true);
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetMarcasByCategoriaAjax(){

        Yii::app()->clientScript->scriptMap=array(
            'jquery.js'=>false,
            'jquery.min.js' => false
        );
        $marcas_seguidas = array();

        $selected_categoria = Yii::app()->request->getPost('cat', false);

        if ($selected_categoria and Yii::app()->request->isAjaxRequest):
            $accion = 'directorio_marcas';
            $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);
            foreach($wepiku_data['marcas_seguidas'] as $marca):
                if(isset($marca['categorias'])):
                    foreach ($marca['categorias'] as $categoria):
                        if($categoria['id'] == $selected_categoria):
                            $marcas_seguidas[$marca['id']] = $marca;
                        endif;
                    endforeach;
                endif;
            endforeach;
            if(count($marcas_seguidas) == 0):
                echo 'No se han encontrado marcas...';
            else:
                echo CJSON::encode($this->renderPartial('_listamarcas', array('marcas_seguidas' => $marcas_seguidas, 'search' => 1), true, true));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetMarcasByLetraAjax(){

        Yii::app()->clientScript->scriptMap=array(
            'jquery.js'=>false,
            'jquery.min.js' => false
        );
        $marcas_seguidas = array();

        $selected_marca = Yii::app()->getRequest()->getParam('letraselec', false);

        if ($selected_marca and Yii::app()->request->isAjaxRequest):

            $accion = 'directorio_marcas';
            $wepiku_data = MarcasController::actionGetWepikuDataBackend($accion);
            if($selected_marca == 'todo'):
                foreach($wepiku_data['marcas_seguidas'] as $marca):
                    $marcas_seguidas[$marca['id']] = $marca;
                endforeach;
            else:
                foreach($wepiku_data['marcas_seguidas'] as $marca):
                    $posicion_letra = stripos($marca['nombre'], $selected_marca);
                    if($posicion_letra !== false and $posicion_letra == 0):
                        $marcas_seguidas[$marca['id']] = $marca;
                    endif;
                endforeach;
            endif;
            if(count($marcas_seguidas) == 0):
                echo 'No se han encontrado marcas...';
            else:
                $this->renderPartial('_listamarcas', array('marcas_seguidas' => $marcas_seguidas, 'search' => 1), false, true);
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }


    public function actionSetMarcaLikeBackend(){

        $post_data = array();
        $marca_id = Yii::app()->request->getPost('mid', false);
        $like = Yii::app()->request->getPost('l', false);
		
		 if($like == 'false'){
			$like = 1;
		}else if($like == 'true'){
			$like = 0;
		}
		
        if($like == '1'){
			$follow = 0;
		}else if($like == '0'){
            $follow = 1;
		}

        if($marca_id and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap=array(
                'jquery.js'=>false,
                'jquery.min.js' => false
            );
            $url = $this->url_services() . '/usuario/api_usuariolikemarca/add_remove?marca_id='.$marca_id.'&is_followed='.$follow;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

//            $jsondecode = CJSON::decode($response);
//            $wepiku_data =(array)$jsondecode;
//            echo 'like: '.$like;
//            echo 'follow: '.$follow;
//            print_r($wepiku_data);
			echo $marca_id;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public static function actionGetMarcasUsuarioBackend(){

        $responseData = array();
        
        $url = self::url_services() . '/campania/api_campanias/marcas_usuario';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->get($url);

        $jsondecode = CJSON::decode($response);
        $wepiku_data =(array)$jsondecode;
        if(isset($wepiku_data['data'])):
            foreach($wepiku_data['data'] as $marcas):
                $responseData[$marcas['marca_id']] = $marcas['nombre_marca'];
            endforeach;
            return $responseData;
        else:
            return array();
        endif;
    }
}
