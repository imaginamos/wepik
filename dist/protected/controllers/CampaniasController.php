<?php

require_once( dirname(__FILE__) . '/../controllers/HotdealsController.php');
require_once( dirname(__FILE__) . '/../controllers/MarcasController.php');

class CampaniasController extends Controller {

    public $layout = 'main';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
//            array('allow',  // allow all users to perform 'list' and 'show' actions
//                'actions'=>array('index', 'view'),
//                'users'=>array('*'),
//            ),
            array('allow', // allow authenticated users to perform any action
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function missingAction($actionID) {
        // Yii::app()->user->setFlash('warning','This is a test');
        // Yii::app()->user->setFlash('danger','This is a test');
        // Yii::app()->user->setFlash('success','This is a test');

        if ($actionID == '' or $actionID == 'main.php'):
            $actionID = 'index';
            $this->layout = 'landing';
        endif;

        if (isset($_GET['theme']))
            Yii::app()->theme = $_GET['theme'];
        $this->render(strtr($actionID, array('.php' => '')));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if (isset($_GET['theme']))
            Yii::app()->theme = $_GET['theme'];
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionDetalle() {

        $this->render('detalle');
    }
    public function actionPrueba() {

        $this->render('prueba');
    }
    
    
    
    public function actionPagofin() {
      
       $data1 = Yii::app()->request->getPost('data1');
      echo $data1;
      exit;
      //  $this->render('detalle');
    }
    

    /*     * Lista de Promociones que el usuario ha decidido seguir con piks* */

    public function actionPiks() {
//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.min.js' => false
//        );
        $this->hasNewsFeed = true;
        $response_marca = array();

        $this->layout = 'main';
        if (isset($_GET['theme']))
            Yii::app()->theme = $_GET['theme'];

        $responseData = CampaniasController::actionObtenerPiksBackend();

        function ordenar($a, $b) {
            return strtotime($a['fecha_finalizacion']) - strtotime($b['fecha_finalizacion']);
        }

        usort($responseData['data'], 'ordenar');

//        $response_newsfeed = HotdealsController::actionNewsFeedBackend();

        foreach ($responseData['data'] as $dataPik):
            $existe_marca = false;

            foreach ($response_marca as $marca):
                if ($marca == $dataPik['marca_id']):
                    $existe_marca = true;
                endif;
            endforeach;
            if (!$existe_marca):
                $response_marca[$dataPik['marca_id']] = MarcasController::actionDetalleMarcaBackend($dataPik['marca_id']);
            endif;
        endforeach;
        $this->render('piks', array('data_hotdeals_json' => $responseData, 'data_marca_json' => $response_marca));
    }

    public static function actionObtenerPiksBackend() {

        $url = self::url_services() . '/campania/api_campanias/getpiks';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $responseData = (array) $jsondecode;
        return $responseData;
    }
    
    public static function actionPikscont() {

       $url = self::url_services() . '/configuracion/api_configuraciongeneral';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $responseData = (array) $jsondecode;
       
        return $responseData;
    }

    public function actionComo() {
//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.min.js' => false
//        );
        $marcas_usuario = MarcasController::actionGetMarcasUsuarioBackend();
//        $marcas_usuario = array();
        $this->render('como', array(
            'marcas_usuario' => $marcas_usuario,
        ));
    }

    
      //---------------------------------
    
    public function actionVolverAcampania() {
		
		$esfreemium = Yii::app()->request->getPost('esfreemium');
		$img_flo = Yii::app()->request->getPost('img_flo');
		
		

        $coberturas_id = array();
        $saldo = 0;
        $costos_elem = array('cost_elem' => array(), 'tot' => 0);
//        $id_costo = array(0, 1);
        $coverageenc = array();
        $boolmensaje = 0;
        $hasDataform = false;
        $tipo_id = 3;
        $tipo_id_camp = 0;
        $marca_id = null;
        $marcas_usuario = MarcasController::actionGetMarcasUsuarioBackend();
        $free = array('cantidad_dias' => '', 'cantidad_pik' => '', 'cantidad_recomendaciones' => '', 'cantidad_reproducciones' => '', 'cantidad_impresiones' => '', 'cantidad_ingresos_detalle' => '', 'cantidad_encuestas' => '');
        $value_free = array('num_dias_vigencia' => 0, 'num_piks' => 0, 'num_recomendaciones' => 0, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => 0, 'num_encuestas' => 0);
        $costos = array('costo_cpc' => 0, 'costo_cpl' => 0, 'costo_cpa' => 0, 'costo_recomendacion' => 0, 'costo_dia' => 0);
        $freemium = 1;
        $html_preguntas = '';
        $html_respuestas = '';
        $html_respuestas_multi = '';
        $formcamp = array();

        $form = new EncuestaForm;
        $campania_id = Yii::app()->request->getPost('idcamp');
        if (isset($campania_id)):
            $dataEncuestaReactivar = CampaniasController::actionGetEditDataEncuesta($campania_id, $form);

            $form = $dataEncuestaReactivar['form'];
            $coverageenc = $dataEncuestaReactivar['coverageenc'];
            $free = $dataEncuestaReactivar['free'];
            $costos = $dataEncuestaReactivar['costos'];
            $saldo = $dataEncuestaReactivar['saldo'];
            $costos_elem = $dataEncuestaReactivar['costos_elem'];
            $tipo_id = $dataEncuestaReactivar['tipo_id'];
            $num_data = $dataEncuestaReactivar['num_data'];
            $html_preguntas = CampaniasController::actionGetPreguntasFromReactivate($dataEncuestaReactivar['preguntas']);
            $hasDataform = true;
        endif;
        $form->scenario = 'solo_encuesta';

        if (Yii::app()->request->isPostRequest):
            $marca_id = Yii::app()->request->getPost('mid');
            if (isset($marca_id) and is_numeric($marca_id)):

                $form->scenario = 'campania_encuesta';
                $formcamp = new CampaniaForm;
                $campaniaData = Yii::app()->request->getPost('CampaniaForm');
                if ($campaniaData):
                    if (is_array($campaniaData)):
                        $tipo_id_camp = Yii::app()->request->getPost('tipo_id');
                    else:
                        $tipo_id_camp = Yii::app()->request->getPost('tipo_id_camp');
                    endif;
                    if ($tipo_id_camp == 2 or $tipo_id_camp == 5):
                        $formcamp->scenario = 'content';
                    elseif ($tipo_id_camp == 1 or $tipo_id_camp == 4):
                        $formcamp->scenario = 'promotion';
                    endif;
                    if (is_array($campaniaData)):
                        $formcamp->attributes = $campaniaData;
                    else:
                        $formcamp->attributes = CJSON::decode($campaniaData);
                    endif;
                endif;
            endif;
            if (Yii::app()->request->getPost('EncuestaForm') or is_numeric($marca_id)):
                $hasDataform = true;
                $form->attributes = Yii::app()->request->getPost('EncuestaForm');
                $num_data = array('num_dias_vigencia' => $form->dias_vigencia_enc, 'num_encuestas' => $form->encuestas_enc);
                if (isset($form->pais_ciudadenc)):
                    $pais_ciudad = $form->pais_ciudadenc;
                else:
                    $pais_ciudad = 0;
                endif;
                if ($form->scenario == 'solo_encuesta'):
                    if (isset($form->coberturaenc)):
                        $coberturas = $form->coberturaenc;
                        if ($pais_ciudad == 1):
                            foreach ($coberturas as $key => $cobertura):
                                $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                            endforeach;
                        elseif ($pais_ciudad == 2):
                            foreach ($coberturas as $key => $cobertura):
                                $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                            endforeach;
                        endif;
                    endif;
                endif;
                if (isset($marca_id) and is_numeric($marca_id)):
                    $marca_enc = $marca_id;
                else:
                    $marca_enc = $form->marcasenc;
                endif;
                if (isset($marca_enc)):
                    if ($form->is_freemium_enc == 1):
                        $freemium = 0;
                    elseif ($form->is_freemium_enc == 2):
                        $freemium = 1;
                    endif;
                    $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($marca_enc, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                    $coverageenc = $ubicaciones['coberturas'];
                    $free = $ubicaciones['freemium'];
                    if ($freemium == 1):
                        $value_free = $ubicaciones['value_freemium'];
                    elseif ($freemium == 0):
                        $value_free = $num_data;
                    endif;
                    $costos = $ubicaciones['costos'];
                    $saldo = $ubicaciones['saldo'];
                    $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, 0, $tipo_id);
                endif;
				
            endif;
        endif;
		
        $data_encuesta = CampaniasController::actionEstructurarDatosPostEncuesta($form, $freemium, $value_free, $coberturas_id , $fe_ini = "" , $fe_fin = "" , $genero = "");
        CampaniasController::actionPlan($formcamp, $data_encuesta, $tipo_id_camp , $img_flo , $esfreemium );
        Yii::app()->end();
		
    }

    
    //-----------------------------------  
    
    
    public function actionEncuesta() {

//        Yii::app()->clientScript->scriptMap=array(
//            'jquery.js'=>false,
//            'jquery.yii.js' => false
//        );        
  //echo "<script>alert('1');</script>";
        $coberturas_id = array();
        $saldo = 0;
        $costos_elem = array('cost_elem' => array(), 'tot' => 0);
//        $id_costo = array(0, 1);
        $coverageenc = array();
        $boolmensaje = 0;
        $hasDataform = false;
        $tipo_id = 3;
        $tipo_id_camp = 0;
        $marca_id = null;
        $marcas_usuario = MarcasController::actionGetMarcasUsuarioBackend();
        $free = array('cantidad_dias' => '', 'cantidad_pik' => '', 'cantidad_recomendaciones' => '', 'cantidad_reproducciones' => '', 'cantidad_impresiones' => '', 'cantidad_ingresos_detalle' => '', 'cantidad_encuestas' => '');
        $value_free = array('num_dias_vigencia' => 0, 'num_piks' => 0, 'num_recomendaciones' => 0, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => 0, 'num_encuestas' => 0);
        $costos = array('costo_cpc' => 0, 'costo_cpl' => 0, 'costo_cpa' => 0, 'costo_recomendacion' => 0, 'costo_dia' => 0);
        $freemium = 1;
        $html_preguntas = '';
        $html_respuestas = '';
        $html_respuestas_multi = '';
        $formcamp = array();

        $form = new EncuestaForm;
		$img_flo = Yii::app()->request->getPost('img_flo');
		$esfreemium = Yii::app()->request->getPost('esfreemium');
		
		
		
        $campania_id = Yii::app()->request->getPost('idcamp');
        if (isset($campania_id)):
       /// echo "<script>alert('2');</script>";
            $dataEncuestaReactivar = CampaniasController::actionGetEditDataEncuesta($campania_id, $form);

            $form = $dataEncuestaReactivar['form'];
            $coverageenc = $dataEncuestaReactivar['coverageenc'];
            $free = $dataEncuestaReactivar['free'];
            $costos = $dataEncuestaReactivar['costos'];
            $saldo = $dataEncuestaReactivar['saldo'];
            $costos_elem = $dataEncuestaReactivar['costos_elem'];
            $tipo_id = $dataEncuestaReactivar['tipo_id'];
            $num_data = $dataEncuestaReactivar['num_data'];
            $html_preguntas = CampaniasController::actionGetPreguntasFromReactivate($dataEncuestaReactivar['preguntas']);
            $hasDataform = true;
        endif;
        $form->scenario = 'solo_encuesta';
//echo "<script>alert('3');</script>";
        if (Yii::app()->request->isPostRequest):
            $marca_id = Yii::app()->request->getPost('mid');
            if (isset($marca_id) and is_numeric($marca_id)):
//echo "<script>alert('4');</script>";
                $form->scenario = 'campania_encuesta';
                $formcamp = new CampaniaForm;
                $campaniaData = Yii::app()->request->getPost('CampaniaForm');
                if ($campaniaData):
                    if (is_array($campaniaData)):
                        $tipo_id_camp = Yii::app()->request->getPost('tipo_id');
                    else:
                        $tipo_id_camp = Yii::app()->request->getPost('tipo_id_camp');
                    endif;
                    if ($tipo_id_camp == 2 or $tipo_id_camp == 5):
                        $formcamp->scenario = 'content';
                    elseif ($tipo_id_camp == 1 or $tipo_id_camp == 4):
                        $formcamp->scenario = 'promotion';
                    endif;
                    if (is_array($campaniaData)):
                        $formcamp->attributes = $campaniaData;
                    else:
                        $formcamp->attributes = CJSON::decode($campaniaData);
                    endif;
                endif;
            endif;
      //echo "<script>alert('5');</script>";
            if (Yii::app()->request->getPost('EncuestaForm') or is_numeric($marca_id)):
      //echo "<script>alert('6');</script>";
                $hasDataform = true;
                $form->attributes = Yii::app()->request->getPost('EncuestaForm');
                $num_data = array('num_dias_vigencia' => $form->dias_vigencia_enc, 'num_encuestas' => $form->encuestas_enc);
                if (isset($form->pais_ciudadenc)):
                    $pais_ciudad = $form->pais_ciudadenc;
                else:
                    $pais_ciudad = 0;
                endif;
                if ($form->scenario == 'solo_encuesta'):
                    if (isset($form->coberturaenc)):
                        $coberturas = $form->coberturaenc;
                        if ($pais_ciudad == 1):
                            foreach ($coberturas as $key => $cobertura):
                                $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                            endforeach;
                        elseif ($pais_ciudad == 2):
                            foreach ($coberturas as $key => $cobertura):
                                $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                            endforeach;
                        endif;
                    endif;
                endif;
                if (isset($marca_id) and is_numeric($marca_id)):
                    $marca_enc = $marca_id;
                else:
                    $marca_enc = $form->marcasenc;
                endif;
                if (isset($marca_enc)):
                    if ($form->is_freemium_enc == 1):
                        $freemium = 0;
                    elseif ($form->is_freemium_enc == 2):
                        $freemium = 1;
                    endif;
                    $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($marca_enc, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                    $coverageenc = $ubicaciones['coberturas'];
                    $free = $ubicaciones['freemium'];
                    if ($freemium == 1):
                        $value_free = $ubicaciones['value_freemium'];
                    elseif ($freemium == 0):
                        $value_free = $num_data;
                    endif;
                    $costos = $ubicaciones['costos'];
                    $saldo = $ubicaciones['saldo'];
                    $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, 0, $tipo_id);
                   // echo "<script>alert('7');</script>";
                endif;

                if ($form->validate()):
               $boolmensaje = 2;
               
                    if ($form->scenario == 'solo_encuesta'):
					
					
                    $fe_ini = Yii::app()->request->getPost('edad_inicial');
                    $fe_fin = Yii::app()->request->getPost('edad_final');
                    $genero = Yii::app()->request->getPost('sexo');
                    
                        $data_encuesta = CampaniasController::actionEstructurarDatosPostEncuesta($form, $freemium, $value_free, $coberturas_id , $fe_ini , $fe_fin , $genero);
                        $encuesta_creada = CampaniasController::actionCrearCampaniaBackend($data_encuesta, $costos_elem);
						
                            echo "<script>
                             
							
                            location.href='../marcas/marcaAdminActivas/".$form->marcasenc."';
                </script>";
						
                    elseif ($form->scenario == 'campania_encuesta'):
					 
                        $data_encuesta = CampaniasController::actionEstructurarDatosPostEncuesta($form, $freemium, $value_free, $coberturas_id, $fe_ini = "", $fe_fin = "" , $genero = "");
                        CampaniasController::actionPlan($formcamp, $data_encuesta, $tipo_id_camp , $img_flo , $esfreemium);
                        Yii::app()->end();
                    endif;
                    
                   // echo "<script>alert('8');</script>";
//                    print_r($data_encuesta);
//                    print_r($encuesta_creada);
                    /* if (isset($encuesta_creada['data']) and is_numeric($encuesta_creada['data']['id'])):
                      $boolmensaje = 2;
                      else:
                      $boolmensaje = 1;
                      endif; */
                    if (isset($encuesta_creada['data']) and is_numeric($encuesta_creada['data']['id'])):
                   /// echo "<script>alert('9');</script>";
                        if (!$encuesta_creada['data']['is_fremium']) {
                            $estado = true;
                            $i = 0;
                            while ($estado) {
                                $pago_campania = CampaniasController::actionConsultarEstadoPago($encuesta_creada['info_pagos']);
                                $estado_resp = trim($pago_campania['descripcion_estado_pagos']);
                                if ($i == 300 || ($estado_resp != 'Transaccion iniciada' && $estado_resp != 'Transaccion esta en proceso')) {
                                    if ($estado_resp == 'Transaccion aprobada') {
                                        $actualizar = CampaniasController::actionActualizarEstadoCampania($encuesta_creada['data']['id']);
                                        $boolmensaje = 2;
                                        $estado = false;
                                    } else {
                                        $eliminar = CampaniasController::actionEliminarCampania($encuesta_creada['data']['id']);
                                        $boolmensaje = 3;
                                        $estado = false;
                                    }
                                } else {
                                    sleep(10);
                                    if ($i == 0) {
                                        $this->render('como', array(
                                            'marcas_usuario' => array(),
                                        ));
                                    }
                                    $i++;
                                }
                            }
                        } else {
                            $boolmensaje = 2;
                        }
                    else:
                        $boolmensaje = 1;
                    endif;
                else:
                    if (isset($form->pregunta_enc_abierta)):
                        foreach ($form->pregunta_enc_abierta as $pregunta_abierta):
                            $tipo_preg_id = 1;
//                            $numero_preguntas = $form->numero_preguntas_abierta;
                            $html_preguntas .= CampaniasController::actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, null, $pregunta_abierta);
                        endforeach;
                    endif;
                    if (isset($form->pregunta_enc_multiple_unic)):
                        foreach ($form->pregunta_enc_multiple_unic as $numero_preguntas => $pregunta_multiple_unica):
                            $tipo_preg_id = 2;
//                            $numero_preguntas = $form->numero_preguntas_multiple_unic;
//                            echo $key.': '.$pregunta_multiple_unica;
                            foreach ($form->respuesta_enc_multiple_unic[$numero_preguntas] as $key => $respuesta_multiple_unica):
                                $is_correcta = $form->respuesta_enc_multiple_unic_correcta[$numero_preguntas][$key];
                                $html_respuestas .= CampaniasController::actionGetRespuestasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas, $respuesta_multiple_unica, $is_correcta);
                            endforeach;
                            $html_preguntas .= CampaniasController::actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas, $pregunta_multiple_unica, $html_respuestas);
                        endforeach;
                    endif;
                    if (isset($form->pregunta_enc_multiple)):
                        foreach ($form->pregunta_enc_multiple as $numero_preguntas_multi => $pregunta_multiple):
                            $tipo_preg_id = 3;
                            foreach ($form->respuesta_enc_multiple[$numero_preguntas_multi] as $key => $respuesta_multiple):
                                $is_correcta = $form->respuesta_enc_multiple_correcta[$numero_preguntas_multi][$key];
                                $html_respuestas_multi .= CampaniasController::actionGetRespuestasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas_multi, $respuesta_multiple, $is_correcta);
                            endforeach;
                            $html_preguntas .= CampaniasController::actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas_multi, $pregunta_multiple, $html_respuestas_multi);
                        endforeach;
                    endif;
                endif;
            endif;
        endif;
        
      
        $this->render('encuesta', array(
            'value_free' => $value_free,
            'form' => $form,
            'free' => $free,
            'boolmensaje' => $boolmensaje,
            'hasDataform' => $hasDataform,
            'marca_id' => $marca_id,
            'coverageenc' => $coverageenc,
            'marcas_usuario' => $marcas_usuario,
            'costos' => $costos,
            'saldo' => $saldo,
            'costos_elementos' => $costos_elem,
            'tipo_id' => $tipo_id,
            'tipo_id_camp' => $tipo_id_camp,
            'html_preguntas' => $html_preguntas,
            'formcamp' => $formcamp,
            'img_flo' => $img_flo,
            'esfreemium' => $esfreemium
        ));
    }

    public function actionGetPreguntasFromReactivate($preguntas) {

        $numero_preguntas_unica = 0;
        $numero_preguntas_multi = 0;
        $html_preguntas = '';
        $html_respuestas = '';
        $html_respuestas_multi = '';

        foreach ($preguntas as $pregunta):
            if ($pregunta['tipoPregunta']['id'] == 1):
                $pregunta_abierta = $pregunta['pregunta'];
                $tipo_preg_id = 1;
                $html_preguntas .= CampaniasController::actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, null, $pregunta_abierta);
            endif;
            if ($pregunta['tipoPregunta']['id'] == 2):
                $pregunta_multiple_unica = $pregunta['pregunta'];
                $numero_preguntas_unica++;
                $tipo_preg_id = 2;
                foreach ($pregunta['respuestas'] as $key => $respuesta_multiple_unica):
                    $is_correcta = $respuesta_multiple_unica['is_correcta'];
                    $respuesta = $respuesta_multiple_unica['respuesta'];
                    $html_respuestas .= CampaniasController::actionGetRespuestasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas_unica, $respuesta, $is_correcta);
                endforeach;
                $html_preguntas .= CampaniasController::actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas_unica, $pregunta_multiple_unica, $html_respuestas);
            endif;
            if ($pregunta['tipoPregunta']['id'] == 3):
                $pregunta_multiple_multi = $pregunta['pregunta'];
                $numero_preguntas_multi++;
                $tipo_preg_id = 3;
                foreach ($pregunta['respuestas'] as $key => $respuesta_multiple_multi):
                    $is_correcta_multi = $respuesta_multiple_multi['is_correcta'];
                    $respuesta_multi = $respuesta_multiple_multi['respuesta'];
                    $html_respuestas_multi .= CampaniasController::actionGetRespuestasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas_multi, $respuesta_multi, $is_correcta_multi);
                endforeach;
                $html_preguntas .= CampaniasController::actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas_multi, $pregunta_multiple_multi, $html_respuestas_multi);
            endif;
        endforeach;

        return $html_preguntas;
    }

    public function actionGetRespuestasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas, $respuesta_multiple, $is_correcta) {

        $add_check = '';

        if ($tipo_preg_id):

            if ($is_correcta == 1):
                $add_check = '<img src="/themes/front/img/check.png" alt="" height="24px" width="24px">';
            endif;

            if ($tipo_preg_id == 2):
                $html_tipo_pregunta = '<div class="col-xs-12">'
                        . '<div class="row">'
                        . '<div class="col-xs-11 col-sm-9">'
                        . '<p>' . $respuesta_multiple . '</p>'
                        . '<input id="EncuestaForm_respuesta_enc_multiple_unic" class="input" name="EncuestaForm[respuesta_enc_multiple_unic][' . $numero_preguntas . '][]" value="' . $respuesta_multiple . '" type="hidden">'
                        . '</div>'
                        . '<div class="col-xs-11 col-sm-2">'
                        . $add_check
                        . '<input id="EncuestaForm_respuesta_enc_multiple_unic_correcta" name="EncuestaForm[respuesta_enc_multiple_unic_correcta][' . $numero_preguntas . '][]" value="' . $is_correcta . '" type="hidden">'
                        . '</div>'
                        . '<div class="col-xs-1">'
                        . '<button class="btn btn-remove-encuestaurl text-center" type="button"><span></span></button>'
                        . '</div>'
                        . '</div>'
                        . '<span class="br-encuestaurl"></span>'
                        . '</div>';
            elseif ($tipo_preg_id == 3):
                $html_tipo_pregunta = '<div class="col-xs-12">'
                        . '<div class="row">'
                        . '<div class="col-xs-11 col-sm-9">'
                        . '<p>' . $respuesta_multiple . '</p>'
                        . '<input id="EncuestaForm_respuesta_enc_multiple" class="input" name="EncuestaForm[respuesta_enc_multiple][' . $numero_preguntas . '][]" value="' . $respuesta_multiple . '" type="hidden">'
                        . '</div>'
                        . '<div class="col-xs-11 col-sm-2">'
                        . $add_check
                        . '<input id="EncuestaForm_respuesta_enc_multiple_correcta" name="EncuestaForm[respuesta_enc_multiple_correcta][' . $numero_preguntas . '][]" value="' . $is_correcta . '" type="hidden">'
                        . '</div>'
                        . '<div class="col-xs-1">'
                        . '<button class="btn btn-remove-encuestaurl text-center" type="button"><span></span></button>'
                        . '</div>'
                        . '</div>'
                        . '<span class="br-encuestaurl"></span>'
                        . '</div>';
            endif;
            return $html_tipo_pregunta;
        endif;
    }

    public function actionEstructurarDatosPostPreguntas($form) {
        $i = 0;
        $j = 0;
        $attr_encuesta = array();
        $preguntas = array();

        $attr_encuesta["cantidad_encuestas"] = $form->encuestas_enc;

        if (isset($form->pregunta_enc_abierta)):
            foreach ($form->pregunta_enc_abierta as $key => $pregunta_abierta):
                $tipo_preg_id = 1;
                $preguntas[$key]['pregunta'] = $pregunta_abierta;
                $preguntas[$key]['tipo_pregunta_id'] = $tipo_preg_id;
                $preguntas[$key]['respuestas'][$key]['respuesta'] = '';
                $preguntas[$key]['respuestas'][$key]['is_correcta'] = 1;
                $preguntas[$key]['respuestas'][$key]['is_abierta'] = 1;
                $i = $key;
            endforeach;
        endif;
        if (isset($form->pregunta_enc_multiple_unic)):
            foreach ($form->pregunta_enc_multiple_unic as $numero_preguntas => $pregunta_multiple_unica):
                $tipo_preg_id = 2;
                $preguntas[$i + $numero_preguntas]['pregunta'] = $pregunta_multiple_unica;
                $preguntas[$i + $numero_preguntas]['tipo_pregunta_id'] = $tipo_preg_id;
                foreach ($form->respuesta_enc_multiple_unic[$numero_preguntas] as $key => $respuesta_multiple_unica):
                    $preguntas[$i + $numero_preguntas]['respuestas'][$key]['respuesta'] = $respuesta_multiple_unica;
                    $is_correcta = $form->respuesta_enc_multiple_unic_correcta[$numero_preguntas][$key];
                    $preguntas[$i + $numero_preguntas]['respuestas'][$key]['is_correcta'] = $is_correcta;
                    $preguntas[$i + $numero_preguntas]['respuestas'][$key]['is_abierta'] = 0;
                endforeach;
                $j = $i + $numero_preguntas;
            endforeach;
        endif;
        if (isset($form->pregunta_enc_multiple)):
            foreach ($form->pregunta_enc_multiple as $numero_preguntas => $pregunta_multiple):
                $tipo_preg_id = 3;
                $preguntas[$j + $numero_preguntas]['pregunta'] = $pregunta_multiple;
                $preguntas[$j + $numero_preguntas]['tipo_pregunta_id'] = $tipo_preg_id;
                foreach ($form->respuesta_enc_multiple[$numero_preguntas] as $key => $respuesta_multiple):
                    $preguntas[$j + $numero_preguntas]['respuestas'][$key]['respuesta'] = $respuesta_multiple;
                    $is_correcta = $form->respuesta_enc_multiple_correcta[$numero_preguntas][$key];
                    $preguntas[$j + $numero_preguntas]['respuestas'][$key]['is_correcta'] = $is_correcta;
                    $preguntas[$j + $numero_preguntas]['respuestas'][$key]['is_abierta'] = 0;
                endforeach;
            endforeach;
        endif;

        $attr_encuesta['encuesta'] = array(
            "nombre" => $form->nombre_encuesta,
            "descripcion" => $form->desc_campania_enc,
            "preguntas" => $preguntas
        );

        return $attr_encuesta;
    }

    public function actionEstructurarDatosPostEncuesta($form, $freemium, $value_free, $coberturas_id , $fe_ini , $fe_fin , $genero) {

        $attr_encuesta = array();
        $preguntas = array();

        $attr_encuesta['marca_id'] = $form->marcasenc;
        $attr_encuesta['tipo_id'] = 3;
        $attr_encuesta['nombre'] = $form->nombre_encuesta;
        $attr_encuesta['descripcion'] = $form->desc_campania_enc;
        $attr_encuesta['terminos'] = "";
        $attr_encuesta['costo_multimedia_id'] = 1;
        if ($freemium == 1):
            $attr_encuesta['dias_activo'] = $value_free['num_dias_vigencia'];
            $attr_encuesta["dias_vigencia"] = $value_free['num_dias_vigencia'];
            $attr_encuesta["cantidad_encuestas"] = $value_free['num_encuestas'];
        elseif ($freemium == 0):
            $attr_encuesta['dias_activo'] = $form->dias_vigencia_enc;
            $attr_encuesta["dias_vigencia"] = $form->dias_vigencia_enc;
            $attr_encuesta["cantidad_encuestas"] = $form->encuestas_enc;
        endif;
        $attr_encuesta["cantidad_pik"] = 0;
        $attr_encuesta["cantidad_impresiones"] = 0;
        $attr_encuesta["cantidad_recomendaciones"] = 0;
        $attr_encuesta["cantidad_vistas_detalle"] = 0;
        $attr_encuesta["cantidad_reproducciones_video"] = 0;
        $attr_encuesta['is_freemium'] = $freemium;
        $attr_encuesta["is_diamante"] = 0;
        if ($form->pais_ciudadenc == 1):
            $is_pais = 1;
        elseif ($form->pais_ciudadenc == 2):
            $is_pais = 0;
        else:
            $is_pais = null;
        endif;
        $attr_encuesta["is_pais"] = $is_pais;
        $attr_encuesta["descuento"] = '';
        if ($freemium == 1):
            $datetime = new DateTime('now');
            $attr_encuesta["fecha_publicacion"] = $datetime->format('d-m-Y H:i:s');
            $datetime->add(new DateInterval('P' . $attr_encuesta['dias_activo'] . 'D'));
            $attr_encuesta["fecha_finalizacion"] = $datetime->format('d-m-Y H:i:s');
        else:
            $fecha_inicial = new DateTime($form->fini_dias_vigencia_enc);
            $attr_encuesta["fecha_publicacion"] = $fecha_inicial->format('d-m-Y H:i:s');
            $fecha_final = new DateTime($form->ffin_dias_vigencia_enc);
            $attr_encuesta["fecha_finalizacion"] = $fecha_final->format('d-m-Y H:i:s');
        endif;
        $attr_encuesta['edad_desde'] = $fe_ini;
        $attr_encuesta['edad_hasta'] = $fe_fin;
        $attr_encuesta['genero'] = $genero;
        $attr_encuesta['sucursales'][] = array();
        if ($is_pais == 1):
            foreach ($coberturas_id as $key => $cobertura_id):
                $attr_encuesta['ciudades'][] = array();
                $attr_encuesta['paises']["'$key'"] = $cobertura_id;
            endforeach;
        elseif ($is_pais == 0):
            foreach ($coberturas_id as $key => $cobertura_id):
                $attr_encuesta['ciudades']["'$key'"] = $cobertura_id;
                $attr_encuesta['paises'][] = array();
            endforeach;
        endif;
        $i = 0;
        $j = 0;
        if (isset($form->pregunta_enc_abierta)):
            foreach ($form->pregunta_enc_abierta as $key => $pregunta_abierta):
                $tipo_preg_id = 1;
                $preguntas[$key]['pregunta'] = $pregunta_abierta;
                $preguntas[$key]['tipo_pregunta_id'] = $tipo_preg_id;
                $preguntas[$key]['respuestas'][$key]['respuesta'] = '';
                $preguntas[$key]['respuestas'][$key]['is_correcta'] = 1;
                $preguntas[$key]['respuestas'][$key]['is_abierta'] = 1;
                $i = $key;
            endforeach;
        endif;
        if (isset($form->pregunta_enc_multiple_unic)):
            foreach ($form->pregunta_enc_multiple_unic as $numero_preguntas => $pregunta_multiple_unica):
                $tipo_preg_id = 2;
                $preguntas[$i + $numero_preguntas]['pregunta'] = $pregunta_multiple_unica;
                $preguntas[$i + $numero_preguntas]['tipo_pregunta_id'] = $tipo_preg_id;
                foreach ($form->respuesta_enc_multiple_unic[$numero_preguntas] as $key => $respuesta_multiple_unica):
                    $preguntas[$i + $numero_preguntas]['respuestas'][$key]['respuesta'] = $respuesta_multiple_unica;
                    $is_correcta = $form->respuesta_enc_multiple_unic_correcta[$numero_preguntas][$key];
                    $preguntas[$i + $numero_preguntas]['respuestas'][$key]['is_correcta'] = $is_correcta;
                    $preguntas[$i + $numero_preguntas]['respuestas'][$key]['is_abierta'] = 0;
                endforeach;
                $j = $i + $numero_preguntas;
            endforeach;
        endif;
        if (isset($form->pregunta_enc_multiple)):
            foreach ($form->pregunta_enc_multiple as $numero_preguntas => $pregunta_multiple):
                $tipo_preg_id = 3;
                $preguntas[$j + $numero_preguntas]['pregunta'] = $pregunta_multiple;
                $preguntas[$j + $numero_preguntas]['tipo_pregunta_id'] = $tipo_preg_id;
                foreach ($form->respuesta_enc_multiple[$numero_preguntas] as $key => $respuesta_multiple):
                    $preguntas[$j + $numero_preguntas]['respuestas'][$key]['respuesta'] = $respuesta_multiple;
                    $is_correcta = $form->respuesta_enc_multiple_correcta[$numero_preguntas][$key];
                    $preguntas[$j + $numero_preguntas]['respuestas'][$key]['is_correcta'] = $is_correcta;
                    $preguntas[$j + $numero_preguntas]['respuestas'][$key]['is_abierta'] = 0;
                endforeach;
            endforeach;
        endif;
        $attr_encuesta['encuesta'] = array(
            "nombre" => $attr_encuesta['nombre'],
            "descripcion" => $attr_encuesta['descripcion'],
            "preguntas" => $preguntas
        );
        $attr_encuesta['multimedia'] = array();

        return $attr_encuesta;
    }

    public function actionPlanEdit($formcamp = array(), $data_encuesta = array(), $tipo_id_camp = 0 , $img_flo = "" , $esfreemium ="") {
				$prueba = 0;	
        $attr_campania = array();
        $coverage = array();
        $subsidiary = array();
        $coberturas_id = array();
        $imagen_tamano = array();
        $img_perfil = array();
        $saldo = 0;
        $costos_elem = array('cost_elem' => array(), 'tot' => 0);
        $id_costo = array(0, 1);
        $tipo_id = 2;
        $marcas_usuario = MarcasController::actionGetMarcasUsuarioBackend();

        $boolmensaje = 0;
        $hasDataform = false;
        $free = array('cantidad_dias' => '', 'cantidad_pik' => '', 'cantidad_recomendaciones' => '', 'cantidad_reproducciones' => '', 'cantidad_impresiones' => '', 'cantidad_ingresos_detalle' => '', 'cantidad_encuestas' => '');
        $value_free = array('num_dias_vigencia' => 0, 'num_piks' => 0, 'num_recomendaciones' => 0, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => 0, 'num_encuestas' => 0);
        $value_free2 = $value_free;
        $costos = array('costo_cpc' => 0, 'costo_cpl' => 0, 'costo_cpa' => 0, 'costo_recomendacion' => 0, 'costo_dia' => 0);
        $freemium = 1;

        $form = new CampaniaForm;
        $campania_id = Yii::app()->request->getPost('idcamp');
        if (Yii::app()->request->getPost('tipo_id_camp')):
            $tipo_id_camp = Yii::app()->request->getPost('tipo_id_camp');
        endif;
        if (Yii::app()->request->getPost('dataEncuesta')):
            $data_encuesta = CJSON::decode(Yii::app()->request->getPost('dataEncuesta'));
        endif;
        if (isset($campania_id)):
		//$eliminar = CampaniasController::actionEliminarCampania($campania_id);
            $dataCampaniaReactivar = CampaniasController::actionGetEditDataCampania($campania_id, $form);
			
			
			
            $form = $dataCampaniaReactivar['form'];
            $coverage = $dataCampaniaReactivar['coverage'];
            $subsidiary = $dataCampaniaReactivar['subsidiary'];
            $free = $dataCampaniaReactivar['free'];
            $imagen_tamano = $dataCampaniaReactivar['imagen_tamano'];
            $costos = $dataCampaniaReactivar['costos'];
            $saldo = $dataCampaniaReactivar['saldo'];
            $costos_elem = $dataCampaniaReactivar['costos_elem'];
            $tipo_id = $dataCampaniaReactivar['tipo_id'];
            if ($tipo_id == 2 or $tipo_id == 5):
                $num_data = $dataCampaniaReactivar['num_data'];
            elseif ($tipo_id == 1 or $tipo_id == 4):
                $num_data2 = $dataCampaniaReactivar['num_data'];
            endif;
            $img_perfil = $dataCampaniaReactivar['img_perfil'];
            $hasDataform = true;
			
			
        endif;

        if ((Yii::app()->request->isPostRequest and Yii::app()->request->getPost('CampaniaForm')) or $formcamp):
            $hasDataform = true;
            if ($tipo_id_camp != 0):
                if ($tipo_id_camp == 1):
                    $tipo_id = 4;
                elseif ($tipo_id_camp == 2):
                    $tipo_id = 5;
                endif;
            else:
                $tipo_id = Yii::app()->request->getPost('tipo_id');
            endif;
            if ($tipo_id == 2 or $tipo_id == 5):
                $form->scenario = 'content';
            elseif ($tipo_id == 1 or $tipo_id == 4):
                $form->scenario = 'promotion';
            endif;
            if ($formcamp):
                $form = $formcamp;
            else:
                $form->attributes = Yii::app()->request->getPost('CampaniaForm');
            endif;
//            if($tipo_id_camp != 0):
//            echo 'tipo_camp: '.$tipo_id;
//            print_r($data_encuesta);
//            print_r($form);
//        endif;

            if ($form->scenario == 'content'):
                if ($tipo_id_camp != 0):
                    $num_data = array('num_dias_vigencia' => $form->dias_vigencia, 'num_piks' => $form->piks, 'num_recomendaciones' => $form->recomendaciones, 'num_visualizaciones' => $form->visualizaciones, 'num_impresiones' => $form->impresiones, 'num_ingresos' => $form->ingresos, 'num_encuestas' => $data_encuesta['cantidad_encuestas']);
                else:
                    $num_data = array('num_dias_vigencia' => $form->dias_vigencia, 'num_piks' => $form->piks, 'num_recomendaciones' => $form->recomendaciones, 'num_visualizaciones' => $form->visualizaciones, 'num_impresiones' => $form->impresiones, 'num_ingresos' => $form->ingresos);
                endif;
                if (isset($form->pais_ciudad)):
                    $pais_ciudad = $form->pais_ciudad;
                else:
                    $pais_ciudad = 0;
                endif;
                if (isset($form->cobertura)):
                    $coberturas = $form->cobertura;
                    if ($pais_ciudad == 1):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    elseif ($pais_ciudad == 2):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    endif;
                endif;

                if (isset($form->marcas) and isset($form->is_freemium)):
                    if ($form->is_freemium == 1):
                        $freemium = 0;
                    elseif ($form->is_freemium == 2):
                        $freemium = 1;
                    endif;
                    if ($form->imagen_tamano != 0):
                        $id_costo = explode("+", $form->imagen_tamano);
                        $costo_multi = $id_costo[1];
                    else:
                        $costo_multi = 1;
                    endif;
                    if ($freemium == 1):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $ubicaciones['value_freemium'];
                        $value_free2 = $ubicaciones['value_freemium'];
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, $costo_multi, $tipo_id);
                    elseif ($freemium == 0):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $num_data;
                        $value_free2 = $ubicaciones['value_freemium'];
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, $costo_multi, $tipo_id);
                    endif;
                    foreach ($multimedia as $multi):
                        $imagen_tamano[$multi['id'] . '+' . $multi['costo']] = $multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')';
                    endforeach;
                endif;
            elseif ($form->scenario == 'promotion'):
                if ($tipo_id_camp != 0):
                    $num_data2 = array('num_dias_vigencia' => $form->dias_vigencia2, 'num_piks' => $form->piks2, 'num_recomendaciones' => $form->recomendaciones2, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => $form->ingresos2, 'num_encuestas' => $data_encuesta['cantidad_encuestas']);
                else:
                    $num_data2 = array('num_dias_vigencia' => $form->dias_vigencia2, 'num_piks' => $form->piks2, 'num_recomendaciones' => $form->recomendaciones2, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => $form->ingresos2);
                endif;
                if (isset($form->pais_ciudadb)):
                    $pais_ciudad = $form->pais_ciudadb;
                else:
                    $pais_ciudad = 0;
                endif;
                if (isset($form->cobertura2)):
                    $coberturas = $form->cobertura2;
                    if ($pais_ciudad == 1):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    elseif ($pais_ciudad == 2):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    endif;
                endif;
                if (isset($form->marcas) and isset($form->is_freemium2)):
                    if ($form->is_freemium2 == 1):
                        $freemium = 0;
                    elseif ($form->is_freemium2 == 2):
                        $freemium = 1;
                    endif;
                    if ($form->imagen_tamano2 != 0):
                        $id_costo = explode("+", $form->imagen_tamano2);
                        $costo_multi = $id_costo[1];
                    else:
                        $costo_multi = 1;
                    endif;
                    if ($freemium == 1):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $ubicaciones['value_freemium'];
                        $value_free2 = $ubicaciones['value_freemium'];
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free2, $costos, $freemium, $costo_multi, $tipo_id);
                    elseif ($freemium == 0):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $ubicaciones['value_freemium'];
                        $value_free2 = $num_data2;
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free2, $costos, $freemium, $costo_multi, $tipo_id);
                    endif;
                    foreach ($multimedia as $multi):
                        $imagen_tamano[$multi['id'] . '+' . $multi['costo']] = $multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')';
                    endforeach;
                endif;
            endif;
            if ($form->validate()):
                $attr_campania['marca_id'] = htmlspecialchars($form->marcas);
                $attr_campania['tipo_id'] = htmlspecialchars($tipo_id);
                $attr_campania['nombre'] = htmlspecialchars($form->nombre_campania);
                if ($form->scenario == 'content'):
                    $imgs_campania = CUploadedFile::getInstances($form, 'imagen_camp');
                    $data_campania = CampaniasController::actionEstructurarDatosPostContent($attr_campania, $form, $freemium, $value_free, $coberturas_id, $imgs_campania, $data_encuesta);
                elseif ($form->scenario == 'promotion'):
         
                    $imgs_campania = CUploadedFile::getInstances($form, 'imagen_camp2');
                    $data_campania = CampaniasController::actionEstructurarDatosPostPromotion($attr_campania, $form, $freemium, $value_free2, $coberturas_id, $imgs_campania, $data_encuesta);
                endif;

                $campania_creada = CampaniasController::actionCrearCampaniaBackend($data_campania, $costos_elem);
//                print_r($campania_creada);
//                print_r($data_campania);

                if (isset($campania_creada['data']) and is_numeric($campania_creada['data']['id'])):
                    foreach ($imgs_campania as $img_campania):
                        //CampaniasController::actionEnviarImagenesBackend($campania_creada['data']['id'], $img_campania);}
                        if($form->imagen_tamano){
                           CampaniasController::actionEnviarImagenesBackend($campania_creada['data']['id'], $img_campania , $form->imagen_tamano);
                        }else{
                           CampaniasController::actionEnviarImagenesBackend($campania_creada['data']['id'], $img_campania , $form->imagen_tamano2);
                        }
                         
                          
                    endforeach;
                    if (!$campania_creada['data']['is_fremium']) {
                        $estado = true;
                        $i = 0;
                        while ($estado) {
                            $pago_campania = CampaniasController::actionConsultarEstadoPago($campania_creada['info_pagos']);
//                            $estado_resp = trim($pago_campania['descripcion_estado_pagos']);
                            $estado_resp = "Transaccion iniciada"; // BORRAR!
                            if ($i == 300 || ($estado_resp != 'Transaccion iniciada' && $estado_resp != 'Transaccion esta en proceso')) {
                                if ($estado_resp == 'Transaccion aprobada') {
                                    $actualizar = CampaniasController::actionActualizarEstadoCampania($campania_creada['data']['id']);
                                    $boolmensaje = 2;
                                    $estado = false;
									
                                } else {
                                    $eliminar = CampaniasController::actionEliminarCampania($encuesta_creada['data']['id']);
                                    $boolmensaje = 3;
                                    $estado = false;
									
                                }
                            } else {
                                //sleep(10);
                                if ($i == 0) {
                                    $this->render('como', array(
                                        'marcas_usuario' => array(),
                                    ));
                                }
                                $i++;
                            }
                        }
                    } else {
                        $boolmensaje = 2;
                    }
                else:
                    $boolmensaje = 1;
                endif;
            endif;
        endif;


        $this->render('plan', array(
            'coverage' => $coverage,
            'subsidiary' => $subsidiary,
            'marcas_usuario' => $marcas_usuario,
            'form' => $form,
            'boolmensaje' => $boolmensaje,
            'hasDataform' => $hasDataform,
            'free' => $free,
            'value_free' => $value_free,
            'value_free2' => $value_free2,
            'imagen_tamano' => $imagen_tamano,
            'costos' => $costos,
            'saldo' => $saldo,
            'costos_elementos' => $costos_elem,
            'tipo_id' => $tipo_id,
            'imagenesperfil' => $img_perfil,            
            'data_encuesta' => $data_encuesta,
			'idcamp' => $campania_id,
            'tipo_id_camp' => $tipo_id_camp,
			'img_flo' => $img_flo,
			'esfreemium' => $esfreemium
			
        ));
		sleep(5);
		$eliminar = CampaniasController::actionEliminarCampania($campania_id);
		
	}
    public function actionPlan($formcamp = array(), $data_encuesta = array(), $tipo_id_camp = 0 , $img_flo = 0 , $esfreemium = 0) {
		
		
		$prueba = 0;	
        $attr_campania = array();
        $coverage = array();
        $subsidiary = array();
        $coberturas_id = array();
        $imagen_tamano = array();
        $img_perfil = array();
        $saldo = 0;
        $costos_elem = array('cost_elem' => array(), 'tot' => 0);
        $id_costo = array(0, 1);
        $tipo_id = 2;
        $marcas_usuario = MarcasController::actionGetMarcasUsuarioBackend();

        $boolmensaje = 0;
        $hasDataform = false;
        $free = array('cantidad_dias' => '', 'cantidad_pik' => '', 'cantidad_recomendaciones' => '', 'cantidad_reproducciones' => '', 'cantidad_impresiones' => '', 'cantidad_ingresos_detalle' => '', 'cantidad_encuestas' => '');
        $value_free = array('num_dias_vigencia' => 0, 'num_piks' => 0, 'num_recomendaciones' => 0, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => 0, 'num_encuestas' => 0);
        $value_free2 = $value_free;
        $costos = array('costo_cpc' => 0, 'costo_cpl' => 0, 'costo_cpa' => 0, 'costo_recomendacion' => 0, 'costo_dia' => 0);
        $freemium = 1;

			 //echo "<script>alert('10');</script>";
	
        $form = new CampaniaForm;
		
		 
        $campania_id = Yii::app()->request->getPost('idcamp');
        if (Yii::app()->request->getPost('tipo_id_camp')):
            $tipo_id_camp = Yii::app()->request->getPost('tipo_id_camp');
        endif;
        if (Yii::app()->request->getPost('dataEncuesta')):
            $data_encuesta = CJSON::decode(Yii::app()->request->getPost('dataEncuesta'));
        endif;
		
		// echo "<script>alert('9');</script>";
        if (isset($campania_id)):
	
            $dataCampaniaReactivar = CampaniasController::actionGetEditDataCampania($campania_id, $form);
				
	
			
			
            $form = $dataCampaniaReactivar['form'];
            $coverage = $dataCampaniaReactivar['coverage'];
            $subsidiary = $dataCampaniaReactivar['subsidiary'];
            $free = $dataCampaniaReactivar['free'];
            $imagen_tamano = $dataCampaniaReactivar['imagen_tamano'];
            $costos = $dataCampaniaReactivar['costos'];
            $saldo = $dataCampaniaReactivar['saldo'];
            $costos_elem = $dataCampaniaReactivar['costos_elem'];
            $tipo_id = $dataCampaniaReactivar['tipo_id'];
            if ($tipo_id == 2 or $tipo_id == 5):
                $num_data = $dataCampaniaReactivar['num_data'];
            elseif ($tipo_id == 1 or $tipo_id == 4):
                $num_data2 = $dataCampaniaReactivar['num_data'];
            endif;
            $img_perfil = $dataCampaniaReactivar['img_perfil'];
            $hasDataform = true;
			
			
        endif;
     //  echo "<script>alert('8');</script>";
        if ((Yii::app()->request->isPostRequest and Yii::app()->request->getPost('CampaniaForm')) or $formcamp):
            $hasDataform = true;
            if ($tipo_id_camp != 0):
                if ($tipo_id_camp == 1):
                    $tipo_id = 4;
                elseif ($tipo_id_camp == 2):
                    $tipo_id = 5;
                endif;
            else:
                $tipo_id = Yii::app()->request->getPost('tipo_id');
            endif;
            if ($tipo_id == 2 or $tipo_id == 5):
                $form->scenario = 'content';
            elseif ($tipo_id == 1 or $tipo_id == 4):
                $form->scenario = 'promotion';
            endif;
            if ($formcamp):
                $form = $formcamp;
            else:
                $form->attributes = Yii::app()->request->getPost('CampaniaForm');
            endif;
//            if($tipo_id_camp != 0):
//            echo 'tipo_camp: '.$tipo_id;
//            print_r($data_encuesta);
//            print_r($form);
//        endif;
           /// echo "<script>alert('7');</script>";
            if ($form->scenario == 'content'):
                if ($tipo_id_camp != 0):
                    $num_data = array('num_dias_vigencia' => $form->dias_vigencia, 'num_piks' => $form->piks, 'num_recomendaciones' => $form->recomendaciones, 'num_visualizaciones' => $form->visualizaciones, 'num_impresiones' => $form->impresiones, 'num_ingresos' => $form->ingresos, 'num_encuestas' => $data_encuesta['cantidad_encuestas']);
                else:
                    $num_data = array('num_dias_vigencia' => $form->dias_vigencia, 'num_piks' => $form->piks, 'num_recomendaciones' => $form->recomendaciones, 'num_visualizaciones' => $form->visualizaciones, 'num_impresiones' => $form->impresiones, 'num_ingresos' => $form->ingresos);
                endif;
                if (isset($form->pais_ciudad)):
                    $pais_ciudad = $form->pais_ciudad;
                else:
                    $pais_ciudad = 0;
                endif;
                if (isset($form->cobertura)):
                    $coberturas = $form->cobertura;
                    if ($pais_ciudad == 1):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    elseif ($pais_ciudad == 2):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    endif;
                endif;
              //  echo "<script>alert('6');</script>";
                if (isset($form->marcas) and isset($form->is_freemium)):
                    if ($form->is_freemium == 1):
                        $freemium = 0;
                    elseif ($form->is_freemium == 2):
                        $freemium = 1;
                    endif;
                    if ($form->imagen_tamano != 0):
                        $id_costo = explode("+", $form->imagen_tamano);
                        $costo_multi = $id_costo[1];
                    else:
                        $costo_multi = 1;
                    endif;
                   // echo "<script>alert('5');</script>";
                    if ($freemium == 1):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $ubicaciones['value_freemium'];
                        $value_free2 = $ubicaciones['value_freemium'];
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, $costo_multi, $tipo_id);
                    elseif ($freemium == 0):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $num_data;
                        $value_free2 = $ubicaciones['value_freemium'];
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, $costo_multi, $tipo_id);
                    endif;
                    foreach ($multimedia as $multi):
                        $imagen_tamano[$multi['id'] . '+' . $multi['costo']] = $multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')';
                    endforeach;
                endif;
            elseif ($form->scenario == 'promotion'):
                if ($tipo_id_camp != 0):
                    $num_data2 = array('num_dias_vigencia' => $form->dias_vigencia2, 'num_piks' => $form->piks2, 'num_recomendaciones' => $form->recomendaciones2, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => $form->ingresos2, 'num_encuestas' => $data_encuesta['cantidad_encuestas']);
                else:
                    $num_data2 = array('num_dias_vigencia' => $form->dias_vigencia2, 'num_piks' => $form->piks2, 'num_recomendaciones' => $form->recomendaciones2, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => $form->ingresos2);
                endif;
                if (isset($form->pais_ciudadb)):
                    $pais_ciudad = $form->pais_ciudadb;
                else:
                    $pais_ciudad = 0;
                endif;
                if (isset($form->cobertura2)):
                    $coberturas = $form->cobertura2;
                    if ($pais_ciudad == 1):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    elseif ($pais_ciudad == 2):
                        foreach ($coberturas as $key => $cobertura):
                            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                        endforeach;
                    endif;
                endif;
                if (isset($form->marcas) and isset($form->is_freemium2)):
                    if ($form->is_freemium2 == 1):
                        $freemium = 0;
                    elseif ($form->is_freemium2 == 2):
                        $freemium = 1;
                    endif;
                    if ($form->imagen_tamano2 != 0):
                        $id_costo = explode("+", $form->imagen_tamano2);
                        $costo_multi = $id_costo[1];
                    else:
                        $costo_multi = 1;
                    endif;
                    //echo "<script>alert('4');</script>";
                    if ($freemium == 1):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $ubicaciones['value_freemium'];
                        $value_free2 = $ubicaciones['value_freemium'];
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free2, $costos, $freemium, $costo_multi, $tipo_id);
                    elseif ($freemium == 0):
                        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $pais_ciudad, $coberturas_id, $freemium, $tipo_id);
                        $coverage = $ubicaciones['coberturas'];
                        $subsidiary = $ubicaciones['sucursales'];
                        $free = $ubicaciones['freemium'];
                        $value_free = $ubicaciones['value_freemium'];
                        $value_free2 = $num_data2;
                        $multimedia = $ubicaciones['multimedia'];
                        $costos = $ubicaciones['costos'];
                        $saldo = $ubicaciones['saldo'];
                        //echo "<script>alert('3');</script>";
                        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free2, $costos, $freemium, $costo_multi, $tipo_id);
                    endif;
                    foreach ($multimedia as $multi):
                        $imagen_tamano[$multi['id'] . '+' . $multi['costo']] = $multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')';
                    endforeach;
                endif;
            endif;
           // echo "<script>alert('2');</script>";
            if ($form->validate()):
                $attr_campania['marca_id'] = htmlspecialchars($form->marcas);
                $attr_campania['tipo_id'] = htmlspecialchars($tipo_id);
                $attr_campania['nombre'] = htmlspecialchars($form->nombre_campania);
                if ($form->scenario == 'content'):
                    $imgs_campania = CUploadedFile::getInstances($form, 'imagen_camp');
                  
                    $data_campania = CampaniasController::actionEstructurarDatosPostContent($attr_campania, $form, $freemium, $value_free, $coberturas_id, $imgs_campania, $data_encuesta);
                elseif ($form->scenario == 'promotion'):
                    $imgs_campania = CUploadedFile::getInstances($form, 'imagen_camp2');
                    $data_campania = CampaniasController::actionEstructurarDatosPostPromotion($attr_campania, $form, $freemium, $value_free2, $coberturas_id, $imgs_campania, $data_encuesta);
                endif;
             $estado = true;
                        $i = 0;
                      
               $campania_creada = CampaniasController::actionCrearCampaniaBackend($data_campania, $costos_elem);
         
                if (isset($campania_creada['data']) and is_numeric($campania_creada['data']['id'])):
               
                    foreach ($imgs_campania as $img_campania):
                    
                    if($form->imagen_tamano){
                        CampaniasController::actionEnviarImagenesBackend($campania_creada['data']['id'], $img_campania , $form->imagen_tamano);
                    }else{
                        CampaniasController::actionEnviarImagenesBackend($campania_creada['data']['id'], $img_campania , $form->imagen_tamano2);
                    }
                      
                    endforeach;
                 
                    if (!$campania_creada['data']['is_fremium']) {
                      
                       echo "<div class='cont-rdb' style='width:288px;  margin:0 auto; margin-top: 187px;'>
                          <img src='http://www.wepiku.com/themes/front/img/redeban-logo.png' style='width: 100%;'><br>
                          <img src='http://www.wepiku.com/themes/front/img/loader_rdb.gif' style=' width: 67px; text-align: center; padding: 1em 3em; margin: 0em 25%;'>
                            </div>";
                      
                            exit;
                      
                       // echo "<script>alert('1leo');</script>";
                  
                        while ($estado) {
                         //echo "<script>alert('2leo');</script>";
                             
                           $pago_campania = CampaniasController::actionConsultarEstadoPago($campania_creada['info_pagos']);
                         
                            $estado_resp = trim($pago_campania['descripcion_estado_pagos']);
                            
                            echo $estado_resp;
                           
                      
                             
                           if ($i == 3000 || ($estado_resp != 'Transaccion iniciada' && $estado_resp != 'Transaccion esta en proceso')) {
                           
                               echo "<script>alert('1');</script>";                             
                                 
                                if ($estado_resp == 'Transaccion aprobada') {
                              
                                    $actualizar = CampaniasController::actionActualizarEstadoCampania($campania_creada['data']['id']);
                                    $boolmensaje = 2;
                                    $estado = false;
                               
									
                                } else {
                                
                                    $eliminar = CampaniasController::actionEliminarCampania($encuesta_creada['data']['id']);
                                    $boolmensaje = 3;
                                    $estado = false;                                  
									
                                }
                               
                            } else {                             
                             
                                if ($i == 0) {
                                    $this->render('como', array(
                                        'marcas_usuario' => array(),
                                    ));
                                }                          
                            }
                            $i++;
                           
                       }
                        
                    } else {
                        $boolmensaje = 2;
                    }
                else:
                    $boolmensaje = 1;
                endif;
            endif;
        endif;


		//echo "<script>alert(".$esfreemium.");</script>";
		
		
        $this->render('plan', array(
            'coverage' => $coverage,
            'subsidiary' => $subsidiary,
            'marcas_usuario' => $marcas_usuario,
            'form' => $form,
            'boolmensaje' => $boolmensaje,
            'hasDataform' => $hasDataform,
            'free' => $free,
            'value_free' => $value_free,
            'value_free2' => $value_free2,
            'imagen_tamano' => $imagen_tamano,
            'costos' => $costos,
            'saldo' => $saldo,
            'costos_elementos' => $costos_elem,
            'tipo_id' => $tipo_id,
            'imagenesperfil' => $img_perfil,            
            'data_encuesta' => $data_encuesta,
			'idcamp' => $campania_id,
            'tipo_id_camp' => $tipo_id_camp,           
            'img_flo' => $img_flo,           
            'esfreemium' => $esfreemium           
			
        ));
	
		
    }

    public function actionEstructurarDatosPostContent($attr_campania, $form, $freemium, $value_free, $coberturas_id, $imgs_campania, $data_encuesta) {

            $cadena_origen = $form->desc_campania;             
            //filtro los enlaces normales
            $cadena_resultante= preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blanck">$0</a>', $cadena_origen);
            //miro si hay enlaces con solamente www, si es así le añado el http://
            $cadena_resultante= preg_replace("/href=\"www/", 'href="http://www', $cadena_resultante);
            
            $attr_campania['descripcion'] = $cadena_resultante;
            //$attr_campania['descripcion'] = htmlspecialchars($cadena_resultante);
        
            
            $cadena_origenterm = $form->term_cond;             
            //filtro los enlaces normales
            $cadena_resultanteterm = preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blanck">$0</a>', $cadena_origenterm);
            //miro si hay enlaces con solamente www, si es así le añado el http://
            $cadena_resultanteterm = preg_replace("/href=\"www/", 'href="http://www', $cadena_resultanteterm);
            
      
       // $attr_campania['terminos'] = htmlspecialchars($form->term_cond);
        
        $attr_campania['terminos'] = $cadena_resultanteterm;
        if ($form->imagen_tamano != 0):
            $id_costo = explode("+", $form->imagen_tamano);
            $id_multi = $id_costo[0];
            $costo_multi = $id_costo[1];
        else:
            $id_multi = 0;
            $costo_multi = 1;
        endif;
        $attr_campania['costo_multimedia_id'] = $id_multi;
        if ($freemium == 1):
            $attr_campania['dias_activo'] = $value_free['num_dias_vigencia'];
            $attr_campania["dias_vigencia"] = $value_free['num_dias_vigencia'];
            $attr_campania["cantidad_pik"] = 0;
            $attr_campania["cantidad_impresiones"] = 0;
            $attr_campania["cantidad_recomendaciones"] = $value_free['num_recomendaciones'];
            $attr_campania["cantidad_vistas_detalle"] = $value_free['num_visualizaciones'];
            $attr_campania["cantidad_reproducciones_video"] = 0;
            if ($data_encuesta):
                $attr_campania["cantidad_encuestas"] = $value_free['num_encuestas'];
            else:
                $attr_campania["cantidad_encuestas"] = 0;
            endif;
        elseif ($freemium == 0):
            $attr_campania['dias_activo'] = $form->dias_vigencia;
            $attr_campania["dias_vigencia"] = $form->dias_vigencia;
            $attr_campania["cantidad_pik"] = 0;
            $attr_campania["cantidad_impresiones"] = 0;
            $attr_campania["cantidad_recomendaciones"] = $form->recomendaciones;
            $attr_campania["cantidad_vistas_detalle"] = $form->ingresos;
            $attr_campania["cantidad_reproducciones_video"] = 0; //Yii::app()->request->getPost('visualizaciones')
            if ($data_encuesta):
                $attr_campania["cantidad_encuestas"] = $form->encuestas_enc;
            else:
                $attr_campania["cantidad_encuestas"] = 0;
            endif;
        endif;
        $attr_campania['is_freemium'] = $freemium;
        if ($form->is_diamond == 1):
            $is_diamond = 1;
        elseif ($form->is_diamond == 2):
            $is_diamond = 0;
        endif;
        $attr_campania["is_diamante"] = $is_diamond;
        if ($form->pais_ciudad == 1):
            $is_pais = 1;
        elseif ($form->pais_ciudad == 2):
            $is_pais = 0;
        endif;
        $attr_campania["is_pais"] = $is_pais;
        $attr_campania["descuento"] = '';
        if ($freemium == 1):
            $datetime = new DateTime('now');
            $attr_campania["fecha_publicacion"] = $datetime->format('d-m-Y H:i:s');
            $datetime->add(new DateInterval('P' . $attr_campania['dias_activo'] . 'D'));
            $attr_campania["fecha_finalizacion"] = $datetime->format('d-m-Y H:i:s');
        else:
            $fecha_inicial = new DateTime($form->fini_dias_vigencia);
            $attr_campania["fecha_publicacion"] = $fecha_inicial->format('d-m-Y H:i:s');
            $fecha_final = new DateTime($form->ffin_dias_vigencia);
            $attr_campania["fecha_finalizacion"] = $fecha_final->format('d-m-Y H:i:s');
        endif;
        foreach ($form->sucursales as $key => $sucursal):
            $attr_campania['sucursales']["'$key'"] = htmlspecialchars($sucursal['id']);
        endforeach;
        if ($attr_campania["is_pais"] == 1):
            foreach ($coberturas_id as $key => $cobertura_id):
                $attr_campania['ciudades'][] = array();
                $attr_campania['paises']["'$key'"] = $cobertura_id;
            endforeach;
        elseif ($attr_campania["is_pais"] == 0):
            foreach ($coberturas_id as $key => $cobertura_id):
                $attr_campania['ciudades']["'$key'"] = $cobertura_id;
                $attr_campania['paises'][] = array();
            endforeach;
        endif;

        if ($data_encuesta):
            $attr_campania['encuesta'] = $data_encuesta['encuesta'];
        else:
            $attr_campania['encuesta'] = array(
                "nombre" => "",
                "descripcion" => "",
                "preguntas" => array()
            );
        endif;

        $attr_campania['edad_desde'] = htmlspecialchars($form->edad_inicial);
        $attr_campania['edad_hasta'] = htmlspecialchars($form->edad_final);
        $attr_campania['genero'] = htmlspecialchars($form->sexo);
        $i = 0;
        if (is_array($form->videos)):
            foreach ($form->videos as $key => $video):
                $attr_campania['multimedia'][$key]['nombre'] = 'video' . $key;
                $attr_campania['multimedia'][$key]['precio'] = 0;
                $attr_campania['multimedia'][$key]['is_video'] = 1;
                $attr_campania['multimedia'][$key]['enlace'] = htmlspecialchars($video);
                $i++;
            endforeach;
        endif;

        // if you want resize the image user following code //
        //
//                if(preg_match('/image/',$file->type)) {
        // use convert command for resize file before make sure you get installed imagegick tool //
//                echo exec("convert ".$file." -scale 220 ".$dir."thumb_".$filename);
        foreach ($imgs_campania as $key => $img_campania):
            $attr_campania['multimedia'][$key + $i]['nombre'] = $img_campania->name;
            $attr_campania['multimedia'][$key + $i]['precio'] = $costo_multi;
            $attr_campania['multimedia'][$key + $i]['is_video'] = 0;
            $attr_campania['multimedia'][$key + $i]['enlace'] = htmlspecialchars(Yii::app()->session['url_img']);
        endforeach;
//        print_r($attr_campania);
//        Yii::app()->end();
        return $attr_campania;
    }

    public function actionEstructurarDatosPostPromotion($attr_campania, $form, $freemium, $value_free, $coberturas_id, $imgs_campania, $data_encuesta) {

        $cadena_origen_pro = $form->desc_campania2;             
            //filtro los enlaces normales
        $cadena_resultante_pro = preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blanck">$0</a>', $cadena_origen_pro);
            //miro si hay enlaces con solamente www, si es así le añado el http://
        $cadena_resultante_pro = preg_replace("/href=\"www/", 'href="http://www', $cadena_resultante_pro);
            
        $attr_campania['descripcion'] = $cadena_resultante_pro;
        //$attr_campania['descripcion'] = htmlspecialchars($form->desc_campania2);
        
        $cadena_origenter_pro = $form->term_cond2;             
            //filtro los enlaces normales
        $cadena_resultanteter_pro = preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blanck">$0</a>', $cadena_origenter_pro);
            //miro si hay enlaces con solamente www, si es así le añado el http://
        $cadena_resultanteter_pro = preg_replace("/href=\"www/", 'href="http://www', $cadena_resultanteter_pro);
        
        
        //$attr_campania['terminos'] = htmlspecialchars($form->term_cond2);
        $attr_campania['terminos'] = $cadena_resultanteter_pro;
        if ($form->imagen_tamano2 != 0):
            $id_costo = explode("+", $form->imagen_tamano2);
            $id_multi = $id_costo[0];
            $costo_multi = $id_costo[1];
        else:
            $id_multi = 0;
            $costo_multi = 1;
        endif;
        $attr_campania['costo_multimedia_id'] = $id_multi;
        if ($freemium == 1):
            $attr_campania['dias_activo'] = $value_free['num_dias_vigencia'];
            $attr_campania["dias_vigencia"] = $value_free['num_dias_vigencia'];
            $attr_campania["cantidad_pik"] = $value_free['num_piks'];
            $attr_campania["cantidad_impresiones"] = 0;
            $attr_campania["cantidad_recomendaciones"] = $value_free['num_recomendaciones'];
            $attr_campania["cantidad_vistas_detalle"] = $value_free['num_visualizaciones'];
            $attr_campania["cantidad_reproducciones_video"] = 0;
            if ($data_encuesta):
                $attr_campania["cantidad_encuestas"] = $value_free['num_encuestas'];
            else:
                $attr_campania["cantidad_encuestas"] = 0;
            endif;
        elseif ($freemium == 0):
            $attr_campania['dias_activo'] = $form->dias_vigencia2;
            $attr_campania["dias_vigencia"] = $form->dias_vigencia2;
            $attr_campania["cantidad_pik"] = $form->piks2;
            $attr_campania["cantidad_impresiones"] = 0;
            $attr_campania["cantidad_recomendaciones"] = $form->recomendaciones2;
            $attr_campania["cantidad_vistas_detalle"] = $form->ingresos2;
            $attr_campania["cantidad_reproducciones_video"] = 0; //Yii::app()->request->getPost('visualizaciones')
            if ($data_encuesta):
                $attr_campania["cantidad_encuestas"] = $form->encuestas_enc2;
            else:
                $attr_campania["cantidad_encuestas"] = 0;
            endif;
        endif;
        $attr_campania['is_freemium'] = $freemium;
        if ($form->is_diamond2 == 1):
            $is_diamond = 1;
        elseif ($form->is_diamond2 == 2):
            $is_diamond = 0;
        endif;
        $attr_campania["is_diamante"] = $is_diamond;
        if ($form->pais_ciudadb == 1):
            $is_pais = 1;
        elseif ($form->pais_ciudadb == 2):
            $is_pais = 0;
        endif;
        $attr_campania["is_pais"] = $is_pais;
        $attr_campania["descuento"] = $form->oferta2;
        if ($freemium == 1):
            $datetime = new DateTime('now');
            $attr_campania["fecha_publicacion"] = $datetime->format('d-m-Y H:i:s');
            $datetime->add(new DateInterval('P' . $attr_campania['dias_activo'] . 'D'));
            $attr_campania["fecha_finalizacion"] = $datetime->format('d-m-Y H:i:s');
        else:
            $fecha_inicial = new DateTime($form->fini_dias_vigencia2);
            $attr_campania["fecha_publicacion"] = $fecha_inicial->format('d-m-Y H:i:s');
            $fecha_final = new DateTime($form->ffin_dias_vigencia2);
            $attr_campania["fecha_finalizacion"] = $fecha_final->format('d-m-Y H:i:s');
        endif;
        foreach ($form->sucursales2 as $key => $sucursal):
            $attr_campania['sucursales']["'$key'"] = htmlspecialchars($sucursal['id']);
        endforeach;
        if ($attr_campania["is_pais"] == 1):
            foreach ($coberturas_id as $key => $cobertura_id):
                $attr_campania['ciudades'][] = array();
                $attr_campania['paises']["'$key'"] = $cobertura_id;
            endforeach;
        elseif ($attr_campania["is_pais"] == 0):
            foreach ($coberturas_id as $key => $cobertura_id):
                $attr_campania['ciudades']["'$key'"] = $cobertura_id;
                $attr_campania['paises'][] = array();
            endforeach;
        endif;

        if ($data_encuesta):
            $attr_campania['encuesta'] = $data_encuesta['encuesta'];
        else:
            $attr_campania['encuesta'] = array(
                "nombre" => "",
                "descripcion" => "",
                "preguntas" => array()
            );
        endif;

        $attr_campania['edad_desde'] = htmlspecialchars($form->edad_inicial2);
        $attr_campania['edad_hasta'] = htmlspecialchars($form->edad_final2);
        $attr_campania['genero'] = htmlspecialchars($form->sexo2);
        $i = 0;
        if (is_array($form->videos2)):
            foreach ($form->videos2 as $key => $video):
                $attr_campania['multimedia'][$key]['nombre'] = 'video' . $key;
                $attr_campania['multimedia'][$key]['precio'] = 0;
                $attr_campania['multimedia'][$key]['is_video'] = 1;
                $attr_campania['multimedia'][$key]['enlace'] = htmlspecialchars($video);
                $i++;
            endforeach;
        endif;
        foreach ($imgs_campania as $key => $img_campania):
            $attr_campania['multimedia'][$key + $i]['nombre'] = $img_campania->name;
            $attr_campania['multimedia'][$key + $i]['precio'] = $costo_multi;
            $attr_campania['multimedia'][$key + $i]['is_video'] = 0;
            $attr_campania['multimedia'][$key + $i]['enlace'] = htmlspecialchars(Yii::app()->session['url_img']);
        endforeach;

        return $attr_campania;
    }

    public function actionEnviarImagenesBackend($campania_id, $img_campania , $medida) {

   
      
        if ($img_campania === null):
            return;
        endif;
//        print_r($img_campania);
        $name = $img_campania->name;
        $type = $img_campania->type;
		
        $path = $img_campania->tempName;
        $post_data = array('img_imagen' => new CURLFile($path, $type, $name) , 'medida' => $medida); //file_get_contents(

        $url = $this->url_services() . '/marca/api_marcamarca/upload_files?is_marca=0&id=' . $campania_id;

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

//        $jsondecode = CJSON::decode($response, true);
//        $responseData =(array)$jsondecode;
//        print_r($responseData);
    }

    public function actionSolicitarPagoCampania($post_campania, $costos_elem) {
        
		
        $costos_elem['tot'] = explode("$", $costos_elem['tot'])[1];
        $costos_elem['tot'] = str_replace(".", "", $costos_elem['tot']);
        $wsdl = new PaymentRedeban();
        $id_transterminal = rand(1, 999999);
        $id_terminal = 'ESB10390';
        //TAG credenciales
        $TipoCredenciales = new TipoCredenciales();
        $TipoCredenciales->idUsuario = 'testImaginamos';
        $TipoCredenciales->clave = 'testImaginamos.2015';
        //TAG cabecera
        //TAG InfoPuntosInteraccion
        $TipoPuntoInteraccion = new TipoInfoPuntoInteraccion();
        $TipoPuntoInteraccion->tipoTerminal = 'WEB';
        $TipoPuntoInteraccion->idTerminal = $id_terminal;
        //$TipoPuntoInteraccion->idAdquiriente = 'idAdquiriente';
        $TipoPuntoInteraccion->idAdquiriente = '0010203040';
        $TipoPuntoInteraccion->idTransaccionTerminal = $id_transterminal;
        $TipoCabeceraSolicitud = new TipoCabeceraSolicitud();
        $TipoCabeceraSolicitud->infoPuntoInteraccion = $TipoPuntoInteraccion;
        //TAG infoCompra
        //TAG infoImpuestos
        $TipoInfoImpuestos = new TipoInfoImpuestos();
        $TipoInfoImpuestos->tipoImpuesto = 'IVA';
        $TipoInfoImpuestos->monto = doubleval($costos_elem['tot'] * 0.16);
        //TAG montoDetallado1
        $TipoMontoDetallado = new TipoMontoDetallado();
        $TipoMontoDetallado->tipoMontoDetallado = 'BaseDevolucionIVA';
        $TipoMontoDetallado->monto = doubleval($costos_elem['tot']);
        $TipoInfoCompra = new TipoInfoCompra();
        $TipoInfoCompra->numeroFactura = '1';
        $TipoInfoCompra->montoTotal = doubleval($costos_elem['tot'] + $costos_elem['tot'] * 0.16);
        $TipoInfoCompra->infoImpuestos = $TipoInfoImpuestos;
        $TipoInfoCompra->montoDetallado = $TipoMontoDetallado;

        $IniciarTransaccionDeCompraSolicitud = new IniciarTransaccionDeCompraSolicitud();
        $IniciarTransaccionDeCompraSolicitud->credenciales = $TipoCredenciales;
        $IniciarTransaccionDeCompraSolicitud->cabeceraSolicitud = $TipoCabeceraSolicitud;
        $IniciarTransaccionDeCompraSolicitud->infoCompra = $TipoInfoCompra;

        try {
            $IniciarTransaccionDeCompraSolicitud = (array) $IniciarTransaccionDeCompraSolicitud;

          //print_r($IniciarTransaccionDeCompraSolicitud);
          
          //echo "<br><br><br><br>";
          
            $IniciarTransaccionDeCompraRespuesta = $wsdl->IniciarTransaccionDeCompra($IniciarTransaccionDeCompraSolicitud);
            
            //echo "actionSolicitarPagoCampania:";
            
            //print_r($IniciarTransaccionDeCompraRespuesta);
            
            //echo "<br><br><br><br>";
            //EXIT; 
            $id_transaccion = $IniciarTransaccionDeCompraRespuesta['infoTransaccionResp']['idTransaccionActual'];
            
              /*echo 'id_transaccion: '.$id_transaccion.'<br>';
              echo 'id_terminal: '.$id_terminal.'<br>';
              echo 'id_transterminal: '.$id_transterminal.'<br>';
            exit;*/
            
            return Array('id_transaccion' => $id_transaccion, 'id_terminal' => $id_terminal, 'id_trans_terminal' => $id_transterminal);
            
             
        } catch (SoapFault $e) {
            echo $e->getMessage();
            die;
        } catch (Exception $e) {
            echo $e->getMessage();
            die;
        }
       
    }

    public function actionConsultarEstadoPago($info_pagos) {     
      

        $wsdl = new PaymentRedeban();
        //TAG credenciales
        $TipoCredenciales = new TipoCredenciales();
        $TipoCredenciales->idUsuario = 'testImaginamos';
        $TipoCredenciales->clave = 'testImaginamos.2015';
     
        $TipoPuntoInteraccion = new TipoInfoPuntoInteraccion();
        $TipoPuntoInteraccion->tipoTerminal = 'WEB';
        $TipoPuntoInteraccion->idTerminal = $info_pagos['id_terminal'];
        //$TipoPuntoInteraccion->idAdquiriente = 'idAdquiriente';
        $TipoPuntoInteraccion->idAdquiriente = '0010203040';
        $TipoPuntoInteraccion->idTransaccionTerminal = $info_pagos['id_trans_terminal'];
        $TipoCabeceraSolicitud = new TipoCabeceraSolicitud();
        $TipoCabeceraSolicitud->infoPuntoInteraccion = $TipoPuntoInteraccion;

        $ConsultarEstadoDePagoSolicitud = new ConsultarEstadoDePagoSolicitud();
        $ConsultarEstadoDePagoSolicitud->credenciales = $TipoCredenciales;
        $ConsultarEstadoDePagoSolicitud->cabeceraSolicitud = $TipoCabeceraSolicitud;
        $ConsultarEstadoDePagoSolicitud->idTransaccion = $info_pagos['id_transaccion'];
     
        try {
            $ConsultarEstadoDePagoSolicitud = array($ConsultarEstadoDePagoSolicitud);
            $ConsultarEstadoDePagoRespuesta = $wsdl->ConsultarEstadoDePago($ConsultarEstadoDePagoSolicitud);
           
            //$estado = $ConsultarEstadoDePagoRespuesta['infoRespuesta']['codRespuesta'];
            
            $estado = "00";
            
            $descripcion = $ConsultarEstadoDePagoRespuesta['infoRespuesta']['descRespuesta'];
           
            $resp = Array('estado_pagos' => $estado, 'descripcion_estado_pagos' => $descripcion);
           
            // print_r($ConsultarEstadoDePagoSolicitud);
             // print_r($resp);
            
      
            //  die; 
            return $resp;
        } catch (Exception $ex) {
          
            echo $e->getMessage();
            die;
        }
        
         
    }

    public function actionCrearCampaniaBackend($post_campania, $costos_elem) {
		
        if ($post_campania['is_freemium'] == 0) {
            $datos_transaccion = $this->actionSolicitarPagoCampania($post_campania, $costos_elem);
		
           $id_transaccion = $datos_transaccion['id_transaccion'];
            $id_terminal = $datos_transaccion['id_terminal'];
             $id_trans_terminal = $datos_transaccion['id_trans_terminal'];
           
              $session=new CHttpSession;
              $session['rdb_id_transaccion'] = $id_transaccion;
              $session['rdb_id_trans_terminal'] = $id_trans_terminal;
    
			//exit;
            $url = 'https://www.pagosrbm.com/GlobalPayWeb/gp/realizarPago.xhtml?idTerminal=' . $id_terminal . '&idTransaccion=' . $id_transaccion;
			
				 
          //echo "<SCRIPT>window.open('" . $url . "','ventana','scrollbars=yes,width=870,height=774,titlebar=no');</SCRIPT>";
          //echo "<SCRIPT>location.href='../hotdeals/pagorespuesta';</SCRIPT>";
            
		
	/*	echo "<SCRIPT>window.open('" . $url . "','ventana','scrollbars=yes,width=870,height=774,titlebar=no'); " .
            "var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
            var eventer = window[eventMethod];
            var messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message';
            eventer(messageEvent,function(e) {
                    alert('El proceso de pago fue finalizado correctamente')  
                    location.href='../campanias/pagorespuesta'                                
                                      
            },false);
            </SCRIPT>";*/
            
              echo "<SCRIPT>window.open('" . $url . "');" .
            "var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
            var eventer = window[eventMethod];
            var messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message';
            eventer(messageEvent,function(e) {
              alert('El proceso de pago fue finalizado correctamente')  
                    location.href='../campanias/pagorespuesta'
            },false);
            </SCRIPT>";
           

            $json = str_replace("'", "", CJSON::encode($post_campania, true));
            $url = $this->url_services() . '/campania/api_campanias/create';

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token, 'Content-Type' => 'application/json'))->post($url, $json);
            
           

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
            $responseData['info_pagos'] = $datos_transaccion;
            
            $session['rdb_id_campaña'] = $responseData['data']['id'];
           
           return $responseData;
            
            
        } else {
            $json = str_replace("'", "", CJSON::encode($post_campania, true));
            $url = $this->url_services() . '/campania/api_campanias/create';

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token, 'Content-Type' => 'application/json'))->post($url, $json);

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
           return $responseData;
        }        
        
    }

    public static function actionActualizarEstadoCampania($camp_id) {
        $post_data = array();
        $url = self::url_services() . '/campania/api_campanias/activar?campania_id=' . $camp_id;
        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
        $jsondecode = CJSON::decode($response, true);
        $responseData = (array) $jsondecode;
        return $responseData;
    }
    
    public static function actionEliminarCampania($camp_id) {
        $post_data = array();
        $url = self::url_services() . '/campania/api_campanias/eliminar?campania_id=' . $camp_id;
        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
        $jsondecode = CJSON::decode($response, true);
        $responseData = (array) $jsondecode;
        return $responseData;
    }

    public static function actionGetInfoCrearCampaniaBackend() {

        $marca_id = Yii::app()->request->getPost('selected', false);
        $tipo_id = Yii::app()->request->getPost('tipo', false);
        $freemium = array('cantidad_dias' => '', 'cantidad_pik' => '', 'cantidad_recomendaciones' => '', 'cantidad_reproducciones' => '', 'cantidad_impresiones' => '', 'cantidad_ingresos_detalle' => '', 'cantidad_encuestas' => '');
        $valores_freemium = array('num_dias_vigencia' => 0, 'num_piks' => 0, 'num_recomendaciones' => 0, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => 0, 'num_encuestas' => 0);
        $costos = array('costo_cpc' => 0, 'costo_cpl' => 0, 'costo_cpa' => 0, 'costo_recomendacion' => 0, 'costo_dia' => 0);

        if ($marca_id != 0 and Yii::app()->request->isAjaxRequest):
            $url = self::url_services() . '/campania/api_campanias/inf_costos_camp?marca_id=' . $marca_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

            $jsondecode = CJSON::decode($response);
            $responseData = (array) $jsondecode['data'];

            $freemium['cantidad_dias'] = 'Publicada<br>hasta <strong>' . $responseData['freemium']['cantidad_dias'] . '</strong> Días';
            $freemium['cantidad_pik'] = 'Pueden hacer<br>hasta <strong>' . $responseData['freemium']['cantidad_pik'] . '</strong> Piks';
            $freemium['cantidad_recomendaciones'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_recomendaciones'] . '</strong> Recomendaciones';
            $freemium['cantidad_reproducciones'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_reproducciones'] . '</strong> Visualizaciones';
            $freemium['cantidad_impresiones'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_impresiones'] . '</strong> Impresiones';
            $freemium['cantidad_ingresos_detalle'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_ingresos_detalle'] . '</strong> Ingresos';

            $valores_freemium['num_dias_vigencia'] = $responseData['freemium']['cantidad_dias'];
            $valores_freemium['num_piks'] = $responseData['freemium']['cantidad_pik'];
            $valores_freemium['num_recomendaciones'] = $responseData['freemium']['cantidad_recomendaciones'];
            $valores_freemium['num_visualizaciones'] = $responseData['freemium']['cantidad_reproducciones'];
            $valores_freemium['num_impresiones'] = $responseData['freemium']['cantidad_impresiones'];
            $valores_freemium['num_ingresos'] = $responseData['freemium']['cantidad_ingresos_detalle'];

            if ($tipo_id == 3 or $tipo_id == 4 or $tipo_id == 5):
                $freemium['cantidad_encuestas'] = 'Se realizarán hasta<br><strong>' . $responseData['freemium']['cantidad_encuestas'] . '</strong> Preguntas';
                $valores_freemium['num_encuestas'] = $responseData['freemium']['cantidad_encuestas'];
            endif;

            $multimedia = $responseData['multimedia'];

            $costos['costo_cpc'] = $responseData['costos'][0]['costo_cpc'];
            $costos['costo_cpl'] = $responseData['costos'][0]['costo_cpl'];
            $costos['costo_cpa'] = $responseData['costos'][0]['costo_cpa'];
            $costos['costo_recomendacion'] = $responseData['costos'][0]['costo_recomendacion'];
            $costos['costo_dia'] = $responseData['costos'][0]['costo_dia'];

            $saldo = $responseData['saldo'];

            $imagen_tamano = CHtml::tag('option', array('value' => ''), CHtml::encode('Elija opción'), true);
          
            foreach ($multimedia as $multi):
              //  $imagen_tamano .= CHtml::tag('option', array('value' => $multi['id'] . '+' . $multi['costo']), CHtml::encode($multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')'), true);
                $imagen_tamano .= CHtml::tag('option', array('value' => $multi['id'] . '+' . $multi['costo']), CHtml::encode($multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')'), true);
            endforeach;
            $is_freemium = 1;
            $costo_multi = 1;

            $costos_elem = CampaniasController::actionCalcularCostoCampania($valores_freemium, $costos, $is_freemium, $costo_multi, $tipo_id);
			

            echo CJSON::encode(array(
                'multimedia' => $multimedia,
                'costos' => $costos,
                'freemium' => $freemium,
                'value_freemium' => $valores_freemium,
                'imagen_tamano' => $imagen_tamano,
                'cost_element' => $costos_elem['cost_elem'],
                'saldo' => $saldo,
                'total' => $costos_elem['tot']
            ));
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public static function actionCalcularCostoCampania($valores_freemium, $costos, $is_freemium, $costo_multi, $tipo_id) {

        $total = 0;

        if ($is_freemium == 1):
            if ($tipo_id == 1 or $tipo_id == 2 or $tipo_id == 4 or $tipo_id == 5):
                //            $costos_accion['dias_vigencia'] = 0;
                $costos_accion['recomendaciones'] = 0;
                $costos_accion['ingresos'] = 0;
                $costos_accion['piks'] = 0;
            endif;
            if ($tipo_id == 3 or $tipo_id == 4 or $tipo_id == 5):
                $costos_accion['encuestas'] = 0;
            endif;
        elseif ($is_freemium == 0):
            if ($tipo_id == 1 or $tipo_id == 2 or $tipo_id == 4 or $tipo_id == 5):
                //            $costos_accion['dias_vigencia'] = 0;
                $costos_accion['recomendaciones'] = $valores_freemium['num_recomendaciones'] * $costos['costo_recomendacion'];
                $costos_accion['ingresos'] = $valores_freemium['num_ingresos'] * $costos['costo_cpc'] * $costo_multi;
                if ($tipo_id == 1 or $tipo_id == 4):
                    $costos_accion['piks'] = $valores_freemium['num_piks'] * $costos['costo_cpl'];
                else:
                    $costos_accion['piks'] = 0;
                endif;
            endif;
            if ($tipo_id == 3 or $tipo_id == 4 or $tipo_id == 5):
                $costos_accion['encuestas'] = $valores_freemium['num_encuestas'] * $costos['costo_cpa'];
            endif;
        endif;

        foreach ($costos_accion as $accion => $costo_accion):
            $costo_acc = Yii::app()->numberFormatter->format('¤#,##0.00', $costo_accion, '$');
//            $costo_acc = Yii::app()->locale->numberFormatter->formatCurrency($costo_accion,'COP');
            $total += $costo_accion;

            $costos_elementos[$accion] = '<div class="row">
                <div class="col-xs-7 text-left">
                    <p><strong>' . $accion . '</strong></p>
                </div>
                <div class="col-xs-5 text-right CampaniaForm_' . $accion . ' CampaniaForm_' . $accion . '2 EncuestaForm_' . $accion . '_enc valor-costo">
                    <p>' . $costo_acc . '</p>
                </div>
            </div>';
        endforeach;
        $total_format = Yii::app()->numberFormatter->format('¤#,##0.00', $total, '$');
//        $total_format = Yii::app()->locale->numberFormatter->formatCurrency($total,'COP');

        return array('cost_elem' => $costos_elementos, 'tot' => $total_format);
    }

    public static function actionGetInfoCrearCampaniaBackendLocal($mid, $pa_ciu = 0, $cobsid = 0, $free, $tipo_id) {

        $marca_id = $mid;
        $pais_ciudad = $pa_ciu;
        $coberturas_id = $cobsid;
        $coberturas = array();
        $sucursales = array();
        $freemium = array('cantidad_dias' => '', 'cantidad_pik' => '', 'cantidad_recomendaciones' => '', 'cantidad_reproducciones' => '', 'cantidad_impresiones' => '', 'cantidad_ingresos_detalle' => '', 'cantidad_encuestas' => '');
        $valores_freemium = array('num_dias_vigencia' => 0, 'num_piks' => 0, 'num_recomendaciones' => 0, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => 0, 'num_encuestas' => 0);
        $costos = array('costo_cpc' => 0, 'costo_cpl' => 0, 'costo_cpa' => 0, 'costo_recomendacion' => 0, 'costo_dia' => 0);

        if (!isset($marca_id)):
            $marca_id = 0;
        endif;
        $url = self::url_services() . '/campania/api_campanias/inf_costos_camp?marca_id=' . $marca_id;

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $auth = array('Authorization: ' . $token);
        $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

        $jsondecode = CJSON::decode($response);
        $responseData = (array) $jsondecode['data'];

        if ($pais_ciudad == 1):
            foreach ($responseData['paises'] as $pais):
                $coberturas[$pais['id']] = $pais['nombre'];
                foreach ($coberturas_id as $cobertura_id):
                    if ($pais['id'] == $cobertura_id):
                        foreach ($pais['sucursales'] as $sucursal):
                            $sucursales[$sucursal['id']] = $sucursal['nombre'];
                        endforeach;
                    endif;
                endforeach;
            endforeach;
        elseif ($pais_ciudad == 2):
            foreach ($responseData['ciudades'] as $ciudad):
                $coberturas[$ciudad['id']] = $ciudad['nombre'];
                foreach ($coberturas_id as $cobertura_id):
                    if ($ciudad['id'] == $cobertura_id):
                        foreach ($ciudad['sucursales'] as $sucursal):
                            $sucursales[$sucursal['id']] = $sucursal['nombre'];
                        endforeach;
                    endif;
                endforeach;
            endforeach;
        endif;

        $freemium['cantidad_dias'] = 'Publicada<br>hasta <strong>' . $responseData['freemium']['cantidad_dias'] . '</strong> Días';
        $freemium['cantidad_pik'] = 'Pueden hacer<br>hasta <strong>' . $responseData['freemium']['cantidad_pik'] . '</strong> Piks';
        $freemium['cantidad_recomendaciones'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_recomendaciones'] . '</strong> Recomendaciones';
        $freemium['cantidad_reproducciones'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_reproducciones'] . '</strong> Visualizaciones';
        $freemium['cantidad_impresiones'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_impresiones'] . '</strong> Impresiones';
        $freemium['cantidad_ingresos_detalle'] = 'Pueden hacer hasta<br><strong>' . $responseData['freemium']['cantidad_ingresos_detalle'] . '</strong> Ingresos';

        $valores_freemium['num_dias_vigencia'] = $responseData['freemium']['cantidad_dias'];
        $valores_freemium['num_piks'] = $responseData['freemium']['cantidad_pik'];
        $valores_freemium['num_recomendaciones'] = $responseData['freemium']['cantidad_recomendaciones'];
        $valores_freemium['num_visualizaciones'] = $responseData['freemium']['cantidad_reproducciones'];
        $valores_freemium['num_impresiones'] = $responseData['freemium']['cantidad_impresiones'];
        $valores_freemium['num_ingresos'] = $responseData['freemium']['cantidad_ingresos_detalle'];

        if ($tipo_id == 3 or $tipo_id == 4 or $tipo_id == 5):
            $freemium['cantidad_encuestas'] = 'Se realizarán hasta<br><strong>' . $responseData['freemium']['cantidad_encuestas'] . '</strong> Preguntas';
            $valores_freemium['num_encuestas'] = $responseData['freemium']['cantidad_encuestas'];
        endif;

        $multimedia = $responseData['multimedia'];

        $costos['costo_cpc'] = $responseData['costos'][0]['costo_cpc'];
        $costos['costo_cpl'] = $responseData['costos'][0]['costo_cpl'];
        $costos['costo_cpa'] = $responseData['costos'][0]['costo_cpa'];
        $costos['costo_recomendacion'] = $responseData['costos'][0]['costo_recomendacion'];
        $costos['costo_dia'] = $responseData['costos'][0]['costo_dia'];

        $saldo = $responseData['saldo'];

        return array(
            'coberturas' => $coberturas,
            'sucursales' => $sucursales,
            'freemium' => $freemium,
            'value_freemium' => $valores_freemium,
            'multimedia' => $multimedia,
            'costos' => $costos,
            'saldo' => $saldo
        );
//        else:
//            throw new CHttpException('403', 'Forbidden access.');
//        endif;
    }

    public function actionGetCoberturasBackendAjax() {
        $pais_ciudad = Yii::app()->request->getPost('sel_pais_ciudad', false);
        $marca_id = Yii::app()->request->getPost('mid', false);

        if ($marca_id != 0 and Yii::app()->request->isAjaxRequest):
            $url = $this->url_services() . '/campania/api_campanias/inf_costos_camp?marca_id=' . $marca_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

            $jsondecode = CJSON::decode($response);
            $responseData = (array) $jsondecode['data'];

            echo CHtml::tag('option', array('value' => 0), CHtml::encode('Elija opción'), true);
			 echo CHtml::tag('option', array('value' => 'todos'), CHtml::encode('Todos'), true);
            if ($pais_ciudad == 1):
                foreach ($responseData['paises'] as $pais):
                    echo CHtml::tag('option', array('value' => $pais['id']), CHtml::encode($pais['nombre']), true);
                endforeach;
            elseif ($pais_ciudad == 2):
                foreach ($responseData['ciudades'] as $ciudad):
                    echo CHtml::tag('option', array('value' => $ciudad['id']), CHtml::encode($ciudad['nombre']), true);
                endforeach;
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetSucursalesBackendAjax() {

        $pais_ciudad = Yii::app()->request->getPost('sel_pais_ciudad', false);
        $cobertura_id = Yii::app()->request->getPost('cob', false);
        $marca_id = Yii::app()->request->getPost('mid', false);

        if ($marca_id != 0 and Yii::app()->request->isAjaxRequest):
            $url = $this->url_services() . '/campania/api_campanias/inf_costos_camp?marca_id=' . $marca_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);

            $jsondecode = CJSON::decode($response);
            $responseData = (array) $jsondecode['data'];

            echo CHtml::tag('option', array('value' => 0), CHtml::encode('Elija opción'), true);
            if ($pais_ciudad == 1):
            echo CHtml::tag('option', array('value' => 'todos'), CHtml::encode('Todos'), true);
                foreach ($responseData['paises'] as $pais):
                    if ($pais['id'] == $cobertura_id):
                        foreach ($pais['sucursales'] as $sucursal):
                            echo CHtml::tag('option', array('value' => $sucursal['id']), CHtml::encode($sucursal['nombre']), true);
                        endforeach;
                    endif;
                endforeach;
            elseif ($pais_ciudad == 2):
            echo CHtml::tag('option', array('value' => 'todos'), CHtml::encode('Todos'), true);
                foreach ($responseData['ciudades'] as $ciudad):
                    if ($ciudad['id'] == $cobertura_id):
                        foreach ($ciudad['sucursales'] as $sucursal):
                            echo CHtml::tag('option', array('value' => $sucursal['id']), CHtml::encode($sucursal['nombre']), true);
                        endforeach;
                    endif;
                endforeach;
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionAsignarMarcaCreacionCampania() {

        $pais_ciudad = Yii::app()->getRequest()->getParam('sel_pais_ciudad', false);
        if ($pais_ciudad):
            Yii::app()->session['pais_ciudad'] = $pais_ciudad;

        endif;

        Yii::app()->end();
    }

    public function actionCompartirCampaniaBackend() {

        $campania_id = Yii::app()->request->getPost('camp', false);
        $share_wepiku = Yii::app()->request->getPost('shwepi', false);
        $modal = Yii::app()->request->getPost('modal', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):
            $post_data = array();

            $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_compartir?campania_id=' . $campania_id . '&is_share_wepiku=' . $share_wepiku;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $auth = array('Authorization: ' . $token);
            $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);
            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;

            if ($responseData['success'] == 1):
//                Yii::app()->user->setFlash('success', $responseData['data']);
                echo CJSON::encode(array(
                    'modal' => $modal,
                    'mensaje' => '<div class="modredes">' . $responseData['data'] . '</div>'
                ));
            else:
//                Yii::app()->user->setFlash('error', '¡Algo sucedió!, no fué posible enviar las respuestas. Intente más tarde');
                echo CJSON::encode('<div class="modredes">¡Algo sucedió!, no fué posible compartir la Campaña. Intente más tarde</div>');
            endif;
        endif;
    }

    public function actionGetCostFromTabsAjax() {
        $value_free = Yii::app()->request->getPost('val_free', false);
        $costos = Yii::app()->request->getPost('cost', false);
        $is_freemium = Yii::app()->request->getPost('is_free', false);
        $costo_multi = Yii::app()->request->getPost('cost_multi', false);
        $tipo_id = Yii::app()->request->getPost('tipoid', false);

        if ($costo_multi):
            $id_costo = explode("+", $costo_multi);
            $costomulti = $id_costo[1];
        else:
            $costomulti = 1;
        endif;

        if ($is_freemium == true):
            $freemium = 0;
        elseif ($is_freemium == false):
            $freemium = 1;
        endif;

        if (Yii::app()->request->isAjaxRequest):
            $costos_elementos = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $freemium, $costomulti, $tipo_id);

            echo CJSON::encode(array(
                'cost_elem' => $costos_elementos['cost_elem'],
                'total' => $costos_elementos['tot']
            ));

        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetPreguntasPorTipoAjaxlocal($tipo_preg_id, $numero_preguntas, $pregunta, $respuestas = '') {

        if ($tipo_preg_id):

            if ($tipo_preg_id == 1):
                $html_tipo_pregunta = '<div class="item_pregunta"><h4 class="titulo_pregunta">Tipo de pregunta Abierta</h4>
                  <fieldset class="col-xs-12 con-field">
                    <label for="pregunta_enc_abierta">Pregunta</label>
                    <textarea class="textarea" name="EncuestaForm[pregunta_enc_abierta][]" id="EncuestaForm_pregunta_enc_abierta">' . $pregunta . '</textarea>
                   </fieldset>
                    <div class="modal-header">
                        <button type="button" class="close close-question" aria-label="Close">
                        </button>
                    </div>
                </div>';
            elseif ($tipo_preg_id == 2):
                $html_tipo_pregunta = '<div class="item_pregunta"><h4 class="titulo_pregunta">Tipo Selección múltiple única respuesta</h4>
                    <fieldset class="col-xs-12 con-field">
                        <label for="pregunta_enc_multiple_unic">Pregunta</label>
                        <textarea class="textarea" name="EncuestaForm[pregunta_enc_multiple_unic][' . ($numero_preguntas) . ']" id="EncuestaForm_pregunta_enc_multiple_unic">' . $pregunta . '</textarea>
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-9 con-field input-respuesta">
                        <label for="respuesta_enc_multiple_unic">Opciones de respuesta</label>
                        <input class="input input-encuestaurl" name="respuesta_enc_multiple_unic[]" type="text">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-2 con-field check-correcta" style="display:none">
                        <label for="respuesta_enc_multiple_correcta">Correcta</label>
                        <input class="input input-encuestaurl-correcta" name="respuesta_enc_multiple_unic_correcta[]" type="checkbox">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-1 con-field con-field-add">
                        <label>&nbsp;</label>
                        <button class="btn btn-add-encuestaurl text-center" type="button" onclick="addAnswerUnic(this);"><span></span></button>
                    </fieldset>
                    <div class="modal-header">
                        <button type="button" class="close close-question" aria-label="Close">
                        </button>
                    </div>
                    <div class="row row-encuestaurl">' . $respuestas . '</div>
                </div>';
            elseif ($tipo_preg_id == 3):
                $html_tipo_pregunta = '<div class="item_pregunta"><h4 class="titulo_pregunta">Tipo Selección múltiple múltiple respuesta</h4>
                    <fieldset class="col-xs-12 con-field">
                        <label for="pregunta_enc_multiple">Pregunta</label>
                        <textarea class="textarea" name="EncuestaForm[pregunta_enc_multiple][' . ($numero_preguntas) . ']" id="EncuestaForm_pregunta_enc_multiple">' . $pregunta . '</textarea>
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-9 con-field input-respuesta">
                        <label for="respuesta_enc_multiple">Opciones de respuesta</label>
                        <input class="input input-encuestaurl" name="respuesta_enc_multiple[]" type="text">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-2 con-field check-correcta" style="display:none">
                        <label for="respuesta_enc_multiple_correcta">Correcta</label>
                        <input class="input input-encuestaurl-correcta" name="respuesta_enc_multiple_correcta[]" type="checkbox">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-1 con-field con-field-add">
                        <label>&nbsp;</label>
                        <button class="btn btn-add-encuestaurl text-center" type="button" onclick="addAnswer(this);"><span></span></button>
                    </fieldset>
                    <div class="modal-header">
                        <button type="button" class="close close-question" aria-label="Close">
                        </button>
                    </div>
                    <div class="row row-encuestaurl">' . $respuestas . '</div>
                </div>';
            endif;
            return $html_tipo_pregunta;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetPreguntasPorTipoAjax() {
        $co = 1;
        $tipo_preg_id = Yii::app()->request->getPost('tipo_preg', false);
        $numero_preguntas = Yii::app()->request->getPost('numpreg', false);

        if ($tipo_preg_id and Yii::app()->request->isAjaxRequest):

            if ($tipo_preg_id == 1):
                $html_tipo_pregunta = '
				<li>
				<div class="item_pregunta"><h4 class="titulo_pregunta">Tipo de pregunta Abierta</h4>
                  <fieldset class="col-xs-12 con-field">
                    <label for="pregunta_enc_abierta"> <div class="nu"></div>Pregunta</label>
                    <textarea class="textarea" name="EncuestaForm[pregunta_enc_abierta][]" id="EncuestaForm_pregunta_enc_abierta"></textarea>
                   </fieldset>
                    <div class="modal-header">
                        <button type="button" class="close close-question" aria-label="Close">
                        </button>
                    </div>
                </div>
				</li>
				';
            elseif ($tipo_preg_id == 2):
                $html_tipo_pregunta = '
				<li>
				<div class="item_pregunta"><h4 class="titulo_pregunta">Tipo Selección múltiple única respuesta</h4>
                    <fieldset class="col-xs-12 con-field">
                        <label for="pregunta_enc_multiple_unic"><div class="nu"></div>Pregunta</label>
                        <textarea class="textarea" name="EncuestaForm[pregunta_enc_multiple_unic][' . ($numero_preguntas) . ']" id="EncuestaForm_pregunta_enc_multiple_unic"></textarea>
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-9 con-field input-respuesta">
                        <label for="respuesta_enc_multiple_unic">Opciones de respuesta</label>
                        <input class="input input-encuestaurl" name="respuesta_enc_multiple_unic[]" type="text">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-2 con-field check-correcta" style="display:none">
                        <label for="respuesta_enc_multiple_correcta">Correcta</label>
                        <input class="input input-encuestaurl-correcta" name="respuesta_enc_multiple_unic_correcta[]" type="checkbox">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-1 con-field con-field-add">
                        <label>&nbsp;</label>
                        <button class="btn btn-add-encuestaurl text-center" type="button" onclick="addAnswerUnic(this);"><span></span></button>
                    </fieldset>
                    <div class="modal-header">
                        <button type="button" class="close close-question" aria-label="Close">
                        </button>
                    </div>
                    <div class="row row-encuestaurl"></div>
                </div>
				</li>
				';
            elseif ($tipo_preg_id == 3):
                $html_tipo_pregunta = '
				<li>
				<div class="item_pregunta"><h4 class="titulo_pregunta">Tipo Selección múltiple múltiple respuesta</h4>
                    <fieldset class="col-xs-12 con-field">
                        <label for="pregunta_enc_multiple"><div class="nu"></div>Pregunta</label>
                        <textarea class="textarea" name="EncuestaForm[pregunta_enc_multiple][' . ($numero_preguntas) . ']" id="EncuestaForm_pregunta_enc_multiple"></textarea>
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-9 con-field input-respuesta">
                        <label for="respuesta_enc_multiple">Opciones de respuesta</label>
                        <input class="input input-encuestaurl" name="respuesta_enc_multiple[]" type="text">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-2 con-field check-correcta" style="display:none">
                        <label for="respuesta_enc_multiple_correcta">Correcta</label>
                        <input class="input input-encuestaurl-correcta" name="respuesta_enc_multiple_correcta[]" type="checkbox">
                    </fieldset>
                    <fieldset class="col-xs-12 col-sm-1 con-field con-field-add">
                        <label>&nbsp;</label>
                        <button class="btn btn-add-encuestaurl text-center" type="button" onclick="addAnswer(this);"><span></span></button>
                    </fieldset>
                    <div class="modal-header">
                        <button type="button" class="close close-question" aria-label="Close">
                        </button>
                    </div>
                    <div class="row row-encuestaurl"></div>
                </div>
				</li>
				';
            endif;
            echo CJSON::encode($html_tipo_pregunta);
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionExportReportToExcel() {
        // Xls Header Row
        $campania_id = Yii::app()->request->getPost('idcamp', false);
        $tipo_id = Yii::app()->request->getPost('tipo', false);
        $dataReporte = array();
        $preguntas = array();

        if (isset($campania_id) and Yii::app()->request->isPostRequest):
            $dataReporte = CampaniasController::actionGetReporteCampaniaBackend($campania_id);

            if ($tipo_id == 1):
                $headercolums = array('Correo', 'Nombre', 'Sexo', 'Pais', 'Ciudad', 'Edad', 'Hizo Pik', 'Compartió', 'Comentarios', 'Reportó', 'Fecha Último Ingreso');
            elseif ($tipo_id == 2):
                $headercolums = array('Correo', 'Nombre', 'Sexo', 'Pais', 'Ciudad', 'Edad', 'Visualizó Video', 'Compartió', 'Comentarios', 'Reportó', 'Fecha Último Ingreso');
            elseif ($tipo_id == 3):

                foreach ($dataReporte as $keydata => $data):

                    foreach ($data['respuestas'] as $key => $respuesta):
                        $preguntas[$key] = $respuesta['pregunta'];
                        $dataReporte[$keydata][6 + $key] = $respuesta['respuesta'];
                    endforeach;
                    array_splice($dataReporte[$keydata], 6, 1);
                endforeach;

                $headercolums = array('Correo', 'Nombre', 'Sexo', 'Pais', 'Ciudad', 'Edad', 'Reportó', 'Fecha Realización Encuesta');
                foreach ($preguntas as $pregunta):
                    $headercolums[] = $pregunta;
                endforeach;
            elseif ($tipo_id == 4):

                foreach ($dataReporte as $keydata => $data):

                    foreach ($data['respuestas'] as $key => $respuesta):
                        $preguntas[$key] = $respuesta['pregunta'];
                        $dataReporte[$keydata][9 + $key] = $respuesta['respuesta'];
                    endforeach;
                    array_splice($dataReporte[$keydata], 9, 1);

                endforeach;
//                $preguntasHeader = implode(',', $preguntas);
                $headercolums = array('Correo', 'Nombre', 'Sexo', 'Pais', 'Ciudad', 'Edad', 'Hizo Pik', 'Compartió', 'Comentarios', 'Reportó', 'Fecha Realización Encuesta', 'Fecha Último Ingreso');
                foreach ($preguntas as $pregunta):
                    $headercolums[] = $pregunta;
                endforeach;
            elseif ($tipo_id == 5):

                foreach ($dataReporte as $keydata => $data):

                    foreach ($data['respuestas'] as $key => $respuesta):
                        $preguntas[$key] = $respuesta['pregunta'];
                        $dataReporte[$keydata][9 + $key] = $respuesta['respuesta'];
                    endforeach;
                    array_splice($dataReporte[$keydata], 9, 1);

                endforeach;
//                $preguntasHeader = implode(',', $preguntas);
                $headercolums = array('Correo', 'Nombre', 'Sexo', 'Pais', 'Ciudad', 'Edad', 'Visualizó Video', 'Compartió', 'Comentarios', 'Reportó', 'Fecha Realización Encuesta', 'Fecha Último Ingreso');
                foreach ($preguntas as $pregunta):
                    $headercolums[] = $pregunta;
                endforeach;
//                array_push($headercolums, $preguntasHeader);
            endif;
            $row = $dataReporte;

            $filename = 'exportReport.xls';
            $xls = new ExportXLS($filename);
            $header = null;
            $xls->addHeader($headercolums);
            $xls->addRow($row);
            $xls->sendFile();
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetReporteCampaniaBackend($campania_id) {

        $url = $this->url_services() . '/campania/api_campanias/obtener_reporte?campania_id=' . $campania_id;
        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer ' . $wepiku_token;
        $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->get($url);
        $jsondecode = CJSON::decode($response);
        $responseData = $jsondecode;

        return $responseData['data'];
    }

    public function actionPausarCampaniaBackendAjax() {

        $post_data = array();
        $campania_id = Yii::app()->request->getPost('camp', false);
        $marca_id = Yii::app()->request->getPost('m', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

            $url = $this->url_services() . '/campania/api_campanias/pausar_campania?campania_id=' . $campania_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
            if ($responseData['success'] == true):
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">La campaña fue pausada correctamente. Se ha reembolsado el saldo de la marca</div>',
                    'mid' => $marca_id
                ));
//                else:
//                    echo CJSON::encode(array(
//                        'mensaje' => '<div class="modredes">¡Algo sucedió!, no fué posible pausar la Campaña. Intente más tarde</div>'
//                    ));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionContinuarCampaniaBackendAjax() {

        $post_data = array();
        $campania_id = Yii::app()->request->getPost('camp', false);
        $marca_id = Yii::app()->request->getPost('m', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

            $url = $this->url_services() . '/campania/api_campanias/continuar_campania?campania_id=' . $campania_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
            if ($responseData['success'] == true):
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">La campaña fue reanudada correctamente. Se ha descontado el saldo de la marca</div>',
                    'success' => 1,
                    'mid' => $marca_id
                ));
            else:
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">' . $responseData['message'] . '</div>',
                    'success' => 0,
                    'campid' => $campania_id
                ));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionTerminarCampaniaBackendAjax() {

        $post_data = array();
        $campania_id = Yii::app()->request->getPost('camp', false);
        $marca_id = Yii::app()->request->getPost('m', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

           // $url = $this->url_services() . '/campania/api_campanias/terminar_campania?campania_id=' . $campania_id;
            $url = $this->url_services() . '/campania/api_campanias/eliminar_campania?campania_id=' . $campania_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
            if ($responseData['success'] == true):
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">La campaña ha terminado correctamente. Puedes consultarla en la sección Finalizadas</div>',
                    'success' => 1,
                    'mid' => $marca_id
                ));
            else:
                echo CJSON::encode(array(                   
                    'mensaje' => '<div class="modredes">' . $responseData['message'] . '</div>',
                    'success' => 0,
                    'campid' => $campania_id
                ));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionRecomendarCampaniaBackendAjax() {

        $post_data = array();
        $campania_id = Yii::app()->request->getPost('camp', false);
        $marca_id = Yii::app()->request->getPost('m', false);

        if ($campania_id != 0 and Yii::app()->request->isAjaxRequest):

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false
            );

            $url = $this->url_services() . '/usuario/api_usuarioaccion/accion_recomendar?campania_id=' . $campania_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->post($url, $post_data);

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
            if ($responseData['success'] == true):
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">' . $responseData['data'] . '. Ya es visible en la sección de NewsFeed</div>',
                    'success' => 1,
                    'mid' => $marca_id
                ));
            else:
                echo CJSON::encode(array(
                    'mensaje' => '<div class="modredes">' . $responseData['data'] . '</div>',
                    'success' => 0,
                    'campid' => $campania_id
                ));
            endif;
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }

    public function actionGetEditDataEncuesta($campania_id, $form) {

        $coberturas_id = array();
        $dataEncuesta = CampaniasController::actionDetalleCampaniaBackend($campania_id);

        $form->marcasenc = $dataEncuesta['marca_id'];
        $form->nombre_encuesta = $dataEncuesta['nombre'];
        if ($dataEncuesta['is_pais'] == 1):
            $form->pais_ciudadenc = 1;
            $coberturas = $dataEncuesta['paises'];
        elseif ($dataEncuesta['is_pais'] == 0):
            $form->pais_ciudadenc = 2;
            $coberturas = $dataEncuesta['ciudades'];
        endif;
        foreach ($coberturas as $key => $cobertura):
            $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
            $form->coberturaenc[$key]['id'] = $cobertura['id'];
            $form->coberturaenc[$key]['nombre'] = $cobertura['nombre'];
        endforeach;
        $form->desc_campania_enc = $dataEncuesta['descripcion'];
        if ($dataEncuesta['is_fremium'] == 1):
            $form->is_freemium_enc = 2;
        elseif ($dataEncuesta['is_fremium'] == 0):
            $form->is_freemium_enc = 1;
        endif;
        $form->dias_vigencia_enc = $dataEncuesta['dias_vigencia'];
        $form->fini_dias_vigencia_enc = $dataEncuesta['fecha_publicacion'];
        $form->ffin_dias_vigencia_enc = $dataEncuesta['fecha_finalizacion'];
        $form->encuestas_enc = $dataEncuesta['cantidad_respuestas'];

        $num_data = array('num_dias_vigencia' => $form->dias_vigencia_enc, 'num_encuestas' => $form->encuestas_enc);
        $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcasenc, $form->pais_ciudadenc, $coberturas_id, $dataEncuesta['is_fremium'], $dataEncuesta['tipo_id']);
        $coverageenc = $ubicaciones['coberturas'];
        $free = $ubicaciones['freemium'];
        if ($dataEncuesta['is_fremium'] == 1):
            $value_free = $ubicaciones['value_freemium'];
        elseif ($dataEncuesta['is_fremium'] == 0):
            $value_free = $num_data;
        endif;
        $costos = $ubicaciones['costos'];
        $saldo = $ubicaciones['saldo'];
        $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $dataEncuesta['is_fremium'], 0, $dataEncuesta['tipo_id']);
        $preguntas = $dataEncuesta['campaniaEncuestas']['encuesta']['preguntas'];

        return array('form' => $form, 'num_data' => $num_data, 'coverageenc' => $coverageenc, 'free' => $free, 'costos' => $costos, 'saldo' => $saldo, 'costos_elem' => $costos_elem, 'tipo_id' => $dataEncuesta['tipo_id'], 'preguntas' => $preguntas);
    }

    public function actionGetEditDataCampania($campania_id, $form) {
		
        $dataCampania = CampaniasController::actionDetalleCampaniaBackend($campania_id);

        $form->marcas = $dataCampania['marca_id'];
        $form->nombre_campania = $dataCampania['nombre'];

        if ($dataCampania['tipo_id'] == 2 or $dataCampania['tipo_id'] == 5):

            $dataCampania['tipo_id'] = 2;
            $form->scenario = 'content';
            
            $descam = str_replace('wwww','<a href="www',$dataCampania['descripcion']);
            $descam = str_replace('http://','',$descam);  
            $descam = str_replace('.com','.com">link</a>',$descam);  
            $descam = str_replace('.co','.co">link</a>',$descam);  
            
            $form->desc_campania = $descam;
            $form->term_cond = $dataCampania['texto_oferta'];
            $form->sexo = $dataCampania['genero'];
            $form->edad_inicial = (int) $dataCampania['edad_desde'];
            $form->edad_final = (int) $dataCampania['edad_hasta'];
            if ($dataCampania['is_pais'] == 1):
                $form->pais_ciudad = 1;
                $coberturas = $dataCampania['paises'];
            elseif ($dataCampania['is_pais'] == 0):
                $form->pais_ciudad = 2;
                $coberturas = $dataCampania['ciudades'];
            endif;
            foreach ($coberturas as $key => $cobertura):
                $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                $form->cobertura[$key]['id'] = $cobertura['id'];
                $form->cobertura[$key]['nombre'] = $cobertura['nombre'];
            endforeach;
            $sucursales = $dataCampania['sucursales'];
            foreach ($sucursales as $key => $sucursal):
                $form->sucursales[$key]['id'] = $sucursal['id'];
                $form->sucursales[$key]['nombre'] = $sucursal['nombre'];
            endforeach;
            $form->is_diamond = $dataCampania['is_diamante'];
            if ($dataCampania['is_fremium'] == 1):
                $form->is_freemium = 2;
            elseif ($dataCampania['is_fremium'] == 0):
                $form->is_freemium = 1;
            endif;
            $form->dias_vigencia = $dataCampania['dias_vigencia'];
            $form->fini_dias_vigencia = $dataCampania['fecha_publicacion'];
            $form->ffin_dias_vigencia = $dataCampania['fecha_finalizacion'];
            $form->recomendaciones = $dataCampania['cantidad_recomendaciones'];
            $form->ingresos = $dataCampania['cantidad_vistas_detalle'];
            foreach ($dataCampania['multimedia'] as $imag_video):
                if ($imag_video['is_video'] == 1):
                    $form->videos[] = $imag_video['enlace'];
                elseif ($imag_video['is_video'] == 0):
                    $img_perfil[] = $imag_video['enlace'];
                endif;
            endforeach;
            $num_data = array('num_dias_vigencia' => $form->dias_vigencia, 'num_piks' => 0, 'num_recomendaciones' => $form->recomendaciones, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => $form->ingresos);

            $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $form->pais_ciudad, $coberturas_id, $dataCampania['is_fremium'], $dataCampania['tipo_id']);
            $coverage = $ubicaciones['coberturas'];
            $subsidiary = $ubicaciones['sucursales'];
            $free = $ubicaciones['freemium'];
            if ($dataCampania['is_fremium'] == 1):
                $value_free = $ubicaciones['value_freemium'];
            elseif ($dataCampania['is_fremium'] == 0):
                $value_free = $num_data;
            endif;
            $multimedia = $ubicaciones['multimedia'];
            $costos = $ubicaciones['costos'];
            $saldo = $ubicaciones['saldo'];
            foreach ($multimedia as $multi):
                $imagen_tamano[$multi['id'] . '+' . $multi['costo']] = $multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')';
                if ($dataCampania['costo_multimedia_id'] == $multi['id']):
                    $costo_multi = $multi['costo'];
                    $form->imagen_tamano = $dataCampania['costo_multimedia_id'] . '+' . $costo_multi;
                endif;
            endforeach;
            $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $dataCampania['is_fremium'], $costo_multi, $dataCampania['tipo_id']);
        elseif ($dataCampania['tipo_id'] == 1 or $dataCampania['tipo_id'] == 4):
		
			
            $dataCampania['tipo_id'] = 1;
            $form->scenario = 'promotion';
            $form->desc_campania2 = $dataCampania['descripcion'];
            $form->term_cond2 = $dataCampania['texto_oferta'];
            $form->oferta2 = $dataCampania['descuento'];
            $form->sexo2 = $dataCampania['genero'];
            $form->edad_inicial2 = (int) $dataCampania['edad_desde'];
            $form->edad_final2 = (int) $dataCampania['edad_hasta'];
            if ($dataCampania['is_pais'] == 1):
                $form->pais_ciudadb = 1;
                $coberturas = $dataCampania['paises'];
            elseif ($dataCampania['is_pais'] == 0):
                $form->pais_ciudadb = 2;
                $coberturas = $dataCampania['ciudades'];
            endif;
            foreach ($coberturas as $key => $cobertura):
                $coberturas_id[$key] = htmlspecialchars($cobertura['id']);
                $form->cobertura2[$key]['id'] = $cobertura['id'];
                $form->cobertura2[$key]['nombre'] = $cobertura['nombre'];
            endforeach;
            $sucursales = $dataCampania['sucursales'];
            foreach ($sucursales as $key => $sucursal):
                $form->sucursales2[$key]['id'] = $sucursal['id'];
                $form->sucursales2[$key]['nombre'] = $sucursal['nombre'];
            endforeach;
            $form->is_diamond2 = $dataCampania['is_diamante'];
            if ($dataCampania['is_fremium'] == 1):
                $form->is_freemium2 = 2;
            elseif ($dataCampania['is_fremium'] == 0):
                $form->is_freemium2 = 1;
            endif;
            $form->dias_vigencia2 = $dataCampania['dias_vigencia'];
            $form->fini_dias_vigencia2 = $dataCampania['fecha_publicacion'];
            $form->ffin_dias_vigencia2 = $dataCampania['fecha_finalizacion'];
            $form->piks2 = $dataCampania['cantidad_pik'];
            $form->recomendaciones2 = $dataCampania['cantidad_recomendaciones'];
            $form->ingresos2 = $dataCampania['cantidad_vistas_detalle'];
            foreach ($dataCampania['multimedia'] as $imag_video):
                if ($imag_video['is_video'] == 1):
                    $form->videos2[] = $imag_video['enlace'];
                elseif ($imag_video['is_video'] == 0):
                    $img_perfil[] = $imag_video['enlace'];
                endif;
            endforeach;

            $num_data = array('num_dias_vigencia' => $form->dias_vigencia2, 'num_piks' => $form->piks2, 'num_recomendaciones' => $form->recomendaciones2, 'num_visualizaciones' => 0, 'num_impresiones' => 0, 'num_ingresos' => $form->ingresos2);
            $ubicaciones = CampaniasController::actionGetInfoCrearCampaniaBackendLocal($form->marcas, $form->pais_ciudadb, $coberturas_id, $dataCampania['is_fremium'], $dataCampania['tipo_id']);
            $coverage = $ubicaciones['coberturas'];
            $subsidiary = $ubicaciones['sucursales'];
            $free = $ubicaciones['freemium'];
            if ($dataCampania['is_fremium'] == 1):
                $value_free = $ubicaciones['value_freemium'];
            elseif ($dataCampania['is_fremium'] == 0):
                $value_free = $num_data;
            endif;
            $multimedia = $ubicaciones['multimedia'];
            $costos = $ubicaciones['costos'];
            $saldo = $ubicaciones['saldo'];
            foreach ($multimedia as $multi):
                $imagen_tamano[$multi['id'] . '+' . $multi['costo']] = $multi['descripcion'] . ' (' . $multi['ancho'] . ' x ' . $multi['alto'] . ')';
                if ($dataCampania['costo_multimedia_id'] == $multi['id']):
                    $costo_multi = $multi['costo'];
                    $form->imagen_tamano2 = $dataCampania['costo_multimedia_id'] . '+' . $costo_multi;
                endif;
            endforeach;
            $costos_elem = CampaniasController::actionCalcularCostoCampania($value_free, $costos, $dataCampania['is_fremium'], $costo_multi, $dataCampania['tipo_id']);
        endif;
			
		
        return array('form' => $form, 'num_data' => $num_data, 'coverage' => $coverage, 'subsidiary' => $subsidiary, 'free' => $free, 'imagen_tamano' => $imagen_tamano, 'costos' => $costos, 'saldo' => $saldo, 'costos_elem' => $costos_elem, 'tipo_id' => $dataCampania['tipo_id'], 'img_perfil' => $img_perfil);
    }

    public function actionDetalleCampaniaBackend($campania_id) {

        if ($campania_id != 0):
            $url = $this->url_services() . '/campania/api_campanias/obtener_detalle?campania_id=' . $campania_id;

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer ' . $wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token))->get($url);

            $jsondecode = CJSON::decode($response, true);
            $responseData = (array) $jsondecode;
            return $responseData['data'];
        else:
            throw new CHttpException('403', 'Forbidden access.');
        endif;
    }
    
    public function actionPagorespuesta() {
      
      $session=new CHttpSession;
      
      $datapago = array('id_terminal' => 'ESB10390' , 'id_trans_terminal'=> $session['rdb_id_trans_terminal'] , 'id_transaccion' => $session['rdb_id_transaccion']);
      
       $pago_campania = CampaniasController::actionConsultarEstadoPago($datapago);
       
      
       
       $estado_resp = trim($pago_campania['descripcion_estado_pagos']);
       
       if($estado_resp == "Transaccion aprobada"){
          $actualizar = CampaniasController::actionActualizarEstadoCampania($session['rdb_id_campaña']);
       }else{
          $eliminar = CampaniasController::actionEliminarCampania($session['rdb_id_campaña']);
       }
                            
        $this->render('pago' , array('estado_resp'=> $estado_resp , 'rdb_id_trans_terminal'=> $session['rdb_id_trans_terminal'] , 'rdb_id_transaccion'=>$session['rdb_id_transaccion'] ));
    }

}
