<?php

class NewsFeedColumn extends CWidget
{
    public $newFeed;
    public $marca;
    
    public function init()
    {
        // this method is called by CController::beginWidget()
    }
 
    public function run()
    {
        $this->render('index', array('data_newsfeed_json' => $this->newFeed, 'data_marca_json' => $this->marca));
    }
}