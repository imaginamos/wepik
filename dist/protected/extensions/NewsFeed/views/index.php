<?php foreach ($data_newsfeed_json['data'] as $newfeed): ?>
<?php if ($newfeed['campania']['tipo_id'] == 2): ?>
    <div class="<?php echo 'item-sub-list item-post' . $newfeed['campania']['tipo_id']; ?>">
    <?php else: ?>
        <div class="<?php echo 'item-sub-list item-post2'; ?>">                            
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-12 img-users">
                <?php $num_usuarios = count($newfeed['usuario']); ?>
                <?php for ($i = 0; $i < ($num_usuarios < 3 ? $num_usuarios : 3); $i++) : $usuario_amigo = $newfeed['usuario'][$i] ?>
                    <?php if($newfeed['puntos']['id'] == 11): ?>
                        <?php echo CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$newfeed['campania']['marca_id']]['img_imagenPerfil'], "", array("class" => "img-circle", "width" => "30px")); ?>
                    <?php else: ?>
                        <?php echo CHtml::image($usuario_amigo['img'], "", array("class" => "img-circle", "width" => "30px")); ?>
                    <?php endif;?>
                <?php endfor; ?>
                <h4>
                    <?php $i = 0; ?>
                    <?php for ($j = 0; $j < ($num_usuarios < 3 ? $num_usuarios : 3); $j++) : $usuario_amigo = $newfeed['usuario'][$j] ?>
                        <?php $i++; ?>
                        <?php if($newfeed['puntos']['id'] == 11): ?>
                            <?php echo '<strong>' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre']. '</strong>'; ?>
                        <?php else:?>
                            <?php echo '<strong>' . $usuario_amigo['name'] . ' ' . $usuario_amigo['lastname'] . ',</strong>'; ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <?php $usu_res = $num_usuarios-3;?>
                    <?php if ($num_usuarios > 3) { echo 'y ' . $usu_res . ' mas'; } ?>
                    <br/>    
                    <?php
                    switch ($newfeed['puntos']['id']):
                        case 1:
                            if ($num_usuarios == 1):
                                echo 'revisó la campaña de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            elseif ($num_usuarios > 1):
                                echo 'revisaron la campaña de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            endif;
                            break;
                        case 2:
                            if ($num_usuarios == 1):
                                echo 'hizo pik en la promoción de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            elseif ($num_usuarios > 1):
                                echo 'hicieron pik en la promoción de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            endif;
                            break;
                        case 4:
                            if ($num_usuarios == 1):
                                echo 'vio un video en la promoción de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            elseif ($num_usuarios > 1):
                                echo 'vieron un video en la promoción de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            endif;
                            break;
                        case 5:
                            if ($num_usuarios == 1):
                                echo 'respondió la encuesta de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            elseif ($num_usuarios > 1):
                                echo 'respondieron la encuesta de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            endif;
                            break;
                        case 7:
                            if ($num_usuarios == 1):
                                echo 'quiere compartir la campaña de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            elseif ($num_usuarios > 1):
                                echo 'quieren compartir la campaña de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            endif;
                            break;
                        case 9:
                            if ($num_usuarios == 1):
                                echo 'comentó en la campaña de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            elseif ($num_usuarios > 1):
                                echo 'comentaron en la campaña de ' . $data_marca_json[$newfeed['campania']['marca_id']]['nombre'];
                            endif;
                            break;
                        case 11:
                            echo ' te recomienda ';
                            break;
                    endswitch;
                    ?>
                </h4>
            </div>
            <div class="col-xs-12 pik-users">
                <?php $hd = new Controller(uniqid());?>
                
                <a class="brand-link" href="<?php echo $hd->createUrl('marcas/marca?m='.$newfeed['campania']['marca_id']); ?>">
                    <?php echo CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$newfeed['campania']['marca_id']]['img_imagenPerfil'], "", array('class' => 'img-circle')); ?>
                </a>
                <?php 
                $unico = 0;
                if ($newfeed['campania']['tipo_id'] == 1 || $newfeed['campania']['tipo_id'] == 2):
                    $modal_newfeed = "#modal-product";
                elseif ($newfeed['campania']['tipo_id'] == 4 || $newfeed['campania']['tipo_id'] == 5):
                    $modal_newfeed = "#modal-poll-product";
                elseif ($newfeed['campania']['tipo_id'] == 3):
                    $modal_newfeed = "#modal-poll-list";
                    $unico = uniqid();
                    echo '<div id="flash-encuesta-feed'.$unico.'"></div>';
                endif; ?>
                <?php $discount = ''; ?>
                <?php if ($newfeed['campania']['tipo_id'] == 1 || $newfeed['campania']['tipo_id'] == 4): ?>
                    <?php $discount = '<div class="discount">'.$newfeed['campania']['descuento'].'</div>'; ?>
                <?php endif; ?>                                                
                <?php $shadow = '<div class="shadow"></div>'; ?>

                <?php $img_promo_newsfeed = CHtml::image(stripslashes($newfeed['campania']['enlace']), ""); ?>

                <?php                                
                    // ajax button to open the modal
                    echo TbHtml::ajaxLink(
                        $discount.$shadow.$img_promo_newsfeed,
                        $this->getController()->createUrl('hotdeals/getInfoCampaniaAjax'),
                        array( 
                            'dataType' => 'json', 
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $newfeed['campania']['id'],
                                'modal' => $modal_newfeed
                            ),
                            'success' => 'function(data){
                                if(data.answered_poll == 1){
                                    var fef = "#flash-encuesta-feed'.$unico.'";                                                    
                                    $(".sub-nav-list "+fef).html(data.mensaje);
                                    $(fef+" .modredes").slideToggle(0);  
                                    $(".dropdown-menu").css({"display": "none"});
                                    $(fef+" .modredes").animate({
                                        opacity: 0
                                        }, 5000, function() {
                                            $(fef+" .modredes").removeAttr("style");
                                                $(".dropdown-menu").removeAttr("style");
                                            });                                                   
                                } else{
                                    openModal("'.$modal_newfeed.'", data.content);
                                };    
                            }'
                        ),
                        array(
                            'id' => 'open-modal-'.uniqid(),
                            'class' => 'brand-promo'
                        )
                    );                                
                ?>                                             
            </div>
            <div class="col-xs-12 name-promo">
                <h5>
                    <a href="#"><?php echo strtoupper($newfeed['campania']['nombre']); ?><span></span></a>
                </h5>
            </div>
        </div>
    </div>
<?php endforeach;        