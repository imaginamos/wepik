<?php

class MarcasMenu extends CWidget
{

    
    public function init()
    {
        // this method is called by CController::beginWidget()
    }
 
    public function run()
    {
        $responseData = array();         
        $controler = new Controller(uniqid());
        $url = $controler->url_services().'/campania/api_campanias/marcas_usuario';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
//        $token = 'Bearer a8dYbYn8w4mRc6x6pAy65Tg258fTnSoQ3SgMiSgT';
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     

        $jsondecode = CJSON::decode($response);
        $responseData =(array)$jsondecode;    
        
        $this->render('index', array('marcas_admin' => $responseData));
    }
}
