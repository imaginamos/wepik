<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';
    public $themeBack = 'base';
    public $themeFront;
    public $title;
    public $subTitle;
    public $url_services;
    /**
     * @var tags in order to config the meta tags of
     * facebook
     */
    public $fb_title;
    public $fb_type;
    public $fb_url;
    public $fb_image;
    public $fb_site_name;
    public $fb_description;

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $response_newsfeed = array();
    public $response_marca = array();
    public $hasNewsFeed = false;
    

    public function init() {
        /**
         * Client languane is updated
         */
        Yii::app()->language = substr(Yii::app()->request->preferredLanguage, 0, 2);
    }

    public static function url_services() {
        $url_services = 'http://app.wepiku.com';
        return $url_services;
    }

    public function buildNewsFeed() {

        $response_marca = array();
        $response_newsfeed = HotdealsController::actionNewsFeedBackend();
        $count = count($response_newsfeed['data']);
        for ($i = 0; $i < $count; $i++) {
            $newfeed = $response_newsfeed['data'][$i];
            $existe_marca_newsfeed = false;

            foreach ($response_marca as $marca):
                if ($marca == $newfeed['campania']['marca_id']):
                    $existe_marca_newsfeed = true;
                endif;
            endforeach;
            if (!$existe_marca_newsfeed):
                $response_marca[$newfeed['campania']['marca_id']] = MarcasController::actionDetalleMarcaBackend($newfeed['campania']['marca_id']);
            endif;
            $clave = $response_newsfeed['data'][$i]['puntos']['fecha'];
            $response_newsfeed['data'][$clave] = $newfeed;
            unset($response_newsfeed['data'][$i]);
        }

        krsort($response_newsfeed['data']);
        $this->response_newsfeed = $response_newsfeed;
        $this->response_marca = $response_marca;
    }

    public function builtHeader() {
        foreach (Yii::app()->getModules() as $name => $config) {
            // if($name=="gii")
            // 	continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if (method_exists($module, 'builtHeader'))
                $module->builtHeader($this);
        }
    }

    public function builtMessages() {
        return Yii::app()->user->getFlashes();
    }

    public function builtTitle() {
        return Yii::app()->name;
    }

    public function builtUrlLogin() {
        return Yii::app()->getModule('users')->urlLogin;
    }

    public function builtUrlProfile() {
        return Yii::app()->getModule('users')->urlProfile;
    }

    public function builtUrlLogout() {
        return Yii::app()->getModule('users')->urlLogout;
    }

    public function builtUrlHome() {
        return $this->createUrl('/');
    }

    public function builtEndBody() {
        foreach (Yii::app()->getModules() as $name => $config) {
            // if($name=="gii")
            // 	continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if (method_exists($module, 'builtEndBody'))
                $module->builtEndBody($this);
        }
    }

    public function builtApp() {
        foreach (Yii::app()->getModules() as $name => $config) {
            // if($name=="gii")
            // 	continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if (method_exists($module, 'builtApp'))
                $module->builtApp($this);
        }
    }

    public function builtMenu() {
        $items = array();
        foreach (Yii::app()->getModules() as $name => $config) {
            // if($name=="gii")
            // 	continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if ($this->hasItems($module) !== false) {
                if ($this->hasVisible($module)) {
                    foreach ($this->hasItems($module) as $itm)
                        $items[] = $itm;
                }
            } else
                $items[] = array('label' => $this->hasLabel($module),
                    'url' => $this->hasUrl($module),
                    'visible' => $this->hasVisible($module),
                    'icon' => $this->hasIcon($module)
                );
        }
        $items[] = $this->addConfigsMenu();
        return $items;
    }

    public function addConfigsMenu() {
        $items = array();
        foreach (Yii::app()->getModules() as $name => $config) {
            if ($name == "gii")
                continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if ($this->hasConfigItems($module) !== false)
                $items = array_merge($this->hasConfigItems($module), $items);
        }
        return array('label' => Yii::t('app', 'Settings'),
            'url' => '#',
            'items' => $items,
            'visible' => true,
            'icon' => 'fa fa-cog'
        );
    }

    public function builtDashboardCounters() {
        $counters = array();
        foreach (Yii::app()->getModules() as $name => $config) {
            if ($name == "gii")
                continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if ($this->dashboardCountersValide($module) !== false)
                foreach ($this->dashboardCountersValide($module) as $count)
                    $counters[] = $count;
        }
        return $counters;
    }

    public function builtAPIAvailable() {
        $counters = array();
        foreach (Yii::app()->getModules() as $name => $config) {
            if ($name == "gii")
                continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if ($this->builtDocApi($module) !== false)
                foreach ($this->builtDocApi($module) as $count)
                    $counters[] = $count;
        }
        return $counters;
    }

    public function builtDashboardReports() {
        $reports = array();
        foreach (Yii::app()->getModules() as $name => $config) {
            if ($name == "gii")
                continue;
            $module = Yii::app()->getModule($name);
            if ($module === null)
                continue;
            if ($this->dashboardReportsValide($module) !== false)
                foreach ($this->dashboardReportsValide($module) as $mod)
                    $reports[] = $mod;
        }
        return $reports;
    }

    protected function hasVisible($module) {
        if (method_exists($module, 'menuVisible'))
            return $module->menuVisible();
        return true;
    }

    protected function hasItems($module) {
        if (method_exists($module, 'menuItems') and $module->menuItems() !== false)
            return $module->menuItems();
        return false;
    }

    protected function hasConfigItems($module) {
        if (method_exists($module, 'configItems') and $module->configItems() !== false)
            return $module->configItems();
        return false;
    }

    protected function hasUrl($module) {
        if (method_exists($module, 'menuUrl') and $module->menuUrl() !== false)
            return $module->menuUrl();
        return array('/' . $module->id);
    }

    protected function hasLabel($module) {
        if (method_exists($module, 'menuLabel') and $module->menuLabel() !== false)
            return $module->menuLabel();
        return ucfirst($module->id);
    }

    protected function hasIcon($module) {
        if (method_exists($module, 'menuIcon') and $module->menuIcon() !== false)
            return $module->menuIcon();
        return null;
    }

    protected function dashboardCountersValide($module) {
        if (method_exists($module, 'dashboardCounters') and $module->dashboardCounters() !== false)
            return $module->dashboardCounters();
        return false;
    }

    protected function builtDocApi($module) {
        if (method_exists($module, 'builtDocApi') and $module->builtDocApi() !== false)
            return $module->builtDocApi();
        return false;
    }

    protected function dashboardReportsValide($module) {
        if (method_exists($module, 'dashboardReports') and $module->dashboardReports() !== false)
            return $module->dashboardReports();
        return false;
    }

    protected function allowTo($role = array()) {
        if (empty($role))
            return;
        if (is_string($role))
            $role = array($role);
        foreach ($role as $row) {
            if (!Yii::app()->user->checkAccess($row))
                throw new CHttpException(403, Yii::t('yii', 'Login Required'));
        }
    }

    protected function isFrontAction() {
        Yii::app()->theme = $this->themeFront;
        $this->layout = '//layouts/content';
    }

    protected function isBackAction() {
        // Only front action can be access
        // without login
        if (Yii::app()->user->isGuest)
            Yii::app()->user->loginRequired();
        Yii::app()->theme = $this->themeBack;
    }

}
