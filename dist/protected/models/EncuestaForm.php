<?php

Class EncuestaForm extends CFormModel{
    
    public $nombre_encuesta;
    public $pais_ciudadenc;
    public $coverageenc;
    public $coberturaenc;
    public $marcasenc;
    public $desc_campania_enc;
    public $dias_vigencia_enc;
    public $fini_dias_vigencia_enc;
    public $ffin_dias_vigencia_enc;    
    public $encuestas_enc;
    public $is_freemium_enc;   
    public $tipo_pregunta;
    public $costo_cpa;
    public $numero_preguntas_abierta;
    public $numero_preguntas_multiple_unic;
    public $numero_preguntas_multiple;
    public $pregunta_enc_abierta;
    public $pregunta_enc_multiple_unic;
    public $respuesta_enc_multiple_unic;
    public $respuesta_enc_multiple_unic_correcta;
    public $pregunta_enc_multiple;
    public $respuesta_enc_multiple;
    public $respuesta_enc_multiple_correcta;

    public function rules(){
        
        return array(
            array('nombre_encuesta, desc_campania_enc', 'required', 'on'=>'campania_encuesta'),
            array('marcasenc, nombre_encuesta, desc_campania_enc, pais_ciudadenc, dias_vigencia_enc', 'required', 'on'=>'solo_encuesta'),
            array('coberturaenc', 'validateEmptyDropDown', 'allowEmpty' => false, 'on'=>'solo_encuesta'),
            array('encuestas_enc, is_freemium_enc', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'Selecciona un número mayor a 1', 'on'=>'campania_encuesta'), 
            array('dias_vigencia_enc, encuestas_enc, is_freemium_enc', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'Selecciona un número mayor a 1', 'on'=>'solo_encuesta'), 
            array('pregunta_enc_abierta, pregunta_enc_multiple_unic, respuesta_enc_multiple_unic, respuesta_enc_multiple_unic_correcta, pregunta_enc_multiple, respuesta_enc_multiple, respuesta_enc_multiple_correcta', 'type', 'type'=>'array', 'allowEmpty' => true)
        );
    }
    
    public function attributeLabels(){
        
        return array(
            'marcasenc' => 'Marcas',
            'nombre_encuesta' => 'Nombre de Encuesta',
            'pais_ciudadenc' => 'La elección de Paises o Ciudades',
            'coberturaenc' => 'Coberturas',
            'desc_campania_enc' => 'Descripción',
        );
    }    
    
    public function validateEmptyDropDown($attributeName, $params){
        $allowEmpty = false;

        if (isset($params['allowEmpty']) and is_bool($params['allowEmpty'])) {
            $allowEmpty = $params['allowEmpty'];
        }
        if (!is_array($this->$attributeName) and empty($this->$attributeName) and count($this->$attributeName) == 0) {                  
            $attributeLabel = $this->getAttributeLabel($attributeName);
            $this->addError($attributeName, "$attributeLabel no puede ser nulo.");    
        }
    }    
}