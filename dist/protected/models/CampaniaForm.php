<?php

class CampaniaForm extends CFormModel
{
    public $marcas;
    public $nombre_campania;
    public $desc_campania;
    public $term_cond;
    public $sexo;
    public $edad_inicial;
    public $edad_final;
    public $is_freemium;
    public $is_diamond;
    public $coverage;
    public $cobertura;
    public $subsidiary;
    public $sucursales;
    public $pais_ciudad; 
    public $dias_vigencia;
    public $fini_dias_vigencia;
    public $ffin_dias_vigencia;
    public $piks;
    public $recomendaciones;
    public $visualizaciones;
    public $impresiones;
    public $ingresos;
    public $videos;
    public $imagen_camp;
    public $imagen_tamano;
    public $costo_cpc;
    public $costo_cpl;
    public $costo_cpa;
    public $costo_recomendacion;
    public $costo_dia;
    public $encuestas_enc;

    public $term_cond2;
    public $desc_campania2;
    public $piks2;
    public $recomendaciones2;
    public $is_freemium2;
    public $is_diamond2;
    public $ingresos2;
    public $dias_vigencia2;
    public $fini_dias_vigencia2;
    public $ffin_dias_vigencia2;
    public $imagen_tamano2;
    public $oferta2;
    public $sexo2;
    public $edad_inicial2;
    public $edad_final2;
    public $videos2;
    public $imagen_camp2;
    public $coverage2;
    public $cobertura2; 
    public $pais_ciudadb;
    public $subsidiary2;
    public $sucursales2;
    public $encuestas_enc2;
    
    public function rules(){
        
        return array(
            array('marcas, nombre_campania', 'required'),
            array('costo_cpc, costo_cpl, costo_cpa, costo_recomendacion, costo_dia', 'numerical', 'integerOnly'=>true), 
            array('desc_campania, sexo, edad_inicial, edad_final, pais_ciudad, dias_vigencia, imagen_tamano', 'required', 'on'=>'content'),           
            array('cobertura', 'validateEmptyDropDown', 'allowEmpty' => false, 'on'=>'content'), 
            array('imagen_camp', 'file', 'allowEmpty'=>false, 'types'=>'jpg,jpeg,gif,png', 'maxSize'=> 1024 * 1024 * 5,  'maxFiles' => 20, 'on'=>'content'), 
            array('sucursales', 'validateEmptyDropDown', 'allowEmpty' => false, 'on'=>'content'),
            array('piks, recomendaciones, is_freemium, is_diamond', 'numerical', 'integerOnly'=>true, 'min'=>0, 'tooSmall'=>'Selecciona un número positivo', 'on'=>'content'),
            array('dias_vigencia, visualizaciones, impresiones, ingresos, encuestas_enc', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'Selecciona un número mayor a 1', 'on'=>'content'),           
//            array('fini_dias_vigencia, ffin_dias_vigencia', 'date', 'on'=>'content'),
            array('videos', 'validateYoutubeURL', 'allowEmpty' => true, 'on'=>'content'),
            array('edad_final', 'compare', 'compareAttribute'=>'edad_inicial', 'operator'=>'>', 'on'=>'content'),
            array('term_cond, imagen_camp', 'safe', 'on'=>'content'),
            
            array('desc_campania2, oferta2, sexo2, edad_inicial2, edad_final2, pais_ciudadb, oferta2, imagen_tamano2', 'required', 'on'=>'promotion'),  
            array('dias_vigencia2, ingresos2, encuestas_enc2', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'Selecciona un número mayor a 1', 'on'=>'promotion'),
            array('piks2, recomendaciones2, is_freemium2, is_diamond2', 'numerical', 'integerOnly'=>true, 'min'=>0, 'tooSmall'=>'Selecciona un número positivo', 'on'=>'promotion'),
            array('edad_final2', 'compare', 'compareAttribute'=>'edad_inicial2', 'operator'=>'>', 'on'=>'promotion'),
            array('videos2', 'validateYoutubeURL', 'allowEmpty' => true, 'on'=>'promotion'),
            array('imagen_camp2', 'file', 'allowEmpty'=>false, 'types'=>'jpg,jpeg,gif,png', 'maxSize'=> 1024 * 1024 * 5,  'maxFiles' => 20, 'on'=>'promotion'), 
            array('cobertura2', 'validateEmptyDropDown', 'allowEmpty' => false, 'on'=>'promotion'),
            array('sucursales2', 'validateEmptyDropDown', 'allowEmpty' => false, 'on'=>'promotion'),            
            array('oferta2', 'length', 'max' => 5, 'allowEmpty' => false, 'on'=>'promotion'),
            array('term_cond2, imagen_camp2', 'safe', 'on'=>'promotion'),            
        );
    }
    
    public function attributeLabels(){
        
        return array(
            'marcas' => 'Marcas',            
            'nombre_campania' => 'Nombre de Campaña',
            'desc_campania, desc_campania2' => 'Descripción',
            'term_cond' => 'Términos y condiciones',
            'edad_inicial' => 'Edad inicial',
            'edad_final' => 'Edad final',
            'sexo' => 'Género',
            'cobertura' => 'Coberturas',
            'sucursales' => 'Sucursales',
            'pais_ciudad' => 'La elección de Paises o Ciudades',
            'imagen_camp' => 'Imágenes',
            'imagen_tamano' => 'Tamaño de imágen',
            
            'desc_campania2' => 'Descripción',
            'term_cond2' => 'Términos y condiciones',
            'edad_inicial2' => 'Edad inicial',
            'edad_final2' => 'Edad final',
            'sexo2' => 'Género',
            'cobertura2' => 'Coberturas',
            'sucursales2' => 'Sucursales',
            'pais_ciudadb' => 'La elección de Paises o Ciudades',
            'imagen_camp2' => 'Imágenes',
            'imagen_tamano2' => 'Tamaño de imágen',            
            'oferta2' => 'Oferta'
        );
    }
    
        public function validateEmptyDropDown($attributeName, $params){
            $allowEmpty = false;

            if (isset($params['allowEmpty']) and is_bool($params['allowEmpty'])) {
                $allowEmpty = $params['allowEmpty'];
            }
            if (!is_array($this->$attributeName) and empty($this->$attributeName) and count($this->$attributeName) == 0) {                  
                $attributeLabel = $this->getAttributeLabel($attributeName);
                $this->addError($attributeName, "$attributeLabel no puede ser nulo.");    
            }
        }    
        
        public function validateYoutubeURL($attributeName, $params){
        
            if (is_array($this->$attributeName) and !empty($this->$attributeName)):
                foreach($this->$attributeName as $item):
                    $pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
                    if(!preg_match($pattern, $item)):
                        $attributeLabel = $this->getAttributeLabel($attributeName);
                        $this->addError($attributeName, "$attributeLabel tiene alguna URL inválida.");                    
                    endif;                
                endforeach;                  
            endif;                      
        }
        
//        public function validateMultiImage($attributeName, $params){
//        
//            if (is_array($this->$attributeName) and !empty($this->$attributeName)):
//                foreach($this->$attributeName as $image):      
//                
//                endforeach;                  
//            endif;           
//        }
}

//El archivo "salud_ABRIL.pdf" no puede ser subido. Solo los archivos con estas extensiones son permitidos: jpg, jpeg, gif, png.
//El archivo "2014-02-14 14.12.35-1.jpg" es muy grande. Su tamaño no puede exceder 2097152 bytes.