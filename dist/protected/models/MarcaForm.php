<?php

class MarcaForm extends CFormModel
{
	public $nombre_marca;
	public $desc_marca;
	public $pagina_web;
        public $pagina_facebook;
        public $page_facebook;
        public $category;
        public $categoria;
        public $moments;
		public $mom_consumo;
		public $foto_cover;
        public $foto_perfil;
        public $location;
        public $position;
        public $admin;
        public $address;
        public $marcaid;
        public $delete;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
                        array('nombre_marca, desc_marca', 'required'),
                        array('foto_cover, foto_perfil', 'file', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png', 'maxSize'=> 5242880, 'maxFiles'=> 1),
                        array('foto_cover, foto_perfil','dimensionValidation'),
                        array('categoria', 'validateEmptyDropDown', 'allowEmpty' => false),
                        array('mom_consumo', 'validateEmptyDropDown', 'allowEmpty' => false),
                        array('location', 'validateEmptyDropDown', 'allowEmpty' => false),
                        array('position', 'validateEmptyDropDown', 'allowEmpty' => false),
                        array('pagina_web', 'validatePageURL', 'allowEmpty' => true),                    
                        array('pagina_facebook', 'validateFacebookURL', 'allowEmpty' => true),
                        array('marcaid', 'numerical', 'integerOnly'=>true),
                        array('address, delete', 'safe'),
                        array('admin', 'type', 'type'=>'array', 'allowEmpty' => true)
                );
	}

	public function attributeLabels()
	{
		return array(
			'nombre_marca'=> 'Nombre de la Marca',
                        'desc_marca'=> 'Descripción de la marca',
                        'categoria' => 'Categoría',
                        'mom_consumo'   => 'Momentos de consumo asociados',
                        'foto_cover' => 'Foto de cover',
                        'foto_perfil' => 'Foto de perfil',
                        'location' => 'Nombre de sucursal',
                        'position' => 'Ubicación de sucursal',
                        'pagina_web' => 'Página Web',
                        'pagina_facebook' => 'Página de Facebook'
		);
	}

        public function validateEmptyDropDown($attributeName, $params){
//            $req = CValidator::createValidator('required', $this, array('categoria') );
//            $req->validate( $this );
            $allowEmpty = false;

            if (isset($params['allowEmpty']) and is_bool($params['allowEmpty'])) {
                $allowEmpty = $params['allowEmpty'];
            }
            if (!is_array($this->$attributeName) and empty($this->$attributeName) and count($this->$attributeName) == 0) {
                $attributeLabel = $this->getAttributeLabel($attributeName);
                $this->addError($attributeName, "$attributeLabel no puede ser nulo.");
            }
        }

        public function validatePageURL($attributeName, $params){

            if (!empty($this->$attributeName)):
                $pattern = '/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i';
                if(!preg_match($pattern, $this->$attributeName)):
                    $attributeLabel = $this->getAttributeLabel($attributeName);
                    $this->addError($attributeName, "$attributeLabel no es una URL válida.");
                endif;
            endif;
        }

        public function validateFacebookURL($attributeName, $params){

            if (is_array($this->$attributeName) and !empty($this->$attributeName)):
                foreach($this->$attributeName as $item):
//                    $pattern = '#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#';
                    $pattern = '/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/';
                    if(!preg_match($pattern, $item)):
                        $attributeLabel = $this->getAttributeLabel($attributeName);
                        $this->addError($attributeName, "$attributeLabel tiene alguna URL inválida.");
                    endif;
                endforeach;
            endif;
        }

        public function dimensionValidation($attributeName, $params){
	    if(is_object($this->$attributeName)){
	        list($width, $height) = getimagesize($this->$attributeName->tempname);
	        if($width!=150 || $height!=150)
	            $this->addError('photo','Photo size should be 150*150 dimension');
	    }
	}
}

        /*array('file', 'file', 'on' => 'insert', 'allowEmpty' => false, 'safe'=>true,
            'maxSize'=> 512000,
            'maxFiles'=> 1,
            'mimeTypes' => 'application/msword, text/plain',
            'tooLarge'=> 'file cannot be larger than 500KB.',
            'wrongMimeType'=> 'Format must be: .doc .txt'
        ),*/
