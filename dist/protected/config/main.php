<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
//Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Wepiku Web',
        'theme'=>'front',

	// preloading 'log' component
	'preload' => array('log', 'bootstrap'),
        'aliases' => array(
            'bootstrap' => realpath(__DIR__. '/../extensions/bootstrap'), // change this if necessary
            'vendor.twbs.bootstrap.dist' => realpath(__DIR__ . '/../extensions/bootstrap/assets'),
        ),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'application.extensions.ExportXLS.*',
                'bootstrap.helpers.TbHtml',
                'bootstrap.components.*',
                'bootstrap.form.*',
                'bootstrap.helpers.*',
                'bootstrap.widgets.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Im4g1n4pp.',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
//			'allowAutoLogin'=>true,
//                        'class'=>'WebUser',
                        'loginUrl'=>array('site/login'),
                        'authTimeout' => 3600,
                        'loginRequiredAjaxResponse' => 'YII_LOGIN_REQUIRED',
		),
                'curl' => array(
                    'class' => 'ext.curl.Curl',
                    'options' => array(/* additional curl options */),
                ),
                'yiitube' => array(
                    'class' => 'ext.Yiitube',
                ),
                'NewsFeedColumn' => array(
                    'class' => 'ext.NewsFeed.NewsFeedColumn',
                ),
//                'session' => array (
//                    'autoStart' => 'true',
                    //'timeout' => 600,
                    //'class' => 'CHttpSession',
                    /*'sessionName' => 'Site Access',
                    'cookieMode' => 'only',
                    'savePath' => '/path/to/new/directory', */
//                ),
		// uncomment the following to enable URLs in path-format
                'urlManager' => array(
                    'urlFormat' => 'path',
                    'showScriptName' => false,
                    //'urlSuffix' => '.html',
                    'rules' => array(
                      'deploy/themes/<theme:\w+>/views/layouts/<page:\w+\.php>' => '<controller>/<page>',
                      'deploy/themes/<theme:\w+>/views/<controller:\w+>/<page:\w+\.php>' => '<controller>/<page>',
                      '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                      '<controller:\w+>/<id:\d+>' => '<controller>/view',
                      '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                      '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                    ),
                ),
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		*/
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
                'bootstrap' => array(
                    'class' => 'bootstrap.components.TbApi',
//                    'cdUrl'=>"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/",
                ),
                'email' => array(
                    'class' => 'application.extensions.email.Email',
                    'deliver' => 'php'
                ),
               'mail' => array(
                  'class' => 'ext.yii-mail.YiiMail',
                    'transportType' => 'smtp',
                    'transportOptions' => array(
                        'host' => 'smtp.mandrillapp.com',
                        'username' => 'wepiku2015@gmail.com',
                        'password' => '2yCKzE8wZP86M33VoEG7mQ',
                        'port' => '587',
                        'encryption'=>'',
                    ),
                    'viewPath' => 'application.views.mail',
                    'logging' => true,
                    'dryRun' => false
                )
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'wepiku2015@gmail.com',
	),
);