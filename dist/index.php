<?php

$ipTest=array(
	'localhost',
);
date_default_timezone_set("America/Bogota"); // you can cange this for your timezone
error_reporting(E_ALL);

// change the following paths if necessary
// Server
// $yii=dirname(__FILE__).'/../../yii/framework/yii.php';
// Local
$yii=dirname(__FILE__).'/../../php/yii-v1-16/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// define if you are in production mode or not, this maybe more beautiful? tell me please
if(in_array($_SERVER['HTTP_HOST'],$ipTest))
	defined('YII_DEBUG') or define('YII_DEBUG',true);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
