<?php // Yii::app()->clientScript->registerCoreScript('jquery-front');?>
<?php echo Yii::app()->bootstrap->register(); ?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>WEPIKU</title>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" rel="shortcut icon" />


<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main_landing.css" />
</head>

<body>



<div class="parallax_fixer"></div>
<div class="loader">
  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png">
</div>

<header>
	<div class="content_header main_container">
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" class="logo left">  
                <?php echo $content; ?>
		<a href="#final_section" class="ms_register">Soy Marca</a>              
	</div>
</header>

<div class="container">
	<div class="section video_section" id="video_section">
		<div class="content_main_logo">
			<div class="v_center1">
				<div class="v_center2">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/main_logo.png">
				</div>
			</div>
		</div>
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bg_home.jpg">
		<a href="#" class="btn_down"></a>
	</div>

	<div class="section section1">
		<a href="#" class="btn_appstore"></a>
		<div class="main_container text-center">
			<p>
			Con WepikU comenzarás a interactuar directamente con tus marcas favoritas de forma personalizada, y podrás conocer promociones, lanzamientos y contenidos relevantes a tus gustos, sin recibir publicidad invasiva. Ademas recibirás beneficios y privilegios sólo por ser activo en la comunidad WepikU ¡Tu tienes el control!
			</p>
		</div>
	</div>
	<div class="section2 text-center">
		<div class="main_container">
			<h2>Tus marcas favoritas en un solo lugar</h2>
			<p>Te presentamos WepikU, la nueva red social con la que puedes comunicarte en un sólo lugar con tus marcas favoritas</p>
		</div>
	</div>

	<div class="main_pin_wrapper">
		<div class="content_pin">
			<div class="pin-frame section3 gradient_bg" id="pin1">
				<div class="v_center1">
					<div class="v_center2">
						<div class="main_container clearfix">
							<div class="pin_text left">
								<h2>Wepiku<br>es móvil</h2>
								<p>Aprovecha los beneficios que tus marcas pueden ofrecerte a tu alrededor en el momento más oportuno. </p>
							</div>
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin1.png" class="right">
						</div>
					</div>
				</div>
			</div>

			<div class="pin-frame section4" id="pin2">
				<div class="v_center1">
					<div class="v_center2">
						<div class="main_container clearfix">
							<div class="pin_text right">
								<h2>Haz parte de la <br>comunidad Wepiku</h2>
								<p>Según tus gustos puedes explorar nuevas marcas similares para multiplicar tus beneficios</p>
							</div>
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin2.png" class="left">
						</div>
					</div>
				</div>
			</div>

			<div class="pin-frame section4" id="pin3">
				<div class="v_center1">
					<div class="v_center2">
						<div class="main_container clearfix">
							<div class="pin_text right">
								<h2>Controla tu <br>privacidad</h2>
								<p>Decide cómo y con qué frecuencia quieres que tus marcas favoritas se comuniquen contigo</p>
							</div>
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin3.png" class="left">
						</div>
					</div>
				</div>
			</div>

			<div class="pin-frame section4" id="pin4">
				<div class="v_center1">
					<div class="v_center2">
						<div class="main_container clearfix">
							<div class="pin_text right">
								<h2>No dejes escapar <br>ninguna oportunidad</h2>
								<p>Haz PIK (Guarda) en tus promociones y contenidos favoritos para que Piki te mantenga alerta y así no dejas pasar ninguna oportunidad. </p>
							</div>
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin4.png" class="left">
						</div>
					</div>
				</div>
			</div>

			<div class="pin-frame section4" id="pin5">
				<div class="v_center1">
					<div class="v_center2">
						<div class="main_container clearfix">
							<div class="pin_text right">
								<h2>Conviértete en <br>un usuario <br>diamante</h2>
								<p>Acumula puntos para ser un usuario privilegiado y así enterarte primero de los nuevos lanzamientos, invitaciones VIP y recibir un trato preferencial.</p>
							</div>
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin5.png" class="left">
						</div>
					</div>
				</div>
			</div>

			<div class="pin-frame section4" id="pin6">
				<div class="v_center1">
					<div class="v_center2">
						<div class="main_container clearfix">
							<div class="pin_text right">
								<h2>Tu opinión vale</h2>
								<p>Cuéntale a tus marcas favoritas tu opinión sobre sus productos o servicios. Comparte tu experiencia, sugerencias e ideas y conviértete en un cliente VIP.</p>
							</div>
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin6.png" class="left">
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="section5">

		<div class="main_container text-center">
			<p>
			Además de nuestro portal web, ya puedes también disfrutar nuestra aplicación para usuarios IPhone, descárgala ahora mismo y comienza a comunicarte con tus marcas preferidas.
			</p>
			<a href="#" class="btn_appstore"></a>



		</div>
	</div>

	<div class="section4 section_marca2 final_section final_usr_section">
		<div class="v_center1">
			<div class="v_center2">
				<div class="main_container clearfix">
					<h2>Crea tu marca ya mismo en WepikU y comienza a construir tu propia comunidad de clientes leales</h2>
					<div class="pin_text ">
						<p>
						Si tienes tu propio negocio, WePikU te da la facilidad de comenzar a desarrollar tu propia comunidad de clientes alrededor de tu empresa o sucursales, de tal forma que te puedan descubrir tus vecinos o clientes potenciales en tu ciudad o a tu alrededor.
						</p>
                                                <?php echo CHtml::link('INGRESO PARA LAS MARCAS', $this->createAbsoluteUrl('/site/loginMarca'), array( 'class' => 'btn_go_in')); ?>
					</div>
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca5.png">
					<?php echo "</br>";?>
					<?php echo "</br>";?>
                    <?php echo CHtml::link('Ver políticas y tratamiento de datos personales', $this->createUrl('site/politicas'), array('target'=>'_blank'));?><?php echo "</br>";?>
                         <?php echo CHtml::link('Ver términos y condiciones', $this->createUrl('site/terminos') , array('target'=>'_blank')); ?>
				</div>
			</div>
		</div>
	</div>


<footer id="final_section">
	<div class="content_footer">
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/imaginamos.jpg">
	</div>
</footer>

</div>

<!--<script src="assets/js/jquery.js"></script>-->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/TweenMax.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.superscrollorama.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/mousewheel.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/easing.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js" type="text/javascript"></script>
<!--<script src="assets/js/TweenMax.min.js"></script>-->
<!--<script src="assets/js/jquery.superscrollorama.js"></script>-->
<!--<script type="text/javascript" src="assets/js/mousewheel.js"></script>-->
<!--<script type="text/javascript" src="assets/js/easing.js"></script>-->
<!--<script src="assets/js/functions.js"></script>-->

</body>
</html>
