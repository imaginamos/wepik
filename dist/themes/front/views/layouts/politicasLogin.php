<?php // Yii::app()->clientScript->registerCoreScript('jquery-front');?>
<?php echo Yii::app()->bootstrap->register(); ?>
<!doctype html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<title>WEPIKU</title>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" rel="shortcut icon" />


<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main_landing.css" />

</head>

<body>



<div class="parallax_fixer"></div>
<div class="loader">
  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png">  
</div>

<header>
	<div class="content_header main_container">
		<a href="http://www.wepiku.com/index.php"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png"></a>
		<?php echo $content; ?>     
	</div>
</header>

<div class="container">
	<div class="main_container">
		<div class="content_politics final_section" style="margin:10px;">
			<h1>Política del Tratamiento de la Información y de Privacidad WePikU</h1>
      		<h2>Tratamiento de Datos Personales</h2></br>
			<div>
Debido a la actividad comercial que desarrolla WePikU, y dando cumplimiento a las directrices de tratamiento de los datos personales establecidas en la Ley 1581 de 2012 “Por la cual se dictan disposiciones generales para la protección de datos personales” y sus Decretos reglamentarios , les informamos a los titulares de los datos personales que WePikU lleva a cabo los siguientes tipos de Tratamiento sobre sus datos personales. Las clases de Tratamiento que se enuncian a continuación deben ser autorizadas por los titulares de los datos de manera previa, expresa e informada: </br></br>

Recolectar información sobre los usuarios de su Sitio Web, Aplicación Móvil y/o Marcas de WePikU y sus vinculadas.</br></br>
Almacenar en nuestras bases de datos la información sobre los usuarios, marcas, vinculadas o proveedores de servicios de WePikU para las finalidades señaladas en la Política en el punto 3.
Utilizar la información proporcionada por los usuarios y titulares de los datos personales para las finalidades consignadas en esta Política en el punto 3.</br></br>
Transferir a terceros (solo con Usuario autores de la información dentro de WepikU), los datos personales para efectos comerciales y de mercadeo y las otras finalidades que se describen en el punto 3.</br></br>

1. Responsable del Tratamiento. WePiku es responsable del Tratamiento de datos personales no públicos  en los términos contenidos en esta Política.</br></br>

Domicilio: Bogotá D.C. - Colombia</br>
Dirección: Cr 11 # 119-63 apto 403</br>
Correo electrónico: info@wepikU.com</br>
Teléfono: (+57) 1 7576836</br>
Página Web: www.wepiku.com</br></br>

Protegiéndolo: Sus derechos frente al Tratamiento de sus datos personales.</br>                                               
WePikU está comprometido con proteger su privacidad y garantizar sus derechos al habeas data para que Usted pueda conocer, actualizar, rectificar, suprimir y eliminar los datos personales que administre WePikU. Por ello, le informamos que Usted, de acuerdo con la Ley 1581 de 2012 y sus Decretos reglamentarios, como titular de datos personales, cuenta con los siguientes derechos: </br></br>

Conocer, actualizar, rectificar y/o suprimir sus datos personales frente a WePikU. Este derecho se podrá ejercer, entre otros, frente a datos personales parciales, inexactos, incompletos, fraccionados y/o aquellos que induzcan a error. </br></br> 

Solicitar prueba de la autorización otorgada a WePikU, salvo que exista una disposición legal que indique que dicha autorización no es necesaria.</br></br>

Presentar solicitudes a WePikU respecto del uso que le han dado a sus datos personales, y a que ésta le entregue tal información. </br></br>

Presentar ante la Autoridad de Protección de Datos Personales de Colombia quejas por infracciones a la Ley 1581 de 2012 y sus Decretos reglamentarios.</br></br>

Revocar su autorización y/o solicitar la supresión de sus datos personales de las bases de datos de WePikU, siempre y cuando no exista un deber legal, como por ejemplo en aquellos casos en que el usuario ha realizado compras existe una obligación legal y tributaria de dejar el registro de las compras realizadas, por lo cual esta información debe permanecer en el registro de ventas de WePikU SAS, lo anterior con fundamento en lo dispuesto en el artículo 28 de la Ley 962 de 2005 ) ó una obligación de carácter contractual en cabeza del titular con WePikU, en virtud de la cual el titular no tenga el derecho de solicitar la supresión de sus datos personales o revocar su autorización para el Tratamiento de los mismos. Si no hay un deber legal o contractual y WePikU no ha suprimido los datos personales del titular de sus bases de datos o no ha revocado la autorización de quien está legitimado para revocarla dentro del término legal para ello, el titular podrá acudir a la Autoridad de Protección de Datos Personales de Colombia para que exija la revocación de la autorización y/o la supresión de los datos personales.</br></br>

Solicitar acceso y acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento.</br></br>

Esta Política garantiza que cualquier información que nos provea será mantenida de manera privada y segura. Para dar fe de esto, en este documento proveemos los detalles de qué información recabamos y de qué manera la utilizamos. Nunca recolectaremos información sin su consentimiento explícito, previo e informado.</br></br>

Ser informado por el responsable del Tratamiento, previa solicitud, respecto del uso que le ha dado a sus datos personales.</br></br>

Este documento es parte integrante de los Términos y Condiciones de WePikU SAS. Mediante la aceptación de los Términos y Condiciones Usted reconoce que ha sido informado sobre nuestra Política.</br></br>


La naturaleza de la información que recolectamos.</br>
Para el desarrollo de sus actividades comerciales y operativas WePikU recolectará, previa autorización expresa, libre e informada, los datos personales de los titulares. Esta información será utilizada y procesada por WepikU para mejorar la efectividad de los contenidos pautados, mejorar la experiencia de los usuarios, y la relevancia de los contenidos entregados. </br></br>

Esta información recolectada incluirá:</br></br>

Información de identificación personal (IIP) como nombres y apellidos, teléfono, dirección y otro tipo de información que le permita a WePikU identificar a los usuarios. Esta información es complementada con los datos registrados en la cuenta de Facebook del usuario, previa autorización del mismo.</br></br>

Dirección IP (Internet Protocol) con el fin de diagnosticar problemas o inconvenientes con nuestro servidor, así como para administrar su Sitio Web. Una dirección de IP es un número que se le asigna a su computadora cuando usa internet. Su dirección de IP también es utilizada para ayudar a identificarle dentro de una sesión particular y para recolectar información demográfica general.</br></br>

Correo electrónico para la utilización del Sitio Web, y la vinculación a WePikU.</br></br>

Información sobre la actividad de los Usuarios dentro de nuestro Sitio Web y/o Aplicación Móvil. Tal información puede incluir la URL de la que provienen, a qué URL acceden frecuentemente, qué navegador están usando, así como también las páginas visitadas, las búsquedas realizadas, las publicaciones, compras, contenidos publicitados, comentarios, likes, publicaciones en otras redes sociales desde la plataforma, entre otros datos relacionados con las funcionalidades de la plataforma. </br></br>

Información recolectada por medio de nuestro Sitio Web y/o Aplicación Móvil, la cual incluye pero no está limitada a la siguiente: </br></br>

Información Financiera </br>
Información Profesional</br>
Información Socio-económica</br>
Información Geográfica</br>
Información Demográfica</br>
Información Psicográfica</br>
Información Conductual</br></br>

Un nombre de usuario y una contraseña para acceder al sistema en línea
Estrato, número de personas que habitan en su hogar, profesión y sexo
Preferencias de acuerdo a encuestas realizadas por WePikU, sus marcas y/o vinculados, y preferencias de acuerdo a su perfil, todo ello para efectuar comunicaciones de mercadeo, promociones y campañas de marketing directo de WePikU y/o sus marcas y vinculadas. 
Información que se encuentre en el dominio público para complementar las bases de datos. A dicha información se le dará el mismo tratamiento señalado en la presente Política de Privacidad.</br></br>


3. Finalidades y uso que hacemos de la información.
Con el fin de suministrar un excelente servicio y para hacer más efectivas, ágiles y seguras las operaciones que los usuarios realicen, WePikU recolectará los datos personales de los usuarios únicamente para las siguientes finalidades:</br></br>

Para hacer más efectivas y seguras las transacciones que se lleven a cabo en WePikU.
Para la administración del Sitio Web y Aplicación Móvil en el que el usuario entra para explorar el contenido y promociones proporcionadas por WePikU, sus marcas y/o vinculados.
Para cumplir a cabalidad con los servicios celebrados con las marcas y usuarios, de acuerdo con sus finalidades para brindar el contenido, promociones, servicios o productos de WePikU, sus marcas y/o vinculados.</br></br>
Para complementar la información y, en general, adelantar las actividades necesarias para gestionar las solicitudes, quejas y reclamos presentadas por las marcas o usuarios de WePikU y por terceros, y direccionarlas a las áreas responsables de emitir las respuestas correspondientes.</br></br>
Enviar información y ofertas comerciales relevantes de productos de WePikU, sus marcas y/o vinculados, así como realizar actividades de mercadeo y/o comercialización de servicios y/o productos que preste o que pudiera llegar a prestar u ofrecer WePikU, sus marcas y/o vinculados.</br></br>
Comunicación hacia las marcas sobre los diferentes programas de beneficios e informaciones promocionales de WepikU. </br></br>
Elaborar estudios de mercado, estadísticas, encuestas, análisis de tendencias del mercado, encuestas de satisfacción sobre los servicios prestados por WePikU.
Para la transmisión de datos personales de los usuarios de la información, hacia los usuarios autores de la información (marcas en WepikU) para fines comerciales, administrativos y/o operativos. </br></br>
Gestionar toda la información necesaria para el cumplimiento de las obligaciones tributarias y de registros comerciales, corporativos y contables de WePikU.
Para identificar a los usuarios cuando ingresen al Sitio Web.
Para ofrecerles todos los servicios y funcionalidades que se adecuan a los gustos e intereses personalizados de los usuarios.</br></br>
Para proceder a la facturación y cobro del WePikU.
Para enviar información o mensajes sobre los nuevos productos y/o servicios, mostrar la publicidad o promoción del momento, banners, noticias sobre WePikU y toda otra información que creamos conveniente.</br></br>
Compartir los datos personales con empresas de servicios o empresas de “outsourcing” que contribuyan a mejorar o a facilitar las operaciones a través de WePikU, dentro de las que se incluyen, medios de pago, seguros o intermediarios de la gestión de pagos. WePikU velará porque las políticas de los terceros tengan estándares similares a los de la presente Política, mediante la firma de acuerdos, convenios y/o contratos.
Para suministrar los datos personales de los titulares a las entidades que intervengan en la resolución de conflictos y que tengan competencia para ello.</br></br>


4. Confidencialidad de la información</br></br>
Los datos de los usuarios serán Tratados únicamente por WePikU en las formas establecidas en esta Política. WePikU hará todo lo que esté a su alcance para proteger la privacidad de la información. En ocasiones puede suceder que en virtud de órdenes judiciales, o de regulaciones legales, WePikU se vea compelido a revelar información a las autoridades o terceras partes. Los titulares reconocen que su autorización no será necesaria, en virtud de la Ley 1581 de 2012 y sus Decretos reglamentarios, cuando los datos personales sean requeridos por una entidad pública o administrativa en ejercicio de sus funciones legales o por orden judicial. WePikU dispondrá de todos los medios tecnológicos y adoptará todas las medidas de seguridad a su alcance para evitar que la información suministrada por los titulares sea interceptada por terceros. El titular declara que conoce y acepta que en aquellos casos en que la información suministrada a WePikU sea obtenida por terceros mediante medios fraudalentos, WePikU no es responsable de dichas conductas fraudulentas.  </br></br>

WePikU aplicará las limitaciones legales al tratamiento de datos sensibles, por lo cual se asegurará que:</br></br>

a) El Titular haya dado su autorización explícita a dicho Tratamiento, salvo en los casos que por ley no sea requerido el otorgamiento de dicha autorización;</br></br>
 
b) El Tratamiento sea necesario para salvaguardar el interés vital del Titular y este se encuentre física o jurídicamente incapacitado. En estos eventos, los representantes legales deberán otorgar su autorización;</br></br>
 
c) El Tratamiento sea efectuado en el curso de las actividades legítimas y con las debidas garantías por parte de una fundación, ONG, asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, religiosa o sindical, siempre que se refieran exclusivamente a sus miembros o a las personas que mantengan contactos regulares por razón de su finalidad. En estos eventos, los datos no se podrán suministrar a terceros sin la autorización del Titular;</br></br>
 
d) El Tratamiento se refiera a datos que sean necesarios para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial;</br></br>
 
e) El Tratamiento tenga una finalidad histórica, estadística o científica. En este evento deberán adoptarse las medidas conducentes a la supresión de identidad de los Titulares.</br></br>





5. Menores de Edad</br></br>
Nuestros servicios para autores de información (Marcas en WepikU), sólo están disponibles para aquellas personas que tengan capacidad legal para contratar. Por lo tanto, aquellos que no cumplan con esta condición solo podrán registrarse como usuario de la información, sin la posibilidad de ser un usuario autor de la información, a través de los padres, tutores o curadores, conforme lo establecido en nuestros Términos y Condiciones, el tutor o representante legal del menor de edad otorgará su autorización una vez el menor haya ejercido su derecho de ser escuchado y se haya valorado su opinión en conformidad con su madurez, autonomía y capacidad para atender el asunto. Los datos personales de los menores de edad deben respetar el interés superior de los niños, niñas y adolescentes y se protejan sus derechos fundamentales. De acuerdo con la Ley 1581 de 2012 y sus Decretos reglamentarios, WePikU no tratará datos sensibles de menores de edad, definidos éstos últimos por la Ley 1581 de 2012 como aquellos que afectan la intimidad del titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen afiliaciones sindicales, el origen racial o étnico, la orientación política, las convicciones religiosas, morales o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promuevan intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición, así como los datos relativos a la salud, a la vida sexual, y los datos biométricos.</br></br>


6. Cookies </br></br>
El usuario del Sitio Web de WePikU conoce y acepta que WePikU podrá utilizar un sistema de seguimiento mediante la utilización de cookies (las "Cookies"). Las Cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios. También ofrecemos ciertas funcionalidades que sólo están disponibles mediante el empleo de Cookies. Las Cookies se utilizan con el fin de conocer los intereses, el comportamiento y la demografía de quienes visitan o son visitantes de nuestro Sitio web y de esa forma, comprender mejor sus necesidades e intereses y darles un mejor servicio o proveerle información relacionada. También usaremos la información obtenida por intermedio de las Cookies para analizar las páginas navegadas por el visitante o usuario, las búsquedas realizadas, mejorar nuestras iniciativas comerciales y promocionales, mostrar publicidad o promociones, banners de interés, noticias sobre WePikU, perfeccionar nuestra oferta de contenidos y artículos, personalizar dichos contenidos, presentación y servicios. </br></br>

Adicionalmente WePikU utiliza las Cookies para que el usuario no tenga que introducir su clave tan frecuentemente durante una sesión de navegación, también para contabilizar y corroborar las registraciones, la actividad del usuario y otros conceptos y acuerdos comerciales, siempre teniendo como objetivo de la instalación de las Cookies, el beneficio del usuario que la recibe, y no será usado con otros fines ajenos a WePikU. Se establece que la instalación, permanencia y existencia de las Cookies en el computador del usuario o del visitante depende de su exclusiva voluntad y puede ser eliminada de su computador cuando así lo desee. Para saber cómo quitar las Cookies del sistema es necesario revisar la sección Ayuda (Help) del navegador. También, se pueden encontrar Cookies u otros sistemas similares instalados por terceros en ciertas páginas de nuestro Sitio Web. WePikU SAS no controla el uso de Cookies por terceros.</br></br>


7. E-mails</br></br>
Nuestra Política, respecto del envío de emails, tiene los siguientes propósitos:
E-mails con recordatorios de los servicios que ofrecemos (especialmente aquellos que aún no haya utilizado o no haya utilizado en un tiempo considerable).
Para enviar información sobre las ofertas que haya pedido.
Como parte de un Newsletter.
Como e-mails promocionales.
Para ofrecer servicios relacionados.
De todas maneras, en cada uno de los e-mails que enviemos siempre ofreceremos la posibilidad de desuscribirse (opt-out) para dejar de recibir e-mails en el futuro.
Si el titular del email no quiere seguir vinculado ni recibiendo emails para las finalidades anteriores, el usuario puede hacer click en "desuscribirte del newsletter" que se encuentra al final de todos los correos diarios que recibas. 


8. Seguridad y almacenamiento</br></br>
Empleamos diversas técnicas de seguridad para proteger tales datos de accesos no autorizados por visitantes del Sitio de dentro o fuera de nuestra compañía. Sin embargo, es necesario tener en cuenta que en Internet pueden llevarse a cabo interceptaciones ilegales o violación de los sistemas o base de datos de WePikU por parte de personas no autorizadas, casos en los cuales los titulares declaran que conocen y aceptan que WePikU no es responsable de esas conductas fraudulentas y que escapan a sus mecanismos de protección.


9. Transferencia en circunstancias especiales</br></br>
Si existe una venta, una fusión, consolidación, cambio en el control societario, transferencia de activos sustancial, reorganización o liquidación de WePikU SAS entonces, en nuestra discreción, podemos transferir, vender o asignar la información recabada en este Sitio a una o más partes relevantes.


10. Servicio al cliente</br></br>
Para atender cualquier duda, consulta, queja, pregunta, reclamación o solicitud de cualquier tipo de información relacionada con sus datos personales, particularmente, para ejercer sus derechos a conocer, actualizar, rectificar y suprimir el dato o revocar la autorización otorgada, WePikU ha dispuesto a info@wepiku.com, como encargado. 

Domicilio: Bogotá - Colombia</br>
Dirección: Cr 11 # 119-63 apto 403</br>
Correo electrónico: info@wepikU.com</br>
Teléfono: (+57) 1 7576836</br>
Página Web: www.wepiku.com</br></br>


11. Procedimientos para ejercer sus derechos</br></br>
11.1. CONSULTAS </br></br>
WePikU dispone de mecanismos para que el titular, sus causahabientes, sus representantes y/o apoderados, aquellos que por estipulación a favor de otro o para otro estén legitimados, o los representantes de menores de edad titulares, formulen CONSULTAS respecto de cuáles son los datos personales del titular que reposan en las bases de datos de WePikU. Estos mecanismos podrán ser trámites electrónicos a través del correo electrónico de info@wepiku.com. Cualquiera que sea el medio, WePikU guardará prueba de la consulta y su respuesta. </br></br>

Antes de proceder, el responsable de atender la consulta verificará: </br></br>

La identidad del titular del dato personal o su representante. Para ello, exigirá la cédula de ciudadanía o documento de identificación original del titular y los poderes especiales o generales según sea el caso. </br></br>
La autorización o contrato con terceros que dieron origen al Tratamiento del dato personal del titular, por parte de WePikU. </br></br>
Si el solicitante tuviere capacidad para formular la consulta, el responsable de atenderla recopilará toda la información sobre el titular que esté contenida en el registro individual de esa persona o que esté vinculada con la identificación del titular dentro de las bases de datos de WePikU. </br></br>

El responsable de atender la consulta dará respuesta al solicitante siempre y cuando éste último tuviere derecho a ello por ser el titular del dato personal, su causahabiente, su representante y/o apoderado, aquellos que por estipulación a favor de otro o para otro estén legitimados, o el responsable legal en el caso de menores de edad. Esta respuesta será enviada dentro de los diez (10) días hábiles contados a partir de la fecha en la que la solicitud fue recibida por WePikU. Esta respuesta será obligatoria aún en los casos en que se considere que el solicitante no tiene capacidad para realizar la consulta, en cuyo caso así se le informará al solicitante y se dará opción de que demuestre el interés y capacidad aportando documentación adicional. </br></br>

En caso de que la solicitud no pueda ser atendida a los diez (10) hábiles, se contactará al solicitante para comunicarle los motivos por los cuales el estado de su solicitud se encuentra en trámite y señalando la fecha en la que se atenderá la consulta, la cual en ningún caso podrá superar los cinco (5) días hábiles siguientes al vencimiento del primer término. Para ello se utilizará el mismo medio o uno similar a aquel mediante el cual fue presentada la consulta. </br></br>

La respuesta definitiva a todas las solicitudes no puede tardar más de quince (15) días hábiles desde la fecha en la que la solicitud inicial fue recibida por WePikU. </br></br>


11.2. RECLAMOS </br></br>
WePikU dispondrá de mecanismos para que el titular, sus causahabientes, sus representantes y/o apoderados, aquellos que por estipulación a favor de otro o para otro estén legitimados, o los representantes de menores de edad titulares, formulen RECLAMOS respecto de (i) datos personales Tratados por WePikU que deben ser objeto de corrección, actualización o supresión, o (ii) el presunto incumplimiento de los deberes legales de WePikU. Estos mecanismos podrán ser electrónicos como correo electrónico. Cualquiera que sea el medio, WePikU deberá guardar prueba de la consulta y su respuesta. </br></br>
El RECLAMO deberá ser presentado por el titular, sus causahabientes; las personas facultadas para representar a niños, niñas o adolecentes, cuando estos sean titulares; sus representantes y/o apoderado; aquellos que por estipulación a favor de otro o para otro estén legitimados, o sus representantes, así: </br></br>

El RECLAMO deberá ser presentado por el titular, sus causahabientes; las personas facultadas para representar a niños, niñas o adolecentes, cuando estos sean titulares; sus representantes y/o apoderado; aquellos que por estipulación a favor de otro o para otro estén legitimados, o sus representantes, así:</br></br>
Deberá dirigirse a info@wepiku.com</br></br>
Deberá contener una descripción de los hechos que dan lugar al reclamo y el objetivo perseguido (actualización, corrección o supresión, o cumplimiento de deberes).
Deberá indicar la dirección y datos de contacto e identificación del reclamante.
Deberá acompañarse por toda la documentación que el reclamante quiera hacer valer.</br></br></br></br>

Antes de proceder, el responsable de atender el reclamo verificará:</br></br>
La identidad del titular del dato personal o su representante. Para ello, puede exigir la cédula de ciudadanía o documento de identificación original del Titular, y los poderes especiales o generales según sea el caso.</br></br>
La autorización o contrato con terceros que dieron origen al Tratamiento, por parte de la WePikU, del dato personal del titular.</br></br>
Si el reclamo o la documentación adicional están incompletos, WePikU requerirá al reclamante por una sola vez dentro de los cinco (5) días siguientes a la recepción del reclamo para que subsane las fallas. Si el reclamante no presenta la documentación e información requerida dentro de los dos (2) meses siguientes a la fecha del reclamo inicial, se entenderá que ha desistido del reclamo.</br></br>
Si por cualquier hecho la persona que recibe el reclamo al interior de WePikU no es competente para resolverlo, dará traslado al [área de protección de datos o persona encargada], dentro de los dos (2) días hábiles siguientes a haber recibido el reclamo, e informará de dicha remisión al reclamante.</br></br>

Una vez recibido el reclamo con la documentación completa, se incluirá en la base de datos de WePikU donde reposen los datos del titular sujetos a reclamo una leyenda que diga “reclamo en trámite” y el motivo del mismo, en un término no mayor a dos (2) días hábiles. Esta leyenda deberá mantenerse hasta que el reclamo sea decidido. </br></br>

El término máximo para atender el reclamo será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo. Cuando no fuere posible atender el reclamo dentro de dicho término, se informará al interesado los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término.</br></br>


12. Modificaciones de las Políticas de Privacidad</br></br>
WePikU podrá modificar en cualquier momento los términos y condiciones de esta Política. Cualquier cambio será notificado previa o simultáneamente a los titulares de los datos personales y será efectivo apenas se haya notificado y/o publicado en el Sitio Web y haya sido debidamente aceptado por el titular de la información. Dependiendo de la naturaleza del cambio podremos anunciar el mismo a través de: (a) la página de inicio del Sitio Web, (b) un e-mail a las personas que estén en nuestra base de datos, o (c) en la Aplicación Móvil. De todas maneras, el continuo uso de nuestro Sitio Web y/o Aplicación Móvil implica el conocimiento por parte del usuario de los Términos de esta Política. Si Usted no está de acuerdo con la Política, absténgase de utilizar el Sitio Web y la Aplicación Móvil y de otorgar su autorización a WePikU para llevar a cabo los Tratamientos aquí descritos de sus datos personales con las finalidades aquí mencionadas. Cualquier modificación de este documento será notificado a los usuarios para que éstos den su consentimiento o aceptación.</br></br>



13. Vigencia</br></br>
Esta Política empezará a regir desde la fecha de su publicación y para los titulares de la información será aplicable desde el momento en que manifiesten su aceptación. Los datos personales que sean almacenados, utilizados o transmitidos permanecerán en la base de datos de WePikU, con base en el criterio de temporalidad, durante el tiempo que sea necesario para las finalidades mencionadas en esta Política, para las cuales fueron recolectados. De este modo, la vigencia de la base de datos está estrechamente relacionada con las finalidades para las cuales fueron recolectados los datos personales. En este sentido, la temporalidad o vigencia está supeditada a las finalidades del Tratamiento aquí consignadas.

14. Autorización</br></br>

Exceptuando los casos definidos en la Ley 1581 de 2012 y sus Decretos reglamentarios, en los casos en que se requiera contar con la autorización previa del titular, WePikU solicitará la aceptación de esta política de tratamiento de datos al momento en que el titular se registra por primera vez en la plataforma y de las modificaciones a la presente política al momento en que el usuario ingrese a su cuenta en la plataforma. La autorización deberá ser expresa e informada.</br></br></br>
</div>
		</div>

	</div>

	<footer>
	<div class="content_footer">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/imaginamos.jpg">
	</div>
</footer>

</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/TweenMax.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.superscrollorama.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/mousewheel.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/easing.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js" type="text/javascript"></script>

</body>