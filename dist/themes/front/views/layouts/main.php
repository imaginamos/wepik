<?php Yii::app()->bootstrap->register(); ?>
<?php //session_start();  ?>
<!DOCTYPE html>
<!--[if lt IE 8]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9 ie8" lang="es"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 ie9" lang="es"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> 
<html class="no-js" lang="es"> <!--<![endif]-->
    <head>
        <title>Wepiku</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="date" content="<?php echo date("Y"); ?>" />
        <meta name="author" content="diseño web: imaginamos.com" />
        <meta name="robots" content="All" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <meta name="viewport" content="width=1320, maximum-scale=2" />
        <meta name="Keywords" content="palabras clave" lang="es" />
        <meta name="Description" content="texto empresarial" lang="es" />
        <link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />

        <!--Styles after front-end-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/add/styles.css" />

            <!-- <script async src="http://modernizr.com/downloads/modernizr-latest.js" type="text/javascript"></script> -->
        <script async type="text/javascript">
            var baseUrl = "<?php echo Yii::app()->theme->baseUrl; ?>";
        </script>

        <?php echo $this->builtHeader() ?>
    </head>
    <body>
        <?php //echo Yii::app()->bootstrap->register(); ?>
        <div id="preload"><div class="preload-logo"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png"></div></div>

        <div class="con-bw"><div class="info-bw"><div class="head-bw clearfix"><div class="logo-bw left"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png"></div><div class="tx-bw left"><p>Oops!... Lo sentimos, este sitio se ha desarrollado para navegadores modernos con el fin de mejorar tu experiencia.</p><p>Para que lo puedas disfrutar es necesario actualizar tu navegador o simplemente descargar e instalar uno mejor.</p></div></div><div class="con-icon-bw left"><a href="https://www.google.com/intl/es/chrome/browser/?hl=es" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-1.png"></div><p>Chrome</p></a></div><div class="con-icon-bw left"><a href="http://www.mozilla.org/es-ES/firefox/new/" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-2.png"></div><p>Firefox</p></a></div><div class="con-icon-bw left"><a href="http://www.opera.com/es-419/computer/" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-3.png"></div><p>Opera</p></a></div><div class="con-icon-bw left"><a href="http://support.apple.com/kb/DL1531?viewlocale=es-ES" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-4.png"></div><p>Safari</p></a></div><div class="con-icon-bw left"><a href="http://windows.microsoft.com/es-es/internet-explorer/download-ie" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-5.png"></div><p>Explorer</p></a></div><div class="con-bt-bw left clearfix"><a class="bt-bw"></a></div></div></div>



        <!-- <div class="line-h"></div>
        <div class="line-v"></div>
        <div class="line-ml"></div>
        <div class="line-mr"></div> -->



        <!--
                        <div id="main-wrapper">
                                <div class="animated fadeIn">
        -->

        <!--div class="container session-message">
        <?php #if(($msgs=$this->builtMessages())!==null and $msgs!==array()):?>
                <div class="row">
                        <div class="col-lg-12">
        <?php #foreach($msgs as $type => $message):?>
                                        <div class="alert alert-<?php #echo $type ?>">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php #echo $message?>
                                        </div>
        <?php #endforeach;?>
                        </div>
                </div>
        <?php #endif;?>
        </div-->


        <section>
            <div class="header-label">

            </div>
            <div class="container-fluid header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-nav">
                            <header>
                                <nav>
                                    <div class="nav-main">
                                        <a class="brand-logo link" href="<?php echo $this->createUrl('hotdeals/index') ?>">
                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png" alt="">
                                        </a>
                                        <div class="user text-center">
                                            <div class="user-img" data-toggle="modal" data-target="#modal-user">
                                                <?php $user_img = CHtml::image(Yii::app()->session['wepiku_user_img'], "", array("width" => "76px", "height" => "76px")); ?>
                                                <?php
                                                echo TbHtml::ajaxLink(
                                                        $user_img, $this->createUrl('site/getInfoPerfilBackend'), array(
                                                    'dataType' => 'json',
                                                    'type' => 'POST',
                                                    'data' => array(
//                                                                                                                                    'camp' => $campania['id'],
                                                        'modal' => '#modal-user'
                                                    ),
                                                    'success' => 'function(data){
                                                                                                                                    openModal("#modal-user", data.content);
                                                                                                                                }'
                                                        ), array(
                                                    'id' => 'open-user-modal-' . uniqid()
                                                        )
                                                );
                                                ?>
                                            </div>
                                            <h2><?php echo Yii::app()->session['facebook_nombre']; ?></h2>
                                        </div>

                                        <ul class="nav-list">
<?php if (Yii::app()->session['is_new'] == false): ?>
                                                <li>
                                                    <a class="nav0" href="<?php echo $this->createUrl('hotdeals/index') ?>"><span class="icon9"></span>Hotdeals</a>
                                                </li>
                                                <li>
                                                    <a class="nav1" href="<?php echo $this->createUrl('site/puntos') ?>"><span class="icon1" style="color: #fff;"></span>Mis puntos</a>
                                                </li>
                                                <li>
                                                    <a class="nav2 piks_btn" href="<?php echo $this->createUrl('campanias/piks') ?>"><span class="icon2"></span>
                                                        Piks
														</a>
															   <?php
														 // print_r($_SESSION);
																
                                if (Yii::app()->session['contpiks'] != 0) {
																	echo TbHtml::badge(Yii::app()->session['contpiks'], array('color' => TbHtml::BADGE_COLOR_WARNING));
                             		}
															?>
                                                 
                                                </li>
                                                <li>
                                                    <a class="nav3" href="<?php echo $this->createUrl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=1') ?>"><span class="icon3"></span>Buscar en Mapa</a>
                                                </li>
                                                <li>

                                                    <a class="nav4" href="<?php echo $this->createUrl('marcas/index') ?>"><span class="icon4"></span>Directorio de marcas</a>
                                                </li>
                                                <li>
                                                    <a class="nav5" href="<?php echo $this->createUrl('marcas/nueva') ?>"><span class="icon5"></span>Crear marca</a>
                                                </li>
                                                <?php $this->widget('ext.Marcas.MarcasMenu');
                                                ?>
                                                <li>
                                                    <a class="nav7" href="<?php echo $this->createUrl('campanias/como') ?>"><span class="icon8"></span>Campañas y encuestas</a>
                                                </li>
                                            <?php endif; ?>
                                            <li>
                                                <a data-toggle="modal" data-target="#modal-stores" class="call-stores"><span class="icon6"></span>Descargar App</a>
                                            </li>

                                            <li>
                                                <a href="<?php echo $this->createUrl('site/logout') ?>"><span class="icon7"></span>Salir</a>
                                            </li>

                                        </ul>

                                        <div class="main-social text-center">
                                            <?php //echo CHtml::link("", $this->createUrl('site/Compartirapp',array('provider'=>'facebook')), array('class' => 'social social1', 'target' =>'_blank'));  ?>
                                            <?php
                                            echo TbHtml::ajaxLink(
                                                    '<div class="social social1"></div>', $this->createUrl('site/CompartirApp'), array(
                                                'dataType' => 'json',
                                                'type' => 'POST',
                                                'data' => array(
                                                    'provider' => 'facebook',
                                                    'modal' => '#modal-comp-facebook'
                                                ),
                                                'success' => 'function(data){
                                                                var left = (screen.width/2)-(558/2);
                                                                var top = (screen.height/2)-(336/2); 
                                                                ventana_secundaria = window.open(data, "feedDialog", "toolbar=0,status=0,scrollbars=no,width=558,height=336, top="+top+", left="+left); 
																
																setTimeout(function(){ 
																	ventana_secundaria.close();
																}, 3000);
																
                                                        }'
                                                    ), array(
                                                'id' => 'open-user-modal-' . uniqid()
                                                    )
													
                                            );
                                            ?>
                                            <?php $url = 'http://www.wepiku.com/hotdeals/index';
                                            $text = 'Wepiku la mejor forma de dar a conocer tus marcas';?>
                                            <?php echo CHtml::Link(
                                                '<div class="social social2"></div>',
                                                'http://twitter.com/intent/tweet?url='.$url.'&text='.$text.'&via=wepiku',
                                                array(
                                                    'id' => 'share-link-twitter'.uniqid(),
                                                    'target' => '_blank'
                                                )                        
                                            ); ?>    
                                            <?php //echo CHtml::link("", $this->createUrl('site/Compartirapp', array('provider' => 'twitter')), array('class' => 'social social2', 'target' => '_blank')); ?>

                                        </div>
                                    </div>
                                </nav>
                            </header>
                        </div>

                        <div id="load-content">
                            <?php echo $content; ?>
                        </div>
                        <?php if ($this->hasNewsFeed == true): ?>
                            <div class="col-xs-12 col-sm-2 col-sub-nav">
                                <div class="sub-nav active">
                                    <button class="btn-sub-nav" type="button">
                                        <span class="btn-arrow"></span>
                                        <span class="btn-icon"></span>
                                    </button>
                                    <?php if (Yii::app()->session['is_new'] == false): ?>
                                        <div class="sub-nav-list">
                                            <?php
                                            echo $this->buildNewsFeed();
                                            $this->widget('ext.NewsFeed.NewsFeedColumn', array(
                                                'newFeed' => $this->response_newsfeed,
                                                'marca' => $this->response_marca));
                                            ?>
                                        </div>
                                        <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>


        <!--
                                </div>
                        </div>
        -->
        <!-- Modal product -->
        <div class="modal modal-item fade" id="modal-product" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal product -->

        <!-- Modal marcas -->
        <div class="modal modal-item fade" id="modal-marca" role="dialog" aria-labelledby="modal-marca" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal marcas -->


        <!-- Modal admin product -->
        <!--        <div class="modal modal-item modal-admin fade" id="modal-admin-product" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
                        <div class="modal-scroll">
                                <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                        <button type="button" class="close close-pro" data-dismiss="modal"></button>
                                                        <div class="admin-actions">
                                                                                                                <div class="caja-iz">
                                                                <button type="button"><span class="admin-icon1"></span><strong>20</strong></button>
                                                                <button type="button"><span class="admin-icon2"></span><strong>140</strong></button>
                                                                <button type="button"><span class="admin-icon3"></span><strong>4</strong></button>
                                                                                                                        <button type="button"><span class="admin-icon3"></span><strong>4</strong></button>
                                                                                                                </div>
                                                                                                                <div class="caja-dr">
                                                                <button type="button" class="pull-right"><span class="admin-icon5"></span>Pausar campaña</button>
                                                                <button type="button" class="pull-right"><span class="admin-icon4"></span>Generar reporte</button>
                                                                <button type="button" class="pull-right"><span class="admin-icon4"></span>Generar reporte</button>
                                                                                                                </div>
                                                                                                                        <div class="clear"></div>
                                                        </div>
        
                                                        <div class="con-modal-slider">
                                                                <div class="discount">
                                                                        45%
                                                                </div>
                                                                <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="8000">
                                                                        <ol class="carousel-indicators">
                                                                    <li data-target=".carousel-fade" data-slide-to="0" class="active"></li>
                                                                    <li data-target=".carousel-fade" data-slide-to="1"></li>
                                                                  </ol>
                                                                  <div class="carousel-inner">
                                                                    <div class="item active">
                                                                     <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/modal-slide1.jpg" alt="">
                                                                    </div>
                                                                    <div class="item">
                                                                      <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/modal-slide2.jpg" alt="">
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="brand-head">
                                                                <div class="modal-share">
<?php // $button_share = CHtml::htmlButton('<span></span>', array('class' => 'btn-share', 'type'=>'button'));  ?>
<?php // echo CHtml::link($button_share, array('site/compartir'));  ?>
        <?php /* echo TbHtml::buttonDropdown('<span></span>',
          array(
          array('label' => 'compartir en Facebook', 'url' => '#'),
          array('label' => 'compartir en Twitter', 'url' => '#'),
          array('label' => 'compartir en Wepiku', 'url' => '#'),
          ), array(
          'class' => 'btn-share'
          )
          ); */ ?>
        <?php // echo TbHtml::button('<span></span>', array('class' => 'btn-pick')); ?>
        <?php //$button_pik = CHtml::htmlButton('<span></span>', array('class' => 'btn-pick', 'type'=>'button')); ?>
        <?php //echo CHtml::link($button_pik, array('site/compartir')); ?>
        <?php //Yii::app()->user->setFlash('success', 'A success message!');?>
                                                                </div>
                                                                <a class="brand-logo" href="#">
                                                                        <img class="img-circle" src="<?php // echo Yii::app()->theme->baseUrl; ?>/img/logo-mini5.png" alt="" width="110">
                                                                </a>
                                                                <h2>VANS CLASSIC VARIOS COLORES</h2>
                                                        </div>
                                                        <div class="row modal-main-info">
                                                                <div class="col-sm-6">
                                                                        <h4>Puntos de venta</h4>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                                </div>
                                                                <div class="col-sm-2 text-center">
                                                                        <a class="go-map" href="">
                                                                                <span></span>
                                                                                <h6>¡Llévame!</h6>
                                                                        </a>
                                                                </div>
                                                                <div class="col-sm-4 text-center">
                                                                        <h4>Disponibilidad</h4>
                                                                        <h2>3</h2>
                                                                </div>
                                                        </div>
                                                        <div class="row modal-icons row-table">
                                                                <div class="col-sm-3 col-cell">
                                                                        <span class="icon1"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-cell">
                                                                        <span class="icon2"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-cell">
                                                                        <span class="icon3"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-cell">
                                                                        <span class="icon4"></span>
                                                                </div>
                                                        </div>
                                                        <div class="row modal-icons-info text-center row-table">
                                                                <div class="col-sm-3 col-cell">
                                                                        <h4>40%</h4>
                                                                </div>
                                                                <div class="col-sm-3 col-cell">
                                                                        <h5>Quedan</h5>
                                                                        <h4>5 días</h4>
                                                                        <h6>del 5 al 10 de octubre</h6>
                                                                </div>
                                                                <div class="col-sm-3 col-cell">
                                                                        <h5><a href="" target="_blank">Visitar web</a></h5>
                                                                </div>
                                                                <div class="col-sm-3 col-cell">
                                                                        <h5><a href="#">Ver tiendas</a></h5>
                                                                </div>
                                                        </div>
                                                        <h3>Descripcion</h3>
                                                        <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer vel enim non nulla mattis elementum quis id tellus. Nam iaculis augue a aliquet pellentesque. Praesent et lectus consectetur, suscipit nunc nec, bibendum ante. Aenean fringilla ipsum id orci viverra, id porta leo interdum. Fusce quis felis eu eros tristique sodales non a mauris.
                                                        </p>
                                                        <h3>Términos y condiciones</h3>
                                                        <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer vel enim non nulla mattis elementum quis id tellus. Nam iaculis augue a aliquet pellentesque. Praesent et lectus consectetur, suscipit nunc nec, bibendum ante. Aenean fringilla ipsum id orci viverra, id porta leo interdum. Fusce quis felis eu eros tristique sodales non a mauris.
                                                        </p>
                                                        <p>
                                                                Vestibulum blandit sit amet urna id posuere. Duis ut justo eget lacus sodales tincidunt. Morbi sed molestie diam, posuere egestas ex. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur sodales mauris dui, nec egestas quam imperdiet ac. Phasellus congue sodales magna eget cursus. Ut ultricies cursus nulla.
                                                        </p>
                                                        <h3>Comentario</h3>
                                                        <div class="comment comment-left">
                                                                <div class="comment-user">
                                                                        <img class="img-circle" src="<?php // echo Yii::app()->theme->baseUrl;  ?>/img/user-img1.jpg" alt="" width="44">
                                                                </div>
                                                                <div class="comment-text">
                                                                        <h4>Nombre de Usuario</h4>
                                                                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                                                        <div class="comment-actions clearfix">
                                                                                <button class="comment-actions-left"></button>
                                                                                <button class="comment-actions-right">
                                                                                        Hace 2 min
                                                                                        <span></span>
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="comment comment-right">
                                                                <div class="comment-user">
                                                                        <img class="img-circle" src="<?php // echo Yii::app()->theme->baseUrl;  ?>/img/user-img1.jpg" alt="" width="44">
                                                                </div>
                                                                <div class="comment-text">
                                                                        <h4>Nombre de Usuario</h4>
                                                                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                                                        <div class="comment-actions clearfix">
                                                                                <button class="comment-actions-left"></button>
                                                                                <button class="comment-actions-right">
                                                                                        Hace 2 min
                                                                                        <span></span>
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="comment comment-left">
                                                                <div class="comment-user">
                                                                        <img class="img-circle" src="<?php // echo Yii::app()->theme->baseUrl;  ?>/img/user-img1.jpg" alt="" width="44">
                                                                </div>
                                                                <div class="comment-text">
                                                                        <h4>Nombre de Usuario</h4>
                                                                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                                                        <div class="comment-actions clearfix">
                                                                                <button class="comment-actions-left"></button>
                                                                                <button class="comment-actions-right">
                                                                                        Hace 2 min
                                                                                        <span></span>
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="comment comment-left">
                                                                <div class="comment-user">
                                                                        <img class="img-circle" src="<?php // echo Yii::app()->theme->baseUrl;  ?>/img/user-img1.jpg" alt="" width="44">
                                                                </div>
                                                                <div class="comment-text">
                                                                        <h4>Nombre de Usuario</h4>
                                                                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                                                        <div class="comment-actions clearfix">
                                                                                <button class="comment-actions-left"></button>
                                                                                <button class="comment-actions-right">
                                                                                        Hace 2 min
                                                                                        <span></span>
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="comment-area">
                                                                <form action="" class="modal-comment clearfix">
                                                                        <textarea></textarea>
                                                                </form>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                        <p>
                                                                Si consideras que este cupón tiene información que
                                                                <a data-toggle="modal" data-target="#modal-report">
                                                                        reportar
                                                                </a>
                                                        </p>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>-->
        <!--/ Modal admin product -->


        <!-- Modal poll product -->
        <div class="modal modal-item fade" id="modal-poll-product" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal poll product -->


        <!-- Modal poll list -->
        <div class="modal modal-item fade" id="modal-poll-list" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal poll list -->


        <!-- Modal user -->
        <div class="modal modal-item modal-user-info fade" id="modal-user" role="dialog" aria-labelledby="modal-user" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal user -->


        <!-- Modal stores -->
        <div class="modal modal-stores fade" id="modal-stores" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-app" data-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <div class="info-stores">
                                <h2>DESCARGA <strong>NUESTRA APP</strong></h2>
                                <a href="https://itunes.apple.com/us/app/apple-store/id982044638?l=en&mt=8" target="_blank">
                                    <p>
                                    Descarga ya la app móvil de WepikU y empieza a interactuar con tus marcas favoritas, en el lugar y momento mas oportuno para ti, no vuelvas a perder ninguno de sus lanzamiento, eventos importantes o promociones, y comparte con tus amigos lo que para ti es importante y te hace feliz.
                                    </p>
                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/store1.png" alt="">
                                </a>
                                <!--<a href="">-->
                                <div style="opacity: 0.25;">
                                    <p>Lorem ipsum dolor sit amet.</p>
                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/store2.png" alt="">
                                </div>
                                <!--</a>-->
                            </div>
                            <div class="app-slider">
                                <div class="device">
                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/device.png" alt="">
                                </div>
                                <div class="carousel slide carousel-stores" data-ride="carousel" data-interval="4000">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                            <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/app-slide1.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/app-slide2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal stores -->


        <!-- Modal report -->
        <div class="modal modal-item fade" id="modal-report" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
            <div class="modal-scroll">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
        <!--/ Modal report -->


        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/misc.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.formatCurrency-1.4.0.min.js" type="text/javascript"></script>
        <!--Functions after front-end-->
        <!--<script src="<?php // echo Yii::app()->theme->baseUrl;  ?>/add/scripts.js" type="text/javascript"></script>-->

<?php echo $this->builtEndBody() ?>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.number.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/application.js" type="text/javascript"></script>
        
    </body>
</html>

<?php
    if (Yii::app()->components['user']->loginRequiredAjaxResponse){
        Yii::app()->clientScript->registerScript('ajaxLoginRequired', '
            $(document).ajaxComplete(function( event, xhr, settings ) {                 
                if (xhr.responseText === "'.Yii::app()->components['user']->loginRequiredAjaxResponse.'") {                   
                    window.location.href = "'.Yii::app()->createUrl('/site/login').'";
                }            
            });           
        ');
    }
?>
<!--window.location.href = "'.Yii::app()->getRequest()->getRequestUri().'"
window.location.href = options.url;
window.location.reload(true)-->
<!--init method
    if (Yii::app()->user->isGuest) {
        /*
         *  covers even the ajax requests if user session has expired
         */
        Yii::app()->user->loginRequired();
    }-->