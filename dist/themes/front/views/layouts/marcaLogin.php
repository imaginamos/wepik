<?php // Yii::app()->clientScript->registerCoreScript('jquery-front');?>
<?php echo Yii::app()->bootstrap->register(); ?>
<!doctype html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>WEPIKU</title>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" rel="shortcut icon" />

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main_landing.css" />

<script src="/themes/front/js/form_validate.js" type="text/javascript"></script>


</head>

<body>



<div class="parallax_fixer"></div>
<div class="loader">
  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png">
</div>

<header>
	<div class="content_header main_container">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" class="logo left">
            <?php echo $content; ?>
            <a href="http://www.wepiku.com/index.php" class="ms_register_now">Soy Usuario</a>                
	</div>
</header>

<div class="container">
	<div class=" video_section video_in" id="video_section">
		<iframe class="main_video" src="https://www.youtube.com/embed/MuqR-yBUbDo?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bg_home.jpg">
                
		
		<a href="#" class="btn_down"></a>
	</div>

	<div class="section section1 section_marca1">
		
		<div class="main_container text-center">
			<a href="#" class="btn_appstore"></a>
			<h2 class="m-top20">¿Cansado de buscar maneras efectivas para ser visible ante tus clientes y de gastar mucho dinero en publicidad sin obtener los resultados esperados?</h2>
			<p>
			Te invitamos a conocer WepikU, la nueva aplicación móvil y web que te facilitará la comunicación entre tu marca y tus clientes.
			</p>
		</div>
	</div>

	<div class="section_marca2 section4 gradient_bg">	
		<div class="v_center1">
			<div class="v_center2">
				<div class="main_container clearfix">
					<div class="pin_text ">
						<h4 >WepikU te permite</h4>
						<h2>Tener una <br>comunicación <br>directa</h2>
						<h3>entre tu marca y sus seguidores</h3>
						<p>Comunícate con tus clientes en el momento más oportuno.</p>
						<a href="#" class="btn_appstore m-top20"></a>
					</div>
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_pin2.png">
				</div>
			</div>
		</div>
	</div>



	<div class="section4 section_marca2">
		<div class="v_center1">
			<div class="v_center2">
				<div class="main_container clearfix">
					<div class="pin_text ">
						<h2>Diseña tus <br>campañas <br>fácilmente</h2>
						<p>Tienes la autonomía de calcular cuánto necesitas invertir en tu campaña según tus objetivos. El presupuesto calculado sólo se irá consumiendo según tu comunidad va interactuando con tu marca.</p>
					</div>
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca1.png">
				</div>
			</div>
		</div>
	</div>

	<div class="section4 section_marca2">
		<div class="v_center1">
			<div class="v_center2">
				<div class="main_container clearfix">
					<div class="pin_text">
						<h2>Segmenta <br>tus campañas</h2>
						<p>Todo el contenido que publiques estará dirigido a una audiencia segmentada por características como género, edad, ciudad, intereses, entre otros. Además puedes definir la cobertura de tu campaña por país, ciudad e incluso zona de cercanía a tus sucursales o puntos de venta.</p>
					</div>
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca2.png">
				</div>
			</div>
		</div>
	</div>

	<div class="section4 section_marca2 marker_section">
		<div class="v_center1">
			<div class="v_center2">
				<div class="main_container clearfix">
					<div class="pin_text ">
                                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca4.png">
						<h2>Se descubierto <br>por geolocalización</h2>
						<p>Por medio de la georeferenciación, tus clientes podrán descubrir tus promociones y ofertas a su alrededor, saber cuál es tu sucursal más cercana y ser dirigidos a ella de inmediato. </p>
					</div>
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca3.png">
				</div>
			</div>
		</div>
	</div>

	<div class="section4 section_marca2">
		<div class="v_center1">
			<div class="v_center2">
				<div class="main_container clearfix">
					<div class="pin_text ">
						<h2>Recibe Feedback <br>y explora tu <br>mercado</h2>
						<p>Has encuestas sobre tus productos o servicios, explora las tendencias del mercado y conoce lo que piensan tus clientes actuales y potenciales.</p>
					</div>
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca5.png">
				</div>
			</div>
		</div>
	</div>

	<div class="section4 section_marca2 mid_img">
		<div class="pin_text ">
			<h2>Obtén resultados <br>en tiemepo real</h2>
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/marca6_icons.png">
			<p>WePikU te permite monitorear en tiempo real las interacciones de los usuarios con el contenido de tu campaña, y así mismo podrás tomar decisiones para maximizar tus resultados.</p>
		</div>
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img_marca6.png">		
	</div>

	<div class="section5">
		
		<div class="main_container text-center">
			<p>
			Regístrate ya, y descarga nuestra App ahora mismo para descubrir todas las facilidades y herramientas intuitivas que tendrán tus clientes para ponerse en contacto con tu marca o negocio, maximizando las oportunidades de venta, y creando tu propia comunidad de usuarios leales.
			</p>

			<a href="#" class="btn_appstore"></a>

			<p class="m-top20">Muy pronto también tendremos disponible la versión móvil para usuarios Android.</p>
		</div>
	</div>

	<div class="contact_section">
		<div class="main_container">
			<h2>Ponte en contacto con nosotros</h2>
			<p>
			¿Tienes preguntas o simplemente deseas obtener más información?, estaremos encantados de responder a todas tus preguntas. Por favor completa el siguiente formulario, o llámanos al 316-4669811.
			</p>

                <?php echo CHtml::beginForm('contacto', 'post'); ?> 
				<div class="clearfix">
					<div class="col_form left">
						<fieldset>
							<label>Nombre</label>
                                                        <?php echo CHtml::TextField('nombre',null, array('class' => 'input1')) ?>
						</fieldset>
						<fieldset>
							<label>Teléfono</label>
                                                        <?php echo CHtml::TextField('telefono',null, array('class' => 'input1')) ?>
						</fieldset>
                                                <?php $paises = array('AF' => 'Afghanistan', 'AX' => 'Åland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua and Barbuda', 'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan', 'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium', 'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia, Plurinational State of', 'BQ' => 'Bonaire, Sint Eustatius and Saba', 'BA' => 'Bosnia and Herzegovina', 'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam', 'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada', 'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China', 'CX' => 'Christmas Island', 'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo', 'CD' => 'Congo, the Democratic Republic of the', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => "Côte d'Ivoire", 'HR' => 'Croatia', 'CU' => 'Cuba', 'CW' => 'Curaçao', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia', 'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana', 'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam', 'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti', 'HM' => 'Heard Island and McDonald Islands', 'VA' => 'Holy See (Vatican City State)', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle of Man', 'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'Kenya', 'KI' => 'Kiribati', 'KP' => "Korea, Democratic People's Republic of", 'KR' => 'Korea, Republic of', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => "Lao People's Democratic Republic", 'LV' => 'Latvia', 'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia, the former Yugoslav Republic of', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States of', 'MD' => 'Moldova, Republic of', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro', 'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NI' => 'Nicaragua', 'NE' => 'Niger', 'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman', 'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar', 'RE' => 'Réunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'BL' => 'Saint Barthélemy', 'SH' => 'Saint Helena, Ascension and Tristan da Cunha', 'KN' => 'Saint Kitts and Nevis' , 'LC' => 'Saint Lucia', 'MF' => 'Saint Martin (French part)', 'PM' => 'Saint Pierre and Miquelon', 'VC' => 'Saint Vincent and the Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome and Principe', 'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SX' => 'Sint Maarten (Dutch part)', 'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia and the South Sandwich Islands', 'SS' => 'South Sudan', 'ES' => 'Spain', 'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard and Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan, Province of China', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania, United Republic of', 'TH' => 'Thailand', 'TL' => 'Timor-Leste', 'TG' => 'Togo', 'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad and Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan', 'TC' => 'Turks and Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom', 'US' => 'United States', 'UM' => 'United States Minor Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela, Bolivarian Republic of', 'VN' => 'Viet Nam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.', 'WF' => 'Wallis and Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe'); ?>
						<fieldset>
							<label>País</label>
							<div class="custom_select">
                                                            <?php echo CHtml::dropDownList('pais',null , $paises, array('empty'=>'--Seleccione país--')) ?>
							</div>
						</fieldset>
						<fieldset>
							<label>Empresa</label>
                                                        <?php echo CHtml::TextField('empresa',null, array('class' => 'input1')) ?>
						</fieldset>
						<fieldset>
							<label>Correo</label>
                                                        <?php echo CHtml::TextField('correo',null, array('class' => 'input1')) ?>
						</fieldset>
					</div>
					<div class="col_form right">
						<fieldset>
							<label>Comentario</label>
                                                        <?php echo CHtml::TextArea('comentario',null, array('class' => 'textarea1')) ?>
						</fieldset>
                        <?php echo CHtml::link('Ver políticas y tratamiento de datos personales', $this->createUrl('site/politicas'), array('target'=>'_blank'));
                         echo CHtml::link('Ver terminos', $this->createUrl('site/terminos') , array('target'=>'_blank')); ?>
					</div>
				</div>
                        <input type="submit" value="ENVIAR" class="btn_go_in"/>
                        <script type="text/javascript">
                            $('.main_container form').submit(function (ev) {
                                var submit = $(this).find('input[type=submit]');
                                submit.val('Enviando...');
                                $.ajax({
                                    headers: {Accept: 'text/plain'},
                                    type: 'POST',
                                    url: '<?php echo CController::createUrl('site/contacto') ?>',
                                    data: $(this).serialize(),
                                }).done(function (data) {									
                                    submit.val('Enviado');
									$('.lightbox_form').fadeToggle("fast");
									$('.close_lightbox').on('click',function(){
										$('.lightbox_form').fadeOut("Fast");
									});
                                    $("form")[0].reset();
									$("form input").attr('placeholder',''); 
									submit.val('ENVIAR');
                                });
                                return false;
								ev.preventDefault();
                            });
                        </script>
                        <?php echo CHtml::endForm(); ?>
		</div>
	</div>

<div class="lightbox_form">
	<div class="lightbox_content">
		<h1>Su solicitud ha sido enviada</h1>
        <div class="close_lightbox">X</div>
	</div>
</div>	
<footer>
	<div class="content_footer">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/imaginamos.jpg">
	</div>
</footer>

</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/form_validate.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/TweenMax.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.superscrollorama.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/mousewheel.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/easing.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js" type="text/javascript"></script>
</body>
</html>