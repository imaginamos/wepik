<?php // Yii::app()->clientScript->registerCoreScript('jquery-front');?>
<?php echo Yii::app()->bootstrap->register(); ?>
<!doctype html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<title>WEPIKU</title>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" rel="shortcut icon" />


<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main_landing.css" />

</head>

<body>



<div class="parallax_fixer"></div>
<div class="loader">
  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png">  
</div>

<header>
	<div class="content_header main_container">
		<a href="http://www.wepiku.com/index.php"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png"></a>
		<?php echo $content; ?>
	</div>
</header>

<div class="container">
	<div class="main_container">
		<div class="content_politics final_section" style="margin:10px;">
			<h1>Términos y Condiciones WePikU SAS</h1>
      		<h2>Glosario</h2></br>
      		<div>
      		<strong>Plataforma:</strong> Se refiere a Wepiku, bien sea en su modalidad WEB o aplicación para móviles.</br></br>
      		<strong>Usuario:</strong> Los usuarios de la plataforma de Wepiku (sea Web o móvil) pueden ser de dos tipos: 1. </br></br>
      		Como usuario de la información (visitar, navegar o interactuar) y 2. Como autor de la información 
      		(crear su propia marca, campaña de mercadeo, encuesta y hacer uso de Wepiku como 
      		herramienta de marketing digital).
      		Servicios: Wepiku ofrece servicios de marketing digital, creación de campañas, contenido y 
      		encuestas para los autores de la información. Para usuarios de la información los servicios son 
      		informativos (consulta de información, consulta de mapas, recepción de notificaciones, alertas, 
      		recomendaciones entre otros).</br></br>
      		<strong>OBJETO:</strong></br></br>
      		WepikU es una innovadora aplicación móvil + web que permite facilitar de manera especializada 
      		la comunicación bidireccional entre cualquier tipo de empresa (Marcas) y sus seguidores o 
      		clientes, de una manera totalmente relevante. Las marcas podrán enviar a sus clientes diferentes 
      		tipos de información por medio de campañas creadas desde la aplicación, entre ellas campañas 
      		con información comercial, encuestas o simplemente contenido. Las marcas asumirán el costo 
      		de sus campañas según los objetivos definidos. Los usuarios o seguidores consumirán la 
      		información que proveen las marcas, ingresando a los diferentes contenidos publicados.
      		Para ingresar a WepikU el usuario solo debe hacer su proceso de log in por medio de su acceso 
      		personal a su cuenta de Facebook, donde se compartirán los datos básicos del usuario de 
      		Facebook con su cuenta de WepikU.</br></br>
      		GENERALIDADES</br></br>
      		Las siguientes páginas contienen información legal que le recomendamos leer completamente 
      		en conjunto con la Política del Tratamiento de la Información y de Privacidad.
      		Aquí es donde Usted acepta nuestras condiciones sobre la forma en que se maneja, opera y 
      		administra WePikU; y sobre la forma en que se permite el acceso de los usuarios y marcas a 
      		nuestros Servicios. En resumen, acá se dice que Usted acepta la forma como asignamos y 
      		entregamos Puntos y Beneficios y acepta las condiciones sobre acceso y uso de los Servicios, 
      		así como nuestra autoridad para impedir su acceso a los Servicios en determinadas 
      		circunstancias. Así mismo, que Usted acepta que no somos responsables por ningún perjuicio 
      		que se le cause por el contenido acá publicado, por usted o por otros, ni por perjuicios que pueda 
      		sufrir en relación con el uso y ejercicio de WePikU que sean responsabilidad de los usuarios o de 
      		las marcas y/o terceros ajenos a la plataforma.</br></br>
      		Hace parte integral de estos términos y condiciones la Política del Tratamiento de la Información 
      		y de Privacidad.</br></br>
      		TÉRMINOS Y CONDICIONES</br></br>
      		Los siguientes términos y condiciones de uso (ahora en adelante “TCU”) establecen las 
      		condiciones de uso de la plataforma Wepiku (“Wepiku”), bien sea en su modalidad web/browser 
      		o como aplicación para móviles (conjuntamente la “Plataforma”), y en general, de utilización, 
      		aprovechamiento y ejecución de cualquier forma de nuestra Plataforma, el contenido, las 
      		aplicaciones y/o los demás servicios que ofrecemos en WePikU (conjuntamente los 
      		“Servicios”). Para que usted (también el “Usuario”) esté facultado y legitimado para ingresar a 
      		WePikU, y hacer uso de los Servicios, es indispensable que previamente haya aceptado los 
      		Términos de Uso. Una vez aceptados los Términos de Uso por el Usuario, estos le serán 
      		legalmente vinculantes y obligatorios. A lo largo del presente documento los términos definidos 
      		podrán ser usados tanto en singular como en plural.   </br></br>  


//

OBLIGACIONES DE LAS PARTES</br></br>  

A continuación se presentan las obligaciones entre WepikU y sus usuarios:</br></br>  

Obligaciones de WepikU:</br></br>  

La prestación del servicio de mobile marketing disponible en la propuesta de valor de la plataforma, manteniendo su alcance y sus características funcionales ofrecidas.</br></br>
Normas para el desarrollo de nuestra actividad: WepikU pone siempre empeño en lo que hace y asevera que:</br></br>
Ejercitaremos la diligencia y capacidad razonables en el cumplimiento de las obligaciones previstas en estos Términos;</br></br>
Tenemos el derecho a ser el canal móvil para distribuir los contenidos, promociones e información comercial del propietario de las marcas, de los cuales por ningún motivo accederemos a tener algún tipo de propiedad intelectual, y por lo cual WepikU no tendrá ninguna responsabilidad en caso de quejas o reclamos de terceros relacionadas con la titularidad y los contenidos publicados por las marcas. </br></br>  

 
Obligaciones del usuario: </br></br>  

Condiciones del Usuario Marca: Los usuarios que desarrollen campañas sobre una marca propia o con los derechos para manejarla tendrán que definir sus propios términos y condiciones y estos serán de aplicación al suministro de sus propios Servicios de la marca. Deberá usted cerciorarse de que cualquier otra persona que canjee una promoción, oferta o cupón en una sucursal o tienda de la marca, obre de acuerdo con estos términos y condiciones. La responsabilidad es de usted exclusivamente.</br></br>  

Información precisa: Deberá cerciorarse de que toda la información que nos proporcione para el lanzamiento de una campaña de mercadeo sea cierta, completa y exacta.</br></br>  

Cupones o promociones que satisfagan sus expectativas: Deberá cerciorarse de que los Cupones y/o los Servicios de la marca satisfagan sus expectativas concretas.</br></br>  

Usos prohibidos: Están prohibidos expresamente los siguientes usos de un Cupón distribuido en la plataforma, y se compromete usted a no hacer (ni permitir que ninguna otra persona haga) nada de lo indicado a continuación:</br></br>

revender el Cupón;</br></br>  

proporcionar datos falsos, incluso nombres, direcciones y datos de contacto falsos, y utilizar fraudulentamente números de tarjeta de debido/de crédito; o
participar en alguna actividad ilícita en relación con el uso de un Cupón.</br></br>  




ACEPTACIÓN DE LOS TÉRMINOS DE USO</br></br> 
Al registrarse en los Servicios y/o utilizarlos de cualquier forma incluyendo, entre otras cosas, visitar o Navegar (tal y como se define más adelante) la Plataforma, el Usuario acepta todos los Términos de Uso aquí contenidos, que también incorporan la Política del Tratamiento de la Información y de Privacidad de WePikU y todas las demás reglas de operación, políticas y procedimientos que puedan ser publicados de vez en cuando en la Plataforma o en cualquiera de los Servicios de WePikU, cada uno de los cuales se incorpora por referencia y cada uno de los cuales puede ser actualizado por WePikU de vez en cuando sin previo aviso de acuerdo con las condiciones establecidas en la sección de “Capacidad” a continuación.</br></br>  
Además, algunos servicios que se ofrecen a través de Wepiku pueden estar sujetos a términos y condiciones adicionales especificados que serán publicados por WePikU a sus usuarios en cualquier momento, requiriendo que los usuarios den su aprobación a los cambios que se desarrollen en el tiempo, para que puedan seguir haciendo uso de la aplicación. El uso que el Usuario haga de dichos servicios está sujeto a dichos términos y condiciones adicionales, que se incorporan a estos Términos de Uso por medio de esta referencia. Estos Términos de Uso se aplican a todos los Usuarios de los Servicios, incluyendo, entre otros, los Usuarios que contribuyen con contenido, información y otros materiales o servicios en la Plataforma o usuarios individuales de los Servicios.</br></br>  
 
CAPACIDAD:</br></br>  
En este acto, Wepiku se reserva el derecho de modificar las TCU, las políticas y condiciones de uso relacionadas al uso y destino de la plataforma, sin perjuicio que dichas modificaciones no podrán afectar las condiciones y derechos adquiridos por el Usuario en las operaciones comerciales realizadas con anterioridad a la publicación de las modificaciones de las TCU, políticas y condiciones de uso y destino de la plataforma. Wepiku podrá modificar los Términos y Condiciones en cualquier momento, haciendo público en la plataforma los términos modificados para que los usuarios se informen y den su consentimiento sobre los mismos a través de los medios de comunicación que dispone la plataforma. Todos los términos modificados entrarán en vigor desde la fecha de su publicación en la plataforma.

REGISTRO:</br></br>  

El Usuario interesado podrá hacer uso de la herramienta para crear campañas para su marca, siempre y cuando se encuentre previamente registrado en la plataforma, para lo cual se hace obligatorio que todos los datos proporcionados en el formulario de la plataforma sean válidos, veraces y actuales. El Usuario se obliga a actualizar de manera oportuna los datos proporcionados y registrados en la plataforma, para ajustarlos a las modificaciones que ocurran con el tiempo. Wepiku NO se hace responsable por la certeza de los datos proporcionados por los Usuarios de la plataforma. Los Usuarios se hacen responsables, en todos los casos, de la veracidad, exactitud, vigencia y autenticidad de los datos. Wepiku se reserva el derecho de solicitar comprobantes, respaldos y/o información adicional, a efectos de corroborar los Datos Personales y de facturación, así como de suspender temporal o definitivamente a aquellos Usuarios cuyos datos no hayan podido ser confirmados. En estos casos de inhabilitación, se imposibilitará al Usuario crear campañas o encuestas, sin que ello genere derecho a algún resarcimiento. La información personal que el Usuario le proporcione a Wepiku, estará asegurada por una clave de acceso, de la cual solo el Usuario tendrá conocimiento. El Usuario es el único responsable de mantener en secreto la Clave. El Usuario se compromete a notificar a Wepiku en forma inmediata y por medio idóneo y fehaciente, cualquier uso no autorizado de su cuenta. La Cuenta es personal, única e intransferible, aunque un mismo usuario puede delegar varios subusuarios de la cuenta para la creación de diferentes campañas. En caso que Wepiku detecte un mal manejo de las Cuentas, podrá cancelar, suspender o inhabilitarlas. En WepikU No esta permitida la publicación de contenido pornográfico, violento, racial, o que afecte de alguna manera clara a la comunidad de la plataforma. Wepiku se reserva el derecho de rechazar cualquier solicitud de registro, como también de cancelar un registro previamente aceptado, en cualquier momento de acuerdo a la siguientes causales de mal manejo, pero sin limitarse solo a estas:</br></br>  

El usuario ha creado una marca, de la que no tiene ningún derecho, autorización o poder legal para hacer uso de la misma. Ej: crear marcas de manera pirata o no oficiales.</br></br>  
El usuario esta generando o promoviendo contenido violento, fraudulento, publicidad engañosa, denigrante, racista, con material sexual infantil, porno, entre otros que puedan perjudicar la imagen de las marcas, o la comunidad de usuarios de la plataforma.</br></br>  
Marcas que lancen campañas con contenidos explícitos que generen competencia desleal o no ética, que pueda perjudicar a sus competidores de manera directa.</br></br>  

Wepiku comunicará al Usuario las razones del rechazo o cancelación del registro, cuando este las solicite por escrito, sin que ello genere algún derecho a indemnización o resarcimiento a favor del Usuario.</br></br>  

PRIVACIDAD Y CONFIDENCIALIDAD</br></br>  

El Usuario declara conocer y aceptar la política de confidencialidad y se obliga a guardar absoluta reserva sobre los datos aquí proporcionados.</br></br>  
Toda la información personal proporcionada por el Usuario es de responsabilidad exclusiva de quien la aporta.
La información ingresada por el Usuario, será utilizada por Wepiku para registrarlo en sus bases de datos, crear cuentas de usuario, procesar y darle seguimiento a los servicios, contestar correos electrónicos y proporcionar información con respecto a su cuenta.</br></br>  
Wepiku se reserva el derecho de usar esta información para enviar correos electrónicos con información relativa a su cuenta o de los servicios contratados, como también para enviar información sobre promociones o nuevos servicios relevantes al usuario. El Usuario siempre podrá solicitar el cese de envío de los correos electrónicos.
Wepiku no venderá, arrendará o intercambiará la información personal proporcionada por los usuarios, con terceros diferentes a los usuarios autores de la información (Marcas en la plataforma).
</br></br>  
PAGO:</br></br>  

Cualquier usuario WepikU puede crear su propia marca por medio de la plataforma, la cual podrá ser auditada por WepikU en cualquier momento para garantizar su validez, o validar si dicho usuario tienen el derecho legal para utilizarla comercialmente. Una vez creada una marca, el usuario podrá diseñar campañas informativas o comerciales de dicha marca, donde de acuerdo a los objetivos definidos o tipo de campaña, la plataforma calculará su costo total a partir de los costos unitarios de los objetivos definidos por el usuario. Antes de poder lanzar dicha campaña el usuario aceptará dicho costo y el sistema conducirá de manera segura al usuario a una pasarela de pagos, para que éste pueda ingresar los datos de su tarjeta de crédito o debito, y datos básicos del tarjetahabiente para realizar el pago online de la campaña. Con el pago del servicio el usuario esta aceptando las características y alcance del mismo. Una vez se confirma el pago exitoso, la campaña quedará publicada según la vigencia definida por el usuario.</br></br>  

Como se mencionó anteriormente el Usuario realizará el pago mediante tarjetas de crédito o débito autorizadas por la plataforma y que se tenga como entidad financiera emisora a un establecimiento de crédito Colombiano autorizado por la Superintendencia Financiera. Para el efecto, en el caso de pago con tarjeta de crédito, el Usuario deberá informar el numero de la tarjeta de crédito, la fecha de caducidad de la tarjeta, los tres (3) dígitos de seguridad, así como el nombre del titular de la misma, en el momento que realiza el pedido en cuestión. En el caso de pago con tarjeta de debito, deberá informar los datos de la tarjeta y el pin de seguridad. El comprobante de compra estará disponible y se podrá visualizar en la plataforma en su versión Web y también será enviado al correo electrónico definido en el proceso de compra. Para evitar cualquier fraude El Usuario deberá notificar a Wepiku cualquier movimiento fraudulento en la tarjeta utilizada para el pago de la campaña, mediante e-mail o vía telefónica, en el menor plazo de tiempo posible para que Wepiku pueda realizar las gestiones oportunas que apliquen.</br></br>  

Las campañas creadas por una marca, dependiendo de su tipo, sus características y objetivos definidos, van a generar un valor calculado que deberá cancelar el usuario, para poder hacer su publicación. El valor pagado se comportará como un saldo a favor o crédito para la campaña creada, que se consumirá de acuerdo a las interacciones que realice su target o audiencia sobre la campaña. Esta campaña podrá ser monitoreada en cualquier momento por su creador, de tal manera que podrá pausarla y hacer modificaciones pertinentes, podrá cancelarla en cualquier momento, o podrá esperar hasta que se cumplan sus metas o vigencia, finalizando el crédito asignado para la misma. Si por cualquier motivo una campaña termina sin consumir la totalidad de su crédito, éste podrá ser usado para la creación de una siguiente campaña para la misma marca. </br></br>  

A continuación se describe algunos de los diferentes tipos de campañas que se pueden crear en la plataforma con su lógica correspondiente para el cobro de las mismas de acuerdo a los objetivos que defina el usuario:</br></br>  

Definición de los tipos de campaña. </br>  
Entre otro, Existen 5 tipos de campaña:</br></br>  

1. Campaña de promoción</br>
2. Campaña de contenido</br>
3. Encuesta</br>
4. Campaña de promoción con encuesta</br>
5. Campaña de contenido con encuesta</br>

A excepción de la encuesta, los demás tipos de contenido pueden tener 3 diferentes tamaños que permiten una mayor notoriedad de la campaña a sus clientes o seguidores.</br></br>
Tamaño de las campañas.</br></br>

1. Económico: 600px de alto</br>
2. Normal: 900px de alto</br>
3. Premium: 1500px de alto</br></br>

Definición de las acciones u objetivos de las campañas que generan costos. </br></br>
El usuario podrá realizar diferentes acciones sobre las campañas publicadas por las marcas, que representarán un costo para ésta ultima, que se verá reflejado en un descuento unitario del crédito que la marca ha prepagado por su campaña. A continuación se describen los diferentes tipo de acciones, que en términos de las marcas corresponderían a los objetivos o metas definidos para una campaña:</br></br>

1. Costo por ingresar al detalle de una campaña.</br></br>

WepikU tiene definido un costo unitario general para la acción en la que un usuario al ingresar al detalle de una campaña.
Esto significa, que se hace un descuento a la marca en el crédito de su campaña respectivo a este rubro. Cuando cada usuario único ingrese al detalle, se va a descontar este valor unitario del crédito de la campaña, y así sucesivamente hasta que cumpla con el numero de ingresos al detalle máximos definidos en los objetivos de la campaña.</br></br>

Una vez se haya acabado el crédito definido para este rubro, la campaña dejará de publicarse a los usuarios o seguidores de la misma y el crédito de los demás rubros u objetivos no completados retornará a la marca como crédito a favor.</br></br>

2. Costo por Pik (guardar el contenido de una campaña)</br>
	(Solo aplica para campañas tipo promoción y promoción con encuesta)</br></br>

WepikU tiene definido un costo unitario general para la acción en la que un usuario guarda el contenido de una campaña.</br></br>

El usuario podrá guardar (hacer pik sobre) el contenido de una campaña en su aplicación, para hacer uso de la misma en otro momento, o para su recordación.</br></br>

La marca puede establecer el número de piks máximos disponibles para los usuarios sobre su campaña, los usuarios solo pueden hacer pik una vez por campaña, luego de hacer pik no pueden deshacer la acción, y cada pik va descontando el numero de piks definido por la marca. Cuando es consumido el total de piks establecidos, la campaña podrá seguir activa pero los usuarios no podrán hacer pik a dicha campaña.</br></br>

Costo por recomendación.</br></br>
WepikU tiene definido un costo unitario general para la acción en la que una marca recomienda públicamente una campaña.</br></br>

Las recomendaciones corresponden a la acción que hace una marca desde la administración de su campaña, donde tienen la opción a través de un botón, que permite a la marca que su campaña aparezca como recomendada en el slider de los news feed de las personas que siguen dicha marca y las personas que siguen marcas dentro de la misma categoría de la marca que se está recomendando.</br></br>

La marca puede ir haciendo recomendaciones a su gusto, una vez consumido el saldo de recomendaciones, el administrador de la marca tendrá que comprar mas crédito para seguir recomendando su campaña. </br></br>

El administrador en cualquier momento podrá pausar su campaña para recargar el saldo de recomendaciones y quedar habilitado para seguir recomendando.</br></br>
Costo por pregunta contestada.</br></br>

Las marcas pueden realizar campañas de encuestas a sus usuarios, donde se cobrará por cada pregunta contestada, sin importar si el usuario contesta la totalidad de las preguntas de la encuesta. WepikU tiene definido un costo unitario general por pregunta contestada.</br></br>

Cada pregunta contestada por cada usuario descontará el crédito de preguntas disponibles que tiene la campaña hasta llegar a cero, una vez se termina el crédito definido, la encuesta ya no estará visible a los usuarios. Debido a que existen campañas de tipo contenido y promoción que pueden incluir una encuesta, si se consume el crédito de preguntas máximas contestadas, podrán seguir vigentes pero la encuesta ya no estará disponible para los nuevos usuarios que vean el detalle de dichas campañas.</br></br>

Relación entre los tamaños de las campañas y los costos aplicados por acción.</br></br>
Los costos aplicados por la acción de ingresar al detalle (de las campañas de contenido, promocional, contenido con encuesta y promoción con encuesta) por parte de los usuarios varía según el tamaño del banner de la campaña, estos tamaños funcionan como un factor multiplicador definido globalmente en la plataforma por WepikU.</br></br>


Devolución de saldo no usado por la marca.</br></br>
El pago realizado para crear una campaña corresponderá a una marca en particular, es decir, el crédito se maneja a nivel marca, el cual puede estar distribuido en varias campañas, si el usuario administrador de una marca pausa una campaña o ésta se finaliza debido al agotamiento del número de ingresos al detalle o por la terminación de su vigencia, el crédito que no se termino de consumir será reembolsado a la marca en forma de crédito disponible.</br></br>

Al momento de crear una nueva campaña, el sistema primero liquidará el crédito disponible y procederá a generar el cobro del excedente, si por el contrario, después de crear la campaña continua con crédito a favor, este remanente seguirá disponible para la creación de nuevas campañas de la misma marca.</br></br>

Como un usuario puede tener varias marcas, el saldo se maneja por marca, es decir, un usuario no puede transferir crédito a favor, de una marca a otra.</br></br>

En ningún caso se hará devolución del dinero, salvo, que se circunscriba uno de los supuestos que den derecho de retracto, y solo, en aquellos eventos en que el mismo se ejerza en los términos establecidos en la Ley 1480 de 2011.</br></br>

FACTURA: </br></br>
Las facturas emitidas por la contratación de servicios de mercadeo podrán ser generadas electrónicamente, para esta opción las facturas de los servicios adquiridos serán enviadas al correo electrónico que para estos efectos señale el usuario.</br></br>
 
PROPIEDAD INTELECTUAL:</br></br>
Se deja expresa constancia que la propiedad intelectual, comercial, e industrial del contenido, diseños, gráficas, sistemas, incluidos hardware y software, sobre los que versa este Contrato son de exclusiva propiedad de Wepiku. Se encuentra prohibida la reproducción total o parcial de la plataforma sin el permiso expreso y por escrito de Wepiku. Así mismo queda totalmente prohibida la copia, reproducción, adaptación, modificación, distribución, comercialización, comunicación pública y/o cualquier otra acción que comporte una infracción de la normativa vigente en Colombia y/o internacionalmente en materia de propiedad intelectual y/o industrial, así como el uso de los contenidos de la plataforma si no es con la previa autorización expresa y por escrito de Wepiku.</br></br>

La titularidad de los contenidos, léase, campañas publicitarias, marcas, entre otros, publicados por los usuarios autores, son de propiedad de estos usuarios, y en ningún caso WePikU tendrá derecho alguno de propiedad intelectual sobre estos contenidos, ajenos a la propiedad intelectual de la plataforma, y por tanto WepikU no tendrá ninguna responsabilidad en caso de quejas o reclamos de terceros relacionadas con la titularidad y los contenidos publicados por las marcas.</br></br>


RESPONSABILIDAD DEL USUARIO:</br></br>
Los Usuarios (en su calidad de autores de la información) son responsables por la información que registren en la plataforma. Por lo anterior, los Usuarios son los únicos responsables ante Wepiku y terceros de las consecuencias que puedan derivar de la utilización con fines o efectos ilícitos o contrarios al presente TCU de aquellos datos que no sean verídicos o fidedignos. </br></br>

VINCULOS:</br></br>
La plataforma puede contener hipervínculos o Links a otros sitios web, que no sean controlados, editados, ni tengan relación alguna con Wepiku, no siendo la Empresa en consecuencia responsable del contenido ni de la exactitud de la información contenida en ellos. </br></br>
La función de los links que se encuentran en la plataforma es meramente informativa, es proporcionada por terceros, y se limita sólo a dar a conocer al Usuario otras fuentes de información relacionadas a las materias propias de la plataforma. Wepiku no tiene responsabilidad alguna respecto de las páginas web que cuenten con links a la plataforma. El establecimiento de estos vínculos no implica en forma alguna la existencia de algún tipo de relación entre Wepiku y el titular de la página web en la que se establezcan los mismos.</br></br>
 
FALLAS DE SISTEMA:</br></br>


WepikU no se responsabiliza por cualquier daño, perjuicio o pérdida al Usuario causados por fallas en el software, en el servidor o en Internet. WepikU tampoco será responsable por cualquier virus que pudiera infectar el equipo del Usuario como consecuencia del acceso, uso o examen del Sitio o a raíz de cualquier transferencia de datos, archivos, imágenes, textos, o audio contenidos en el mismo. Los Usuarios no podrán imputarle responsabilidad alguna ni exigir pago por lucro cesante, en virtud de perjuicios resultantes de dificultades técnicas o fallas en el software, los sistemas o en Internet. WepikU no garantiza el acceso y uso continuado o ininterrumpido del Sitio. WePikU dispondrá de todos los mecanismos tecnológicos y de seguridad necesarios para garantizar la prestación de los servicios a su cargo en las condiciones pactadas, no obstante los Usuarios y Marcas declaran que conocen y aceptan que pueden presentarse ciertos eventos que tienen como fuente software, internet, servidores y otros, que pueden dar lugar a fallas en el sistema, con ocasión de las cuales el sistema puede eventualmente no estar disponible debido a dificultades técnicas o fallas, o por cualquier otra circunstancia ajena a WepikU; casos en los cuales WePikU dispondrá los medios necesarios para restablecer de manera ágil y efectiva los servicios. </br></br>


PROHIBICIONES:</br></br>

Además de las prohibiciones que expresamente se contengan en otras secciones de los presentes términos y condiciones, se prohíbe a los Usuarios usar el Sitio para cargar, hacer avisos o de cualquier otra forma transmitir o distribuir cualquier ítem, incluyendo sin limitación alguna virus computacionales, caballos troyanos, gusanos, mecanismos de apagado automático o cualquier otro sistema, programa o archivo disruptivo; interferir con la seguridad de este sitio; infringir patentes comerciales, marcas registradas, secretos comerciales e industriales, derechos de publicidad o cualquier otros derechos propietarios de cualquier persona natural o jurídica, colectividades, etc.; impedir o interrumpir el uso del sitio por parte de terceros; usar programas automáticos, mecanismos o procesos manuales para monitorear, copiar, resumir, o extraer información de cualquier otra forma desde este sitio; usar las cuentas y claves de terceros Usuarios, o cualquier otra información sin el consentimiento previo y por escrito de su titular; crear cuentas o utilizar el sitio proporcionando datos falsos; transmitir desde este sitio SPAM, cadenas, correo basura o cualquier otro tipo de correo masivo no solicitado; cargar, distribuir o diseminar desde el sitio material o información de carácter amenazante, de acoso, difamatorio, obsceno, pornográfico, fraudulento, engañador, o que de cualquier otra forma pudiere ser atentatorio contra la Ley, las buenas costumbres, el orden público o que viole los derechos de cualquier otra parte, o que contenga solicitudes de recaudación de fondos, entre otras.</br></br>

CANCELACIONES, DEVOLUCIONES O PROBLEMAS:</br></br>

Si ha caducado una campaña y aun existe un crédito que no se consumió, el usuario podrá usarlo como un saldo a favor para completar la compra de su próxima campaña, para la misma marca. Este saldo a favor no podrá devolverse al usuario, solo se podrá consumir de la manera como se mencionó anteriormente. </br></br>
¿Insatisfecho? ¿No era lo que esperaba?: Si tiene una reclamación y se pone en contacto con los servicios de atención al cliente (info@wepikU.com) puede ocurrir una de las siguientes cosas:</br></br>
En el caso de ser un usuario que consume los contenidos del canal, y se tiene el caso que WepikU No es responsable de la reclamación, le pediremos que se ponga en contacto con la marca o intentaremos nosotros mismos ponernos en contacto con la marca para resolver esta controversia. Tenga en cuenta que la Marca es responsable (y NO WepikU) de prestar los Servicios de la Marca . Si la Marca acepta su responsabilidad adecuadamente, todo está bien. Si la Marca no acepta su responsabilidad adecuadamente, deberá actuar legalmente contra la Marca.</br></br>
En el caso de ser un usuario que genera los contenidos del canal gestionando una Marca dentro de WepikU, será este ultimo quien se pondrá en contacto con el usuario para resolver esta controversia de la mejor manera posible para las partes. 
WepikU no serán NUNCA responsable en lo referente a los Servicios o contenidos ofrecidos por una marca, ni de prestar dichos servicios, que correspondan a lo comunicado en una campaña creada por la marca.</br></br>


INDEMNIDAD</br></br>

Salvo los casos de dolo, mala fe o culpa grave con la que hubiere actuado WePikU, el usuario que gestiona una marca se obliga a abonar todos los costos y gastos que contraiga WePikU en relación con las reclamaciones y procedimientos judiciales que formulen o pretendan formular terceros contra nosotros y que se deriven del incumplimiento de estos Términos por el usuario.</br></br>


JURISDICCIÓN Y LEY APLICABLE:</br></br>
Este acuerdo estará regido por las leyes de la República de Colombia. 
Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a las leyes aplicables y a los Tribunales competentes para su conocimiento</br></br>

Wepiku SAS, NIT 9007793304 Bogotá, Colombia, E-mail: info@wepiku.com.</br></br>



      		</div>
		</div>
	</div>
</div>

	<footer>
	<div class="content_footer">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/imaginamos.jpg">
	</div>
</footer>

</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/TweenMax.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.superscrollorama.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/mousewheel.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/easing.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js" type="text/javascript"></script>

</body>