<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<div class="col-xs-12 col-sm-7 col-grid col-main grid-section section-id section-home">
    <div class="top-search clearfix">
        <form action="#" class="top-form pull-left clearfix">
            <button class="btn-search" type="button"></button>
            <div id="auto-search">
                <input class="typeahead" type="text">
            </div>
        </form>
		<div  class="container-fluid brand-page-head main-section section-id" data-section="hotdeals"></div>
        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="sub-list-icon"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
            </ul>
        </div>
    </div>
    <div class="wepiku-grid" style="top:50px">

        <?php
      // print_r($_SESSION);
        
        $con = 1;
        foreach ($data_hotdeals_json['data'] as $campania): ?>
            <?php if ($campania['tipo_id'] == 3): ?>
                <!-- Item -->
                <div class="wepiku-item">
                    <div class="item item-type3" data-toggle="modal" data-target="#modal-poll-list">
                        <div class="item-type3-bg" style="background-image: url(<?php echo Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenCover']; ?>);">
                            <?php $img_encuesta = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenCover'], "",array("style"=>"margin-bottom: -135px; height: 134px; position: relative;z-index: 9;opacity: 0; width: 100%;")); ?>
                        </div>

                        <?php
                            // ajax button to open the modal
                            echo TbHtml::ajaxLink(
                                $img_encuesta,
                                $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                array(
                                    'dataType' => 'json',
                                    'type' => 'POST',
                                    'data' => array(
                                        'camp' => $campania['id'],
                                        'modal' => '#modal-poll-list'
                                    ),
                                    'success' => 'function(data){
                                        openModal("#modal-poll-list", data.content);
                                    }'
                                ),
                                array(
                                    'id' => 'open-modal-'.$campania['marca_id'].$campania['id']
                                )
                            );
                        ?>
                        <div class="caption">
                            <p>
                            <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_pencil.png" alt=""></span>
                               
                            </p>
                            <h2 class="clearfix">
                                <?php echo $campania['descripcion'] ?>
                            </h2>
                            <div class="logo">
                                <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Item -->

            <?php elseif ($campania['tipo_id'] == 1 || $campania['tipo_id'] == 4): ?>
                <?php
                if ($campania['tipo_id'] == 1):
                    $modal = "#modal-product";
                elseif ($campania['tipo_id'] == 4):
                    $modal = "#modal-poll-product";
                endif;
                ?>
                <?php if ($campania['costo_multimedia_id'] == 1): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo1,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'cache' => false,
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }',
                                        'error' => 'function(xhr, status, error) {
                                                    openModal("'.$modal.'", xhr.status + " " +xhr.statusText, xhr.responseText);
                                                }'                                        
                                    ),
                                    array(
                                        'id' => 'open-modal-'.$campania['marca_id'].$campania['id'],
                                        'live'  => false
                                        //'class' => 'img-main'
                                    )
                                );
                            ?>
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                                <p>
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                                </p>
                            </div>
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <?php $img_promo_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo2,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.$campania['marca_id'].$campania['id'],
                                        'live'  => false
                                        //'class' => 'img-main'
                                    )
                                );
                            ?>
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                    <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo3,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.$campania['marca_id'].$campania['id'],
                                        'live'  => false
                                        //'class' => 'img-main'
                                    )
                                );
                            ?>
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php endif; ?>

            <?php elseif ($campania['tipo_id'] == 2 || $campania['tipo_id'] == 5): ?>
                <?php
                if ($campania['tipo_id'] == 2):
                    $modal2 = "#modal-product";
                elseif ($campania['tipo_id'] == 5):
                    $modal2 = "#modal-poll-product";
                endif;
                ?>
                <?php if ($campania['costo_multimedia_id'] == 1): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo1,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.$campania['marca_id'].$campania['id'],
                                        //'class' => 'img-main'
                                    )
                                );
                            ?>
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                                <p>
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                                </p>
                            </div>
                            <?php $img_promo_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo2,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.$campania['marca_id'].$campania['id'],
                                        //'class' => 'img-main'
                                    )
                                );
                            ?>
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                    <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo3,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array(
                                        'dataType' => 'json',
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.$campania['marca_id'].$campania['id'],
                                        //'class' => 'img-main'
                                    )
                                );
                            ?>
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                        </div>
                    </div>
                    <!--/ Item -->
                <?php endif; ?>
            <?php endif; ?>

        <?php
        $con++;
        endforeach; ?>
    </div>
</div>



<!--<script src="http://code.jquery.com/jquery-latest.js"></script>-->
<script type="text/javascript">  
        /*
         * Se obtienen los valores que vienen como parametros
         * y se arma la URL que se va a invocar
         * */   
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


</script>

<?php
        /*
         * funcion que se ejecuta cuando la pagina
         * finaliza de carga y si llega con los parametros
         * necesarios ejecuta el evento ajax que abre el hotdeals
         * Tambien pide geolocalización para recargar la página 
         * con los hotdeals pertenencientes a su posición
         * */
Yii::app()->clientScript->registerScript('getGeolocation', '
    $(window).load(function() {  
        var getLat = getUrlParameter("lat");
        var getLng = getUrlParameter("lng");
        var m = getUrlParameter("m");
        var i = getUrlParameter("i");
        if(m != undefined && i != undefined){
			setTimeout(function(){ 
			console.log(m+i);
					document.getElementById("open-modal-"+m+i).click();
			}, 3000);
          
        }                         
    });           
');
?>