<div class="modal-header">
<button type="button" class="close close-pro" data-dismiss="modal"></button>
<?php if($bar_admin == 1): ?>
    <div style="height: 70px !important;" class="admin-actions">
        <div class="caja-iz">
            <button type="button"><strong><?php echo $campania['preguntas_respondidas'] ?></strong>Preguntas Respondidas</button>
            <button type="button"><strong><?php echo $campania['usuarios_encuestados'] ?></strong>Usuarios Encuestados</button>            
        </div>
        <div style="width: 240px !important;" class="caja-dr">
            <?php if($estado_campania=='A'): ?>        
                <?php echo TbHtml::ajaxButton(
                        '<span class="admin-icon5"></span>Pausar campaña', 
                        $this->createUrl('campanias/pausarCampaniaBackendAjax'),
                        array( 
                            'dataType' => 'json', 
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'm' => $campania['marca_id']
                            ),
                            'success' => 'function(data){
                                $("#modal-poll-list .modal-header #flash-pause-campania").html(data.mensaje);
                                $("#flash-pause-campania .modredes").slideToggle(0);  
                                $(".dropdown-menu").css({"display": "none"});
                                $("#flash-pause-campania .modredes").animate({
                                    opacity: 0
                                    }, 4000, function() {
                                            $("#flash-pause-campania .modredes").removeAttr("style");
                                            $(".dropdown-menu").removeAttr("style");
                                            jQuery.yii.submitForm(this,"/marcas/marcaAdminActivas",{"m":data.mid});
                                            return false;                                        
                                    });                                
                            }'
                        ),
                        array(
                            'id' => 'pause-camp-'.uniqid(),
                            'class' => 'pull-right',
                            'reporte' => 1
                        )
                        ); ?>  
            <?php elseif($estado_campania=='P'): ?>
                <?php echo TbHtml::ajaxButton(
                        '<span class="admin-icon5"></span>Continuar campaña', 
                        $this->createUrl('campanias/continuarCampaniaBackendAjax'),
                        array( 
                            'dataType' => 'json', 
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'm' => $campania['marca_id']
                            ),
                            'success' => 'function(data){
                                $("#modal-poll-list .modal-header #flash-pause-campania").html(data.mensaje);
                                $("#flash-pause-campania .modredes").slideToggle(0);  
                                $(".dropdown-menu").css({"display": "none"});
                                $("#flash-pause-campania .modredes").animate({
                                    opacity: 0
                                    }, 4000, function() {
                                        $("#flash-pause-campania .modredes").removeAttr("style");
                                        $(".dropdown-menu").removeAttr("style");                                    
                                        if(parseInt(data.success) === 1){
                                            jQuery.yii.submitForm(this,"marcas/marcaAdminPausadas",{"m":data.mid});
                                            return false;
                                            xhrObj.abort();
                                        }else{
                                            jQuery.yii.submitForm(this,"campanias/encuesta",{"idcamp":data.campid});
                                            return false;
                                            xhrObj.abort();
                                        }        
                                    });                                
                            }'
                        ),
                        array(
                            'id' => 'continue-camp-'.uniqid(),
                            'class' => 'pull-right',
                            'reporte' => 1
                        )
                        ); ?>   
            <?php elseif($estado_campania=='T'): ?>
                <?php echo CHtml::HtmlButton('<span class="admin-icon7"></span>Reactivar campaña', array('submit' => array('campanias/encuesta'), 'params'=>array('idcamp'=>$campania['id']), 'class' => 'pull-right')); ?>  
            <?php endif; ?>
            <!--<button type="button" class="pull-right"><span class="admin-icon4"></span>Generar reportes</button>-->
            <?php echo CHtml::HtmlButton('<span class="admin-icon4"></span>Generar reportes ', array('submit' => array('campanias/exportReportToExcel'), 'params'=>array('idcamp'=>$campania['id'], 'tipo'=>$campania['tipo_id']), 'class' => 'pull-right')); ?>    
        </div>														
        <div class="clear"></div>
    </div>
<?php endif; ?>
<div id="flash-pause-campania"></div>
<div class="con-modal-slider modal-header-bg">
        <div class="header-mask"></div>
        <!--<img src="https://scontent-mia.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/11000056_890608347628752_2353305717206992596_n.jpg?oh=3eedb25b5057cbc1f779e217327c5a33&oe=55736EDC" alt="">-->
        <?php echo CHtml::image($imagen_multimedia, ""); ?>
</div>    
</div>
<div class="modal-body">
<div class="brand-head">
    <?php //echo 'id: '.$campania['id'] ?>
    <?php //echo 'tipo_id: '.$campania['tipo_id'] ?>        
    
    <?php $logo_marca = CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class' => 'img-circle', 'width' => '110')); ?>
    <?php echo CHtml::link($logo_marca, $this->createUrl('marcas/marca?m='.$campania['marca_id']), array('class' => 'brand-logo')); ?>                
    
    <h2><?php echo $encuestas['nombre'] ?></h2>
</div>
<?php echo CHtml::beginForm(null, null, array ('class' => 'form-poll clearfix')); ?>  
<?php $i = 0; $j = 0; $k = 0; ?>
    <?php foreach ($encuestas['preguntas'] as $pregunta): ?>
    <?php echo CHtml::hiddenfield('campania_id', $campania['id']); ?>
    <?php echo CHtml::hiddenfield('encuesta_id', $encuestas['id']); ?>
        <h3><?php echo $pregunta['pregunta'] ?></h3>
        <p><?php echo $pregunta['tipoPregunta']['nombre'] ?></p>      
        <?php echo CHtml::hiddenfield('preg_tipo_id[]', $pregunta['id'].'+'.$pregunta['tipoPregunta']['id']); ?>       
        
        <?php if($pregunta['tipoPregunta']['id'] == 1): ?>
            <?php // foreach ($pregunta['respuestas'] as $respuesta): ?>        
                <fieldset>
                    <?php echo CHtml::TextArea('text_preg_abierta'.$i,null, array('class' => 'textarea')) ?>
                    <?php echo CHtml::hiddenfield('respu_abierta_id'.$i, 'respu_abierta_id'.$i); ?>
                </fieldset> 
            <?php $i++; ?>
            <?php // endforeach; ?>      
        <?php elseif ($pregunta['tipoPregunta']['id'] == 2): ?>
            <?php foreach ($pregunta['respuestas'] as $respuesta): ?>           
                <fieldset>
                    <?php echo CHtml::radioButton('preg_unic_resp'.$j, false, array(
                        'value'=>$respuesta['id'].'+'.$respuesta['respuesta'],
                        'id'=>'preg_unic_resp'.$respuesta['id'],
                        'name'=>'question'.$pregunta['id'], 
                        'uncheckValue'=>null,
                    ),
                    array('class' => 'custom-check')
                    ); ?>                    
                    <?php echo CHtml::Label($respuesta['respuesta'], 'preg_unic_resp'.$respuesta['id'], array('style' => "display: inline; margin-right: 20px;")); ?>   
                </fieldset> 
            <?php endforeach; ?>   
            <?php $j++; ?>        
        <?php elseif ($pregunta['tipoPregunta']['id'] == 3): ?>
            <?php foreach ($pregunta['respuestas'] as $respuesta): ?>              
                <fieldset>
                    <?php echo CHtml::checkBox('preg_multi_resp'.$k.'[]', false, array(
                        'value'=>$respuesta['id'].'+'.$respuesta['respuesta'],
                        'id'=>'preg_multi_resp'.$respuesta['id'],
                        'name'=>'question'.$pregunta['id'], 
                        'uncheckValue'=>null,                        
                    )); ?>
                    <?php echo CHtml::Label($respuesta['respuesta'], 'preg_multi_resp'.$respuesta['id'], array('style' => "display: inline; margin-right: 20px;")); ?>
                </fieldset>      
            <?php endforeach; ?>
            <?php $k++; ?> 
        <?php endif; ?>           
        <hr>        
    <?php endforeach; ?>
    <?php
     echo CHtml::ajaxSubmitButton(
            'ENVIAR', 
            $this->createurl('hotdeals/enviarRespuestasBackend'), 
            array( 
                'dataType' => 'json', 
                'type' => 'POST',
                'success' => 'function(data){
                    if(data.resp == 1){
                        $("#modal-poll-list").modal("hide");
                        $("#modal-poll-product .modal-body #flash-encuesta").html(data.mensaje);
                        $("#flash-encuesta .modredes").slideToggle(0);  
                        $(".dropdown-menu").css({"display": "none"});
                        $("#flash-encuesta .modredes").animate({
                            opacity: 0
                            }, 5000, function() {
                                $("#flash-encuesta .modredes").removeAttr("style");
                                    $(".dropdown-menu").removeAttr("style");
                                });                    
                    }else{
                        $("#modal-poll-list .modal-footer #flash-encuesta").html(data.mensaje);
                        $("#flash-encuesta .modredes").slideToggle(0);  
                        $(".dropdown-menu").css({"display": "none"});
                        $("#flash-encuesta .modredes").animate({
                            opacity: 0
                            }, 5000, function() {
                                $("#flash-encuesta .modredes").removeAttr("style");
                                    $(".dropdown-menu").removeAttr("style");
                                });                    
                    }
                }'
            ),             
            array(
                'id' => 'send-answer-form'.uniqid(),
                'class'=>'btn btn-primary',
                'type'=>'button',
                'name'=>'button'
        //'htmlOptions'=>array('onclick' => 'function(){$(this).dialog("close")}')
            )
     ); ?>            
<?php echo CHtml::endForm(); ?>
<div id="flash-report"></div>
</div>
<div class="modal-footer"  style="position: relative;">     
<div id="flash-encuesta"></div>  
<p>
        Si consideras que este cupón tiene información que
<!--        <a data-toggle="modal" data-target="#modal-report">
                
        </a>-->
        <?php                                
            // ajax button to open the modal
            echo TbHtml::ajaxLink(
                'reportar',
                $this->createUrl('hotdeals/nuevoReportePublicacionAjax'),
                array( 
                    'dataType' => 'json', 
                    'type' => 'POST',
                    'data' => array(
                        'camp' => $campania_id,
                        'marca' => $marca,
                        'imagmulti' => $imagen_multimedia,
                        'modal' => $modal                    
                    ),
                    'success' => 'function(data){
                        openModal("#modal-report", data.content);
                    }'
                ),
                array(
                    'id' => 'open-modal-'.uniqid()
                )
            );                                
        ?>          
</p>    
</div>