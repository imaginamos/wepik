

<h3>Comentarios</h3>
<?php foreach ($comentarios as $comentario): ?>
    <?php if(isset($comentario['usuario_id'])):?>
    <?php if($comentario['usuario_id'] == Yii::app()->session['wepiku_user_id']): ?>   
        <?php $link_edit = CHtml::ajaxLink(
            'Editar',
            $this->createUrl('hotdeals/editarComentarioAjax?comid='.$comentario['id'].'&camp='.$campania_id.'&com='.$comentario['comentario']), 
            array(
                'update' => '#comentario-'.$comentario['id']
                ),
            array(
                'live'=>false,
                'id' => 'edit-comment'.uniqid(),            
                'tabindex' => -1            
            )    
        ); ?> 
        <?php $link_delete = CHtml::ajaxLink(
            'Borrar',
            $this->createUrl('hotdeals/borrarComentarioAjax?comid='.$comentario['id'].'&camp='.$campania_id), 
            array(
                'update' => '#comentario-capa-'.$comentario['id']
                ),
            array(
                'id' => 'delete-comment'.uniqid(),            
                'tabindex' => -1            
            )    
        ); ?> 
    <?php else: ?>
        <?php $link_edit = ''; $link_delete = ''; ?>
    <?php endif; ?> 
    <?php if($usuario_id_camp == Yii::app()->session['wepiku_user_id']): ?>
        <?php $link_answer = CHtml::ajaxLink(
            'Responder',
            $this->createUrl('hotdeals/editRespuComentarioAjax?comid='.$comentario['id'].'&camp='.$campania_id.'&campuid='.$usuario_id_camp.'&logo='.$logo.'&marc='.$nombre_marca), 
            array(
                'update' => '#nuevo-comentario-hijo-'.$comentario['id'] 
                ),
            array(
                'id' => 'answer-comment'.uniqid(),            
                'tabindex' => -1            
            )    
        ); ?> 
    <?php else: ?>
        <?php $link_answer = ''; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php if(isset($comentario['id'])):  ?>
    <div class="comment comment-left" id="comentario-capa-<?php echo $comentario['id'] ?>">
            <div class="comment-user">
                    <?php echo CHtml::image($comentario['usuario_img'], "", array('class' => 'img-circle', 'width' => '44')) ?>
            </div>
            <div class="comment-text">
                    <h4><?php echo $comentario['usuario_nombre'] ?></h4>
                    <div id="comentario-<?php echo $comentario['id'] ?>">
                    <p class="text-justify"><?php echo $comentario['comentario'] ?></p>
                    </div>
                    <div class="comment-actions clearfix">
                            <!--<button class="comment-actions-left"></button>-->
                        <?php echo $comentario['fecha_comentario']; ?> 
                        <?php if($comentario['usuario_id'] == Yii::app()->session['wepiku_user_id'] || $usuario_id_camp == Yii::app()->session['wepiku_user_id']): ?> 
                            <?php echo TbHtml::buttonDropdown('<span></span>', array(
                                chtml::tag(
                                    'li', 
                                    array('role' => 'menuitem'), 
                                    $link_edit
                                ),     
                                chtml::tag(
                                    'li', 
                                    array('role' => 'menuitem'), 
                                    $link_delete
                                ), 
                                chtml::tag(
                                    'li', 
                                    array('role' => 'menuitem'), 
                                    $link_answer
                                ),                                         
                            ),
                            array('class' => 'comment-actions-right')
                            ); ?>
                        <?php endif; ?> 
                    </div>
            </div>
    </div>
    <?php endif;?>
    <?php if(isset($comentario['id'])):  ?>
    <div id="nuevo-comentario-hijo-<?php echo $comentario['id'] ?>"></div>
    <?php if(isset($comentario['comentario_hijo_id'])): ?>
        <?php foreach ($comentario['comentario_hijo_id'] as $comentario_hijo): ?>
            <?php if($comentario_hijo['usuario_id'] == Yii::app()->session['wepiku_user_id']): ?>   
                <?php $link_edit_hijo = CHtml::ajaxLink(
                    'Editar',
                    $this->createUrl('hotdeals/editarComentarioAjax?comid='.$comentario_hijo['id'].'&camp='.$campania_id.'&com='.$comentario['comentario']), 
                    array(
                        'update' => '#comentario-'.$comentario_hijo['id']
                        ),
                    array(
                        'id' => 'edit-comment'.uniqid(),            
                        'tabindex' => -1            
                    )    
                ); ?> 
                <?php $link_delete_hijo = CHtml::ajaxLink(
                    'Borrar',
                    $this->createUrl('hotdeals/borrarComentarioAjax?comid='.$comentario_hijo['id'].'&camp='.$campania_id), 
                    array(
                        'update' => '#comentario-capa-'.$comentario_hijo['id']
                        ),
                    array(
                        'id' => 'delete-comment'.uniqid(),            
                        'tabindex' => -1            
                    )    
                ); ?> 
            <?php else: ?>
                <?php $link_edit_hijo = ''; $link_delete_hijo = ''; ?>
            <?php endif; ?>     
            <div class="comment comment-right" id="comentario-capa-<?php echo $comentario_hijo['id'] ?>">
                    <div class="comment-user">
                        <?php echo CHtml::image(Yii::app()->session['url_img'].$logo, "", array('class' => 'img-circle', 'width' => '44')) ?>
                    </div>
                    <div class="comment-text">
                            <h4><?php echo $nombre_marca ?></h4>
                            <div id="comentario-<?php echo $comentario_hijo['id'] ?>">
                                <p class="text-justify"><?php echo $comentario_hijo['comentario'] ?></p>
                            </div>
                            <div class="comment-actions clearfix">
                                    <!--<button class="comment-actions-left"></button>-->
                                    <!--<button class="comment-actions-right">-->
                                            <?php // echo $comentario_hijo['fecha_comentario'] ?>
<!--                                            <span></span>
                                    </button>-->
                                    <?php echo $comentario_hijo['fecha_comentario']; ?> 
                                    <?php if($comentario_hijo['usuario_id'] == Yii::app()->session['wepiku_user_id'] || $usuario_id_camp == Yii::app()->session['wepiku_user_id']): ?> 
                                        <?php echo TbHtml::buttonDropdown('<span></span>', array(
                                            chtml::tag(
                                                'li', 
                                                array('role' => 'menuitem'), 
                                                $link_edit_hijo
                                            ),     
                                            chtml::tag(
                                                'li', 
                                                array('role' => 'menuitem'), 
                                                $link_delete_hijo
                                            ),                                         
                                        ),
                                        array('class' => 'comment-actions-right')
                                        ); ?>
                                    <?php endif; ?>                                     
                            </div>
                    </div>
            </div>            
        <?php endforeach; ?>        
    <?php endif; ?>
    <?php endif;?>
<?php endforeach; 