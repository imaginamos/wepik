<!--					<section>-->
<div class="col-xs-12 col-sm-7 col-grid col-main section-id section-how">
    <div class="top-search clearfix">
        <form action="#" class="top-form pull-left clearfix">
            <button class="btn-search" type="button"></button>
            <div id="auto-search">
                <input class="typeahead" type="text">
            </div>
        </form>
        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="sub-list-icon"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
            </ul>
        </div>
    </div>
    <div class="info-grl info-polls clearfix">
        <div class="row row-table">
            <div class="col-sm-6 col-cell">
                <h1>
                    ¿CÓMO<br>
                    HACER<br>
                    UNA<br>
                    CAMPAÑA?
                </h1>
                <button class="see-poll-info">Ver más<span></span></button>
            </div>
            <div class="col-sm-6 col-cell">
                <div class="poll-banner">
                    <div class="poll-banner-icon1">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/banner-icon1.png" alt="">
                    </div>
                    <div class="poll-banner-icon2">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/banner-icon2.png" alt="">
                    </div>
                    <div class="poll-banner-step poll-banner-step1 text-center">
                        <div class="table">
                            <div class="table-cell">
                                <span>1</span>
                            </div>
                        </div>
                    </div>
                    <div class="poll-banner-step poll-banner-step2 text-center">
                        <div class="table">
                            <div class="table-cell">
                                <span>2</span>
                            </div>
                        </div>
                    </div>
                    <div class="poll-banner-step poll-banner-step3 text-center">
                        <div class="table">
                            <div class="table-cell">
                                <span>3</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row-steps clearfix">

            <!-- Step -->
            <div class="col-xs-12">
                <div class="row row-info-step">
                    <div class="col-xs-7 text-left">
                        <h2>PASO 1</h2>
                        <p>
                            Una vez ya has creado tu marca, puedes hacer clic sobre la opción CAMPAÑAS Y ENCUESTAS, en el menú principal.
                        </p>
                    </div>
                    <div class="col-xs-5">
                        <div class="step-img">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/paso-1.png" alt="">
                            <div class="step-number step-number1">1</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Step -->

            <!-- Step -->
            <div class="col-xs-12">
                <div class="row row-info-step">
                    <div class="col-xs-5">
                        <div class="step-img">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/paso-2.png" alt="">
                            <div class="step-number step-number2">2</div>
                        </div>
                    </div>
                    <div class="col-xs-7 justify">
                        <h2>PASO 2</h2>
                        <p>Selecciona el tipo de campaña que quieres lanzar, existen tres opciones según tus objetivos de mercadeo:</p>
                        <br/>
                        <p><strong>Promoción:</strong> Atrae a tus clientes actuales y potenciales por medio de ofertas, lanzamientos, o descuentos relevantes con los gustos y preferencias de los mismos, con el fin de incrementar tus ventas, trafico en tus sucursales o redireccionarlos a tu comercio electrónico.</p>
                        <br/>
                        <p><strong>Contenido:</strong> Mantén a tus clientes informados sobre tus nuevos productos, lanzamientos, artículos relacionados con tus servicios, comerciales, videos explicativos, noticias y tendencias de tu industria, documentales relacionados con tus productos, entre otros contenidos relevantes a tu audiencia.</p>
                        <br/>
                        <p><strong>Encuesta:</strong> Explora tu mercado de manera efectiva, por medio de campañas de encuesta perfiladas con la audiencia másmas relevante, recibe feedback rápidamente sobre tus productos o servicios y conoce mejor a tus clientes.</p>
                    </div>
                </div>
            </div>
            <!-- Step -->

            <!-- Step -->
            <div class="col-xs-12">
                <div class="row row-info-step">
                    <div class="col-xs-7 text-left">
                        <h2>PASO 3</h2>
                        <p><strong>Personaliza tu campaña:</strong> Define los objetivos que esperas alcanzar con tu campaña y toda la información básica de la misma.</p>
                    </div>
                    <div class="col-xs-5">
                        <div class="step-img">
                                 <img src="<?php echo Yii::app()->theme->baseUrl;  ?>/img/paso-3.png" alt=""> 
                            <div class="step-number step-number3">3</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Step -->
            
            <!-- Step -->
            <div class="col-xs-12">
                <div class="row row-info-step">
                    <div class="col-xs-5">
                        <div class="step-img">
                                 <img src="<?php echo Yii::app()->theme->baseUrl;  ?>/img/paso-4.png" alt=""> 
                            <div class="step-number step-number4">4</div>
                        </div>
                    </div>                    
                    <div class="col-xs-7 justify">
                        <h2>PASO 4</h2>
                        <p>
                            Si la campaña es de pago, después de su creación el sistema te llevará al proceso de pagos, donde podrás ingresar los datos de tu tarjeta de crédito preferida y listo! Conforme tus clientes van interactuando con tu campaña y se van cumpliendo tus objetivos de mercadeo definidos, el monto invertido de tu campaña se irá descontando. 
                            Tus campañas son flexibles, podrás pausarlas, cancelarlas o modificarlas en cualquier momento después de creada y en un escenario pesimista en que la efectividad de la campaña no sea la esperada no pierdes el dinero! Lo puedes reasignar a otra campaña sin ningún problema.
                        </p>
                    </div>
                </div>
            </div>
            <!-- Step --> 
            
            <!-- Step -->
            <div class="col-xs-12">
                <div class="row row-info-step">
                    <div class="col-xs-7 justify">
                        <h2>PASO 5</h2>
                        <p>
                            Finalmente con WepikU podrás monitorear los resultados de tus campañas en tiempo real permitiéndote tomar decisiones en el momento mas oportuno.                            
                        </p>    
                    </div>
                    <div class="col-xs-5">
                        <div class="step-img">
                                 <img src="<?php echo Yii::app()->theme->baseUrl;  ?>/img/paso-5.png" alt=""> 
                            <div class="step-number step-number5">5</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Step -->
            <div class="col-xs-12">
                <div class="row row-info-step">
                    <div class="justify">
                        <p>
                            <strong>NOTA:</strong> Por tiempo limitado todas las funciones de la plataforma estarán disponibles para todas las marcas, sin ningún costo.
                        </p>    
                    </div>
                </div>
            </div>            
        </div>
        <div class="row row-create">
            <div class="col-sm-6 text-left">
                <h2>CAMPAÑA</h2>
                <h4>PROMO O CONTENIDO</h4>
                <p>Hazte visible para tus clientes actuales y potenciales. Haz clic aquí y diseña tu campaña de mercadeo de una forma fácil y rápida.</p>
                <?php if ($marcas_usuario): ?>
                    <a href="<?php echo $this->createUrl('campanias/plan') ?>">Crear campaña<span></span></a>
                <?php else :?>
                    <?php echo CHtml::link(
                       'Crear campaña',
                       "#",
                       array(
                           'submit' => $this->createUrl('marcas/nueva'),
                           'params' => array('mensaje' => 'Para crear una campaña es necesario ser administrador de al menos una marca')
                       )
                   ); ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-6 text-right">
                <h2>ENCUESTAS</h2>
                <p>
                Crea y publica fácilmente encuestas segmentadas que te permitirán conocer mejor lo que piensan las personas que realmente quieren tu marca, obtén información valiosa en tiempo real con la que podrás mejorar tus productos, servicios y experiencia de tus clientes
                </p>
                <?php if ($marcas_usuario): ?>
                    <a href="<?php echo $this->createUrl('campanias/encuesta') ?>">Crear encuesta<span></span></a>
                <?php else :?>
                    <?php echo CHtml::link(
                       'Crear encuesta',
                       "#",
                       array(
                           'submit' => $this->createUrl('marcas/nueva'),
                           'params' => array('mensaje' => 'Para crear una encuestra es necesario ser administrador de al menos una marca')
                       )
                   ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


</div>


<div class="col-xs-12 col-sm-2 col-sub-nav">


    <div class="sub-nav active">
        <button class="btn-sub-nav" type="button">
            <span class="btn-arrow"></span>
            <span class="btn-icon"></span>
        </button>
    </div>
</div>


<!--					</section>-->
