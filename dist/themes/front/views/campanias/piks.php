<!--					<section>-->

<div class="col-xs-12 col-sm-7 col-grid col-main grid-section section-id section-piks">
    <div class="top-search clearfix">
        <form action="#" class="top-form pull-left clearfix">
            <button class="btn-search" type="button"></button>
            <div id="auto-search">
                <input class="typeahead" type="text">
            </div>
        </form>
        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="sub-list-icon"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
            </ul>
        </div>
    </div>
    <div class="wepiku-grid">

        <?php
        $con = 1;
		//ECHO count($data_hotdeals_json['data']);
        foreach ($data_hotdeals_json['data'] as $campania):
            ?>
            <?php
            if ($campania['tipo_id'] == 1):
                $modal = "#modal-product";
            elseif ($campania['tipo_id'] == 4):
                $modal = "#modal-poll-product";
            endif;
            ?>
    <?php if ($campania['costo_multimedia_id'] == 1): ?>
                <!-- Item -->
                <div class="wepiku-item">
                    <div class="item item-type1">
                        <div class="discount">
        <?php echo $campania['descuento']; ?>
                        </div>
                        <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                        </div>
                        <?php $img_promo_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                        <?php
                        // ajax button to open the modal
                        echo TbHtml::ajaxLink(
                                $img_promo_costo1, $this->createUrl('hotdeals/getInfoCampaniaAjax'), array(
                            'dataType' => 'json',
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'modal' => $modal
                            ),
                            'success' => 'function(data){
                                            openModal("' . $modal . '", data.content);
                                        }'
                                ), array(
                            'id' => 'open-modal-' . uniqid(),
                                //'class' => 'img-main'
                                )
                        );
                        ?>
                        <div class="caption">
                            <div class="shadow"></div>
                            <div class="col-xs-4 col-cell">
                                <div class="logo">
        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                    <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m=' . $campania['marca_id'])); ?>
                                </div>
                            </div>
        <?php if ($campania['is_diamante'] == 1): ?>
                                <div class="diamont pull-right">
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                </div>
        <?php endif; ?>
                            <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                        </div>
                        <div class="item-date">
        <?php
        $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $date = date_create($campania['fecha_finalizacion']);
        ?>
                            <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                        </div>
                    </div>
                </div>
                <!--/ Item -->
    <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                <!-- Item -->
                <div class="wepiku-item">
                    <div class="item item-type1">
                        <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                        </div>
                        <div class="discount">
        <?php echo $campania['descuento']; ?>
                        </div>
                            <?php $img_promo_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                        <?php
                        // ajax button to open the modal
                        echo TbHtml::ajaxLink(
                                $img_promo_costo2, $this->createUrl('hotdeals/getInfoCampaniaAjax'), array(
                            'dataType' => 'json',
                            'type' => 'POST',
                            'data' => array(
                                'camp' => $campania['id'],
                                'modal' => $modal
                            ),
                            'success' => 'function(data){
                                            openModal("' . $modal . '", data.content);
                                        }'
                                ), array(
                            'id' => 'open-modal-' . uniqid(),
                                //'class' => 'img-main'
                                )
                        );
                        ?>
                        <div class="caption">
                            <div class="shadow"></div>
                            <div class="col-xs-4 col-cell">
                                <div class="logo">
                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m=' . $campania['marca_id'])); ?>
                                </div>
                            </div>
                                    <?php if ($campania['is_diamante'] == 1): ?>
                                <div class="diamont pull-right">
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                </div>
                            <?php endif; ?>
                            <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                        </div>
                        <div class="item-date">
                            <?php
                            $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                            $date = date_create($campania['fecha_finalizacion']);
                            ?>
                            <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                        </div>
                    </div>
                </div>
                <!--/ Item -->
    <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                <!-- Item -->
                <div class="wepiku-item">
                    <div class="item item-type1">
                        <div class="discount">
                <?php echo $campania['descuento']; ?>
                        </div>
                        <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                        </div>
        <?php $img_promo_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
        <?php
        // ajax button to open the modal
        echo TbHtml::ajaxLink(
                $img_promo_costo3, $this->createUrl('hotdeals/getInfoCampaniaAjax'), array(
            'dataType' => 'json',
            'type' => 'POST',
            'data' => array(
                'camp' => $campania['id'],
                'modal' => $modal
            ),
            'success' => 'function(data){
                                            openModal("' . $modal . '", data.content);
                                        }'
                ), array(
            'id' => 'open-modal-' . uniqid(),
                //'class' => 'img-main'
                )
        );
        ?>
                        <div class="caption">
                            <div class="shadow"></div>
                            <div class="col-xs-4 col-cell">
                                <div class="logo">
                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json[$campania['marca_id']]['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m=' . $campania['marca_id'])); ?>
                                </div>
                            </div>
        <?php if ($campania['is_diamante'] == 1): ?>
                                <div class="diamont pull-right">
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                </div>
                                    <?php endif; ?>
                            <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                        </div>
                        <div class="item-date">
                            <?php
                            $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                            $date = date_create($campania['fecha_finalizacion']);
                            ?>
                            <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                        </div>
                    </div>
                </div>
                <!--/ Item -->
                        <?php endif; ?>
                    <?php endforeach; ?>
    </div>
</div>



<!--
                                        <h1 class="main-title clearfix">Home</h1>
                                        <i class="fa fa-camera-retro fa-5x"></i>
                                        <a class="link" href="<?php #echo $this->createUrl('section')        ?>">Go section</a>
-->


<!--					</section>-->
