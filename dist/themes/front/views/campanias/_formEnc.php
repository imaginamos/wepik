<!--					<section>-->

<div class="col-xs-12 col-sm-7 col-grid col-main main-section section-id section-encuesta" data-section="create">
    <div class="top-search clearfix">
            <form action="#" class="top-form pull-left clearfix">
                    <button class="btn-search" type="button"></button>
                    <div id="auto-search">
                    <input class="typeahead" type="text">
                    </div>
            </form>
            <div class="dropdown pull-right">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="sub-list-icon"></span>
              </button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
              </ul>
            </div>
    </div>


    <div class="info-grl info-form clearfix">
      <div class="row row-info-form">
        <div class="col-xs-12">
          <h1>CREAR ENCUESTA</h1>
        </div>
        <br/>
        <br/>
        <br/>                    
        <div class="col-xs-12">
        <?php if($boolmensaje == 1): ?>    
          <div class="modredes" style="display: block;">¡Algo sucedió!, no fué posible crear la Campaña. Intente más tarde.</div>
          <br/>
          <br/>
          <br/>                                             
        <?php endif; ?>     
        <div id="flash-message-campania">
            <?php if($boolmensaje == 2): ?>             
                <div class="modredes" style="display: none;">¡La Encuesta ha sido creada exitosamente!</div>
                <br/>
                <br/>
                <br/>             
            <?php endif; ?>             
        </div>                 
        <div class="col-xs-12">
        <?php echo CHtml::beginForm('encuesta', 'post', array ('class' => 'grl-form')); ?>  
	
            <?php $num_dias_vigencia = $value_free['num_dias_vigencia']!=0?$value_free['num_dias_vigencia']:$form->dias_vigencia_enc ?>
            <?php $num_encuestas = $value_free['num_encuestas']!=0?$value_free['num_encuestas']:$form->encuestas_enc ?>
            <?php $val_free = array('num_dias_vigencia' => $num_dias_vigencia, 'num_encuestas' => $num_encuestas) ?>            
            <?php echo CHtml::HiddenField('EncuestaForm[costo_cpa]', $costos['costo_cpa'], array('class' => 'costo_cpa EncuestaForm_encuestas_enc')); ?>            
            <?php echo CHtml::HiddenField('tipo_id', $tipo_id, array('class' => 'tipoCamp')); ?>
            <?php echo CHtml::HiddenField('tipo_id_camp', $tipo_id_camp); ?>
            <?php echo CHtml::HiddenField('EncuestaForm[numero_preguntas_abierta]', 0, array('id' => 'num_preguntas_abierta')); ?>
            <?php echo CHtml::HiddenField('EncuestaForm[numero_preguntas_multiple_unic]', 0, array('id' => 'num_preguntas_multiple_unic')); ?>
            <?php echo CHtml::HiddenField('EncuestaForm[numero_preguntas_multiple]', 0, array('id' => 'num_preguntas_multiple')); ?>
            <?php echo CHtml::HiddenField('mid', isset($marca_id)?$marca_id:null); ?>
            <?php echo CHtml::HiddenField('CampaniaForm', isset($formcamp->marcas)?CJSON::encode($formcamp):null); ?>
			   <?php echo CHtml::HiddenField('img_flo', $img_flo); ?>
			   <?php echo CHtml::HiddenField('esfreemium', $esfreemium); ?>
            <?php if(!is_numeric($marca_id)): ?>            
                <fieldset class="con-field">
                    <?php echo CHtml::Label('* Marcas', 'marcasenc') ?>
                    <?php echo CHtml::activeDropDownList($form, 'marcasenc', $marcas_usuario, array(
                        'class' => 'multi-select col-xs-4', 
                        'empty'=>'Seleccione Marca ',
                        'ajax'=>array(
                            'dataType' => 'json', 
                            'type'=>'POST',
                            'url'=>$this->createUrl('campanias/getInfoCrearCampaniaBackend'),
                            'data'=>array(
                                'selected' => 'js:this.value',
                                'tipo' => 'js:$("#tipo_id").val()'
                            ),
                            'success' => 'function(data){
                                $.each(data.freemium, function(key, value){
                                    $("."+key).html(value);
                                });
                                $.each(data.value_freemium, function(key, value){
                                    $("."+key).val(value);
                                    $("."+key).siblings(".counter-number").find(".number").html(value);
                                });   
                                $.each(data.costos, function(key, value){
                                    $("."+key).val(value);
                                }); 
                                var values = "";
                                $.each(data.cost_element, function(key, value){
                                    values = values.concat(value);
                                });                      
                                $("#elemento_costo").html(values);
                                $(".saldo").html(data.saldo);
                                $("#total").children("p").html(data.total);
                            }'
                         ),                            
                    )); ?>   
                    <?php echo CHtml::error($form, 'marcasenc'); ?>   
                </fieldset>
            <?php endif; ?>
            <div class="row">
                <fieldset class="col-xs-12 con-field">
                    <?php echo CHtml::Label('* Nombre de la encuesta', 'nombre_encuesta'); ?>
                    <?php echo CHtml::activeTextField($form, 'nombre_encuesta', array('class' => 'input')) ?>  
                    
                    <?php //echo CHtml::error($form,'nombre_encuesta'); ?>
                </fieldset>                
            </div>
            <?php if(!is_numeric($marca_id)): ?> <!--Id de marca que biene de la campaña-->
                <div class="row">                                    
                    <fieldset class="con-field" id="check_pais_ciudad">   
                        <?php echo CHtml::Label('* Coberturas de paises y/o ciudades', 'pais_ciudadenc') ?> 
                        <?php echo CHtml::activeRadioButton($form, 'pais_ciudadenc', array(
                            'value'=>1,
                            'id'=>'pais_ciudad1',
                            'class'=>'radioCob',
                            'uncheckValue'=>null,
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>$this->createUrl('campanias/getCoberturasBackendAjax'),
                                'data'=>array(
                                    'sel_pais_ciudad' => 'js:this.value',
                                    'mid' => 'js:$("#EncuestaForm_marcasenc").find(":selected").val()'
                                ),                                         
                                'success' => 'function(data){
                                    $("#pais_ciudad1").attr("checked", true);
                                    $("#EncuestaForm_coverageenc").html(data);
                                    $("#EncuestaForm_coverageenc").siblings("ul").empty();
                                }'
                            )
                        )); ?>
                        <?php echo CHtml::Label('Paises', 'pais_ciudad1', array('style' => "display: inline; margin-right: 20px;")); ?>                                    

                        <?php echo CHtml::activeRadioButton($form, 'pais_ciudadenc', array(            
                            'value'=>2,
                            'id'=>'pais_ciudad2',
                            'class'=>'radioCob',
                            'uncheckValue'=>null,
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>$this->createUrl('campanias/getCoberturasBackendAjax'),
                                'data'=>array(
                                    'sel_pais_ciudad' => 'js:this.value',
                                    'mid' => 'js:$("#EncuestaForm_marcasenc").find(":selected").val()'                                    
                                ),      
                                'success' => 'function(data){
                                    $("#pais_ciudad2").attr("checked", true);
                                    $("#EncuestaForm_coverageenc").html(data);
                                    $("#EncuestaForm_coverageenc").siblings("ul").empty();
                                }'
                            )                                        
                        )); 
                        ?>
                        <?php echo CHtml::Label('Ciudades', 'pais_ciudad2', array('style' => "display: inline; margin-right: 20px;")); ?>
                        <?php echo CHtml::error($form, 'pais_ciudadenc') ?>
                    </fieldset>                 
                    <fieldset class="col-xs-12 con-field">
                        <div class="row row-multi">
                            <?php echo CHtml::activeDropDownList(
                                $form, 
                                'coverageenc', 
                                $coverageenc,
                                array(
                                    'class' => 'multi-select col-xs-4', 
                                    'onchange' => 'multiSelect7(this); $("#EncuestaForm_coverageenc option:nth-child(1)").attr("disabled", "disabled");',                                                                                                  
                                )) ?>
                                <ul class="multi-list col-xs-8">
                                    <?php if($hasDataform and isset($form->coberturaenc)): ?>  
                                        <?php foreach($form->coberturaenc as $key => $cobertura): ?>
                                            <li onclick="this.parentNode.removeChild(this);">
                                                <input type="hidden" name="EncuestaForm[coberturaenc][<?php echo $key ?>][id]" value="<?php echo $cobertura['id'] ?>">
                                                <input type="hidden" name="EncuestaForm[coberturaenc][<?php echo $key ?>][nombre]" value="<?php echo $cobertura['nombre'] ?>">                                            
                                                <?php echo $cobertura['nombre']; ?>
                                           </li>                                         
                                        <?php endforeach; ?>
                                   <?php endif; ?>                                            
                                </ul> 
                        </div>
                        <?php echo CHtml::error($form, 'coberturaenc') ?>   
                    </fieldset>
                </div>
            <?php endif; ?>
            
            
            
              <!--edad genero-->
              <?php
              if($costos['costo_cpa'] == ""){
              ?>
                  <fieldset class="col-xs-3 con-field">
                                <label for="sexo">* Género</label>                              
                         <select class="select" name="sexo" id="EncuestaForm_sexo">
                            <option value="">Elija opción</option>
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                            <option value="T">Todos</option>
                         </select>                                                           
                  </fieldset>
                            <fieldset class="col-xs-1 con-field">
                                <label><br><p></p>de:</label>
                            </fieldset>
                         
                          <fieldset class="col-xs-4 con-field">
                             <label for="edad_inicial">* Rango de edades</label>    
                             <select class="select" name="edad_inicial" id="EncuestaForm_edad_inicial">
                                <option value="">Elija opción</option>
                                
                                   <?php                                   
                                      for($i=15; $i<101; $i++):                                
                                        echo '<option value="'.$i.'">'.$i.' años</option>';
                                      endfor; 
                                  ?>
                              </select>                                                            
                          </fieldset>
                          
                            <fieldset class="col-xs-1 con-field">
                                <label for="hasta"><br><p></p>hasta:</label>
                            </fieldset>
                            
                            <fieldset class="col-xs-3 con-field ancla-pregunta">
                                <label>&nbsp;</label> 
                             <select class="select" name="edad_final" id="EncuestaForm_edad_inicial">
                                <option value="">Elija opción</option>
                                
                                   <?php                                   
                                      for($i=15; $i<101; $i++):                                
                                        echo '<option value="'.$i.'">'.$i.' años</option>';
                                      endfor; 
                                  ?>
                              </select>                                                            
                          </fieldset>
                <?php
                              }
                ?>
                   <!--edad genero FIN--> 


                   <!-- Diamantes   -->

<!-- Diamante -->
                   
              <fieldset class="col-xs-12 con-field">
                <?php echo CHtml::Label('* Descripción', 'desc_campania_enc') ?>
                <?php echo CHtml::activeTextArea($form, 'desc_campania_enc', array('class' => 'textarea')) ?> 
                <?php //echo CHtml::error($form, 'desc_campania_enc') ?>                
              </fieldset>
            <div class="row">

                    <div class="col-xs-12 text-left info-form ">
                            <h3>PREGUNTAS</h3>
                            <div class="con-field" style="color: #039CC3;">(Cobro por pregunta contestada)</div>        
                    </div>

           		

            </div>
		
            <ol id="listado_preguntas">
			
                <?php if($hasDataform and isset($html_preguntas)): ?> 
                    <?php echo $html_preguntas ?>
                <?php endif; ?>
				
				
				 <fieldset class="col-xs-12 col-sm-6 con-field list-pre-flo">
                <?php echo CHtml::Label('* Agregar Pregunta', 'tipo_pregunta') ?>
                <div class="row row-multi">                
                <?php echo CHtml::activeDropDownList(
                        $form, 
                        'tipo_pregunta', 
                        array(1 => 'Abierta', 2 => 'Selección múltiple única respuesta', 3 => 'Selección múltiple múltiple respuesta'), 
                        array(
                            'class' => 'select col-xs-4',
//                            'id' => 'tipo-pre',
                            'empty'=>'Elija tipo pregunta',
                            'ajax' => array(
                                'dataType' => 'json',                                 
                                'type'=>'POST',
                                'url'=>$this->createUrl('campanias/getPreguntasPorTipoAjax'),
                                'data'=>array(
                                    'tipo_preg' => 'js:this.value', 
                                    'numpreg' => 'js:getCantidadPreguntas(this.value)',
                                    
                                ),      
                                'success' => 'function(data){
                                    $("#listado_preguntas").append(data);
                                    $("#EncuestaForm_tipo_pregunta").val("");
                                    
                                   
                                   
                                   
                                   
                                }'
                            )  
                        )) ?>   

<?php echo CHtml::error($form, 'tipo_pregunta') ?>   						
                </div>

            </fieldset>			
            </ol>
            <div class="row">
              <fieldset class="col-xs-12 con-field">
                <div class="label-card">
                  <div class="row">
                    <div class="col-xs-10">
                        <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-small.png" alt=""></span>
                        <p><strong>Paga con tarjeta de crédito,</strong> Puedes pagar tus campañas con tarjeta de crédito personal o corporativa, solo tienes que seguir los pasos, y proporcionar la información solicitada y listo!
                    </div>
                    <div class="col-xs-2 text-right">
                        <?php echo CHtml::activeCheckBox($form, 'is_freemium_enc', array(            
                            'value'=>1,
                            'class'=>'check-active check-active-content',
                            'uncheckValue'=>2,                                  
                        )); 
                        ?>                      
                    </div>
                  </div>
                </div>
              </fieldset>
            </div>

            <div class="row row-item-polls">
            <?php if(!is_numeric($marca_id)): ?>
              <div class="col-xs-6">
                <div class="item-poll">
                  <h3>
                    Días Vigencia
                    <span class="item-icon1"></span>
                  </h3>
                  <div class="item-poll-box">
                    <div class="item-poll-box-text clearfix">
                      <div class="table">
                        <div class="table-cell">
                            <h2  class="cantidad_dias">               
                                <?php echo $free['cantidad_dias'] ?>
                      
                            </h2>
                        </div>
                      </div>
                      <p>&nbsp;</p>
                    </div>
                    <div class="item-poll-box-edit item-poll-content white-bg">
                      <div class="item-poll-date item-poll-date-content"></div>
                      <div class="item-poll-date-info">
            <?php endif; ?>            
                            <input id="date-poll-content" value="" type="hidden">
            <?php if(!is_numeric($marca_id)): ?>                             
                            <?php echo CHtml::HiddenField('EncuestaForm[dias_vigencia_enc]', $num_dias_vigencia, array('class'=>'num_dias_vigencia')); ?>
                            <?php echo CHtml::HiddenField('EncuestaForm[fini_dias_vigencia_enc]', false, array('class'=>'fini_dias_vigencia')); ?>
                            <?php echo CHtml::HiddenField('EncuestaForm[ffin_dias_vigencia_enc]', false, array('class'=>'ffin_dias_vigencia')); ?>                    
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>
              <div class="col-xs-6">
                <div class="item-poll">
                  <h3>
                    Cantidad de preguntas
                    <span class="item-icon2"></span>
                  </h3>
                  <div class="item-poll-box">
                    <div class="item-poll-box-text clearfix">
                      <div class="table">
                        <div class="table-cell">
                            <h2  class="cantidad_encuestas">
                                <?php echo $free['cantidad_encuestas'] ?>
                            </h2>
                        </div>
                      </div>
                      <p>&nbsp;</p>
                    </div>
                    <div class="item-poll-box-edit item-poll-content">
                      <div class="table">
                        <div class="table-cell">
                          <div class="cart-counter">
                            <button class="btn-count-down pull-left" type="button"><span></span></button>
                            <span class="counter-number">
                                    <span class="number"><?php echo $num_encuestas ?></span>
                            </span>                            
                            <button class="btn-count-up pull-right" type="button"><span></span></button>
                            <?php echo CHtml::TextField('EncuestaForm[encuestas_enc]', $num_encuestas, array('class'=>'numeros_costos num_encuestas input')); ?> 
                            <?php echo CHtml::error($form, 'encuestas_enc') ?>                            
                          </div>
                        </div>
                      </div>
                      <!-- <div class="item-poll-date"></div>
                      <div class="item-poll-date-info">
                        <input id="date-poll" value="">
                      </div> -->
                    </div>
                  </div>
                  <!-- <div class="item-poll-text">
                    <div class="table">
                      <div class="table-cell">
                        <h2>
                          Pueden hacer<br>
                          hasta <strong>10</strong> Piks
                        </h2>
                      </div>
                    </div>
                    <p>Clic para editar</p>
                  </div> -->
                </div>
              </div>

            </div>
        </div>
      </div>
    </div>
</div>


<div class="col-xs-12 col-sm-2 col-sub-nav section-plan col-plan">
        <div class="sub-nav active">
                <button class="btn-sub-nav" type="button">
                        <span class="btn-arrow"></span>
                        <span class="btn-icon"></span>
                </button>
        </div>
        <div class="plan-cost">
                <div class="plan-cost-head baj-cam">
                        <h3>COTIZA TU CAMPAÑA</h3>
                        <p>
                                    Con esta herramienta puedes ir calculando cuánto te va a costar tu campaña. Añade los ítems que deseas, según tus objetivos de mercadeo y ajusta la campaña según tu presupuesto.                                        
                        </p>
                        <div class="row row-cost-info">
                                <div class="col-xs-7 text-left">
                                        <h5><span class="blue">SALDO ACTIVO</span></h5>
                                </div>
                                <div class="col-xs-5 text-right">
                                        <p><span class="blue saldo"><?php echo $saldo ?></span></p>
                                </div>
                        </div>
                        <div class="row row-cost-info">
                                <div class="col-xs-12 text-left">
                                        <h5>COSTO CAMPAÑA</h5>
                                </div>
                                <div class="col-xs-12" id="elemento_costo">
                                    <?php if($hasDataform and isset($costos_elementos)): ?>  
                                        <?php foreach ($costos_elementos['cost_elem'] as $accion => $costo_accion): ?>
                                            <?php echo $costo_accion ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>                            
                        </div>
                </div>
                <div class="plan-cost-total">
                        <div class="row">
                                <div class="col-xs-4 text-left">
                                        <h5>TOTAL</h5>
                                </div>
                                <div class="col-xs-8 text-right" id="total">
                                        <p><?php echo $costos_elementos['tot'] ?></p>
                                </div>
                        </div>
                </div>
                <div class="plan-cost-create">
                    <?php
                     echo CHtml::submitButton('CREAR', array(
                        'class'=>'btn',
                        'type'=>'button'
                         )
                     ); 
					 if($costos['costo_cpa'] != ""){
						 echo '<div class="btn-volver btn" type="submit" name="yt1" >VOLVER<div>';
					 }
                 
					 ?>   
					 
                </div>
        </div>
</div>
<?php echo CHtml::endForm(); ?> 
 
<div class="plan-cost-create" style="margin: 5px 0;">
    <?php echo CHtml::beginForm('VolverAcampania', 'post', array ('class' => 'grl-form')); ?>  
        <div class="plan-cost-create">
            <?php echo CHtml::HiddenField('EncuestaForm[costo_cpa]', $costos['costo_cpa'], array('class' => 'costo_cpa EncuestaForm_encuestas_enc')); ?>            
                <?php echo CHtml::HiddenField('tipo_id', $tipo_id, array('class' => 'tipoCamp')); ?>
                <?php echo CHtml::HiddenField('tipo_id_camp', $tipo_id_camp); ?>
                <?php echo CHtml::HiddenField('EncuestaForm[numero_preguntas_abierta]', 0, array('id' => 'num_preguntas_abierta')); ?>
                <?php echo CHtml::HiddenField('EncuestaForm[numero_preguntas_multiple_unic]', 0, array('id' => 'num_preguntas_multiple_unic')); ?>
                <?php echo CHtml::HiddenField('EncuestaForm[numero_preguntas_multiple]', 0, array('id' => 'num_preguntas_multiple')); ?>
                <?php echo CHtml::HiddenField('mid', isset($marca_id)?$marca_id:null); ?>
                <?php echo CHtml::HiddenField('CampaniaForm', isset($formcamp->marcas)?CJSON::encode($formcamp):null); ?>
				<?php echo CHtml::HiddenField('img_flo', $img_flo); ?>
				<?php echo CHtml::HiddenField('esfreemium', $esfreemium); ?>
            <?php echo CHtml::submitButton('VOLVER', array( 'class'=>'btn click_volver', 'type'=>'button'  , 'style'=>'display: none;')  ); ?>   
        </div>
    <?php echo CHtml::endForm(); ?>     
</div>    

    
<!--					</section>-->
<script type="text/javascript">


// $(document).ready(function(){
    
//     var validarContacto = function(e){
        
//         // var expresionRegular = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
//         var marca = $('#EncuestaForm_marcasenc');
//         var name = $('#EncuestaForm_nombre_encuesta');
//         var descripcion = $('#EncuestaForm_desc_campania_enc');
//         var paisSelect = $('#EncuestaForm_coverageenc'); 
//         var pregunta = $('#EncuestaForm_tipo_pregunta');
        
        
//         //var validate = ['name','phone','pais','empresa','correo'];
        
//         // if(marca.val().trim() === ''){
//         //     e.preventDefault();
//         //     marca.css({
//         //         'border': '3px solid #039dc3'
//         //     });
//         //     marca.focus();
//         //     return false;
//         // }
//         // else{
//         //     marca.css({
//         //         'border': 'none'
//         //     });
//         // }

//         if(pregunta.val().trim() === ''){
//             e.preventDefault();
//             pregunta.css({
//                 'border': '3px solid #000'
//             });
//             pregunta.focus();
//             return false;
//         }
//         else{
//             pregunta.css({
//                 'border': 'none'
//             });
//         }
    
//     }

//     $('.btn').on('click', validarContacto);
    

    

    
// });



$(window).load(function() {
	
	$('.btn-volver').click(function(){
		$('.click_volver').trigger('click');
	});
    
    var mensaje = '<?php echo $boolmensaje ?>',
        marca_id = '<?php echo $form->marcasenc ?>';
    if(parseInt(mensaje) === 2){
        $("#flash-message-campania .modredes").css({ display: "block" });
        $("#flash-message-campania .modredes").animate({
            opacity: 0
            }, 6000, function() {
                    jQuery.yii.submitForm(this,"/marcas/marcaAdminActivas",{"m":marca_id});
                    return false;
            });        
    }
    
    // CUANDO VIENE DEL PAGO CON TARJETA 
    var pago = '<?php echo isset($_REQUEST["pago"]) ? $_REQUEST["pago"] : ""; ?>';
    //var pagopromo = '<?php echo (isset($_REQUEST["pagopromo"])) ? $_REQUEST["pagopromo"] : "false"; ?>';
    
    if(pago == 'true')
    {
        $(".check-active-content").iCheck("check");
        $(".label-card").attr("style", "display:none");
    }
    else
    {
        $(".check-active-content").iCheck("uncheck");  
        $(".label-card").attr("style", "display:none");
    }
    
    if($(".check-active-content").is(":checked")) {
        $(".item-poll-content").addClass("active");
//        $(this).next().attr('class', 'blue');                              
    }    
    if($(".check-active-promotion").is(":checked")){
        $(".item-poll-promotion").addClass("active");        
    }
});

/*
$(document).ready(function() {
$('.list-pre-flo').hide(0);
		$(window).scroll(function(){
          var windowHeight = $(window).scrollTop();
          var contenido2 = $(".ancla-pregunta").offset();
          contenido2 = contenido2.top;
   
              if(windowHeight >= contenido2  ){
            $('.list-pre-flo').fadeIn(500);
   
              }else{
            $('.list-pre-flo').fadeOut(500);
          }
		});
    
});
*/
                
                
                

</script>    