<!--					<section>-->


<div class="container-fluid brand-page-head section-id">
    <img src="<?php echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover']; ?>" alt="">
</div>
<div class="brand-head-fx"></div>


<div class="col-xs-12 col-sm-7 col-grid col-main grid-section brand-section">
    <div class="top-search clearfix">
        <form action="#" class="top-form pull-left clearfix">
            <button class="btn-search" type="button"></button>
            <div id="auto-search">
                <input class="typeahead" type="text">
            </div>
        </form>
        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                <span class="sub-list-icon"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
            </ul>
        </div>
    </div>


    <div class="mg-brand-logo text-center">
	
	
        <div class="table">
            <div class="table-cell">
                <!--<img class="banner-marca" src="<?php //echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover']; ?>">-->
                <div class="brand-page-logo">
                    <img class="img-circle" src="<?php echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil']; ?>">                               
                    <span class='brand-like active'>
                    <?php
                        $corazon="";
                    if($data_marca_json['is_checked']==1):
                        $texto = "Favoritos";
                        $activa = 1;
                        $like = "<div class='like-icon'></span>";
                        echo "<div class='like-icon'></span>";
                        $corazon = "<div class='corazon favorito'></div>";
                        
                    else:
                        $texto = "Favoritos";
                        $activa = 0;
                        echo "<div class=''></div>";
                        $like = "<div></span>";
                        $corazon = "<div class='corazon'></div>";
                    endif;?>
                    </span>
                </div>
                <h1><?php echo $data_marca_json['nombre'];?></h1>
            </div>
        </div>
    </div>


    <div class="brand-info">
        <h6 class="text-center">
           <?php echo $data_marca_json['descripcion'];?>
        </h6>
        <div class="row row-table row-brand-links text-center">
            <div class="col-sm-3 col-cell">
                <a href="<?php echo $data_marca_json['url_facebook'];?>" target="_blank">
                    <span class="icon-1"></span>
                    <p class="text-left">Página de facebook</p>
                </a>
            </div>
            <div class="col-sm-3 col-cell">
                <button class="btn-favorite">
<!--                    ---------------->
                    
                    <?php
                        
                       echo CHtml::link(
                           $corazon.
                           '<p class="text-left">Agregar / Quitar Favorito </p>',   
                           '#', array(
                           'id' => 'send-link2-' . uniqid(),
                           'class' => 'marca-' . $data_marca_json['id'],
//                                            'return' => false, // this is already false by default - read documentation
                           'ajax' => array(
                               'url' => $this->createUrl('marcas/setMarcaLikeBackend'),
                               'dataType' => 'json',
                               'type' => 'POST',
                               'data' => array(
                                   'mid' => $data_marca_json['id'],
                                   'l'   => 'js:$(".corazon").hasClass("favorito")'
                               ),
                                'success'=>'function(){
									console.log(paso);
                                    var divheart = $(".corazon");
									
									
                                    if (!divheart.hasClass("favorito")) {
                                        $(".corazon").addClass("favorito");
                                        $(".brand-like div").addClass("like-icon");
                                    } else {
                                        $(".corazon").removeClass("favorito");
                                        $(".brand-like div").removeClass("like-icon").attr("style", "display: inline-block; height: 28px; margin-top: 5px;");
                                    }                                                    
                                }'
                           )                           
                        ));
                       ?>  
                    
                </button>
            </div>
            <div class="col-sm-3 col-cell">
                <a href="<?php echo $data_marca_json['pagina_web'];?>" target="_blank">
                    <span class="icon-3"></span>
                    <p class="text-left">Visitar web</p>
                </a>
            </div>
            <div class="col-sm-3 col-cell">
                <a href="<?php echo $this->createurl('mapas/mapaSucurCampaniaSelecBackend/mapa?t=2&m='.$data_marca_json['id'])?>">
                    <span class="icon-4"></span>
                    <p class="text-left">Ubicación tiendas</p>
                </a>
            </div>
        </div>
    </div>
    <div class="wepiku-grid">

        <?php 
        $con = 1;
		//print_r($data_hotdeals_json['data']);
        foreach ($data_hotdeals_json['data'] as $campania): ?>
            <?php 
            echo "<script>
                $(document).ready(function(){
                    $('#cli".$con."').click(function(){
                    $('#img_cli".$con."').trigger('click');
                         });
                 });                            
                  </script>";
             ?> 
            <?php if ($campania['tipo_id'] == 3): ?>
                <!-- Item -->
                <div class="wepiku-item">
                    <div class="item item-type3" data-toggle="modal" data-target="#modal-poll-list">
                        <div class="item-type3-bg" style="background-image: url(<?php echo Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover']; ?>);">
                            <?php $img_encuesta = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenCover'], "",array("style"=>"margin-bottom: -135px; height: 134px; position: relative;z-index: 9;opacity: 0;")); ?>
                        </div>
                        
                        <?php                                
                            // ajax button to open the modal
                            echo TbHtml::ajaxLink(
                                $img_encuesta,
                                $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                array( 
                                    'dataType' => 'json', 
                                    'type' => 'POST',
                                    'data' => array(
                                        'camp' => $campania['id'],
                                        'modal' => '#modal-poll-list'
                                    ),
                                    'success' => 'function(data){
                                        openModal("#modal-poll-list", data.content);
                                    }'
                                ),
                                array(
                                    'id' => 'open-modal-'.uniqid()
                                )
                            );                                
                        ?>                        
                       <div class="caption">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_pencil.png" alt=""></span>
                            </p>
                            <h2 class="clearfix">
        <?php echo $campania['nombre']; ?>
                            </h2>
                            <div class="logo">
                                <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m=' . $campania['marca_id'])); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Item -->

            <?php elseif ($campania['tipo_id'] == 1 || $campania['tipo_id'] == 4): ?>
                <?php
                if ($campania['tipo_id'] == 1):
                    $modal = "#modal-product";
                elseif ($campania['tipo_id'] == 4):
                    $modal = "#modal-poll-product";
                endif;
                ?>
                <?php if ($campania['costo_multimedia_id'] == 1): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo1,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>  
                        </div> 
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                                <p>
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                                </p>
                            </div>
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <?php $img_promo_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo2,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                              
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                    <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>                                
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>   
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="discount">
                                <?php echo $campania['descuento']; ?>
                            </div>
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_promo_sale.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo3,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                            <div class="item-date">
                                <?php
                                $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
                                $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                                $date = date_create($campania['fecha_finalizacion']);
                                ?>
                                <h5 class="text-center"><?php echo 'Valido hasta el ' . $dias[date_format($date, 'w')] . " " . date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1] . " del " . date_format($date, 'Y'); ?></h5>
                            </div>   
                        </div>
                    </div>
                    <!--/ Item -->
                <?php endif; ?>

            <?php elseif ($campania['tipo_id'] == 2 || $campania['tipo_id'] == 5): ?>
                <?php
                if ($campania['tipo_id'] == 2):
                    $modal2 = "#modal-product";
                elseif ($campania['tipo_id'] == 5):
                    $modal2 = "#modal-poll-product";
                endif;
                ?>
                <?php if ($campania['costo_multimedia_id'] == 1): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo1 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo1,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>
                        </div> 
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 2): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                                <p>
                                    <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                                </p>
                            </div>
                            <?php $img_promo_costo2 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo2,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                              
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                    <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>                                
                            </div>  
                        </div>
                    </div>
                    <!--/ Item -->
                <?php elseif ($campania['costo_multimedia_id'] == 3): ?>
                    <!-- Item -->
                    <div class="wepiku-item">
                        <div class="item item-type1">
                            <div class="destacado-item">
                            <p>
                                <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ic_launch.png" alt=""></span>
                            </p>
                            </div>
                            <?php $img_promo_costo3 = CHtml::image(stripslashes($campania['enlace']), "campania", array('class' => 'img-main')); ?>
                            <?php                                
                                // ajax button to open the modal
                                echo TbHtml::ajaxLink(
                                    $img_promo_costo3,
                                    $this->createUrl('hotdeals/getInfoCampaniaAjax'),
                                    array( 
                                        'dataType' => 'json', 
                                        'type' => 'POST',
                                        'data' => array(
                                            'camp' => $campania['id'],
                                            'modal' => $modal2
                                        ),
                                        'success' => 'function(data){
                                            openModal("'.$modal2.'", data.content);
                                        }'
                                    ),
                                    array(
                                        'id' => 'open-modal-'.uniqid(),
                                        //'class' => 'img-main'
                                    )
                                );                                
                            ?>                            
                            <div class="caption">
                                <div class="shadow"></div>
                                <div class="col-xs-4 col-cell">
                                    <div class="logo">
                                        <?php $image = CHtml::image(Yii::app()->session['url_img'] . $data_marca_json['img_imagenPerfil'], ""); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                        <?php echo CHtml::link($image, $this->createUrl('marcas/marca?m='.$campania['marca_id'])); ?>
                                    </div>
                                </div>
                                <?php if ($campania['is_diamante'] == 1): ?>
                                     <div class="diamont pull-right">
                                         <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamante.png" alt=""></span>
                                    </div>
                                <?php endif; ?>
                                <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                            </div>  
                        </div>
                    </div>
                    <!--/ Item -->
                <?php endif; ?>
            <?php endif; ?>
                    
        <?php 
        $con++;
        endforeach; ?>
    </div>
</div>
<script>
$(document).ready(function(){
	$(".btn-favorite").click(function(){
										
										if($(".corazon").hasClass("favorito")){
											$(".corazon").removeClass("favorito");
											$(".corazon").addClass("nofavorito");
											$(".brand-like div").removeClass("like-icon").attr("style", "display: inline-block; height: 28px; margin-top: 5px;");
										
										}else{
											 $(".corazon").addClass("favorito");
											$(".brand-like div").addClass("like-icon");
										}
									});
});
</script>


<!--					</section>-->
