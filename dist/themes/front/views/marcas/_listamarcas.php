
	 <?php
    
   foreach ($marcas_seguidas as $marca): ?>       
         
                <li class="col-xs-2">
                        <div class="mark-item">
                                <span class="mark-name"><?php echo $marca['nombre']; ?></span>           
                                <?php $image_marca = CHtml::image(Yii::app()->session['url_img'].$marca['img_imagenPerfil'], "", array('class'=>'img-circle', "width"=>"110px" ,"height"=>"110px")); ?>                                
                                
                                <?php if (Yii::app()->session['is_new'] == false): ?>
                                    <?php echo CHtml::link(
                                            $image_marca, 
                                            $this->createUrl('marcas/marca', array('m' => $marca['id'])),
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>
                                <?php else: ?>
                                    <?php echo CHtml::link(
                                            $image_marca, 
                                            "#",
                                            array(
                                                'class' => 'logo img-circle modal-marca'
                                            )
                                        ); ?>   
                                <?php endif; ?>
                                
                                
								<?php//if ($marca['is_suggested'] == 1 and $marca['is_checked'] == 0){ ?>
									<!--<div class="fav--><?php //echo $marca['id']; ?> <!-- state " onclick="favorito('--><?php //echo $marca['id']; ?><!--','--><?php //echo $marca['is_checked']; ?><!--')"><span></span></div>-->
								<?php //}else if($marca['is_suggested'] == 1 and $marca['is_checked'] == 0){ ?>
									<!--<div class="fav---><?php //echo $marca['id']; ?><!-- state " onclick="favorito('--><?php //echo $marca['id']; ?><!--','--><?php //echo $marca['is_checked']; ?><!--')"><span></span></div>-->
								<?php //}else if($marca['is_checked'] == 1){ ?>
									<!--<div class="fav---><?php //echo $marca['id']; ?><!-- state favorite" onclick="favorito('--><?php //echo $marca['id']; ?><!--','--><?php //echo $marca['is_checked']; ?><!--')"><span></span></div>-->
								<?php //} ?>     
                
                <?php
                  if($marca['is_checked'] == 1){
                ?>    
                    <div class="fav-<?php echo $marca['id']; ?> state favorite" onclick="favorito('<?php echo $marca['id']; ?>','<?php echo $marca['is_checked']; ?>')"><span></span></div>
                <?php  
                  }else{
                ?>
									<div class="fav-<?php echo $marca['id']; ?> state " onclick="favorito('<?php echo $marca['id']; ?>','<?php echo $marca['is_checked']; ?>')"><span></span></div>
                <?php
                  }
                ?>
                
                        </div>
                </li>
         
        <?php endforeach; ?>   
		
		<script>
			function favorito(id, estado){
				console.log('id: '+id);
				 $.ajax({
						url: "<?php echo $this->createUrl('marcas/setMarcaLikeBackend'); ?>", 
						data: {mid: id , l: estado},
						type: "POST",
						success: function(data) {
							console.log('id_res: '+data);
							if(!$('.fav-'+data).hasClass("favorite")){
								$('.fav-'+data).addClass('favorite');
							}else{
								$('.fav-'+data).removeClass('favorite');
							}
							
						  return data; 
						}
				  });
			}
		</script>
        
        <!-- Item -->