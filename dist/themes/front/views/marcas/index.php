
<div class="col-xs-12 col-sm-7 col-grid col-main main-section section-id" data-section="marks">
    <div class="top-search clearfix visible">
        <form action="#" class="top-form pull-left clearfix">
            <button class="btn-search" type="button"></button>
            <div id="auto-search">
            <!--<input class="typeahead" type="text">-->
                <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'name' => 'search-marca',
                    'value' => '',
                    'source' =>
                    'js:function( event, ui ) {
                                $.ajax({
                                    url:"' . $this->createUrl('marcas/getMarcasAjax') . '",
                                    data:{seach:event.term},
                                    success: function(data) {
                                        $("#list-abc").html(data);
                                        $("div.ln-letters").children("a").removeClass("ln-selected");
                                    }
                                })
                            }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => '1',
                    ),
                    'htmlOptions' => array(
                        'placeholder' => 'Escriba una marca a buscar...',
                        'class' => 'typeahead',
                        'type' => 'text',
                        'id' => 'send-link1-' . uniqid(),
                    ),
                ));
                ?>
            </div>
                <?php if (Yii::app()->session['is_new'] == true): ?>
                <!--<button class="btn-search-login" type="submit" name="button">Continuar</button>--> 
                <div class="conti-marca">
                    <?php
                    echo CHtml::Link(
                            'CONTINUAR', $this->createAbsoluteUrl('/site/login', array('provider' => 'facebook')), array('style' => 'color: #fff;')
                    );
                    ?>
                </div>
            </form>
    <?php else:  ?>
            <!--</form>-->
            <div class="dropdown pull-right" style="right: -72px;" >
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    <span class="sub-list-icon"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <?php foreach ($categorias as $categoria): ?>
                        <li role="presentation">
                            <?php
                            echo CHtml::ajaxLink(
                                    $categoria['nombre'], CController::createUrl('marcas/getMarcasByCategoriaAjax'), array(
                                'dataType' => 'json',
                                'type' => 'POST',
                                'data' => array(
                                    'cat' => $categoria['id']
                                ),
                                'success' => 'function(data){
                                                    $("#list-abc").html(data);
                                                    $("div.ln-letters").children("a").removeClass("ln-selected");
                                                }'
                                    ), array(
                                'id' => 'send-link-' . uniqid(),
                                'role' => 'menuitem',
                                'tabindex' => "-1"
                                    )
                            );
                            ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>                    
        <?php endif; ?>
    </div>

    <?php $lista_letras = array('todo', null, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'); ?>
    <div class="row marks-list">
        <div id="tabpage_1" class="tabContainer">
            <div id="list-abc-nav" class="listNav">
                <div class="ln-letters">
				
                    <?php 
					$co= 1;
						foreach ($lista_letras as $letra){
					?>
							<a id='let-<?php echo $co; ?>' class="letra" onclick='buscarletr("<?php echo $letra; ?>",<?php echo $co; ?>);'><?php echo $letra; ?></a>
					<?php
						$co++;
					}
					?>
                <script>             
					function buscarletr(letra , id){
						console.log('letra: '+letra);
						console.log('id: '+id);
						$('.letra').removeAttr('style');
									 $.ajax({
										url: "getMarcasByLetraAjax", 
										data: {letraselec: letra},
										type: "GET",
										success: function(data) {
											$("#list-abc").html(data);
											$("#let-"+id).css({'font-weight':'bold'});
										  return data; 
										}
									  });
					}
				</script>         
			
                </div>
            </div>
            <ul id="list-abc" class="filter-list row">
                <?php $this->renderPartial('_listamarcas', array('marcas_seguidas' => $marcas_seguidas, 'search' => 0)); ?>
            </ul>                        
        </div>
    </div>
</div>
<!--</div>-->
<!--					</section>-->
<?php if (Yii::app()->session['is_new'] == true): ?>
    <div class="fondo-modal"></div>
    <div class="motal-content-marca">
        <div class="caja_mod_marca">
            <div class="caja-txt">
                Selecciona las marcas que te gustan para recibir promociones solo de las cosas que te gustan
            </div>
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/flechas_notext.png">
        </div>
    </div>    
    <?php if (!isset(Yii::app()->session['email']) || in_array(Yii::app()->session['email'], array('', null))): ?>
        <div class="modal-email" style="margin-left: 25%; margin-top: 1%;">
            <label for="correo">Para continuar por favor ingresa tu correo electronico</label>
            <input id="correo" type="email" name="correo" placeholder="E-mail" class="txt" required/>
            <input type="submit"class="ingresar" value="Ingresar">
        </div>
    <?php endif; ?>
<?php endif; ?>
<script type="text/javascript">
    $('#correo+input').click(function () {
        $.ajax({
            type: 'POST',
            url: '<?php echo CController::createUrl('marcas/email') ?>',
            data: {'email': $('#correo').val()}
        }).done(function (data) {
            alert("Correo actualizado correctamente");
            $('.modal-email').slideToggle(0);
        });
        return false;
    });
</script>