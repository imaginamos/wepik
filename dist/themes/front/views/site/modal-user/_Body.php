<div class="modal-body">
    <div class="brand-head">
        <a class="brand-logo" href="#">
            <img class="img-circle" src="<?php echo $usuario['img']; ?>" alt="" width="110">
        </a>
        <h2><?php echo $usuario['name'] . ' ' . $usuario['lastname']; ?></h2>
    </div>

    <div class="user-info">
        <div class="user-info-item"><span class="user-icon1"></span>Bogotá Colombia</div>
        <div class="user-info-item"><span class="user-icon2"></span>
            <?php $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $date = date_create($usuario['birthdate']);
            ?>
<?php echo date_format($date, 'd') . " de " . $meses[date_format($date, 'n') - 1]; ?>
        </div>
        <a class="user-info-item" href="<?php echo $usuario['facebook_link']; ?>" target="_blank"><span class="user-icon3"></span>Visitar página <br> de Facebook</a>
    </div>

    <div class="user-history">

        <!-- Item type1 -->
<?php foreach ($usuario['acciones'] as $accion): ?>
            <div class="item-sub-list item-post3">
                <div class="row">
                    <div class="col-xs-12 img-users">
                        <img class="img-circle" src="<?php echo $usuario['img']; ?>" alt="" width="54">
                    </div>
                    <div class="col-xs-12 name-users">
                        <h4><strong>Tú </strong> 
                            <?php
                            switch ($accion['puntos_id']):
                                case 1:
                                    echo "revisaste la campaña <b>" . $accion['camp_nombre'] . "</b>";
                                    break;
                                case 2:
                                    echo "hicist pik en la campaña <b>" . $accion['camp_nombre'] . "</b>";
                                    break;
                                case 5:
                                    echo "respondiste la encuesta <b>" . $accion['camp_nombre'] . "</b>";
                                    break;
                                case 7:
                                    echo "compartirste la campaña <b>" . $accion['camp_nombre'] . "</b>";
                                    break;
                                case 9:
                                    echo "comentaste la campaña <b>" . $accion['camp_nombre'] . "</b>";
                                    break;
                                case 11:
                                    echo "recomendaste la campaña <b>" . $accion['camp_nombre'] . "</b>";
                                    break;
                            endswitch;
                            ?> 
                        <?php echo $accion['fecha_creacion']; ?></h4>
                    </div>
                    <div class="col-xs-12 pik-users">
                        <?php $hd = new Controller(uniqid()); ?>

                        <a href="<?php echo $hd->createUrl('marcas/marca?m=' . $accion['marca_id']); ?>">
                            <img class="img-circle" src="<?php echo $accion['logo_marca']; ?>" alt="">
                        </a>
                        <a class="brand-promo text-right" href="<?php echo 'http://www.wepiku.com/hotdeals/index?m=' . $accion['marca_id'] . '&i=' . $accion['campania_id']; ?>">
                            <img src="<?php echo $accion['camp_enlace']; ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--/ Item type3 -->
<?php endforeach; ?>
    </div>
</div>