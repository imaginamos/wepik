/*!
  Author: Stive Zambrano
  Creation date: March of 2015
  Project name: wepiku
  File name: application.js
  Description: Global functions
*/

// hasClass
function hasClass(elem, className) {
    "use strict";
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}


// toggleClass
function toggleClass(elem, className) {
    "use strict";
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, " ") + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0) {
            newClass = newClass.replace(" " + className + " ", " ");
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}








window.onload = function () {
    "use strict";

    $("#preload").delay(2000).animate({bottom: "100%", opacity: 0, top: "-100%", scale: 2}, 800);

    if ($(".main-section").data("section") === "map") {
        $("#map-container").storeLocator();
    }

};






document.addEventListener("DOMContentLoaded", function () {
// $(document).ready(function () {
    "use strict";

    var
        // wH = $(window).innerHeight(),
        // wW = $(window).innerWidth(),
        Fn = Function;
        // tMs = TweenMax.set,
        // tMt = TweenMax.to;

    // Misc
    if (new Fn("/*@cc_on return document.documentMode===10@*/")()) {
        document.documentElement.className += " ie10";
    }

    // $(".over-bw").hover(function () {
    //     tMt($(this).find(".icon-bw"), 0.6, {marginTop: -12, ease: Cubic.easeOut});
    // }, function () {
    //     tMt($(this).find(".icon-bw"), 0.6, {marginTop: 0, ease: Cubic.easeOut});
    // });
    //
    // $(".bt-bw").on("click", function () {
    //     tMt(".con-bw", 0.6, {autoAlpha: 0, ease: Cubic.easeOut});
    // });


    $("html, body").keyup(function (e) {
        if (e.keyCode === 27) {
            $(".modal-item").modal("hide");
            $(".modal-stores").modal("hide");
            $("body").removeClass("app-active");
        }
    });





    if ($(".main-section").data("section") === "map") {
        $(".nav3").addClass("active");


        document.querySelector(".map-filter").onclick = function () {
            var
                wrapper = document.querySelector(".categories-filter");
            toggleClass(wrapper, "active");
        };

    }



    $(window).scroll(function () {
        var pSc = $(window).scrollTop();
        if (pSc >= 50) {
            console.log(">=50");
        } else {
            console.log("<50");
        }
    });




  /*Add*/
    if ($(".footer-ahorranito").size() > 0) {
        $(".footer-ahorranito").ahorranito();
    }
});


$("[data-call='main-slider']").each(function () {
    "use strict";
    $(this).bxSlider();
});
