$('.ms_register').on('click',function(e){
	$('body, html').animate({
		'scrollTop': $('.content_footer').offset().top + 1080+'px'
	},1000);
	e.preventDefault();
});



function cancelEvent(event) {
   if (event.preventDefault) {
      event.preventDefault();
   } else {
      event.returnValue = false;
   }
}

$(window).load(function(e) {
   $('.loader').fadeOut('slow')
});


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};



$(function(){

  $(".fb_login").html("Ingresar con Facebook");

  $('.custom_select select').change(function(){
    var thisval = $(this).children('option:selected').text();
    $(this).siblings('span').text(thisval);
  })

  $('.btn_down').click(function(){
    cancelEvent(event)
    var position1 = $('.video_section').height();
    $('html,body').animate({scrollTop:position1},1200)
  })

	if( isMobile.any() ) {

  }else{
    var pinAnimations = new TimelineLite();

  // set duration, in pixels scrolled, for pinned element
  var pinDur = 2000;
  var winHeight = $(window).innerHeight();
  var controller = $.superscrollorama();
  var pinAnimations = new TimelineLite();
    pinAnimations
      .append([
        TweenMax.to($('#pin2'), 1, {opacity:1}),
        TweenMax.to($('#pin1'), 1, {opacity:0})
      ])
      .append([
        TweenMax.to($('#pin3'), 1, {opacity:1}),
        TweenMax.to($('#pin2'), 1, {opacity:0})
      ])
      .append([
        TweenMax.to($('#pin4'), 1, {opacity:1}),
        TweenMax.to($('#pin3'), 1, {opacity:0})
      ])
      .append([
        TweenMax.to($('#pin5'), 1, {opacity:1}),
        TweenMax.to($('#pin4'), 1, {opacity:0})
      ])
      .append([
        TweenMax.to($('#pin6'), 1, {opacity:1}),
        TweenMax.to($('#pin5'), 1, {opacity:0})
      ])


    controller.pin($('.content_pin'), pinDur, {
          anim:pinAnimations,
          onPin: function() {
            $('.content_pin').css('height','100%');
          },
          onUnpin: function() {
            $('.content_pin').css('height', winHeight);
          }
        });
  }

});





$(window).bind('load scroll', function(){
  var winWidth = $(window).innerWidth();
  var valScroll = $(window).scrollTop();
  /*--if(winWidth>1024){
       $('.content_menu').css('top',valScroll);
    }else{
       $('.content_menu').css('top',0);
    }--*/
});

$(window).bind('load resize', function(){
  if( isMobile.any() ) {
  }else{
    var winHeight = $(window).innerHeight();
    $('.content_pin').height(winHeight);
  }

});



bandera = 0
if( isMobile.any() ) {
}else{
  if($('.main_pin_wrapper').size()>0){
    $('.main_pin_wrapper').mousewheel( function(event, delta) {
      var valScroll = $(window).scrollTop();
      var scrollTime = 800;
      if (!(navigator.appVersion.indexOf("Mac")!=-1)){
        var scrollTime2 = 800;
      }else{
        var scrollTime2 = 1300;
      }

      var position1 = $('.video_section').height();
      var position2 = $('.main_pin_wrapper').offset().top;
      var position3 = position2+400;
      var position4 = position2+800;
      var position5 = position2+1200;
      var position6 = position2+1600;
      var position7 = position2+2000;
      var position8 = $('.section5').offset().top;

      if (delta < 0){

        if(bandera == 0){
          bandera = 1
          if(valScroll < position1){
            $('html,body').animate({scrollTop:position1},scrollTime);
          }
          else if(valScroll >= position1 && valScroll < position2){
            $('html,body').animate({scrollTop:position2},scrollTime);

          }else if(valScroll >= position2 && valScroll < position3){
            $('html,body').animate({scrollTop:position3},scrollTime);

          }else if(valScroll >= position3 && valScroll < position4){
            $('html,body').animate({scrollTop:position4},scrollTime);

          }else if(valScroll >= position4 && valScroll < position5){
            $('html,body').animate({scrollTop:position5},scrollTime);

          }else if(valScroll >= position5 && valScroll < position6){
            $('html,body').animate({scrollTop:position6},scrollTime);

          }else if(valScroll >= position6 && valScroll < position7){
            $('html,body').animate({scrollTop:position7},scrollTime);

          }else if(valScroll >= position7 && valScroll < position8){
            $('html,body').animate({scrollTop:position8},scrollTime);

          }else{
            $('html,body').animate({scrollTop:'+=300'},scrollTime);
          }

          setTimeout(function(){
            bandera = 0
          },scrollTime2);

        }

      } else{

        if(bandera == 0){
          bandera = 1
          if(valScroll <= position1){
            $('html,body').animate({scrollTop:0},scrollTime);
          }
          else if(valScroll > position1 && valScroll <= position2){
            $('html,body').animate({scrollTop:position1},scrollTime);

          }else if(valScroll > position2 && valScroll <= position3){
            $('html,body').animate({scrollTop:position2},scrollTime);

          }else if(valScroll > position3 && valScroll <= position4){
            $('html,body').animate({scrollTop:position3},scrollTime);

          }else if(valScroll > position4 && valScroll <= position5){
            $('html,body').animate({scrollTop:position4},scrollTime);

          }else if(valScroll > position5 && valScroll <= position6){
            $('html,body').animate({scrollTop:position5},scrollTime);

          }else if(valScroll > position6 && valScroll <= position7){
            $('html,body').animate({scrollTop:position6},scrollTime);

          }else if(valScroll > position7 && valScroll <= position8){
            $('html,body').animate({scrollTop:position7},scrollTime);

          }else{
            $('html,body').animate({scrollTop:position8},scrollTime);
          }

          setTimeout(function(){
            bandera = 0
          },scrollTime2);
        }
      }
      return false;
    });

  }



}
