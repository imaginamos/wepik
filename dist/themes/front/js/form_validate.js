$(document).ready(function(){
	
	var validarContacto = function(e){
		
		var expresionRegular = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var expresionNumero = /^([0-9])*$/;
		var name = $('#nombre');
		var phone = $('#telefono');
		var pais = $('#pais');
		var empresa = $('#empresa');
		var correo = $('#correo');
		var comentario = $('#comentario');
		
		//var validate = ['name','phone','pais','empresa','correo'];
		
		if(name.val() == ""){
			e.preventDefault();
			name.css({
				'border': '3px solid #039dc3',
				'color': '#fff'
			});
			name.attr('placeholder','Digita tu nombre');
			name.focus();
			return false;
		}
		else{
			name.css({
				'border': 'none'
			});
		}
		if( (phone.val() == "") || (phone.val().length < 7) || (!expresionNumero.test(phone.val()))){
			e.preventDefault();
			phone.val("");
            phone.css({
				'border': '3px solid #039dc3',
				'color': '#fff'
			});
			phone.attr('placeholder','Escribe un Teléfono de contacto valido');
			phone.focus();
			return false;
		}
		else{
			phone.css({
				'border': 'none'
			});
		}
		if(pais.val() == '') {
			e.preventDefault();
			pais.css({
				'border': '3px solid #039dc3',
				'color': '#fff'
			});
			return false;
   		} 
		else {
        	pais.css({
				'border': 'none'
			});
    	}
		if(empresa.val() == ""){
			e.preventDefault();
			empresa.css({
				'border': '3px solid #039dc3',
				'color': '#fff'
			});
			empresa.attr('placeholder','Escribe el nombre de la empresa');
			empresa.focus();
			return false;
		}
		else{
			empresa.css({
				'border': 'none'
			});
		}
		if( (correo.val() == "") || (!expresionRegular.test(correo.val()))){
			e.preventDefault();
			correo.val("");
			correo.css({
				'border': '3px solid #039dc3'
			});
			correo.attr('placeholder','Digita un correo electrónico valido');
			correo.focus();
			return false;
		}
		else{
			correo.css({
				'border': 'none'
			});
		}
		if(comentario.val() == ""){
			e.preventDefault();
			comentario.css({
				'border': '3px solid #039dc3',
				'color': '#fff'
			});
			comentario.attr('placeholder','Escribe un comentario');
			comentario.focus();
			return false;
		}
		else{
			comentario.css({
				'border': 'none'
			});
		}
	
	}

	$('.btn_go_in').on('click', validarContacto);
	

	

	
});




