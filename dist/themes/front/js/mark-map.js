var valmarca_id = $("#MarcaForm_marcaid").val(); 
var map = new google.maps.Map(document.getElementById("map"), {
mapTypeControl: false,
panControl: false,
scaleControl: false,
streetViewControl: false,
zoom: 9,
zoomControl: true,
zoomControlOptions: {
style: google.maps.ZoomControlStyle.LARGE,
position: google.maps.ControlPosition.RIGHT_BOTTOM
}
}),
map = new google.maps.Map(document.getElementById("map"), {
mapTypeControl: false,
panControl: false,
scaleControl: false,
streetViewControl: false,
zoom: 9,
zoomControl: true,
zoomControlOptions: {
style: google.maps.ZoomControlStyle.LARGE,
position: google.maps.ControlPosition.RIGHT_BOTTOM
}
}),
myMarker = new google.maps.Marker({
draggable: true,
icon: "../themes/front/img/marker-default.png",
position: new google.maps.LatLng(4.598056, -74.075833)
});


$(document).on("click", ".btn-add-point", function () {
console.log('btn-add-point');

var location = $(".location").val(),
position = $(".position").val(),
latlng = new google.maps.LatLng(4.598056, -74.075833);
                                //address = '';
                                //new google.maps.Geocoder().geocode({ 'LatLng': latlng }, function(results){results[0].formatted_address;});
                                //<input name='address[]' type='hidden' value='" + address + "'> //en append                          
  $(".position").removeClass('error');  
  $(".location").removeClass('error');  
  if(location != "")
    {
        if(position !="" && position !='Ubicación')
        {
            myMarker.setPosition(latlng);
            map.setCenter(latlng);
                if($.isNumeric(valmarca_id)){
                    var uniqueid = 'iden'+Math.random().toString().slice(2,11);
                    $(".info-location").append("<div id="+uniqueid+"><input name='MarcaForm[location][]' type='hidden' class='point-location' value='" + location + "'><input name='MarcaForm[position][]' type='hidden' class='point-position' value='" + position + "'><input name='MarcaForm[address][]' type='hidden' value='address'><input name='MarcaForm[delete][]' type='hidden' class='input-eliminado' value='0'></div><div id="+uniqueid+" class='row row-new-position'><div class='col-xs-12 col-sm-6'><h5>" + location + "</h5></div><div class='col-xs-12 col-sm-4'><h5 class='position-reg'>" + position + "</h5></div><div class='col-xs-6 col-sm-1'><button class='btn btn-edit-position' type='button' data-toggle='modal' data-target='#modal-map-edit'></button></div><div class='col-xs-6 col-sm-1'><button class='btn btn-remove' type='button'></button></div><span class='row-br'></span></div>");
                } else {
                    $(".info-location").append("<div class='row row-new-position'><div class='col-xs-12 col-sm-6'><h5>" + location + "</h5><input name='MarcaForm[location][]' type='hidden' class='point-location' value='" + location + "'></div><div class='col-xs-12 col-sm-4'><h5 class='position-reg'>" + position + "</h5><input name='MarcaForm[position][]' type='hidden' class='point-position' value='" + position + "'><input name='MarcaForm[address][]' type='hidden' value='address'></div><div class='col-xs-6 col-sm-1'><button class='btn btn-edit-position' type='button' data-toggle='modal' data-target='#modal-map-edit'></button></div><div class='col-xs-6 col-sm-1'><button class='btn btn-remove' type='button'></button></div><span class='row-br'></span></div>");
                }
            $(".location").val("");
            $(".position").val("Ubicación");
        }
        else
        {
            $(".position").addClass('error');
        }
    }
    else{ 
        $(".location").addClass('error');
    }  
});

//-------------------------------
$(document).on("click", ".btn-edit-position", function () {
  console.log('btn-edit-position');
$(this).parent().siblings().find(".position-reg").text("Ubicación");
$(this).parent().siblings().find(".position-reg").addClass("position-edit");
setTimeout(function () {
var center = map.getCenter();
google.maps.event.trigger(map, "resize");
map.setCenter(center);
}, 400);
});


google.maps.event.addListener(myMarker, "dragend", function(evt){
  console.log('dragend');
document.querySelector(".position").value = evt.latLng.lat().toFixed(6) + ", " + evt.latLng.lng().toFixed(6);
});


map.setCenter(myMarker.position);
myMarker.setMap(map);


$(document).on("click", ".btn-marker", function () {
"use strict";
console.log('btn-marker');
console.log("new");
setTimeout(function () {
console.log("time");
var center = map.getCenter();
google.maps.event.trigger(map, "resize");
map.setCenter(center);
}, 400);
});


$(document).on("click", ".close-map", function () {
"use strict";
console.log('close-map');
$(".position-reg").removeClass("position-edit");
});


$(document).on("click", ".modal-backdrop", function () {
"use strict";
console.log('modal-backdrop');
$(".position-reg").removeClass("position-edit");
});


$("html, body").on("keyup", function (e) {
  console.log('keyup');
if (e.keyCode === 27) {
$(".position-reg").removeClass("position-edit");
}
});

$(document).on("click", ".perfil-info-close", function () {
  console.log('perfil-info-close');
$(this).parent(".perfil-info").remove();
});

$('#file-cover').on('change', function(evt){
  console.log('file-cover');
if (0 < evt.target.files.length) {
// get file object which is seled by user
var _image               = evt.target.files[0];
var _reader              = new FileReader();
_reader.onload       = function(evt) {
$('.brand-page-logo-cover').css('display', 'inline');
$('#file-cover-img').attr('src', evt.target.result);
};
_reader.readAsDataURL(_image);
} else {
$('#file-cover-img').attr('src', null);
}
});

$('#file-perfil').on('change', function(evt){
  console.log('file-perfil');
if (0 < evt.target.files.length) {
// get file object which is seled by user
var _image               = evt.target.files[0];
var _reader              = new FileReader();
_reader.onload       = function(evt) {
$('.brand-page-logo-perfil').css('display', 'inline');
$('#file-perfil-img').attr('src', evt.target.result);
};
_reader.readAsDataURL(_image);
} else {
$('#file-perfil-img').attr('src', null);
}
});

var input = /** @type {!HTMLInputElement} */(
      document.getElementById('pac-input'));

  var types = document.getElementById('type-selector');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = myMarker;

  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Seleccione una opción de la busqueda");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    
    var lat = marker.position.toString().split(',')[0].split('(')[1].toString();
    var lng = marker.position.toString().split(',')[1].split(')')[0].toString();
    //document.querySelector(".position").value = evt.latLng.lat().toFixed(6) + ", " + evt.latLng.lng().toFixed(6);
    document.querySelector(".position").value = Number(lat).toFixed(6)  + ", " + Number(lng).toFixed(6);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    radioButton.addEventListener('click', function() {
      autocomplete.setTypes(types);
    });
  }

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);



// divloc = document.getElementById('info-location');
// sucursal = divloc.getElementsByTagName('div').length;
